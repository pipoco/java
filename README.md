# README #

# Dependencies: #
* 1) That project are using Glassfish 4.1.13 and Mysql 5.1
* 2) That project are using Maven for automatically load dependencies and build project

# Project Configuration: #
* 1) Configure DataSource "jdbc/events" in Glassfish
* 2) For the application send mails, please configure the MailSession "mail/events" in Glassfish
* 3) Create schema in Mysql. Need help? Contact the project administrator
* 4) Run the application. The tables are automatically created with Hibernate
* 5) Use the load scripts inside the project to load datas in database
* 6) Stop the application and run again
* 7) Success

# Database - Creation of the schema: #
* DROP DATABASE IF EXISTS `<schema_name>`;
* CREATE SCHEMA IF NOT EXISTS `<schema_name>` DEFAULT CHARACTER SET utf8mb4 ;
* ALTER SCHEMA `<schema_name>`  DEFAULT COLLATE utf8mb4_unicode_ci ;
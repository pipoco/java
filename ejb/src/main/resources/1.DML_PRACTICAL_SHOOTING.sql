INSERT INTO TBL_QUAR_CONFIGURATION (DT_CREATION, DT_UPDATED, NR_VERSION, FL_AUTO_CONFIG, TXT_CRON_EXPRESSION, TXT_JOB_CLASS_NAME, NM_PROCESS, TXT_SERVER_EXECUTION)
VALUES (curdate(), curdate(), 0, 0, '0 0/1 * 1/1 * ? *', 'br.com.practicalshooting.quartz.jobs.MailJob', 'Mail Queue', 's100802.nocix.net');

-- CRIANDO O CADASTRO DE MODALIDADES --
INSERT INTO TBL_MODALITIES (DT_CREATION, DT_UPDATED, NR_VERSION, DS_MODALITY, NM_MODALITY)
VALUES (CURDATE(), CURDATE(), 0, 'Tiro prático com armas curtas', 'IPSC');
INSERT INTO TBL_MODALITIES (DT_CREATION, DT_UPDATED, NR_VERSION, DS_MODALITY, NM_MODALITY)
VALUES (CURDATE(), CURDATE(), 0, 'Tiro prático com armas curtas', 'IPSC HANDGUN');
INSERT INTO TBL_MODALITIES (DT_CREATION, DT_UPDATED, NR_VERSION, DS_MODALITY, NM_MODALITY)
VALUES (CURDATE(), CURDATE(), 0, 'Tiro prático com armas curtas', 'MINI RILFE');
INSERT INTO TBL_MODALITIES (DT_CREATION, DT_UPDATED, NR_VERSION, DS_MODALITY, NM_MODALITY)
VALUES (CURDATE(), CURDATE(), 0, 'Tiro prático com armas curtas', 'SEMINÁRIO');
INSERT INTO TBL_MODALITIES (DT_CREATION, DT_UPDATED, NR_VERSION, DS_MODALITY, NM_MODALITY)
VALUES (CURDATE(), CURDATE(), 0, 'Tiro defensivo', 'IDSC');
INSERT INTO TBL_MODALITIES (DT_CREATION, DT_UPDATED, NR_VERSION, DS_MODALITY, NM_MODALITY)
VALUES (CURDATE(), CURDATE(), 0, 'Tiro de saque rápido com armas curtas', 'SAQUE RÁPIDO');
INSERT INTO TBL_MODALITIES (DT_CREATION, DT_UPDATED, NR_VERSION, DS_MODALITY, NM_MODALITY)
VALUES (CURDATE(), CURDATE(), 0, 'Tiro com espingarda calibre 12', 'IPSC SHOTGUN');
INSERT INTO TBL_MODALITIES (DT_CREATION, DT_UPDATED, NR_VERSION, DS_MODALITY, NM_MODALITY)
VALUES (CURDATE(), CURDATE(), 0, 'Tiro de fogo central a 25m com carabina .38', 'CARABINA PUMA');
INSERT INTO TBL_MODALITIES (DT_CREATION, DT_UPDATED, NR_VERSION, DS_MODALITY, NM_MODALITY)
VALUES (CURDATE(), CURDATE(), 0, 'Tiro com carabina .22', 'SILHUETA METÁLICA');
INSERT INTO TBL_MODALITIES (DT_CREATION, DT_UPDATED, NR_VERSION, DS_MODALITY, NM_MODALITY)
VALUES (CURDATE(), CURDATE(), 0, 'Tiro de fogo central com carabina .22 a 50m', 'FOGO CENTRAL 50m .22');
INSERT INTO TBL_MODALITIES (DT_CREATION, DT_UPDATED, NR_VERSION, DS_MODALITY, NM_MODALITY)
VALUES (CURDATE(), CURDATE(), 0, 'Tiro de fogo central com carabina .22 a 25m', 'FOGO CENTRAL 25m .22');
INSERT INTO TBL_MODALITIES (DT_CREATION, DT_UPDATED, NR_VERSION, DS_MODALITY, NM_MODALITY)
VALUES (CURDATE(), CURDATE(), 0, 'Tiro de com revólver .38', 'NRA RÁPIDO');

-- CRIANDO O CADASTRO DE TIPOS DE PARTICIPANTES --
INSERT INTO TBL_PARTICIPANTS (DT_CREATION, DT_UPDATED, NR_VERSION, DS_PUBLIC, NM_PUBLIC)
VALUES (CURDATE(), CURDATE(), 0, 'Apenas para os oficias de prova', 'ROs - Rangers Offices');
INSERT INTO TBL_PARTICIPANTS (DT_CREATION, DT_UPDATED, NR_VERSION, DS_PUBLIC, NM_PUBLIC)
VALUES (CURDATE(), CURDATE(), 0, 'Aberto para todos os públicos', 'Livre');
INSERT INTO TBL_PARTICIPANTS (DT_CREATION, DT_UPDATED, NR_VERSION, DS_PUBLIC, NM_PUBLIC)
VALUES (CURDATE(), CURDATE(), 0, 'Apenas para associados do clube', 'Associados');
INSERT INTO TBL_PARTICIPANTS (DT_CREATION, DT_UPDATED, NR_VERSION, DS_PUBLIC, NM_PUBLIC)
VALUES (CURDATE(), CURDATE(), 0, 'Apenas atiradores que possuem CR válido', 'Atiradores com CR');

-- CRIANDO O CADASTRO DE MODULO --
INSERT INTO TBL_MODULES (DT_CREATION, DT_UPDATED, NR_VERSION, NM_MODULE, SUPER_MODULE_SKU) 
VALUES (CURDATE(), CURDATE(), 0, 'BASE', null);
INSERT INTO TBL_MODULES (DT_CREATION, DT_UPDATED, NR_VERSION, NM_MODULE, SUPER_MODULE_SKU) 
VALUES (CURDATE(), CURDATE(), 0, 'CADASTROS', (SELECT MODU.MODU_SKU
                                                 FROM TBL_MODULES MODU
                                                 WHERE MODU.NM_MODULE = 'BASE'));

-- CRIANDO O CADASTRO DE FUNÇÕES --
INSERT INTO TBL_FUNCTIONS (DT_CREATION, DT_UPDATED, NR_VERSION, NM_FUNCTION, DS_PATH, MODU_SKU)
VALUES (CURDATE(), CURDATE(), 0, 'HOME', '/home.jsf', (SELECT MODU.MODU_SKU
                                                 FROM TBL_MODULES MODU
                                                 WHERE MODU.NM_MODULE = 'BASE'));
INSERT INTO TBL_FUNCTIONS (DT_CREATION, DT_UPDATED, NR_VERSION, NM_FUNCTION, DS_PATH, MODU_SKU)
VALUES (CURDATE(), CURDATE(), 0, 'EVENTOS', '/pages/envent/list.jsf', (SELECT MODU.MODU_SKU
                                                 FROM TBL_MODULES MODU
                                                 WHERE MODU.NM_MODULE = 'BASE'));
INSERT INTO TBL_FUNCTIONS (DT_CREATION, DT_UPDATED, NR_VERSION, NM_FUNCTION, DS_PATH, MODU_SKU)
VALUES (CURDATE(), CURDATE(), 0, 'CLUBES', '/pages/club/list.jsf', (SELECT MODU.MODU_SKU
                                                 FROM TBL_MODULES MODU
                                                 WHERE MODU.NM_MODULE = 'BASE'));
INSERT INTO TBL_FUNCTIONS (DT_CREATION, DT_UPDATED, NR_VERSION, NM_FUNCTION, DS_PATH, MODU_SKU)
VALUES (CURDATE(), CURDATE(), 0, 'INSTRUTORES', '/pages/instructor/list.jsf', (SELECT MODU.MODU_SKU
                                                 FROM TBL_MODULES MODU
                                                 WHERE MODU.NM_MODULE = 'BASE'));
INSERT INTO TBL_FUNCTIONS (DT_CREATION, DT_UPDATED, NR_VERSION, NM_FUNCTION, DS_PATH, MODU_SKU)
VALUES (CURDATE(), CURDATE(), 0, 'ALUNOS', '/pages/instructor/show.jsf', (SELECT MODU.MODU_SKU
                                                 FROM TBL_MODULES MODU
                                                 WHERE MODU.NM_MODULE = 'BASE'));
INSERT INTO TBL_FUNCTIONS (DT_CREATION, DT_UPDATED, NR_VERSION, NM_FUNCTION, DS_PATH, MODU_SKU)
VALUES (CURDATE(), CURDATE(), 0, 'CURSOS', '/pages/course/show.jsf', (SELECT MODU.MODU_SKU
                                                 FROM TBL_MODULES MODU
                                                 WHERE MODU.NM_MODULE = 'BASE'));

-- CRIANDO O CADASTRO DE AÇÕES --
INSERT INTO TBL_ACTIONS (DT_CREATION, DT_UPDATED, NR_VERSION, DS_COMMAND, NM_ACTION, FUNC_SKU)
VALUES (CURDATE(), CURDATE(), 0, 'VIEW', 'VISUALIZAR', (SELECT FUNC.FUNC_SKU
                                                 FROM TBL_FUNCTIONS FUNC
                                                 WHERE FUNC.NM_FUNCTION = 'HOME'));
INSERT INTO TBL_ACTIONS (DT_CREATION, DT_UPDATED, NR_VERSION, DS_COMMAND, NM_ACTION, FUNC_SKU)
VALUES (CURDATE(), CURDATE(), 0, 'INSERT', 'INCLUIR', (SELECT FUNC.FUNC_SKU
                                                 FROM TBL_FUNCTIONS FUNC
                                                 WHERE FUNC.NM_FUNCTION = 'EVENTOS'));
INSERT INTO TBL_ACTIONS (DT_CREATION, DT_UPDATED, NR_VERSION, DS_COMMAND, NM_ACTION, FUNC_SKU)
VALUES (CURDATE(), CURDATE(), 0, 'UPDATE', 'ATUALIZAR', (SELECT FUNC.FUNC_SKU
                                                 FROM TBL_FUNCTIONS FUNC
                                                 WHERE FUNC.NM_FUNCTION = 'EVENTOS'));
INSERT INTO TBL_ACTIONS (DT_CREATION, DT_UPDATED, NR_VERSION, DS_COMMAND, NM_ACTION, FUNC_SKU)
VALUES (CURDATE(), CURDATE(), 0, 'VIEW', 'VISUALIZAR', (SELECT FUNC.FUNC_SKU
                                                 FROM TBL_FUNCTIONS FUNC
                                                 WHERE FUNC.NM_FUNCTION = 'EVENTOS'));
INSERT INTO TBL_ACTIONS (DT_CREATION, DT_UPDATED, NR_VERSION, DS_COMMAND, NM_ACTION, FUNC_SKU)
VALUES (CURDATE(), CURDATE(), 0, 'DELETE', 'REMOVER', (SELECT FUNC.FUNC_SKU
                                                 FROM TBL_FUNCTIONS FUNC
                                                 WHERE FUNC.NM_FUNCTION = 'EVENTOS'));
INSERT INTO TBL_ACTIONS (DT_CREATION, DT_UPDATED, NR_VERSION, DS_COMMAND, NM_ACTION, FUNC_SKU)
VALUES (CURDATE(), CURDATE(), 0, 'INSERT', 'INCLUIR', (SELECT FUNC.FUNC_SKU
                                                 FROM TBL_FUNCTIONS FUNC
                                                 WHERE FUNC.NM_FUNCTION = 'CLUBES'));
INSERT INTO TBL_ACTIONS (DT_CREATION, DT_UPDATED, NR_VERSION, DS_COMMAND, NM_ACTION, FUNC_SKU)
VALUES (CURDATE(), CURDATE(), 0, 'UPDATE', 'ATUALIZAR', (SELECT FUNC.FUNC_SKU
                                                 FROM TBL_FUNCTIONS FUNC
                                                 WHERE FUNC.NM_FUNCTION = 'CLUBES'));
INSERT INTO TBL_ACTIONS (DT_CREATION, DT_UPDATED, NR_VERSION, DS_COMMAND, NM_ACTION, FUNC_SKU)
VALUES (CURDATE(), CURDATE(), 0, 'VIEW', 'VISUALIZAR', (SELECT FUNC.FUNC_SKU
                                                 FROM TBL_FUNCTIONS FUNC
                                                 WHERE FUNC.NM_FUNCTION = 'CLUBES'));
INSERT INTO TBL_ACTIONS (DT_CREATION, DT_UPDATED, NR_VERSION, DS_COMMAND, NM_ACTION, FUNC_SKU)
VALUES (CURDATE(), CURDATE(), 0, 'DELETE', 'REMOVER', (SELECT FUNC.FUNC_SKU
                                                 FROM TBL_FUNCTIONS FUNC
                                                 WHERE FUNC.NM_FUNCTION = 'CLUBES'));
INSERT INTO TBL_ACTIONS (DT_CREATION, DT_UPDATED, NR_VERSION, DS_COMMAND, NM_ACTION, FUNC_SKU)
VALUES (CURDATE(), CURDATE(), 0, 'INSERT', 'INCLUIR', (SELECT FUNC.FUNC_SKU
                                                 FROM TBL_FUNCTIONS FUNC
                                                 WHERE FUNC.NM_FUNCTION = 'INSTRUTORES'));
INSERT INTO TBL_ACTIONS (DT_CREATION, DT_UPDATED, NR_VERSION, DS_COMMAND, NM_ACTION, FUNC_SKU)
VALUES (CURDATE(), CURDATE(), 0, 'UPDATE', 'ATUALIZAR', (SELECT FUNC.FUNC_SKU
                                                 FROM TBL_FUNCTIONS FUNC
                                                 WHERE FUNC.NM_FUNCTION = 'INSTRUTORES'));
INSERT INTO TBL_ACTIONS (DT_CREATION, DT_UPDATED, NR_VERSION, DS_COMMAND, NM_ACTION, FUNC_SKU)
VALUES (CURDATE(), CURDATE(), 0, 'VIEW', 'VISUALIZAR', (SELECT FUNC.FUNC_SKU
                                                 FROM TBL_FUNCTIONS FUNC
                                                 WHERE FUNC.NM_FUNCTION = 'INSTRUTORES'));
INSERT INTO TBL_ACTIONS (DT_CREATION, DT_UPDATED, NR_VERSION, DS_COMMAND, NM_ACTION, FUNC_SKU)
VALUES (CURDATE(), CURDATE(), 0, 'DELETE', 'REMOVER', (SELECT FUNC.FUNC_SKU
                                                 FROM TBL_FUNCTIONS FUNC
                                                 WHERE FUNC.NM_FUNCTION = 'INSTRUTORES'));
INSERT INTO TBL_ACTIONS (DT_CREATION, DT_UPDATED, NR_VERSION, DS_COMMAND, NM_ACTION, FUNC_SKU)
VALUES (CURDATE(), CURDATE(), 0, 'INSERT', 'INCLUIR', (SELECT FUNC.FUNC_SKU
                                                 FROM TBL_FUNCTIONS FUNC
                                                 WHERE FUNC.NM_FUNCTION = 'ALUNOS'));
INSERT INTO TBL_ACTIONS (DT_CREATION, DT_UPDATED, NR_VERSION, DS_COMMAND, NM_ACTION, FUNC_SKU)
VALUES (CURDATE(), CURDATE(), 0, 'UPDATE', 'ATUALIZAR', (SELECT FUNC.FUNC_SKU
                                                 FROM TBL_FUNCTIONS FUNC
                                                 WHERE FUNC.NM_FUNCTION = 'ALUNOS'));
INSERT INTO TBL_ACTIONS (DT_CREATION, DT_UPDATED, NR_VERSION, DS_COMMAND, NM_ACTION, FUNC_SKU)
VALUES (CURDATE(), CURDATE(), 0, 'VIEW', 'VISUALIZAR', (SELECT FUNC.FUNC_SKU
                                                 FROM TBL_FUNCTIONS FUNC
                                                 WHERE FUNC.NM_FUNCTION = 'ALUNOS'));
INSERT INTO TBL_ACTIONS (DT_CREATION, DT_UPDATED, NR_VERSION, DS_COMMAND, NM_ACTION, FUNC_SKU)
VALUES (CURDATE(), CURDATE(), 0, 'DELETE', 'REMOVER', (SELECT FUNC.FUNC_SKU
                                                 FROM TBL_FUNCTIONS FUNC
                                                 WHERE FUNC.NM_FUNCTION = 'ALUNOS'));
INSERT INTO TBL_ACTIONS (DT_CREATION, DT_UPDATED, NR_VERSION, DS_COMMAND, NM_ACTION, FUNC_SKU)
VALUES (CURDATE(), CURDATE(), 0, 'INSERT', 'INCLUIR', (SELECT FUNC.FUNC_SKU
                                                 FROM TBL_FUNCTIONS FUNC
                                                 WHERE FUNC.NM_FUNCTION = 'CURSOS'));
INSERT INTO TBL_ACTIONS (DT_CREATION, DT_UPDATED, NR_VERSION, DS_COMMAND, NM_ACTION, FUNC_SKU)
VALUES (CURDATE(), CURDATE(), 0, 'UPDATE', 'ATUALIZAR', (SELECT FUNC.FUNC_SKU
                                                 FROM TBL_FUNCTIONS FUNC
                                                 WHERE FUNC.NM_FUNCTION = 'CURSOS'));
INSERT INTO TBL_ACTIONS (DT_CREATION, DT_UPDATED, NR_VERSION, DS_COMMAND, NM_ACTION, FUNC_SKU)
VALUES (CURDATE(), CURDATE(), 0, 'VIEW', 'VISUALIZAR', (SELECT FUNC.FUNC_SKU
                                                 FROM TBL_FUNCTIONS FUNC
                                                 WHERE FUNC.NM_FUNCTION = 'CURSOS'));
INSERT INTO TBL_ACTIONS (DT_CREATION, DT_UPDATED, NR_VERSION, DS_COMMAND, NM_ACTION, FUNC_SKU)
VALUES (CURDATE(), CURDATE(), 0, 'DELETE', 'REMOVER', (SELECT FUNC.FUNC_SKU
                                                 FROM TBL_FUNCTIONS FUNC
                                                 WHERE FUNC.NM_FUNCTION = 'CURSOS'));
                                                 
-- CRIANDO O CADASTRO DE PERFIL --
INSERT INTO TBL_PROFILES (DT_CREATION, DT_UPDATED, NR_VERSION, NM_PROFILE)
VALUES (CURDATE(), CURDATE(), 0, 'Administrador');
INSERT INTO TBL_PROFILES (DT_CREATION, DT_UPDATED, NR_VERSION, NM_PROFILE)
VALUES (CURDATE(), CURDATE(), 0, 'Instrutor');
INSERT INTO TBL_PROFILES (DT_CREATION, DT_UPDATED, NR_VERSION, NM_PROFILE)
VALUES (CURDATE(), CURDATE(), 0, 'Associado');
INSERT INTO TBL_PROFILES (DT_CREATION, DT_UPDATED, NR_VERSION, NM_PROFILE)
VALUES (CURDATE(), CURDATE(), 0, 'Usuário');
                                                 
-- CRIANDO O CADASTRO DE PERMISSÕES - ADMINISTRADOR --
INSERT INTO TBL_PERMISSIONS (DT_CREATION, DT_UPDATED, NR_VERSION, ACTI_SKU, PROF_SKU)
VALUES (CURDATE(), CURDATE(), 0, (SELECT ACTI.ACTI_SKU
                                                 FROM TBL_ACTIONS ACTI
                                                 INNER JOIN TBL_FUNCTIONS FUNC ON FUNC.FUNC_SKU = ACTI.FUNC_SKU
                                                 WHERE ACTI.NM_ACTION = 'VISUALIZAR' 
                                                 AND FUNC.NM_FUNCTION = 'HOME'), 
                                                 (SELECT PROF.PROF_SKU
                                                 FROM TBL_PROFILES PROF
                                                 WHERE PROF.NM_PROFILE = 'Administrador'));
INSERT INTO TBL_PERMISSIONS (DT_CREATION, DT_UPDATED, NR_VERSION, ACTI_SKU, PROF_SKU)
VALUES (CURDATE(), CURDATE(), 0, (SELECT ACTI.ACTI_SKU
                                                 FROM TBL_ACTIONS ACTI
                                                 INNER JOIN TBL_FUNCTIONS FUNC ON FUNC.FUNC_SKU = ACTI.FUNC_SKU
                                                 WHERE ACTI.NM_ACTION = 'INCLUIR' 
                                                 AND FUNC.NM_FUNCTION = 'EVENTOS'), 
                                                 (SELECT PROF.PROF_SKU
                                                 FROM TBL_PROFILES PROF
                                                 WHERE PROF.NM_PROFILE = 'Administrador'));
INSERT INTO TBL_PERMISSIONS (DT_CREATION, DT_UPDATED, NR_VERSION, ACTI_SKU, PROF_SKU)
VALUES (CURDATE(), CURDATE(), 0, (SELECT ACTI.ACTI_SKU
                                                 FROM TBL_ACTIONS ACTI
                                                 INNER JOIN TBL_FUNCTIONS FUNC ON FUNC.FUNC_SKU = ACTI.FUNC_SKU
                                                 WHERE ACTI.NM_ACTION = 'ATUALIZAR' 
                                                 AND FUNC.NM_FUNCTION = 'EVENTOS'), 
                                                 (SELECT PROF.PROF_SKU
                                                 FROM TBL_PROFILES PROF
                                                 WHERE PROF.NM_PROFILE = 'Administrador'));
INSERT INTO TBL_PERMISSIONS (DT_CREATION, DT_UPDATED, NR_VERSION, ACTI_SKU, PROF_SKU)
VALUES (CURDATE(), CURDATE(), 0, (SELECT ACTI.ACTI_SKU
                                                 FROM TBL_ACTIONS ACTI
                                                 INNER JOIN TBL_FUNCTIONS FUNC ON FUNC.FUNC_SKU = ACTI.FUNC_SKU
                                                 WHERE ACTI.NM_ACTION = 'VISUALIZAR' 
                                                 AND FUNC.NM_FUNCTION = 'EVENTOS'), 
                                                 (SELECT PROF.PROF_SKU
                                                 FROM TBL_PROFILES PROF
                                                 WHERE PROF.NM_PROFILE = 'Administrador'));
INSERT INTO TBL_PERMISSIONS (DT_CREATION, DT_UPDATED, NR_VERSION, ACTI_SKU, PROF_SKU)
VALUES (CURDATE(), CURDATE(), 0, (SELECT ACTI.ACTI_SKU
                                                 FROM TBL_ACTIONS ACTI
                                                 INNER JOIN TBL_FUNCTIONS FUNC ON FUNC.FUNC_SKU = ACTI.FUNC_SKU
                                                 WHERE ACTI.NM_ACTION = 'REMOVER' 
                                                 AND FUNC.NM_FUNCTION = 'EVENTOS'), 
                                                 (SELECT PROF.PROF_SKU
                                                 FROM TBL_PROFILES PROF
                                                 WHERE PROF.NM_PROFILE = 'Administrador'));
INSERT INTO TBL_PERMISSIONS (DT_CREATION, DT_UPDATED, NR_VERSION, ACTI_SKU, PROF_SKU)
VALUES (CURDATE(), CURDATE(), 0, (SELECT ACTI.ACTI_SKU
                                                 FROM TBL_ACTIONS ACTI
                                                 INNER JOIN TBL_FUNCTIONS FUNC ON FUNC.FUNC_SKU = ACTI.FUNC_SKU
                                                 WHERE ACTI.NM_ACTION = 'INCLUIR' 
                                                 AND FUNC.NM_FUNCTION = 'CLUBES'), 
                                                 (SELECT PROF.PROF_SKU
                                                 FROM TBL_PROFILES PROF
                                                 WHERE PROF.NM_PROFILE = 'Administrador'));
INSERT INTO TBL_PERMISSIONS (DT_CREATION, DT_UPDATED, NR_VERSION, ACTI_SKU, PROF_SKU)
VALUES (CURDATE(), CURDATE(), 0, (SELECT ACTI.ACTI_SKU
                                                 FROM TBL_ACTIONS ACTI
                                                 INNER JOIN TBL_FUNCTIONS FUNC ON FUNC.FUNC_SKU = ACTI.FUNC_SKU
                                                 WHERE ACTI.NM_ACTION = 'ATUALIZAR' 
                                                 AND FUNC.NM_FUNCTION = 'CLUBES'), 
                                                 (SELECT PROF.PROF_SKU
                                                 FROM TBL_PROFILES PROF
                                                 WHERE PROF.NM_PROFILE = 'Administrador'));
INSERT INTO TBL_PERMISSIONS (DT_CREATION, DT_UPDATED, NR_VERSION, ACTI_SKU, PROF_SKU)
VALUES (CURDATE(), CURDATE(), 0, (SELECT ACTI.ACTI_SKU
                                                 FROM TBL_ACTIONS ACTI
                                                 INNER JOIN TBL_FUNCTIONS FUNC ON FUNC.FUNC_SKU = ACTI.FUNC_SKU
                                                 WHERE ACTI.NM_ACTION = 'VISUALIZAR' 
                                                 AND FUNC.NM_FUNCTION = 'CLUBES'), 
                                                 (SELECT PROF.PROF_SKU
                                                 FROM TBL_PROFILES PROF
                                                 WHERE PROF.NM_PROFILE = 'Administrador'));
INSERT INTO TBL_PERMISSIONS (DT_CREATION, DT_UPDATED, NR_VERSION, ACTI_SKU, PROF_SKU)
VALUES (CURDATE(), CURDATE(), 0, (SELECT ACTI.ACTI_SKU
                                                 FROM TBL_ACTIONS ACTI
                                                 INNER JOIN TBL_FUNCTIONS FUNC ON FUNC.FUNC_SKU = ACTI.FUNC_SKU
                                                 WHERE ACTI.NM_ACTION = 'REMOVER' 
                                                 AND FUNC.NM_FUNCTION = 'CLUBES'), 
                                                 (SELECT PROF.PROF_SKU
                                                 FROM TBL_PROFILES PROF
                                                 WHERE PROF.NM_PROFILE = 'Administrador'));
INSERT INTO TBL_PERMISSIONS (DT_CREATION, DT_UPDATED, NR_VERSION, ACTI_SKU, PROF_SKU)
VALUES (CURDATE(), CURDATE(), 0, (SELECT ACTI.ACTI_SKU
                                                 FROM TBL_ACTIONS ACTI
                                                 INNER JOIN TBL_FUNCTIONS FUNC ON FUNC.FUNC_SKU = ACTI.FUNC_SKU
                                                 WHERE ACTI.NM_ACTION = 'INCLUIR' 
                                                 AND FUNC.NM_FUNCTION = 'ALUNOS'), 
                                                 (SELECT PROF.PROF_SKU
                                                 FROM TBL_PROFILES PROF
                                                 WHERE PROF.NM_PROFILE = 'Administrador'));
INSERT INTO TBL_PERMISSIONS (DT_CREATION, DT_UPDATED, NR_VERSION, ACTI_SKU, PROF_SKU)
VALUES (CURDATE(), CURDATE(), 0, (SELECT ACTI.ACTI_SKU
                                                 FROM TBL_ACTIONS ACTI
                                                 INNER JOIN TBL_FUNCTIONS FUNC ON FUNC.FUNC_SKU = ACTI.FUNC_SKU
                                                 WHERE ACTI.NM_ACTION = 'ATUALIZAR' 
                                                 AND FUNC.NM_FUNCTION = 'ALUNOS'), 
                                                 (SELECT PROF.PROF_SKU
                                                 FROM TBL_PROFILES PROF
                                                 WHERE PROF.NM_PROFILE = 'Administrador'));
INSERT INTO TBL_PERMISSIONS (DT_CREATION, DT_UPDATED, NR_VERSION, ACTI_SKU, PROF_SKU)
VALUES (CURDATE(), CURDATE(), 0, (SELECT ACTI.ACTI_SKU
                                                 FROM TBL_ACTIONS ACTI
                                                 INNER JOIN TBL_FUNCTIONS FUNC ON FUNC.FUNC_SKU = ACTI.FUNC_SKU
                                                 WHERE ACTI.NM_ACTION = 'VISUALIZAR' 
                                                 AND FUNC.NM_FUNCTION = 'ALUNOS'), 
                                                 (SELECT PROF.PROF_SKU
                                                 FROM TBL_PROFILES PROF
                                                 WHERE PROF.NM_PROFILE = 'Administrador'));
INSERT INTO TBL_PERMISSIONS (DT_CREATION, DT_UPDATED, NR_VERSION, ACTI_SKU, PROF_SKU)
VALUES (CURDATE(), CURDATE(), 0, (SELECT ACTI.ACTI_SKU
                                                 FROM TBL_ACTIONS ACTI
                                                 INNER JOIN TBL_FUNCTIONS FUNC ON FUNC.FUNC_SKU = ACTI.FUNC_SKU
                                                 WHERE ACTI.NM_ACTION = 'REMOVER' 
                                                 AND FUNC.NM_FUNCTION = 'ALUNOS'), 
                                                 (SELECT PROF.PROF_SKU
                                                 FROM TBL_PROFILES PROF
                                                 WHERE PROF.NM_PROFILE = 'Administrador'));
INSERT INTO TBL_PERMISSIONS (DT_CREATION, DT_UPDATED, NR_VERSION, ACTI_SKU, PROF_SKU)
VALUES (CURDATE(), CURDATE(), 0, (SELECT ACTI.ACTI_SKU
                                                 FROM TBL_ACTIONS ACTI
                                                 INNER JOIN TBL_FUNCTIONS FUNC ON FUNC.FUNC_SKU = ACTI.FUNC_SKU
                                                 WHERE ACTI.NM_ACTION = 'INCLUIR' 
                                                 AND FUNC.NM_FUNCTION = 'CURSOS'), 
                                                 (SELECT PROF.PROF_SKU
                                                 FROM TBL_PROFILES PROF
                                                 WHERE PROF.NM_PROFILE = 'Administrador'));
INSERT INTO TBL_PERMISSIONS (DT_CREATION, DT_UPDATED, NR_VERSION, ACTI_SKU, PROF_SKU)
VALUES (CURDATE(), CURDATE(), 0, (SELECT ACTI.ACTI_SKU
                                                 FROM TBL_ACTIONS ACTI
                                                 INNER JOIN TBL_FUNCTIONS FUNC ON FUNC.FUNC_SKU = ACTI.FUNC_SKU
                                                 WHERE ACTI.NM_ACTION = 'ATUALIZAR' 
                                                 AND FUNC.NM_FUNCTION = 'CURSOS'), 
                                                 (SELECT PROF.PROF_SKU
                                                 FROM TBL_PROFILES PROF
                                                 WHERE PROF.NM_PROFILE = 'Administrador'));
INSERT INTO TBL_PERMISSIONS (DT_CREATION, DT_UPDATED, NR_VERSION, ACTI_SKU, PROF_SKU)
VALUES (CURDATE(), CURDATE(), 0, (SELECT ACTI.ACTI_SKU
                                                 FROM TBL_ACTIONS ACTI
                                                 INNER JOIN TBL_FUNCTIONS FUNC ON FUNC.FUNC_SKU = ACTI.FUNC_SKU
                                                 WHERE ACTI.NM_ACTION = 'VISUALIZAR' 
                                                 AND FUNC.NM_FUNCTION = 'CURSOS'), 
                                                 (SELECT PROF.PROF_SKU
                                                 FROM TBL_PROFILES PROF
                                                 WHERE PROF.NM_PROFILE = 'Administrador'));
INSERT INTO TBL_PERMISSIONS (DT_CREATION, DT_UPDATED, NR_VERSION, ACTI_SKU, PROF_SKU)
VALUES (CURDATE(), CURDATE(), 0, (SELECT ACTI.ACTI_SKU
                                                 FROM TBL_ACTIONS ACTI
                                                 INNER JOIN TBL_FUNCTIONS FUNC ON FUNC.FUNC_SKU = ACTI.FUNC_SKU
                                                 WHERE ACTI.NM_ACTION = 'REMOVER' 
                                                 AND FUNC.NM_FUNCTION = 'CURSOS'), 
                                                 (SELECT PROF.PROF_SKU
                                                 FROM TBL_PROFILES PROF
                                                 WHERE PROF.NM_PROFILE = 'Administrador'));
INSERT INTO TBL_PERMISSIONS (DT_CREATION, DT_UPDATED, NR_VERSION, ACTI_SKU, PROF_SKU)
VALUES (CURDATE(), CURDATE(), 0, (SELECT ACTI.ACTI_SKU
                                                 FROM TBL_ACTIONS ACTI
                                                 INNER JOIN TBL_FUNCTIONS FUNC ON FUNC.FUNC_SKU = ACTI.FUNC_SKU
                                                 WHERE ACTI.NM_ACTION = 'INCLUIR' 
                                                 AND FUNC.NM_FUNCTION = 'INSTRUTORES'), 
                                                 (SELECT PROF.PROF_SKU
                                                 FROM TBL_PROFILES PROF
                                                 WHERE PROF.NM_PROFILE = 'Administrador'));
INSERT INTO TBL_PERMISSIONS (DT_CREATION, DT_UPDATED, NR_VERSION, ACTI_SKU, PROF_SKU)
VALUES (CURDATE(), CURDATE(), 0, (SELECT ACTI.ACTI_SKU
                                                 FROM TBL_ACTIONS ACTI
                                                 INNER JOIN TBL_FUNCTIONS FUNC ON FUNC.FUNC_SKU = ACTI.FUNC_SKU
                                                 WHERE ACTI.NM_ACTION = 'ATUALIZAR' 
                                                 AND FUNC.NM_FUNCTION = 'INSTRUTORES'), 
                                                 (SELECT PROF.PROF_SKU
                                                 FROM TBL_PROFILES PROF
                                                 WHERE PROF.NM_PROFILE = 'Administrador'));
INSERT INTO TBL_PERMISSIONS (DT_CREATION, DT_UPDATED, NR_VERSION, ACTI_SKU, PROF_SKU)
VALUES (CURDATE(), CURDATE(), 0, (SELECT ACTI.ACTI_SKU
                                                 FROM TBL_ACTIONS ACTI
                                                 INNER JOIN TBL_FUNCTIONS FUNC ON FUNC.FUNC_SKU = ACTI.FUNC_SKU
                                                 WHERE ACTI.NM_ACTION = 'VISUALIZAR' 
                                                 AND FUNC.NM_FUNCTION = 'INSTRUTORES'), 
                                                 (SELECT PROF.PROF_SKU
                                                 FROM TBL_PROFILES PROF
                                                 WHERE PROF.NM_PROFILE = 'Administrador'));
INSERT INTO TBL_PERMISSIONS (DT_CREATION, DT_UPDATED, NR_VERSION, ACTI_SKU, PROF_SKU)
VALUES (CURDATE(), CURDATE(), 0, (SELECT ACTI.ACTI_SKU
                                                 FROM TBL_ACTIONS ACTI
                                                 INNER JOIN TBL_FUNCTIONS FUNC ON FUNC.FUNC_SKU = ACTI.FUNC_SKU
                                                 WHERE ACTI.NM_ACTION = 'REMOVER' 
                                                 AND FUNC.NM_FUNCTION = 'INSTRUTORES'), 
                                                 (SELECT PROF.PROF_SKU
                                                 FROM TBL_PROFILES PROF
                                                 WHERE PROF.NM_PROFILE = 'Administrador'));
                                                 
-- CRIANDO O CADASTRO DE PERMISSÕES - INSTRUCTOR --
INSERT INTO TBL_PERMISSIONS (DT_CREATION, DT_UPDATED, NR_VERSION, ACTI_SKU, PROF_SKU)
VALUES (CURDATE(), CURDATE(), 0, (SELECT ACTI.ACTI_SKU
                                                 FROM TBL_ACTIONS ACTI
                                                 INNER JOIN TBL_FUNCTIONS FUNC ON FUNC.FUNC_SKU = ACTI.FUNC_SKU
                                                 WHERE ACTI.NM_ACTION = 'VISUALIZAR' 
                                                 AND FUNC.NM_FUNCTION = 'HOME'), 
                                                 (SELECT PROF.PROF_SKU
                                                 FROM TBL_PROFILES PROF
                                                 WHERE PROF.NM_PROFILE = 'Instrutor'));
INSERT INTO TBL_PERMISSIONS (DT_CREATION, DT_UPDATED, NR_VERSION, ACTI_SKU, PROF_SKU)
VALUES (CURDATE(), CURDATE(), 0, (SELECT ACTI.ACTI_SKU
                                                 FROM TBL_ACTIONS ACTI
                                                 INNER JOIN TBL_FUNCTIONS FUNC ON FUNC.FUNC_SKU = ACTI.FUNC_SKU
                                                 WHERE ACTI.NM_ACTION = 'VISUALIZAR' 
                                                 AND FUNC.NM_FUNCTION = 'EVENTOS'), 
                                                 (SELECT PROF.PROF_SKU
                                                 FROM TBL_PROFILES PROF
                                                 WHERE PROF.NM_PROFILE = 'Instrutor'));
INSERT INTO TBL_PERMISSIONS (DT_CREATION, DT_UPDATED, NR_VERSION, ACTI_SKU, PROF_SKU)
VALUES (CURDATE(), CURDATE(), 0, (SELECT ACTI.ACTI_SKU
                                                 FROM TBL_ACTIONS ACTI
                                                 INNER JOIN TBL_FUNCTIONS FUNC ON FUNC.FUNC_SKU = ACTI.FUNC_SKU
                                                 WHERE ACTI.NM_ACTION = 'VISUALIZAR' 
                                                 AND FUNC.NM_FUNCTION = 'CLUBES'), 
                                                 (SELECT PROF.PROF_SKU
                                                 FROM TBL_PROFILES PROF
                                                 WHERE PROF.NM_PROFILE = 'Instrutor'));
INSERT INTO TBL_PERMISSIONS (DT_CREATION, DT_UPDATED, NR_VERSION, ACTI_SKU, PROF_SKU)
VALUES (CURDATE(), CURDATE(), 0, (SELECT ACTI.ACTI_SKU
                                                 FROM TBL_ACTIONS ACTI
                                                 INNER JOIN TBL_FUNCTIONS FUNC ON FUNC.FUNC_SKU = ACTI.FUNC_SKU
                                                 WHERE ACTI.NM_ACTION = 'INCLUIR' 
                                                 AND FUNC.NM_FUNCTION = 'ALUNOS'), 
                                                 (SELECT PROF.PROF_SKU
                                                 FROM TBL_PROFILES PROF
                                                 WHERE PROF.NM_PROFILE = 'Instrutor'));
INSERT INTO TBL_PERMISSIONS (DT_CREATION, DT_UPDATED, NR_VERSION, ACTI_SKU, PROF_SKU)
VALUES (CURDATE(), CURDATE(), 0, (SELECT ACTI.ACTI_SKU
                                                 FROM TBL_ACTIONS ACTI
                                                 INNER JOIN TBL_FUNCTIONS FUNC ON FUNC.FUNC_SKU = ACTI.FUNC_SKU
                                                 WHERE ACTI.NM_ACTION = 'ATUALIZAR' 
                                                 AND FUNC.NM_FUNCTION = 'ALUNOS'), 
                                                 (SELECT PROF.PROF_SKU
                                                 FROM TBL_PROFILES PROF
                                                 WHERE PROF.NM_PROFILE = 'Instrutor'));
INSERT INTO TBL_PERMISSIONS (DT_CREATION, DT_UPDATED, NR_VERSION, ACTI_SKU, PROF_SKU)
VALUES (CURDATE(), CURDATE(), 0, (SELECT ACTI.ACTI_SKU
                                                 FROM TBL_ACTIONS ACTI
                                                 INNER JOIN TBL_FUNCTIONS FUNC ON FUNC.FUNC_SKU = ACTI.FUNC_SKU
                                                 WHERE ACTI.NM_ACTION = 'VISUALIZAR' 
                                                 AND FUNC.NM_FUNCTION = 'ALUNOS'), 
                                                 (SELECT PROF.PROF_SKU
                                                 FROM TBL_PROFILES PROF
                                                 WHERE PROF.NM_PROFILE = 'Instrutor'));
INSERT INTO TBL_PERMISSIONS (DT_CREATION, DT_UPDATED, NR_VERSION, ACTI_SKU, PROF_SKU)
VALUES (CURDATE(), CURDATE(), 0, (SELECT ACTI.ACTI_SKU
                                                 FROM TBL_ACTIONS ACTI
                                                 INNER JOIN TBL_FUNCTIONS FUNC ON FUNC.FUNC_SKU = ACTI.FUNC_SKU
                                                 WHERE ACTI.NM_ACTION = 'REMOVER' 
                                                 AND FUNC.NM_FUNCTION = 'ALUNOS'), 
                                                 (SELECT PROF.PROF_SKU
                                                 FROM TBL_PROFILES PROF
                                                 WHERE PROF.NM_PROFILE = 'Instrutor'));
INSERT INTO TBL_PERMISSIONS (DT_CREATION, DT_UPDATED, NR_VERSION, ACTI_SKU, PROF_SKU)
VALUES (CURDATE(), CURDATE(), 0, (SELECT ACTI.ACTI_SKU
                                                 FROM TBL_ACTIONS ACTI
                                                 INNER JOIN TBL_FUNCTIONS FUNC ON FUNC.FUNC_SKU = ACTI.FUNC_SKU
                                                 WHERE ACTI.NM_ACTION = 'ATUALIZAR' 
                                                 AND FUNC.NM_FUNCTION = 'INSTRUTORES'), 
                                                 (SELECT PROF.PROF_SKU
                                                 FROM TBL_PROFILES PROF
                                                 WHERE PROF.NM_PROFILE = 'Instrutor'));
INSERT INTO TBL_PERMISSIONS (DT_CREATION, DT_UPDATED, NR_VERSION, ACTI_SKU, PROF_SKU)
VALUES (CURDATE(), CURDATE(), 0, (SELECT ACTI.ACTI_SKU
                                                 FROM TBL_ACTIONS ACTI
                                                 INNER JOIN TBL_FUNCTIONS FUNC ON FUNC.FUNC_SKU = ACTI.FUNC_SKU
                                                 WHERE ACTI.NM_ACTION = 'VISUALIZAR' 
                                                 AND FUNC.NM_FUNCTION = 'INSTRUTORES'), 
                                                 (SELECT PROF.PROF_SKU
                                                 FROM TBL_PROFILES PROF
                                                 WHERE PROF.NM_PROFILE = 'Instrutor'));

-- CRIANDO O CADASTRO DE PERMISSÕES - ASSOCIADO --
INSERT INTO TBL_PERMISSIONS (DT_CREATION, DT_UPDATED, NR_VERSION, ACTI_SKU, PROF_SKU)
VALUES (CURDATE(), CURDATE(), 0, (SELECT ACTI.ACTI_SKU
                                                 FROM TBL_ACTIONS ACTI
                                                 INNER JOIN TBL_FUNCTIONS FUNC ON FUNC.FUNC_SKU = ACTI.FUNC_SKU
                                                 WHERE ACTI.NM_ACTION = 'VISUALIZAR' 
                                                 AND FUNC.NM_FUNCTION = 'HOME'), 
                                                 (SELECT PROF.PROF_SKU
                                                 FROM TBL_PROFILES PROF
                                                 WHERE PROF.NM_PROFILE = 'Associado'));
INSERT INTO TBL_PERMISSIONS (DT_CREATION, DT_UPDATED, NR_VERSION, ACTI_SKU, PROF_SKU)
VALUES (CURDATE(), CURDATE(), 0, (SELECT ACTI.ACTI_SKU
                                                 FROM TBL_ACTIONS ACTI
                                                 INNER JOIN TBL_FUNCTIONS FUNC ON FUNC.FUNC_SKU = ACTI.FUNC_SKU
                                                 WHERE ACTI.NM_ACTION = 'VISUALIZAR' 
                                                 AND FUNC.NM_FUNCTION = 'EVENTOS'), 
                                                 (SELECT PROF.PROF_SKU
                                                 FROM TBL_PROFILES PROF
                                                 WHERE PROF.NM_PROFILE = 'Associado'));
INSERT INTO TBL_PERMISSIONS (DT_CREATION, DT_UPDATED, NR_VERSION, ACTI_SKU, PROF_SKU)
VALUES (CURDATE(), CURDATE(), 0, (SELECT ACTI.ACTI_SKU
                                                 FROM TBL_ACTIONS ACTI
                                                 INNER JOIN TBL_FUNCTIONS FUNC ON FUNC.FUNC_SKU = ACTI.FUNC_SKU
                                                 WHERE ACTI.NM_ACTION = 'VISUALIZAR' 
                                                 AND FUNC.NM_FUNCTION = 'CLUBES'), 
                                                 (SELECT PROF.PROF_SKU
                                                 FROM TBL_PROFILES PROF
                                                 WHERE PROF.NM_PROFILE = 'Associado'));
INSERT INTO TBL_PERMISSIONS (DT_CREATION, DT_UPDATED, NR_VERSION, ACTI_SKU, PROF_SKU)
VALUES (CURDATE(), CURDATE(), 0, (SELECT ACTI.ACTI_SKU
                                                 FROM TBL_ACTIONS ACTI
                                                 INNER JOIN TBL_FUNCTIONS FUNC ON FUNC.FUNC_SKU = ACTI.FUNC_SKU
                                                 WHERE ACTI.NM_ACTION = 'ATUALIZAR' 
                                                 AND FUNC.NM_FUNCTION = 'CURSOS'), 
                                                 (SELECT PROF.PROF_SKU
                                                 FROM TBL_PROFILES PROF
                                                 WHERE PROF.NM_PROFILE = 'Associado'));
INSERT INTO TBL_PERMISSIONS (DT_CREATION, DT_UPDATED, NR_VERSION, ACTI_SKU, PROF_SKU)
VALUES (CURDATE(), CURDATE(), 0, (SELECT ACTI.ACTI_SKU
                                                 FROM TBL_ACTIONS ACTI
                                                 INNER JOIN TBL_FUNCTIONS FUNC ON FUNC.FUNC_SKU = ACTI.FUNC_SKU
                                                 WHERE ACTI.NM_ACTION = 'VISUALIZAR' 
                                                 AND FUNC.NM_FUNCTION = 'CURSOS'), 
                                                 (SELECT PROF.PROF_SKU
                                                 FROM TBL_PROFILES PROF
                                                 WHERE PROF.NM_PROFILE = 'Associado'));
INSERT INTO TBL_PERMISSIONS (DT_CREATION, DT_UPDATED, NR_VERSION, ACTI_SKU, PROF_SKU)
VALUES (CURDATE(), CURDATE(), 0, (SELECT ACTI.ACTI_SKU
                                                 FROM TBL_ACTIONS ACTI
                                                 INNER JOIN TBL_FUNCTIONS FUNC ON FUNC.FUNC_SKU = ACTI.FUNC_SKU
                                                 WHERE ACTI.NM_ACTION = 'REMOVER' 
                                                 AND FUNC.NM_FUNCTION = 'CURSOS'), 
                                                 (SELECT PROF.PROF_SKU
                                                 FROM TBL_PROFILES PROF
                                                 WHERE PROF.NM_PROFILE = 'Associado'));
INSERT INTO TBL_PERMISSIONS (DT_CREATION, DT_UPDATED, NR_VERSION, ACTI_SKU, PROF_SKU)
VALUES (CURDATE(), CURDATE(), 0, (SELECT ACTI.ACTI_SKU
                                                 FROM TBL_ACTIONS ACTI
                                                 INNER JOIN TBL_FUNCTIONS FUNC ON FUNC.FUNC_SKU = ACTI.FUNC_SKU
                                                 WHERE ACTI.NM_ACTION = 'VISUALIZAR' 
                                                 AND FUNC.NM_FUNCTION = 'INSTRUTORES'), 
                                                 (SELECT PROF.PROF_SKU
                                                 FROM TBL_PROFILES PROF
                                                 WHERE PROF.NM_PROFILE = 'Associado'));
                                                 
-- CRIANDO O CADASTRO DE PERMISSÕES - USUARIO --
INSERT INTO TBL_PERMISSIONS (DT_CREATION, DT_UPDATED, NR_VERSION, ACTI_SKU, PROF_SKU)
VALUES (CURDATE(), CURDATE(), 0, (SELECT ACTI.ACTI_SKU
                                                 FROM TBL_ACTIONS ACTI
                                                 INNER JOIN TBL_FUNCTIONS FUNC ON FUNC.FUNC_SKU = ACTI.FUNC_SKU
                                                 WHERE ACTI.NM_ACTION = 'VISUALIZAR' 
                                                 AND FUNC.NM_FUNCTION = 'HOME'), 
                                                 (SELECT PROF.PROF_SKU
                                                 FROM TBL_PROFILES PROF
                                                 WHERE PROF.NM_PROFILE = 'Usuário'));
INSERT INTO TBL_PERMISSIONS (DT_CREATION, DT_UPDATED, NR_VERSION, ACTI_SKU, PROF_SKU)
VALUES (CURDATE(), CURDATE(), 0, (SELECT ACTI.ACTI_SKU
                                                 FROM TBL_ACTIONS ACTI
                                                 INNER JOIN TBL_FUNCTIONS FUNC ON FUNC.FUNC_SKU = ACTI.FUNC_SKU
                                                 WHERE ACTI.NM_ACTION = 'VISUALIZAR' 
                                                 AND FUNC.NM_FUNCTION = 'EVENTOS'), 
                                                 (SELECT PROF.PROF_SKU
                                                 FROM TBL_PROFILES PROF
                                                 WHERE PROF.NM_PROFILE = 'Usuário'));
INSERT INTO TBL_PERMISSIONS (DT_CREATION, DT_UPDATED, NR_VERSION, ACTI_SKU, PROF_SKU)
VALUES (CURDATE(), CURDATE(), 0, (SELECT ACTI.ACTI_SKU
                                                 FROM TBL_ACTIONS ACTI
                                                 INNER JOIN TBL_FUNCTIONS FUNC ON FUNC.FUNC_SKU = ACTI.FUNC_SKU
                                                 WHERE ACTI.NM_ACTION = 'VISUALIZAR' 
                                                 AND FUNC.NM_FUNCTION = 'CLUBES'), 
                                                 (SELECT PROF.PROF_SKU
                                                 FROM TBL_PROFILES PROF
                                                 WHERE PROF.NM_PROFILE = 'Usuário'));
INSERT INTO TBL_PERMISSIONS (DT_CREATION, DT_UPDATED, NR_VERSION, ACTI_SKU, PROF_SKU)
VALUES (CURDATE(), CURDATE(), 0, (SELECT ACTI.ACTI_SKU
                                                 FROM TBL_ACTIONS ACTI
                                                 INNER JOIN TBL_FUNCTIONS FUNC ON FUNC.FUNC_SKU = ACTI.FUNC_SKU
                                                 WHERE ACTI.NM_ACTION = 'ATUALIZAR' 
                                                 AND FUNC.NM_FUNCTION = 'CURSOS'), 
                                                 (SELECT PROF.PROF_SKU
                                                 FROM TBL_PROFILES PROF
                                                 WHERE PROF.NM_PROFILE = 'Usuário'));
INSERT INTO TBL_PERMISSIONS (DT_CREATION, DT_UPDATED, NR_VERSION, ACTI_SKU, PROF_SKU)
VALUES (CURDATE(), CURDATE(), 0, (SELECT ACTI.ACTI_SKU
                                                 FROM TBL_ACTIONS ACTI
                                                 INNER JOIN TBL_FUNCTIONS FUNC ON FUNC.FUNC_SKU = ACTI.FUNC_SKU
                                                 WHERE ACTI.NM_ACTION = 'VISUALIZAR' 
                                                 AND FUNC.NM_FUNCTION = 'CURSOS'), 
                                                 (SELECT PROF.PROF_SKU
                                                 FROM TBL_PROFILES PROF
                                                 WHERE PROF.NM_PROFILE = 'Usuário'));
INSERT INTO TBL_PERMISSIONS (DT_CREATION, DT_UPDATED, NR_VERSION, ACTI_SKU, PROF_SKU)
VALUES (CURDATE(), CURDATE(), 0, (SELECT ACTI.ACTI_SKU
                                                 FROM TBL_ACTIONS ACTI
                                                 INNER JOIN TBL_FUNCTIONS FUNC ON FUNC.FUNC_SKU = ACTI.FUNC_SKU
                                                 WHERE ACTI.NM_ACTION = 'REMOVER' 
                                                 AND FUNC.NM_FUNCTION = 'CURSOS'), 
                                                 (SELECT PROF.PROF_SKU
                                                 FROM TBL_PROFILES PROF
                                                 WHERE PROF.NM_PROFILE = 'Usuário'));
INSERT INTO TBL_PERMISSIONS (DT_CREATION, DT_UPDATED, NR_VERSION, ACTI_SKU, PROF_SKU)
VALUES (CURDATE(), CURDATE(), 0, (SELECT ACTI.ACTI_SKU
                                                 FROM TBL_ACTIONS ACTI
                                                 INNER JOIN TBL_FUNCTIONS FUNC ON FUNC.FUNC_SKU = ACTI.FUNC_SKU
                                                 WHERE ACTI.NM_ACTION = 'VISUALIZAR' 
                                                 AND FUNC.NM_FUNCTION = 'INSTRUTORES'), 
                                                 (SELECT PROF.PROF_SKU
                                                 FROM TBL_PROFILES PROF
                                                 WHERE PROF.NM_PROFILE = 'Usuário'));
CALL CREATE_EVENT(
					'1ª Etapa Estadual de IPSC HANDGUN e MINE RILFE'/*V_NM_EVENT*/,
					3 /*V_TP_EVENT*/,
					'2017-03-05'/*V_DS_FIRST_DAY*/,
					'05/03/17 09:00 às 17:00'/*V_DS_DAYS*/,					
                    'SINDIPOL'/*V_NM_CLUB*/,
                    '09:00:00'/*V_DS_HOUR_START*/,
                    '17:00:00'/*V_DS_HOUR_END*/,
                    'IPSC HANDGUN'/*V_DS_MODALITY*/,
                    'MINI RILFE'/*V_DS_MODALITY*/,
                    'Atiradores com CR'/*V_DS_PARTICIPATION*/
				 );

CALL CREATE_EVENT(
					'SEMINÁRIO DE ROs (NÍVEL I)'/*V_NM_EVENT*/,
					0 /*V_TP_EVENT*/,
					'2017-03-26'/*V_DS_FIRST_DAY*/,
					'26/03/17 09:00 às 17:00'/*V_DS_DAYS*/,					
                    'CTVV'/*V_NM_CLUB*/,
                    '09:00:00'/*V_DS_HOUR_START*/,
                    '17:00:00'/*V_DS_HOUR_END*/,
                    'SEMINÁRIO'/*V_DS_MODALITY*/,
                    null/*V_DS_MODALITY*/,
                    'ROs - Rangers Offices'/*V_DS_PARTICIPATION*/
				 );

CALL CREATE_EVENT(
					'2ª Etapa Estadual de IPSC HANDGUN e MINI RILFE'/*V_NM_EVENT*/,
					3 /*V_TP_EVENT*/,
					'2017-04-23'/*V_DS_FIRST_DAY*/,
					'23/04/17 09:00 às 17:00'/*V_DS_DAYS*/,					
                    'CTCI'/*V_NM_CLUB*/,
                    '09:00:00'/*V_DS_HOUR_START*/,
                    '17:00:00'/*V_DS_HOUR_END*/,
                    'IPSC HANDGUN'/*V_DS_MODALITY*/,
                    'MINI RILFE'/*V_DS_MODALITY*/,
                    'Atiradores com CR'/*V_DS_PARTICIPATION*/
				 );

CALL CREATE_EVENT(
					'1ª Etapa Estadual de IPSC SHOTGUN e SILHUETA METÁLICA'/*V_NM_EVENT*/,
					3 /*V_TP_EVENT*/,
					'2017-05-07'/*V_DS_FIRST_DAY*/,
					'07/05/17 09:00 às 17:00'/*V_DS_DAYS*/,					
                    'CTC'/*V_NM_CLUB*/,
                    '09:00:00'/*V_DS_HOUR_START*/,
                    '17:00:00'/*V_DS_HOUR_END*/,
                    'IPSC SHOTGUN'/*V_DS_MODALITY*/,
					'SILHUETA METÁLICA'/*V_DS_MODALITY*/,
                    'Atiradores com CR'/*V_DS_PARTICIPATION*/
				 );

CALL CREATE_EVENT(
					'2ª Etapa Estadual de IPSC SHOTGUN e SILHUETA METÁLICA'/*V_NM_EVENT*/,
					3 /*V_TP_EVENT*/,
					'2017-05-28'/*V_DS_FIRST_DAY*/,
					'28/05/17 09:00 às 17:00'/*V_DS_DAYS*/,					
                    'SINDIPOL'/*V_NM_CLUB*/,
                    '09:00:00'/*V_DS_HOUR_START*/,
                    '17:00:00'/*V_DS_HOUR_END*/,
                    'IPSC SHOTGUN'/*V_DS_MODALITY*/,
                    'SILHUETA METÁLICA'/*V_DS_MODALITY*/,
                    'Atiradores com CR'/*V_DS_PARTICIPATION*/
				 );

CALL CREATE_EVENT(
					'3ª Etapa Estadual de IPSC HANDGUN e MINI RILFE'/*V_NM_EVENT*/,
					3 /*V_TP_EVENT*/,
					'2017-06-18'/*V_DS_FIRST_DAY*/,
					'18/06/17 09:00 às 17:00'/*V_DS_DAYS*/,					
                    'CTTF'/*V_NM_CLUB*/,
                    '09:00:00'/*V_DS_HOUR_START*/,
                    '17:00:00'/*V_DS_HOUR_END*/,
                    'IPSC HANDGUN'/*V_DS_MODALITY*/,
                    'MINI RILFE'/*V_DS_MODALITY*/,
                    'Atiradores com CR'/*V_DS_PARTICIPATION*/
				 );

CALL CREATE_EVENT(
					'3ª Etapa Estadual de IPSC SHOTGUN e IPSC HANDGUN'/*V_NM_EVENT*/,
					3 /*V_TP_EVENT*/,
					'2017-09-07'/*V_DS_FIRST_DAY*/,
					'09/07/17 09:00 às 17:00'/*V_DS_DAYS*/,					
                    'CTVV'/*V_NM_CLUB*/,
                    '09:00:00'/*V_DS_HOUR_START*/,
                    '17:00:00'/*V_DS_HOUR_END*/,
                    'IPSC SHOTGUN'/*V_DS_MODALITY*/,
                    'IPSC HANDGUN'/*V_DS_MODALITY*/,
                    'Atiradores com CR'/*V_DS_PARTICIPATION*/
				 );
				 
CALL CREATE_EVENT(
					'3ª Etapa Estadual de SILHUETA METÁLICA'/*V_NM_EVENT*/,
					3 /*V_TP_EVENT*/,
					'2017-09-03'/*V_DS_FIRST_DAY*/,
					'03/09/17 09:00 às 17:00'/*V_DS_DAYS*/,					
                    'APOFES'/*V_NM_CLUB*/,
                    '09:00:00'/*V_DS_HOUR_START*/,
                    '17:00:00'/*V_DS_HOUR_END*/,
                    'SILHUETA METÁLICA'/*V_DS_MODALITY*/,
                    null/*V_DS_MODALITY*/,
                    'Atiradores com CR'/*V_DS_PARTICIPATION*/
				 );

CALL CREATE_EVENT(
					'5ª Etapa Estadual de IPSC HANDGUN'/*V_NM_EVENT*/,
					3 /*V_TP_EVENT*/,
					'2017-09-24'/*V_DS_FIRST_DAY*/,
					'24/09/17 09:00 às 17:00'/*V_DS_DAYS*/,					
                    'CTTF'/*V_NM_CLUB*/,
                    '09:00:00'/*V_DS_HOUR_START*/,
                    '17:00:00'/*V_DS_HOUR_END*/,
                    'IPSC HANDGUN'/*V_DS_MODALITY*/,
                    null/*V_DS_MODALITY*/,
                    'Atiradores com CR'/*V_DS_PARTICIPATION*/
				 );

CALL CREATE_EVENT(
					'4ª Etapa Estadual de SILHUETA METÁLICA'/*V_NM_EVENT*/,
					3 /*V_TP_EVENT*/,
					'2017-09-24'/*V_DS_FIRST_DAY*/,
					'24/09/17 09:00 às 17:00'/*V_DS_DAYS*/,					
                    'CTTF'/*V_NM_CLUB*/,
                    '09:00:00'/*V_DS_HOUR_START*/,
                    '17:00:00'/*V_DS_HOUR_END*/,
                    'SILHUETA METÁLICA'/*V_DS_MODALITY*/,
                    null/*V_DS_MODALITY*/,
                    'Atiradores com CR'/*V_DS_PARTICIPATION*/
				 );
				 
 CALL CREATE_EVENT(
 					'4ª Etapa Estadual de IPSC SHOTGUN e SILHUETA METÁLICA'/*V_NM_EVENT*/,
 					3 /*V_TP_EVENT*/,
 					'2017-10-22'/*V_DS_FIRST_DAY*/,
 					'22/10/17 09:00 às 17:00'/*V_DS_DAYS*/,					
                     'CCTE'/*V_NM_CLUB*/,
                     '09:00:00'/*V_DS_HOUR_START*/,
                     '17:00:00'/*V_DS_HOUR_END*/,
                     'IPSC SHOTGUN'/*V_DS_MODALITY*/,
                     'SILHUETA METÁLICA'/*V_DS_MODALITY*/,
                     'Atiradores com CR'/*V_DS_PARTICIPATION*/
 				 );

CALL CREATE_EVENT(
					'5ª Etapa Estadual de IPSC SHOTGUN'/*V_NM_EVENT*/,
					3 /*V_TP_EVENT*/,
					'2017-11-19'/*V_DS_FIRST_DAY*/,
					'19/11/17 09:00 às 17:00'/*V_DS_DAYS*/,					
                    'CTVV'/*V_NM_CLUB*/,
                    '09:00:00'/*V_DS_HOUR_START*/,
                    '17:00:00'/*V_DS_HOUR_END*/,
                    'IPSC SHOTGUN'/*V_DS_MODALITY*/,
                    null/*V_DS_MODALITY*/,
                    'Atiradores com CR'/*V_DS_PARTICIPATION*/
				 );

CALL CREATE_EVENT(
					'6ª Etapa Estadual de IPSC SHOTGUN e SILHUETA METÁLICA'/*V_NM_EVENT*/,
					3 /*V_TP_EVENT*/,
					'2017-11-19'/*V_DS_FIRST_DAY*/,
					'19/11/17 09:00 às 17:00'/*V_DS_DAYS*/,					
                    'CTVV'/*V_NM_CLUB*/,
                    '09:00:00'/*V_DS_HOUR_START*/,
                    '17:00:00'/*V_DS_HOUR_END*/,
                    'IPSC SHOTGUN'/*V_DS_MODALITY*/,
                    'SILHUETA METÁLICA'/*V_DS_MODALITY*/,
                    'Atiradores com CR'/*V_DS_PARTICIPATION*/
				 );

CALL CREATE_EVENT(
					'7ª Etapa Estadual de SILHUETA METÁLICA'/*V_NM_EVENT*/,
					3 /*V_TP_EVENT*/,
					'2017-11-19'/*V_DS_FIRST_DAY*/,
					'19/11/17 09:00 às 17:00'/*V_DS_DAYS*/,					
                    'CTCI'/*V_NM_CLUB*/,
                    '09:00:00'/*V_DS_HOUR_START*/,
                    '17:00:00'/*V_DS_HOUR_END*/,
                    'SILHUETA METÁLICA'/*V_DS_MODALITY*/,
                    null/*V_DS_MODALITY*/,
                    'Atiradores com CR'/*V_DS_PARTICIPATION*/
				 );
				 
CALL CREATE_EVENT(
					'CAMPEONATO'/*V_NM_EVENT*/,
					1 /*V_TP_EVENT*/,
					'2017-01-22'/*V_DS_FIRST_DAY*/,
					'22/01/17 09:00 às 17:00'/*V_DS_DAYS*/,					
                    'CTVV'/*V_NM_CLUB*/,
                    '09:00:00'/*V_DS_HOUR_START*/,
                    '17:00:00'/*V_DS_HOUR_END*/,
                    'SAQUE RÁPIDO'/*V_DS_MODALITY*/,
                    null/*V_DS_MODALITY*/,
                    'Associados'/*V_DS_PARTICIPATION*/
				 );
				 
CALL CREATE_EVENT(
					'1ª COPA BRASIL DE IDSC - TIRO DEFENSIVO'/*V_NM_EVENT*/,
					2 /*V_TP_EVENT*/,
					'2017-02-19'/*V_DS_FIRST_DAY*/,
					'19/02/17 09:00 às 17:00'/*V_DS_DAYS*/,					
                    'CTVV'/*V_NM_CLUB*/,
                    '09:00:00'/*V_DS_HOUR_START*/,
                    '17:00:00'/*V_DS_HOUR_END*/,
                    'IDSC'/*V_DS_MODALITY*/,
                    null/*V_DS_MODALITY*/,
                    'Atiradores com CR'/*V_DS_PARTICIPATION*/
				 );
				 
CALL CREATE_EVENT(
					'1º CAMPEONATO SHOTGUN e IPSC'/*V_NM_EVENT*/,
					1 /*V_TP_EVENT*/,
					'2017-03-19'/*V_DS_FIRST_DAY*/,
					'19/03/17 09:00 às 17:00'/*V_DS_DAYS*/,					
                    'CTVV'/*V_NM_CLUB*/,
                    '09:00:00'/*V_DS_HOUR_START*/,
                    '17:00:00'/*V_DS_HOUR_END*/,
                    'IPSC SHOTGUN'/*V_DS_MODALITY*/,
                    'IPSC HANDGUN'/*V_DS_MODALITY*/,
                    'Associados'/*V_DS_PARTICIPATION*/
				 ); 
				 
CALL CREATE_EVENT(
					'1ª ETAPA DO BRASILEIRO DE IDSC - TIRO DEFENSIVO'/*V_NM_EVENT*/,
					2 /*V_TP_EVENT*/,
					'2017-04-30'/*V_DS_FIRST_DAY*/,
					'30/04/17 09:00 às 17:00'/*V_DS_DAYS*/,					
                    'CTVV'/*V_NM_CLUB*/,
                    '09:00:00'/*V_DS_HOUR_START*/,
                    '17:00:00'/*V_DS_HOUR_END*/,
                    'IDSC'/*V_DS_MODALITY*/,
                    null/*V_DS_MODALITY*/,
                    'Atiradores com CR'/*V_DS_PARTICIPATION*/
				 );

CALL CREATE_EVENT(
					'1º CAMPEONATO SAQUE RÁPIDO e CARABINA PUMA'/*V_NM_EVENT*/,
					1 /*V_TP_EVENT*/,
					'2017-05-21'/*V_DS_FIRST_DAY*/,
					'21/05/17 09:00 às 17:00'/*V_DS_DAYS*/,					
                    'CTVV'/*V_NM_CLUB*/,
                    '09:00:00'/*V_DS_HOUR_START*/,
                    '17:00:00'/*V_DS_HOUR_END*/,
                    'SAQUE RÁPIDO'/*V_DS_MODALITY*/,
                    'CARABINA PUMA'/*V_DS_MODALITY*/,
                    'Associados'/*V_DS_PARTICIPATION*/
				 ); 
				 
CALL CREATE_EVENT(
					'2ª ETAPA DO ESTADUAL DE IDSC - TIRO DEFENSIVO'/*V_NM_EVENT*/,
					3 /*V_TP_EVENT*/,
					'2017-06-11'/*V_DS_FIRST_DAY*/,
					'11/06/17 09:00 às 17:00'/*V_DS_DAYS*/,					
                    'CTVV'/*V_NM_CLUB*/,
                    '09:00:00'/*V_DS_HOUR_START*/,
                    '17:00:00'/*V_DS_HOUR_END*/,
                    'IDSC'/*V_DS_MODALITY*/,
                    null/*V_DS_MODALITY*/,
                    'Atiradores com CR'/*V_DS_PARTICIPATION*/
				 );

CALL CREATE_EVENT(
					'2º CAMPEONATO SAQUE RÁPIDO e CARABINA PUMA'/*V_NM_EVENT*/,
					1 /*V_TP_EVENT*/,
					'2017-08-27'/*V_DS_FIRST_DAY*/,
					'27/08/17 09:00 às 17:00'/*V_DS_DAYS*/,					
                    'CTVV'/*V_NM_CLUB*/,
                    '09:00:00'/*V_DS_HOUR_START*/,
                    '17:00:00'/*V_DS_HOUR_END*/,
                    'SAQUE RÁPIDO'/*V_DS_MODALITY*/,
                    'CARABINA PUMA'/*V_DS_MODALITY*/,
                    'Associados'/*V_DS_PARTICIPATION*/
				 ); 
				 
CALL CREATE_EVENT(
					'2º CAMPEONATO SHOTGUN e IPSC'/*V_NM_EVENT*/,
					1 /*V_TP_EVENT*/,
					'2017-09-17'/*V_DS_FIRST_DAY*/,
					'17/09/17 09:00 às 17:00'/*V_DS_DAYS*/,					
                    'CTVV'/*V_NM_CLUB*/,
                    '09:00:00'/*V_DS_HOUR_START*/,
                    '17:00:00'/*V_DS_HOUR_END*/,
                    'IPSC SHOTGUN'/*V_DS_MODALITY*/,
                    'IPSC HANDGUN'/*V_DS_MODALITY*/,
                    'Associados'/*V_DS_PARTICIPATION*/
				 ); 
				 
CALL CREATE_EVENT(
					'2ª COPA BRASIL DE IDSC - TIRO DEFENSIVO'/*V_NM_EVENT*/,
					2 /*V_TP_EVENT*/,
					'2017-11-15'/*V_DS_FIRST_DAY*/,
					'15/11/17 09:00 às 17:00'/*V_DS_DAYS*/,					
                    'CTVV'/*V_NM_CLUB*/,
                    '09:00:00'/*V_DS_HOUR_START*/,
                    '17:00:00'/*V_DS_HOUR_END*/,
                    'IDSC'/*V_DS_MODALITY*/,
                    null/*V_DS_MODALITY*/,
                    'Atiradores com CR'/*V_DS_PARTICIPATION*/
				 );
				 
CALL CREATE_EVENT(
					'3ª ETAPA DO ESTADUAL DE IDSC - TIRO DEFENSIVO'/*V_NM_EVENT*/,
					3 /*V_TP_EVENT*/,
					'2017-12-10'/*V_DS_FIRST_DAY*/,
					'10/12/17 09:00 às 17:00'/*V_DS_DAYS*/,					
                    'CTVV'/*V_NM_CLUB*/,
                    '09:00:00'/*V_DS_HOUR_START*/,
                    '17:00:00'/*V_DS_HOUR_END*/,
                    'IDSC'/*V_DS_MODALITY*/,
                    null/*V_DS_MODALITY*/,
                    'Atiradores com CR'/*V_DS_PARTICIPATION*/
				 );

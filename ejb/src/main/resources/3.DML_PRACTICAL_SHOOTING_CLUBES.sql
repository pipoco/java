-- #################################################### CTVV ######################################################## --
INSERT INTO TBL_CLUBS (DT_CREATION, DT_UPDATED, NR_VERSION, NUM_SITU_ADMINISTRATOR, TXT_LINK_IMAGE, TXT_MAP_LINK, NM_NAME, NM_PRESIDENT, TXT_REFERENCE, USER_SKU, CD_CLUB, VL_ORDER)
VALUES (CURDATE(), CURDATE(), 0, NULL, 'ctvv_logo.png', NULL, 'Clube de Tiro Vila Velha', 'Felipe Rodrigues',
                   'Em frente a pousada recanto da lua', NULL, 'CTVV', 1);
INSERT INTO TBL_CONTACTS (DT_CREATION, DT_UPDATED, NR_VERSION, DS_CONTACT, TP_CONTACT)
VALUES (CURDATE(), CURDATE(), 0, 'clubedetiro@hotmail.com', 3);
INSERT INTO TBL_CLUB_CONTACTS (CLCT_SKU, CLUB_SKU)
VALUES ((SELECT CONT.CONT_SKU
         FROM TBL_CONTACTS CONT
         WHERE CONT.DS_CONTACT = 'clubedetiro@hotmail.com'), (SELECT CLUB.CLUB_SKU
                                                     FROM TBL_CLUBS CLUB
                                                     WHERE CLUB.CD_CLUB = 'CTVV'));
INSERT INTO TBL_CONTACTS (DT_CREATION, DT_UPDATED, NR_VERSION, DS_CONTACT, TP_CONTACT)
VALUES (CURDATE(), CURDATE(), 0, '27 998646009', 0);
INSERT INTO TBL_CLUB_CONTACTS (CLCT_SKU, CLUB_SKU)
VALUES ((SELECT CONT.CONT_SKU
         FROM TBL_CONTACTS CONT
         WHERE CONT.DS_CONTACT = '27 998646009'), (SELECT CLUB.CLUB_SKU
                                                   FROM TBL_CLUBS CLUB
                                                   WHERE CLUB.CD_CLUB = 'CTVV'));
INSERT INTO TBL_CLUB_ADDRESSES (CLAD_SKU, CLUB_SKU)
VALUES ((SELECT ADDR.ADDR_SKU
         FROM TBL_ADDRESSES ADDR
         WHERE ADDR.TXT_ZIP_CODE = '29.129-326'), (SELECT CLUB.CLUB_SKU
                                                   FROM TBL_CLUBS CLUB
                                                   WHERE CLUB.CD_CLUB = 'CTVV'));

-- #################################################### CTC ######################################################## --
INSERT INTO TBL_CLUBS (DT_CREATION, DT_UPDATED, NR_VERSION, NUM_SITU_ADMINISTRATOR, TXT_LINK_IMAGE, TXT_MAP_LINK, NM_NAME, NM_PRESIDENT, TXT_REFERENCE, USER_SKU, CD_CLUB, VL_ORDER)
VALUES (CURDATE(), CURDATE(), 0, NULL, 'ctc_logo.png', NULL, 'Clube de Tiro de Colatina', '', '', NULL, 'CTC', 4);
INSERT INTO TBL_CONTACTS (DT_CREATION, DT_UPDATED, NR_VERSION, DS_CONTACT, TP_CONTACT)
VALUES (CURDATE(), CURDATE(), 0, 'tirocolatina@gmail.com', 3);
INSERT INTO TBL_CLUB_CONTACTS (CLCT_SKU, CLUB_SKU)
VALUES ((SELECT CONT.CONT_SKU
         FROM TBL_CONTACTS CONT
         WHERE CONT.DS_CONTACT = 'tirocolatina@gmail.com'), (SELECT CLUB.CLUB_SKU
                                                    FROM TBL_CLUBS CLUB
                                                    WHERE CLUB.CD_CLUB = 'CTC'));
INSERT INTO TBL_ADDRESSES (DT_CREATION, DT_UPDATED, NR_VERSION, TXT_CITY, TXT_COMPLEMENT, TXT_DISTRICT, TXT_NUMBER, TXT_PLACE, TXT_STATE, TXT_ZIP_CODE)
VALUES (CURDATE(), CURDATE(), 0, 'Colatina', '', 'Operário Colatina', '68', 'Rua Humberto de Campos', 'ES', '29.701-280');
INSERT INTO TBL_CLUB_ADDRESSES (CLAD_SKU, CLUB_SKU)
VALUES ((SELECT ADDR.ADDR_SKU
         FROM TBL_ADDRESSES ADDR
         WHERE ADDR.TXT_ZIP_CODE = '29.701-280'), (SELECT CLUB.CLUB_SKU
                                                   FROM TBL_CLUBS CLUB
                                                   WHERE CLUB.CD_CLUB = 'CTC'));

-- #################################################### CTCI ######################################################## --
INSERT INTO TBL_CLUBS (DT_CREATION, DT_UPDATED, NR_VERSION, NUM_SITU_ADMINISTRATOR, TXT_LINK_IMAGE, TXT_MAP_LINK, NM_NAME, NM_PRESIDENT, TXT_REFERENCE, USER_SKU, CD_CLUB, VL_ORDER)
VALUES (CURDATE(), CURDATE(), 0, NULL, 'ctci_logo.png', NULL, 'Clube de Tiro de Cachoeiro de Itapemirim',
                   'José Tadeu da Silva', '', NULL, 'CTCI', 2);
INSERT INTO TBL_CONTACTS (DT_CREATION, DT_UPDATED, NR_VERSION, DS_CONTACT, TP_CONTACT)
VALUES (CURDATE(), CURDATE(), 0, 'clubedetiro-secretaria@hotmail.com', 3);
INSERT INTO TBL_CONTACTS (DT_CREATION, DT_UPDATED, NR_VERSION, DS_CONTACT, TP_CONTACT)
VALUES (CURDATE(), CURDATE(), 0, '28 999466205', 0);
INSERT INTO TBL_CLUB_CONTACTS (CLCT_SKU, CLUB_SKU)
VALUES ((SELECT CONT.CONT_SKU
         FROM TBL_CONTACTS CONT
         WHERE CONT.DS_CONTACT = 'clubedetiro-secretaria@hotmail.com'), (SELECT CLUB.CLUB_SKU
                                                     FROM TBL_CLUBS CLUB
                                                     WHERE CLUB.CD_CLUB = 'CTCI'));
INSERT INTO TBL_CLUB_CONTACTS (CLCT_SKU, CLUB_SKU)
VALUES ((SELECT CONT.CONT_SKU
         FROM TBL_CONTACTS CONT
         WHERE CONT.DS_CONTACT = '28 999466205'), (SELECT CLUB.CLUB_SKU
                                                   FROM TBL_CLUBS CLUB
                                                   WHERE CLUB.CD_CLUB = 'CTCI'));

-- #################################################### SINDIPOL ######################################################## --
INSERT INTO TBL_CLUBS (DT_CREATION, DT_UPDATED, NR_VERSION, NUM_SITU_ADMINISTRATOR, TXT_LINK_IMAGE, TXT_MAP_LINK, NM_NAME, NM_PRESIDENT, TXT_REFERENCE, USER_SKU, CD_CLUB, VL_ORDER)
VALUES (CURDATE(), CURDATE(), 0, NULL, 'sindipol_logo.png', NULL, 'Clube de Tiro Sindipol', 'Fanzeres', '', NULL,
                   'SINDIPOL', 8);
INSERT INTO TBL_CONTACTS (DT_CREATION, DT_UPDATED, NR_VERSION, DS_CONTACT, TP_CONTACT)
VALUES (CURDATE(), CURDATE(), 0, 'fanzeres@clubedetirosindipol.com.br', 3);
INSERT INTO TBL_CONTACTS (DT_CREATION, DT_UPDATED, NR_VERSION, DS_CONTACT, TP_CONTACT)
VALUES (CURDATE(), CURDATE(), 0, '27 33283082', 2);
INSERT INTO TBL_CLUB_CONTACTS (CLCT_SKU, CLUB_SKU)
VALUES ((SELECT CONT.CONT_SKU
         FROM TBL_CONTACTS CONT
         WHERE CONT.DS_CONTACT = 'fanzeres@clubedetirosindipol.com.br'), (SELECT CLUB.CLUB_SKU
                                                         FROM TBL_CLUBS CLUB
                                                         WHERE CLUB.CD_CLUB = 'SINDIPOL'));
INSERT INTO TBL_CLUB_CONTACTS (CLCT_SKU, CLUB_SKU)
VALUES ((SELECT CONT.CONT_SKU
         FROM TBL_CONTACTS CONT
         WHERE CONT.DS_CONTACT = '27 33283082'), (SELECT CLUB.CLUB_SKU
                                                   FROM TBL_CLUBS CLUB
                                                   WHERE CLUB.CD_CLUB = 'SINDIPOL'));
INSERT INTO TBL_ADDRESSES (DT_CREATION, DT_UPDATED, NR_VERSION, TXT_CITY, TXT_COMPLEMENT, TXT_DISTRICT, TXT_NUMBER, TXT_PLACE, TXT_STATE, TXT_ZIP_CODE)
VALUES (CURDATE(), CURDATE(), 0, 'Serra', '', 'Jardim Carapina', '4672', 'Av. Marginal', 'ES', '29.161-701');
INSERT INTO TBL_CLUB_ADDRESSES (CLAD_SKU, CLUB_SKU)
VALUES ((SELECT ADDR.ADDR_SKU
         FROM TBL_ADDRESSES ADDR
         WHERE ADDR.TXT_ZIP_CODE = '29.161-701'), (SELECT CLUB.CLUB_SKU
                                                   FROM TBL_CLUBS CLUB
                                                   WHERE CLUB.CD_CLUB = 'SINDIPOL'));

-- #################################################### CCT ######################################################## --
INSERT INTO TBL_CLUBS (DT_CREATION, DT_UPDATED, NR_VERSION, NUM_SITU_ADMINISTRATOR, TXT_LINK_IMAGE, TXT_MAP_LINK, NM_NAME, NM_PRESIDENT, TXT_REFERENCE, USER_SKU, CD_CLUB, VL_ORDER)
VALUES (CURDATE(), CURDATE(), 0, NULL, 'cct_logo.png', NULL, 'Clube Canarense de Tiro', 'Homero', '', NULL, 'CCT', 7);
INSERT INTO TBL_CONTACTS (DT_CREATION, DT_UPDATED, NR_VERSION, DS_CONTACT, TP_CONTACT)
VALUES (CURDATE(), CURDATE(), 0, '27 37640847', 2);
INSERT INTO TBL_CLUB_CONTACTS (CLCT_SKU, CLUB_SKU)
VALUES ((SELECT CONT.CONT_SKU
         FROM TBL_CONTACTS CONT
         WHERE CONT.DS_CONTACT = '27 37640847'), (SELECT CLUB.CLUB_SKU
                                                   FROM TBL_CLUBS CLUB
                                                   WHERE CLUB.CD_CLUB = 'CCT'));
INSERT INTO TBL_ADDRESSES (DT_CREATION, DT_UPDATED, NR_VERSION, TXT_CITY, TXT_COMPLEMENT, TXT_DISTRICT, TXT_NUMBER, TXT_PLACE, TXT_STATE, TXT_ZIP_CODE)
VALUES (CURDATE(), CURDATE(), 0, 'Pedro Canário', '', 'Pedro Canário', 'S/N', 'Pedro Canário', 'ES', '29.000-111');
INSERT INTO TBL_CLUB_ADDRESSES (CLAD_SKU, CLUB_SKU)
VALUES ((SELECT ADDR.ADDR_SKU
         FROM TBL_ADDRESSES ADDR
         WHERE ADDR.TXT_ZIP_CODE = '29.000-111'), (SELECT CLUB.CLUB_SKU
                                                   FROM TBL_CLUBS CLUB
                                                   WHERE CLUB.CD_CLUB = 'CCT'));

-- #################################################### APOFES ######################################################## --
INSERT INTO TBL_CLUBS (DT_CREATION, DT_UPDATED, NR_VERSION, NUM_SITU_ADMINISTRATOR, TXT_LINK_IMAGE, TXT_MAP_LINK, NM_NAME, NM_PRESIDENT, TXT_REFERENCE, USER_SKU, CD_CLUB, VL_ORDER)
VALUES (CURDATE(), CURDATE(), 0, NULL, 'apofes_logo.png', NULL,
                   'Associação dos Policiais Federais do Estado do Espírito-Santo', '', '', NULL, 'APOFES', 5);
INSERT INTO TBL_CONTACTS (DT_CREATION, DT_UPDATED, NR_VERSION, DS_CONTACT, TP_CONTACT)
VALUES (CURDATE(), CURDATE(), 0, 'contato@apofes.org.br', 3);
INSERT INTO TBL_CONTACTS (DT_CREATION, DT_UPDATED, NR_VERSION, DS_CONTACT, TP_CONTACT)
VALUES (CURDATE(), CURDATE(), 0, '27 33260768', 2);
INSERT INTO TBL_CLUB_CONTACTS (CLCT_SKU, CLUB_SKU)
VALUES ((SELECT CONT.CONT_SKU
         FROM TBL_CONTACTS CONT
         WHERE CONT.DS_CONTACT = 'contato@apofes.org.br'), (SELECT CLUB.CLUB_SKU
                                                       FROM TBL_CLUBS CLUB
                                                       WHERE CLUB.CD_CLUB = 'APOFES'));
INSERT INTO TBL_CLUB_CONTACTS (CLCT_SKU, CLUB_SKU)
VALUES ((SELECT CONT.CONT_SKU
         FROM TBL_CONTACTS CONT
         WHERE CONT.DS_CONTACT = '27 33260768'), (SELECT CLUB.CLUB_SKU
                                                   FROM TBL_CLUBS CLUB
                                                   WHERE CLUB.CD_CLUB = 'APOFES'));
INSERT INTO TBL_ADDRESSES (DT_CREATION, DT_UPDATED, NR_VERSION, TXT_CITY, TXT_COMPLEMENT, TXT_DISTRICT, TXT_NUMBER, TXT_PLACE, TXT_STATE, TXT_ZIP_CODE)
VALUES (CURDATE(), CURDATE(), 0, 'VILA VELHA', 'Lote 79', 'São Torquato', 'Quadra 79', 'Rua Vale do Rio Doce', 'ES', '29.111-111');
INSERT INTO TBL_CLUB_ADDRESSES (CLAD_SKU, CLUB_SKU)
VALUES ((SELECT ADDR.ADDR_SKU
         FROM TBL_ADDRESSES ADDR
         WHERE ADDR.TXT_ZIP_CODE = '29.111-111'), (SELECT CLUB.CLUB_SKU
                                                   FROM TBL_CLUBS CLUB
                                                   WHERE CLUB.CD_CLUB = 'APOFES'));

-- #################################################### CTML ######################################################## --
INSERT INTO TBL_CLUBS (DT_CREATION, DT_UPDATED, NR_VERSION, NUM_SITU_ADMINISTRATOR, TXT_LINK_IMAGE, TXT_MAP_LINK, NM_NAME, NM_PRESIDENT, TXT_REFERENCE, USER_SKU, CD_CLUB, VL_ORDER)
VALUES (CURDATE(), CURDATE(), 0, NULL, 'ctml_logo.png', NULL, 'Clube de Tiro Mata do Lago', '', '', NULL, 'CTML', 6);
INSERT INTO TBL_CONTACTS (DT_CREATION, DT_UPDATED, NR_VERSION, DS_CONTACT, TP_CONTACT)
VALUES (CURDATE(), CURDATE(), 0, '27 33712332', 2);
INSERT INTO TBL_CLUB_CONTACTS (CLCT_SKU, CLUB_SKU)
VALUES ((SELECT CONT.CONT_SKU
         FROM TBL_CONTACTS CONT
         WHERE CONT.DS_CONTACT = '27 33712332'), (SELECT CLUB.CLUB_SKU
                                                   FROM TBL_CLUBS CLUB
                                                   WHERE CLUB.CD_CLUB = 'CTML'));
INSERT INTO TBL_ADDRESSES (DT_CREATION, DT_UPDATED, NR_VERSION, TXT_CITY, TXT_COMPLEMENT, TXT_DISTRICT, TXT_NUMBER, TXT_PLACE, TXT_STATE, TXT_ZIP_CODE)
VALUES (CURDATE(), CURDATE(), 0, 'Linhares', '', 'Jardim Laguna', '23', 'Rua Projetada', 'ES', '29.900-000');
INSERT INTO TBL_CLUB_ADDRESSES (CLAD_SKU, CLUB_SKU)
VALUES ((SELECT ADDR.ADDR_SKU
         FROM TBL_ADDRESSES ADDR
         WHERE ADDR.TXT_ZIP_CODE = '29.900-000'), (SELECT CLUB.CLUB_SKU
                                                   FROM TBL_CLUBS CLUB
                                                   WHERE CLUB.CD_CLUB = 'CTML'));

-- #################################################### CTTF ######################################################## --
INSERT INTO TBL_CLUBS (DT_CREATION, DT_UPDATED, NR_VERSION, NUM_SITU_ADMINISTRATOR, TXT_LINK_IMAGE, TXT_MAP_LINK, NM_NAME, NM_PRESIDENT, TXT_REFERENCE, USER_SKU, CD_CLUB, VL_ORDER)
VALUES (CURDATE(), CURDATE(), 0, NULL, 'cttf_logo.png', NULL, 'Clube de Tiro de Teixeira de Freitas', '', '', NULL, 'CTTF', 3);
INSERT INTO TBL_CONTACTS (DT_CREATION, DT_UPDATED, NR_VERSION, DS_CONTACT, TP_CONTACT)
VALUES (CURDATE(), CURDATE(), 0, 'falecom@cttf.com.br', 3);
INSERT INTO TBL_CONTACTS (DT_CREATION, DT_UPDATED, NR_VERSION, DS_CONTACT, TP_CONTACT)
VALUES (CURDATE(), CURDATE(), 0, '22 998324125', 0);
INSERT INTO TBL_CLUB_CONTACTS (CLCT_SKU, CLUB_SKU)
VALUES ((SELECT CONT.CONT_SKU
         FROM TBL_CONTACTS CONT
         WHERE CONT.DS_CONTACT = 'falecom@cttf.com.br'), (SELECT CLUB.CLUB_SKU
                                                    FROM TBL_CLUBS CLUB
                                                    WHERE CLUB.CD_CLUB = 'CTTF'));
INSERT INTO TBL_CLUB_CONTACTS (CLCT_SKU, CLUB_SKU)
VALUES ((SELECT CONT.CONT_SKU
         FROM TBL_CONTACTS CONT
         WHERE CONT.DS_CONTACT = '22 998324125'), (SELECT CLUB.CLUB_SKU
                                                   FROM TBL_CLUBS CLUB
                                                   WHERE CLUB.CD_CLUB = 'CTTF'));
INSERT INTO TBL_ADDRESSES (DT_CREATION, DT_UPDATED, NR_VERSION, TXT_CITY, TXT_COMPLEMENT, TXT_DISTRICT, TXT_NUMBER, TXT_PLACE, TXT_STATE, TXT_ZIP_CODE)
VALUES (CURDATE(), CURDATE(), 0, 'Teixeira de Freitas', '', 'Bela Vista', '285', 'Rua Águas Claras', 'BA', '45.990-280');
INSERT INTO TBL_CLUB_ADDRESSES (CLAD_SKU, CLUB_SKU)
VALUES ((SELECT ADDR.ADDR_SKU
         FROM TBL_ADDRESSES ADDR
         WHERE ADDR.TXT_ZIP_CODE = '45.990-280'), (SELECT CLUB.CLUB_SKU
                                                   FROM TBL_CLUBS CLUB
                                                   WHERE CLUB.CD_CLUB = 'CTTF'));
                                                   
                                                   
-- #################################################### CCTE ######################################################## --
INSERT INTO TBL_CLUBS (DT_CREATION, DT_UPDATED, NR_VERSION, NUM_SITU_ADMINISTRATOR, TXT_LINK_IMAGE, TXT_MAP_LINK, NM_NAME, NM_PRESIDENT, TXT_REFERENCE, USER_SKU, CD_CLUB, VL_ORDER)
VALUES (CURDATE(), CURDATE(), 0, NULL, 'ccte_logo.png', NULL, 'Clube Campista de Tiro Esportivo', '', '', NULL, 'CCTE', 9);
INSERT INTO TBL_CONTACTS (DT_CREATION, DT_UPDATED, NR_VERSION, DS_CONTACT, TP_CONTACT)
VALUES (CURDATE(), CURDATE(), 0, '22 27383882', 0);
INSERT INTO TBL_CLUB_CONTACTS (CLCT_SKU, CLUB_SKU)
VALUES ((SELECT CONT.CONT_SKU
         FROM TBL_CONTACTS CONT
         WHERE CONT.DS_CONTACT = '22 27383882'), (SELECT CLUB.CLUB_SKU
                                                   FROM TBL_CLUBS CLUB
                                                   WHERE CLUB.CD_CLUB = 'CCTE'));
INSERT INTO TBL_ADDRESSES (DT_CREATION, DT_UPDATED, NR_VERSION, TXT_CITY, TXT_COMPLEMENT, TXT_DISTRICT, TXT_NUMBER, TXT_PLACE, TXT_STATE, TXT_ZIP_CODE)
VALUES (CURDATE(), CURDATE(), 0, 'Campos dos Goytacazes', '', 'Parque Guarus', '840', 'Av. Beira Lago', 'RJ', '28.070-530');
INSERT INTO TBL_CLUB_ADDRESSES (CLAD_SKU, CLUB_SKU)
VALUES ((SELECT ADDR.ADDR_SKU
         FROM TBL_ADDRESSES ADDR
         WHERE ADDR.TXT_ZIP_CODE = '28.070-530'), (SELECT CLUB.CLUB_SKU
                                                   FROM TBL_CLUBS CLUB
                                                   WHERE CLUB.CD_CLUB = 'CCTE'));
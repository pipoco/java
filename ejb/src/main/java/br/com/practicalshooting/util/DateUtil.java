package br.com.practicalshooting.util;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;

import org.apache.log4j.Logger;

public class DateUtil {
    private static final int SEGUNDO = 1000;
    private static final int MINUTO = 60;
    private static final int HORA = 60;
    private static final int HORAS_POR_DIA = 24;
    private static final int ULTIMAHORA = 23;
    private static final int ULTIMOMINUTO = 59;
    private static final int ULTIMOSEGUNDO = 59;
    private static final int PRIMEIRAHORA = 0;
    private static final int PRIMEIROMINUTO = 0;
    private static final int PRIMEIROSEGUNDO = 0;
    private static final int PRIMEIROMILISEGUNDO = 0;
    private static final String TIME_ZONE_ID = "America/Sao_Paulo";
    private static final Logger LOGGER = Logger.getLogger(LoggerUtil.LOGGER_INFO);

    private static Map<String, SimpleDateFormat> formatters = new HashMap<String, SimpleDateFormat>();

    private DateUtil() {
        // Para o sonar
    }

    public static final int getDifference(final Date dataInicio, final Date dataFim) {
        Double tempo = new Double((dataFim.getTime() - dataInicio.getTime()) / SEGUNDO);
        tempo = tempo / (HORA * MINUTO) / HORAS_POR_DIA;
        return tempo.intValue();
    }

    public static final String doubleToHM(Double valorHora) {
        if (valorHora == null) {
            return "00:00";
        }

        DecimalFormat decimalFormat = new DecimalFormat("#0.00");
        String[] horaMinuto = decimalFormat.format(valorHora).split(",");
        Integer horaI = new Integer(horaMinuto[0]);
        Double minutoD = new Double(new Integer(horaMinuto[1]) * 0.6);
        Integer minutoI = new Integer(minutoD.toString().split("\\.")[0]);
        String hora = horaI < 10 ? "0" + horaMinuto[0] : horaMinuto[0];
        String minuto = minutoI < 10 ? "0" + minutoI.toString() : minutoI.toString();
        return hora + ":" + minuto;
    }

    public static final Date addDays(final Date data, final int numeroDias) {
        Calendar c = Calendar.getInstance();
        c.setTime(data);
        c.add(Calendar.DAY_OF_YEAR, numeroDias);
        return c.getTime();
    }

    public static final Date addDaysJumpingWeekends(final Date data, final int numeroDias) {
        Date temp = data;
        if (numeroDias <= 0) {
            return data;
        }
        int adicionados = 0;
        do {
            Integer diaSemana = DateUtil.dayOfWeek(temp);
            switch (diaSemana) {
            case Calendar.SATURDAY:
                temp = DateUtil.addDays(temp, 1);
                break;
            case Calendar.SUNDAY:
                temp = DateUtil.addDays(temp, 1);
                break;
            default:
                temp = DateUtil.addDays(temp, 1);
                adicionados++;
                if (DateUtil.dayOfWeek(temp).equals(Calendar.SATURDAY)) {
                    temp = DateUtil.addDays(temp, 2);
                }
                break;
            }
        } while (adicionados != numeroDias);
        return temp;
    }

    public static final Date addMonth(final Date data, final int numeroMeses) {
        Calendar c = Calendar.getInstance();
        c.setTime(data);
        c.add(Calendar.MONTH, numeroMeses);
        return c.getTime();

    }

    public static final Integer dayOfWeek(final Date data) {
        GregorianCalendar calendar = new GregorianCalendar();
        calendar.setTime(data);
        calendar.setFirstDayOfWeek(Calendar.SUNDAY);
        return calendar.get(Calendar.DAY_OF_WEEK);
    }

    public static final Integer dayOfMonth(final Date data) {
        GregorianCalendar calendar = new GregorianCalendar();
        calendar.setTime(data);
        return calendar.get(Calendar.DAY_OF_MONTH);
    }

    public static final Integer monthOfYear(final Date data) {
        GregorianCalendar calendar = new GregorianCalendar();
        calendar.setTime(data);
        return calendar.get(Calendar.MONTH) + 1;
    }

    public static final Integer getYear(final Date data) {
        GregorianCalendar calendar = new GregorianCalendar();
        calendar.setTime(data);
        return calendar.get(Calendar.YEAR);
    }

    public static boolean isBusinessDay(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        if (cal.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY || cal.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) {
            return false;
        }

        return true;
    }

    public static Date getLastBusinessDayOfDate(Date date) {
        boolean isBusiness = isBusinessDay(date);
        if (isBusiness) {
            return date;
        }

        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        while (!isBusiness) {
            cal.add(Calendar.DAY_OF_MONTH, -1);
            isBusiness = isBusinessDay(cal.getTime());
        }
        return cal.getTime();
    }

    public static Date getLastBusinessDayOfMonth(Date date) {
        Date ultimaData = getLastDateOfMonth(date);
        return getLastBusinessDayOfDate(ultimaData);
    }

    public static Date getLastDateOfMonth(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        int lastDate = cal.getActualMaximum(Calendar.DATE);
        cal.set(Calendar.DATE, lastDate);
        return cal.getTime();
    }

    public static Date getFirstDateOfMonth(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        int firstDate = cal.getActualMinimum(Calendar.DATE);
        cal.set(Calendar.DATE, firstDate);
        return cal.getTime();
    }

    public static Date create(int year, int month, int day, int hour, int minute) {
        Calendar c = Calendar.getInstance();
        c.set(year, month, day, hour, minute);
        return c.getTime();
    }

    public static final Date dateTimeFull(final Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.set(Calendar.HOUR_OF_DAY, ULTIMAHORA);
        cal.set(Calendar.MINUTE, ULTIMOMINUTO);
        cal.set(Calendar.SECOND, ULTIMOSEGUNDO);
        return cal.getTime();
    }

    public static String parse(Date data, String pattern) {
        return getDateFormatter(pattern).format(data);
    }

    public static Date parse(final String dataString, final String pattern) throws ParseException {
        return getDateFormatter(pattern).parse(dataString);
    }

    public static SimpleDateFormat getDateFormatter(String pattern) {
        SimpleDateFormat formatter = formatters.get(pattern);
        if (formatter == null) {
            formatter = new SimpleDateFormat(pattern);
            formatters.put(pattern, formatter);
        }
        return formatter;
    }

    public static final String parseTrunc(final Date data) {
        return parse(data, "dd/MM/yyyy");
    }

    public static final Date trunc(final Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.set(Calendar.HOUR_OF_DAY, PRIMEIRAHORA);
        cal.set(Calendar.MINUTE, PRIMEIROMINUTO);
        cal.set(Calendar.SECOND, PRIMEIROSEGUNDO);
        cal.set(Calendar.MILLISECOND, PRIMEIROMILISEGUNDO);
        return cal.getTime();
    }

    public static final Date correctDSTCalendar(Date date) {
        try {
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);
            Date atual = new Date();
            TimeZone zone = TimeZone.getTimeZone(TIME_ZONE_ID);
            if (zone.inDaylightTime(atual) && zone.inDaylightTime(date)) {
                calendar.add(Calendar.MILLISECOND, zone.getDSTSavings());
                return calendar.getTime();
            }
            if (!zone.inDaylightTime(atual) && zone.inDaylightTime(date)) {
                return calendar.getTime();
            }
            if (zone.inDaylightTime(atual) && !zone.inDaylightTime(date)) {
                calendar.add(Calendar.MILLISECOND, zone.getDSTSavings());
                return calendar.getTime();
            }
            return calendar.getTime();
        } catch (Exception e) {
            LOGGER.error("", e);
        }
        return date;
    }

    public static final Date correctDST(Date date) {
        try {
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);
            Date atual = new Date();
            TimeZone zone = TimeZone.getTimeZone(TIME_ZONE_ID);
            if (zone.inDaylightTime(atual) && zone.inDaylightTime(date)) {
                calendar.add(Calendar.MILLISECOND, zone.getDSTSavings());
                return calendar.getTime();
            }
            if (!zone.inDaylightTime(atual) && zone.inDaylightTime(date)) {
                return calendar.getTime();
            }
            if (zone.inDaylightTime(atual) && !zone.inDaylightTime(date)) {
                calendar.add(Calendar.MILLISECOND, zone.getDSTSavings());
                return calendar.getTime();
            }
            return calendar.getTime();
        } catch (Exception e) {
            LOGGER.error("", e);
        }
        return date;
    }

    public static final Date subtractDST(Date date) {
        try {
            TimeZone zone = TimeZone.getTimeZone(TIME_ZONE_ID);
            Date atual = new Date();
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);
            if (zone.inDaylightTime(atual) && zone.inDaylightTime(date)) {
                calendar.add(Calendar.MILLISECOND, calendar.getTimeZone().getDSTSavings() * -1);
                return calendar.getTime();
            }
            if (!zone.inDaylightTime(atual) && zone.inDaylightTime(date)) {
                return calendar.getTime();
            }
            if (zone.inDaylightTime(atual) && !zone.inDaylightTime(date)) {
                calendar.add(Calendar.MILLISECOND, calendar.getTimeZone().getDSTSavings() * -1);
                return calendar.getTime();
            }
            return calendar.getTime();
        } catch (Exception e) {
            LOGGER.error("", e);
        }
        return date;
    }

    public static final Date subtractDSTCalendar(Date date) {
        try {
            TimeZone zone = TimeZone.getTimeZone(TIME_ZONE_ID);
            Date atual = new Date();
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);
            if (zone.inDaylightTime(atual) && zone.inDaylightTime(date)) {
                calendar.add(Calendar.MILLISECOND, calendar.getTimeZone().getDSTSavings() * -1);
                return calendar.getTime();
            }
            if (!zone.inDaylightTime(atual) && zone.inDaylightTime(date)) {
                return calendar.getTime();
            }
            if (zone.inDaylightTime(atual) && !zone.inDaylightTime(date)) {
                calendar.add(Calendar.MILLISECOND, calendar.getTimeZone().getDSTSavings() * -1);
                return calendar.getTime();
            }
            return calendar.getTime();
        } catch (Exception e) {
            LOGGER.error("", e);
        }
        return date;
    }

    public static Integer getWorkingDaysBetweenTwoDates(final Date startDate, final Date endDate) {
        if (startDate.compareTo(endDate) > 0) {
            return 0;
        }
        Date inicio = startDate;
        Date fim = endDate;
        int workDays = 0;
        while (inicio.compareTo(fim) <= 0) {
            if (isBusinessDay(inicio)) {
                ++workDays;
            }
            inicio = addDays(inicio, 1);
        }

        return workDays;
    }
}

package br.com.practicalshooting.util;

public class UserAgentUtil {

    private final String browserName;
    private final Double browserVersion;

    public UserAgentUtil(String user, String userAgent) {
        String browsername = "";
        String browserversion = "";

        if (user.contains("msie")) {
            String substring = userAgent.substring(userAgent.indexOf("MSIE")).split(";")[0];
            browsername = substring.split(" ")[0].replace("MSIE", "IE");
            browserversion = substring.split(" ")[1];
        } else if (user.contains("safari") && user.contains("version")) {
            browsername = userAgent.substring(userAgent.indexOf("Safari")).split(" ")[0].split("/")[0];
            browserversion = userAgent.substring(userAgent.indexOf("Version")).split(" ")[0].split("/")[1];
        } else if (user.contains("opr") || user.contains("opera")) {
            if (user.contains("opera")) {
                browsername = userAgent.substring(userAgent.indexOf("Opera")).split(" ")[0].split("/")[0];
                browserversion = userAgent.substring(userAgent.indexOf("Version")).split(" ")[0].split("/")[1];
            } else if (user.contains("opr")) {
                browsername = userAgent.substring(userAgent.indexOf("OPR")).split(" ")[0].replace("/", "-").replace("OPR", "Opera");
            }
        } else if (user.contains("chrome")) {
            browsername = userAgent.substring(userAgent.indexOf("Chrome")).split(" ")[0].split("/")[0];
            browserversion = userAgent.substring(userAgent.indexOf("Chrome")).split(" ")[0].split("/")[1];
        } else if (user.indexOf("mozilla/7.0") > -1 || user.indexOf("netscape6") != -1 || user.indexOf("mozilla/4.7") != -1 || user.indexOf("mozilla/4.78") != -1 || user.indexOf("mozilla/4.08") != -1 || user.indexOf("mozilla/3") != -1) {
            browsername = "Netscape-?";
        } else if (user.contains("firefox")) {
            browsername = userAgent.substring(userAgent.indexOf("Firefox")).split(" ")[0].split("/")[0];
            browserversion = userAgent.substring(userAgent.indexOf("Firefox")).split(" ")[0].split("/")[1];
        } else {
            browsername = "UnKnown, More-Info: " + userAgent;
        }

        // Tratamento da Versao
        String[] version = browserversion.split("[/.]");
        if (version.length == 0) {
            // Manter browserVersion = browserVersion
        } else if (version.length >= 2) {
            browserversion = version[0] + "." + version[1];
        } else {
            browserversion = version[0];
        }

        this.browserName = browsername;
        this.browserVersion = Double.parseDouble(browserversion);
    }

    public String getBrowserName() {
        return this.browserName;
    }

    public Double getBrowserVersion() {
        return this.browserVersion;
    }
}

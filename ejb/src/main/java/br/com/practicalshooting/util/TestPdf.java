package br.com.practicalshooting.util;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Font.FontFamily;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.GrayColor;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

public class TestPdf {

    /** Path to the resulting PDF file. */
    public static final String RESULT = "/Users/julioeffgen/Desktop/hello_memory.pdf";
    public static final int TOTAL_COLUMNS = 54;

    /**
     * Creates a PDF file: hello_memory.pdf
     *
     * @param args
     *            no arguments needed
     */
    public static void main(String[] args) throws DocumentException, IOException {
        File file = new File(RESULT);
        file.getParentFile().mkdirs();
        new TestPdf().createPdf(RESULT);
    }

    public void createPdf(String dest) throws IOException, DocumentException {
        Document document = new Document(PageSize.A4);
        PdfWriter.getInstance(document, new FileOutputStream(dest));
        document.open();
        PdfPTable table = new PdfPTable(TOTAL_COLUMNS);
        table.setWidthPercentage(85);
        table.getDefaultCell().setUseAscender(true);
        table.getDefaultCell().setUseDescender(true);
        this.createBlankCell(table);
        this.createBlankCell(table);
        this.createBlankCell(table);
        this.createBlankCell(table);
        this.createBlankCell(table);
        this.createBlankCell(table);
        this.createBlankCell(table);
        this.createBlankCell(table);
        this.createContentCellWithoutBorder(table, ResourceMessages.getMessage("cert.course.title"), Element.ALIGN_CENTER, Font.NORMAL);
        this.createContentCellWithoutBorder(table, ResourceMessages.getMessage("cert.course.subtitle"), Element.ALIGN_CENTER, Font.BOLD);
        this.createContentCellWithoutBorder(table, ResourceMessages.getMessage("cert.course.candidate.data", "JULIO EFFGEN MOISES", "1.374.103 SSP-ES", "100.071.597-30", "RUA DR. AZAMBUJA Nº 83, CENTRO - VITÓRIA/ES - CEP: 29.015-070"), Element.ALIGN_LEFT, Font.NORMAL);
        this.createBlankCell(table);
        this.createContentCellWithoutBorder(table, ResourceMessages.getMessage("cert.course.tag.gun.especification"), Element.ALIGN_LEFT, Font.NORMAL);
        this.createContentCellWithoutBorder(table, ResourceMessages.getMessage("cert.course.tag.gun.type", "REVÓLVER"), Element.ALIGN_LEFT, Font.NORMAL);
        this.createContentCellWithoutBorder(table, ResourceMessages.getMessage("cert.course.tag.gun.caliber", ".38"), Element.ALIGN_LEFT, Font.NORMAL);
        this.createContentCellWithoutBorder(table, ResourceMessages.getMessage("cert.course.tag.result", "X", " "), Element.ALIGN_LEFT, Font.NORMAL);
        this.createBlankCell(table);
        this.createBlankCell(table);
        this.createContentCellWithoutBorder(table, ResourceMessages.getMessage("cert.course.tag.substantiation"), Element.ALIGN_LEFT, Font.NORMAL);
        this.createContentCellWithoutBorder(table, ResourceMessages.getMessage("cert.course.theorical.test.note", "100"), Element.ALIGN_LEFT, Font.NORMAL);
        this.createContentCellWithoutBorder(table, ResourceMessages.getMessage("cert.course.practical.test.note", "90"), Element.ALIGN_LEFT, Font.NORMAL);
        this.createBlankCell(table);
        this.createBlankCell(table);
        this.createBlankCell(table);
        this.createBlankCell(table);
        this.createContentCellWithoutBorder(table, ResourceMessages.getMessage("cert.course.local.date", "30/03/2017"), Element.ALIGN_LEFT, Font.NORMAL);
        this.createBlankCell(table);
        this.createBlankCell(table);
        this.createBlankCell(table);
        this.createBlankCell(table);
        this.createContentCellWithoutBorder(table, "________________________________", Element.ALIGN_CENTER, Font.BOLD);
        this.createContentCellWithoutBorder(table, "CLAUDIO VIDEIRA LEANDRO", Element.ALIGN_CENTER, Font.BOLD);
        this.createContentCellWithoutBorder(table, ResourceMessages.getMessage("cert.course.practical.test.place", "CLUBE DE TIRO VILA VELHA"), Element.ALIGN_CENTER, Font.NORMAL);
        this.createContentCellWithoutBorder(table, ResourceMessages.getMessage("cert.course.practical.test.place", "CLUBE DE TIRO VILA VELHA"), Element.ALIGN_CENTER, Font.NORMAL);
        document.add(table);
        document.close();
    }

    private void createContentCellWithoutBorder(PdfPTable table, String content, int align, int fontType) {
        Font font = new Font(FontFamily.TIMES_ROMAN, 12, fontType, GrayColor.GRAYBLACK);
        PdfPCell infoCell = new PdfPCell(new Phrase(content, font));
        infoCell.setHorizontalAlignment(align);
        infoCell.setColspan(TOTAL_COLUMNS);
        infoCell.setPaddingBottom(5);
        infoCell.setPaddingRight(5);
        infoCell.setPaddingLeft(5);
        infoCell.setBorder(0);
        table.addCell(infoCell);
    }

    private void createBlankCell(PdfPTable table) {
        PdfPCell infoCell = new PdfPCell(new Phrase("   "));
        infoCell.setColspan(TOTAL_COLUMNS);
        infoCell.setBorderWidthRight(0);
        infoCell.setBorderWidthLeft(0);
        infoCell.setBorder(0);
        table.addCell(infoCell);
    }
}

package br.com.practicalshooting.util;

import java.io.Serializable;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;

public class MailMessage implements Serializable {

    private static final long serialVersionUID = -1956299969434922675L;
    private String remetente;
    private InternetAddress[] para;
    private String assunto;
    private String mensagem;

    public MailMessage assunto(String assunto) {
        this.assunto = assunto;
        return this;
    }

    public MailMessage mensagem(String mensagem) {
        this.mensagem = mensagem;
        return this;
    }

    public MailMessage para(Object... para) throws AddressException {
        this.para = new InternetAddress[para.length];
        for (int i = 0; i < para.length; i++) {
            this.para[i] = new InternetAddress(para[i].toString());
        }
        return this;
    }

    public MailMessage remetente(String remetente) {
        this.remetente = remetente;
        return this;
    }

    public void setAssunto(String assunto) {
        this.assunto = assunto;
    }

    public void setMensagem(String mensagem) {
        this.mensagem = mensagem;
    }

    public void setPara(InternetAddress[] para) {
        this.para = para;
    }

    public void setRemetente(String remetente) {
        this.remetente = remetente;
    }

    public String getAssunto() {
        return this.assunto;
    }

    public String getMensagem() {
        return this.mensagem;
    }

    public InternetAddress[] getPara() {
        return this.para;
    }

    public String getRemetente() {
        return this.remetente;
    }
}

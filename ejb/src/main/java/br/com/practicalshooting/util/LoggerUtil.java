package br.com.practicalshooting.util;

public class LoggerUtil {

    public static final String LOGGER_DEBUG = "loggerDebug";

    public static final String LOGGER_INFO = "loggerInfo";

    private LoggerUtil() {
        // para sonar
    }

}

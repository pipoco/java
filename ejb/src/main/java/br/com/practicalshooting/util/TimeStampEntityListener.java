package br.com.practicalshooting.util;

import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;

import org.joda.time.LocalDate;

import br.com.practicalshooting.model.EntitySku;

public class TimeStampEntityListener {

    @PrePersist
    @PreUpdate
    public void setDate(final EntitySku entity) {
        final LocalDate now = LocalDate.now();
        if (entity.getCreationDate() == null) {
            entity.setCreationDate(now);
        }
        entity.setUpdatedDate(now);
    }
}

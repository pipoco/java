package br.com.practicalshooting.util.exception;

public class EventsException extends RuntimeException {
    /**
     * 
     */
    private static final long serialVersionUID = 3001218325299894315L;

    private final String message;

    public EventsException(String message) {
        super(message);
        this.message = message;
    }

    public EventsException(String message, Throwable cause) {
        super(message, cause);
        this.message = message;
    }

    public EventsException(Throwable cause) {
        super(cause);
        this.message = "error.undefined";
    }

    @Override
    public String getMessage() {
        return this.message;
    }
}
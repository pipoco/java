package br.com.practicalshooting.util.exception;

import java.util.ArrayList;
import java.util.List;

public class MultipleEventsException extends RuntimeException {
    /**
     * 
     */
    private static final long serialVersionUID = -9117236423169818888L;

    private List<EventsException> exceptions;

    public MultipleEventsException() {
        super();
        this.exceptions = new ArrayList<>();
    }

    public MultipleEventsException(EventsException ex) {
        super();
        this.exceptions = new ArrayList<>();
        this.exceptions.add(ex);
    }

    public void add(EventsException e) {
        this.exceptions.add(e);
    }

    public void add(String message) {
        this.exceptions.add(new EventsException(message));
    }

    public void add(String message, Throwable origin) {
        this.exceptions.add(new EventsException(message, origin));
    }

    public List<EventsException> getExceptions() {
        return this.exceptions;
    }

    public void setExceptions(List<EventsException> exceptions) {
        this.exceptions = exceptions;
    }

    public boolean hasErros() {
        return !this.exceptions.isEmpty();
    }

    public void existsErros() {
        if (this.hasErros()) {
            throw this;
        }
    }
}
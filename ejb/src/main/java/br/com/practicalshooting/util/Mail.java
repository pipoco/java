package br.com.practicalshooting.util;

import java.io.Serializable;
import java.util.Date;

import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.mail.Address;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.MimeMessage;

@Stateless
public class Mail implements Serializable {

    private static final long serialVersionUID = 1047363947272201233L;

    @Resource(mappedName = "mail/events")
    private Session mailSession;

    private final String CHARSET = "UTF-8";

    public void send(MailMessage mailMessage) throws MessagingException {
        MimeMessage message = new MimeMessage(this.mailSession);
        Address[] to = mailMessage.getPara();
        Address[] reply = { to[0] };

        message.setRecipients(Message.RecipientType.TO, to);
        message.setReplyTo(reply);
        message.setSubject(mailMessage.getAssunto(), this.CHARSET);
        message.setSentDate(new Date());
        message.setText(mailMessage.getMensagem(), this.CHARSET, "html");
        Transport.send(message);
    }
}

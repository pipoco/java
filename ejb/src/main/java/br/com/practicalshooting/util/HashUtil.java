package br.com.practicalshooting.util;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

import org.apache.log4j.Logger;

public class HashUtil {

    private static final Logger LOGGER = Logger.getLogger(LoggerUtil.LOGGER_INFO);

    private static final String SHA1 = "SHA1";
    private static final String MD5 = "MD5";

    private HashUtil() {
        // Para o sonar
    }

    public static String SHA1(String text) {
        return hash(text, SHA1);
    }

    public static String MD5(String text) {
        return hash(text, MD5);
    }

    public static String hash(String frase, String algoritmo) {
        try {
            MessageDigest md = MessageDigest.getInstance(algoritmo);
            md.update(frase.getBytes());
            return stringHexa(md.digest());
        } catch (NoSuchAlgorithmException e) {
            LOGGER.error("", e);
        }
        return null;
    }

    private static String stringHexa(byte[] bytes) {
        StringBuilder s = new StringBuilder();
        for (int i = 0; i < bytes.length; i++) {
            int parteAlta = (bytes[i] >> 4 & 0xf) << 4;
            int parteBaixa = bytes[i] & 0xf;
            if (parteAlta == 0) {
                s.append('0');
            }
            s.append(Integer.toHexString(parteAlta | parteBaixa));
        }
        return s.toString();
    }

    public static String createPwd(int length) {
        SecureRandom random = new SecureRandom();
        char[] chars = new char[length];
        for (int i = 0; i < chars.length; i++) {
            int v = random.nextInt(10 + 26 + 26);
            char c;
            if (v < 10) {
                c = (char) ('0' + v);
            } else if (v < 36) {
                c = (char) ('a' - 10 + v);
            } else {
                c = (char) ('A' - 36 + v);
            }
            chars[i] = c;
        }
        return new String(chars);
    }
}

package br.com.practicalshooting.util;

import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;

import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

public class EmailTemplateRead {

    private static final Logger LOGGER = Logger.getLogger(LoggerUtil.LOGGER_INFO);

    private EmailTemplateRead() {
        // Para o sonar
    }

    public static String readEmailTemplate(String userName, String contentMessage, String facebookLink, String instagramLink) {
        try {
            URL location = EmailTemplateRead.class.getProtectionDomain().getCodeSource().getLocation();
            String content = new String(Files.readAllBytes(Paths.get(location.getPath() + "/emailTemplate.txt")));
            content = content.replaceAll("##user_name##", userName);
            content = content.replaceAll("##message_content_body##", contentMessage);
            content = content.replaceAll("##facebook_link##", facebookLink);
            content = content.replaceAll("##instagram_link##", instagramLink);
            content = content.replaceAll("##site_link##", getAbsoluteApplicationUrl());
            return content;
        } catch (Exception e) {
            LOGGER.error("ERROR", e);
        }
        return null;
    }

    public static String getAbsoluteApplicationUrl() throws URISyntaxException {
        ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
        HttpServletRequest request = (HttpServletRequest) externalContext.getRequest();
        URI uri = new URI(request.getRequestURL().toString());
        URI newUri = new URI(uri.getScheme(), null, uri.getHost(), uri.getPort(), request.getContextPath().toString(), null, null);
        return newUri.toString();
    }
}
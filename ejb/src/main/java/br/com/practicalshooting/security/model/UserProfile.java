package br.com.practicalshooting.security.model;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import br.com.practicalshooting.model.EntitySku;
import br.com.practicalshooting.model.User;

@Entity
@Table(name = "`CZ_USER_X_PROFILE`", uniqueConstraints = @UniqueConstraint(name = "UK_CZ_USER_X_PROFILE_01", columnNames = { "`USER_SKU`", "`PROF_SKU`" }))
@AttributeOverrides(value = { @AttributeOverride(name = "id", column = @Column(name = "`USPR_SKU`")) })
@Inheritance(strategy = InheritanceType.JOINED)
public class UserProfile extends EntitySku {

    /**
     *
     */
    private static final long serialVersionUID = -6538704439076264522L;

    @ManyToOne(fetch = FetchType.EAGER, optional = false)
    @JoinColumn(name = "`PROF_SKU`", foreignKey = @ForeignKey(name = "FK_USPR_X_PROF"))
    private Profile profile;

    @ManyToOne(fetch = FetchType.EAGER, optional = false)
    @JoinColumn(name = "`USER_SKU`", foreignKey = @ForeignKey(name = "FK_USPR_X_USER"))
    private User user;

    public Profile getProfile() {
        return this.profile;
    }

    public void setProfile(Profile profile) {
        this.profile = profile;
    }

    public User getUser() {
        return this.user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}

package br.com.practicalshooting.security.model;

import java.util.List;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import br.com.practicalshooting.model.EntitySku;

@Entity
@Table(name = "`TBL_PROFILES`", uniqueConstraints = @UniqueConstraint(name = "UK_TBL_PROFILE_01", columnNames = { "`NM_PROFILE`" }))
@AttributeOverrides(value = { @AttributeOverride(name = "id", column = @Column(name = "`PROF_SKU`")) })
public class Profile extends EntitySku {

    /**
     *
     */
    private static final long serialVersionUID = 1332572837058076547L;

    @Column(name = "`NM_PROFILE`", length = 128)
    private String name;

    @OneToMany(mappedBy = "profile", fetch = FetchType.LAZY)
    private List<Permission> permissions;

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Permission> getPermissions() {
        return this.permissions;
    }

    public void setPermissions(List<Permission> permissions) {
        this.permissions = permissions;
    }
}

package br.com.practicalshooting.security.model;

import java.util.List;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import br.com.practicalshooting.model.EntitySku;

@Entity
@Table(name = "`TBL_MODULES`")
@AttributeOverrides(value = { @AttributeOverride(name = "id", column = @Column(name = "`MODU_SKU`")) })
@Inheritance(strategy = InheritanceType.JOINED)
public class Module extends EntitySku {

    /**
     *
     */
    private static final long serialVersionUID = 154385020440665129L;

    @Column(name = "`NM_MODULE`", length = 128)
    private String name;

    @ManyToOne(fetch = FetchType.LAZY, optional = true)
    @JoinColumn(name = "`SUPER_MODULE_SKU`", foreignKey = @ForeignKey(name = "FK_SUPER_MODULE_X_MODU"))
    private Module superModule;

    @OneToMany(mappedBy = "module", fetch = FetchType.LAZY)
    private List<Function> functions;

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Module getSuperModule() {
        return this.superModule;
    }

    public void setSuperModule(Module superModule) {
        this.superModule = superModule;
    }

    public List<Function> getFunctions() {
        return this.functions;
    }

    public void setFunctions(List<Function> functions) {
        this.functions = functions;
    }
}
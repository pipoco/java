package br.com.practicalshooting.security.model;

import java.util.List;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import br.com.practicalshooting.model.EntitySku;

@Entity
@Table(name = "`TBL_ACTIONS`")
@AttributeOverrides(value = { @AttributeOverride(name = "id", column = @Column(name = "`ACTI_SKU`")) })
@Inheritance(strategy = InheritanceType.JOINED)
public class Action extends EntitySku {

    /**
     *
     */
    private static final long serialVersionUID = -6284967664277207762L;

    @Column(name = "`NM_ACTION`", length = 128)
    private String name;

    @Column(name = "`DS_COMMAND`", length = 128)
    private String command;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "`FUNC_SKU`", foreignKey = @ForeignKey(name = "FK_ACTI_X_FUNC"))
    private Function function;

    @OneToMany(mappedBy = "action", fetch = FetchType.LAZY)
    private List<Permission> permissions;

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCommand() {
        return this.command;
    }

    public void setCommand(String command) {
        this.command = command;
    }

    public Function getFunction() {
        return this.function;
    }

    public void setFunction(Function function) {
        this.function = function;
    }

    public List<Permission> getPermissions() {
        return this.permissions;
    }

    public void setPermissions(List<Permission> permissions) {
        this.permissions = permissions;
    }
}
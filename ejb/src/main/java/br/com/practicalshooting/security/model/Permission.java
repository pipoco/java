package br.com.practicalshooting.security.model;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import br.com.practicalshooting.model.EntitySku;

@Entity
@Table(name = "`TBL_PERMISSIONS`", uniqueConstraints = @UniqueConstraint(name = "UK_PERM_PROF_X_ACTI_01", columnNames = { "`PROF_SKU`", "`ACTI_SKU`" }))
@AttributeOverrides(value = { @AttributeOverride(name = "id", column = @Column(name = "`PERM_SKU`")) })
@Inheritance(strategy = InheritanceType.JOINED)
public class Permission extends EntitySku {

    /**
     *
     */
    private static final long serialVersionUID = 351638715655206930L;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "`PROF_SKU`", foreignKey = @ForeignKey(name = "FK_PERM_X_PROF"))
    private Profile profile;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "`ACTI_SKU`", foreignKey = @ForeignKey(name = "FK_PERM_X_ACTI"))
    private Action action;

    public Profile getProfile() {
        return this.profile;
    }

    public void setProfile(Profile profile) {
        this.profile = profile;
    }

    public Action getAction() {
        return this.action;
    }

    public void setAction(Action action) {
        this.action = action;
    }

}

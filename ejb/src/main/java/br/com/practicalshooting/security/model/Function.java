package br.com.practicalshooting.security.model;

import java.util.List;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import br.com.practicalshooting.model.EntitySku;

@Entity
@Table(name = "`TBL_FUNCTIONS`")
@AttributeOverrides(value = { @AttributeOverride(name = "id", column = @Column(name = "`FUNC_SKU`")) })
@Inheritance(strategy = InheritanceType.JOINED)
public class Function extends EntitySku {

    /**
     *
     */
    private static final long serialVersionUID = 9010135282755893223L;

    @Column(name = "`NM_FUNCTION`", length = 128)
    private String name;

    @Column(name = "`DS_PATH`", length = 128)
    private String path;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "`MODU_SKU`", foreignKey = @ForeignKey(name = "FK_FUNC_X_MODU"))
    private Module module;

    @OneToMany(mappedBy = "function", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<Action> actions;

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPath() {
        return this.path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public Module getModule() {
        return this.module;
    }

    public void setModule(Module module) {
        this.module = module;
    }

    public List<Action> getActions() {
        return this.actions;
    }

    public void setActions(List<Action> actions) {
        this.actions = actions;
    }
}

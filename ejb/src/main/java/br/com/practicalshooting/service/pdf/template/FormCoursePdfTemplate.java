package br.com.practicalshooting.service.pdf.template;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Font.FontFamily;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.GrayColor;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfGState;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

import br.com.practicalshooting.model.Candidate;
import br.com.practicalshooting.model.WeaponOfCourse;
import br.com.practicalshooting.util.ResourceMessages;

public class FormCoursePdfTemplate {

    public static final String RESULT = "/Users/julioeffgen/Desktop/hello_memory.pdf";
    public static final int TOTAL_COLUMNS = 54;
    private final Candidate candidate;

    public FormCoursePdfTemplate(Candidate candidate) {
        this.candidate = candidate;
    }

    public void createCoursePdf(ByteArrayOutputStream baos) throws DocumentException, IOException {
        Document document = new Document(PageSize.A4);
        PdfWriter writer = PdfWriter.getInstance(document, baos);
        document.open();
        this.addBackgroudImage(writer);
        PdfPTable table = new PdfPTable(TOTAL_COLUMNS);
        table.setWidthPercentage(100);
        table.getDefaultCell().setUseAscender(true);
        table.getDefaultCell().setUseDescender(true);
        this.createHeaderCell(table, ResourceMessages.getMessage("form.course.pdf.title"));
        this.createInfoCell(table, ResourceMessages.getMessage("form.course.pdf.info"), Element.ALIGN_JUSTIFIED);
        this.createPlaceCell(table, this.candidate.getInstructor().getInstitution(), ResourceMessages.getMessage("form.course.pdf.place"), this.candidate.getRealization().toDate());
        this.createBlankCell(table);
        this.createInfoCell(table, ResourceMessages.getMessage("form.course.pdf.lbl.candidate.data"), Element.ALIGN_LEFT);
        this.createSingleContentCell(table, 5, ResourceMessages.getMessage("form.course.pdf.lbl.tag.name"), this.candidate.getUser().getName());
        this.createDoubleContentCell(table, 3, ResourceMessages.getMessage("form.course.pdf.lbl.tag.father"), 23, this.candidate.getFatherName(), 4, ResourceMessages.getMessage("form.course.pdf.lbl.tag.mother"), this.candidate.getMotherName());
        this.createDoubleContentCell(table, 3, ResourceMessages.getMessage("form.course.pdf.lbl.tag.rg"), 23, this.candidate.getRg(), 4, ResourceMessages.getMessage("form.course.pdf.lbl.tag.cpf"), this.candidate.getCpf());
        this.createDoubleContentCell(table, 8, ResourceMessages.getMessage("form.course.pdf.lbl.tag.phone"), 18, this.candidate.popPhone(), 6, ResourceMessages.getMessage("form.course.pdf.lbl.tag.email"), this.candidate.popMail());
        this.createSingleContentCell(table, 8, ResourceMessages.getMessage("form.course.pdf.lbl.tag.address"), this.candidate.formattedAddress().replaceAll("<br/>", ". "));
        this.createContentCell(table, this.buildMessageDocs(this.candidate), Element.ALIGN_JUSTIFIED_ALL);
        this.createBlankCell(table);
        this.createInfoCell(table, ResourceMessages.getMessage("form.course.pdf.lbl.purpose"), Element.ALIGN_LEFT);
        this.createContentCell(table, this.buildMessagePurpose(this.candidate), Element.ALIGN_JUSTIFIED_ALL);
        this.createBlankCell(table);
        this.createInfoCell(table, ResourceMessages.getMessage("form.course.pdf.lbl.instructor.monitor"), Element.ALIGN_LEFT);
        this.createContentCell(table, this.candidate.getInstructor().getUser().getName(), Element.ALIGN_LEFT);
        this.createBlankCell(table);
        this.createInfoCell(table, ResourceMessages.getMessage("form.course.pdf.lbl.theorical.test"), Element.ALIGN_CENTER);
        this.createInfoCell(table, ResourceMessages.getMessage("form.course.pdf.lbl.punctuation"), Element.ALIGN_LEFT);
        this.createContentCell(table, ResourceMessages.getMessage("form.course.pdf.lbl.desc.theorical.note", this.candidate.getTheoreticalNote(), 20, this.candidate.getPunctuationTheoretical()), Element.ALIGN_LEFT);
        this.createInfoCell(table, ResourceMessages.getMessage("form.course.pdf.lbl.practical.test"), Element.ALIGN_CENTER);
        for (WeaponOfCourse weaponOfCourse : this.candidate.getWeapons()) {
            this.createInfoCell(table, ResourceMessages.getMessage("form.course.pdf.lbl.practical.gun", ResourceMessages.getMessage(weaponOfCourse.getWeaponType().name())), Element.ALIGN_LEFT);
            this.createContentCell(table, ResourceMessages.getMessage("form.course.pdf.lbl.desc.practical.note", weaponOfCourse.getCaliber(), weaponOfCourse.getPunctuationPractice(), 20, weaponOfCourse.getPracticeNote()), Element.ALIGN_LEFT);
        }
        this.createContentCellWithoutBorder(table, ResourceMessages.getMessage("form.course.pdf.lbl.receipt", this.formatDate(this.candidate.getReceiptReports().toDate()), this.candidate.getObservations()), Element.ALIGN_LEFT);
        this.createBlankCell(table);
        this.createBlankCell(table);
        this.createContentCellWithoutBorder(table, "____________________________________________", Element.ALIGN_CENTER);
        this.createContentCellWithoutBorder(table, ResourceMessages.getMessage("form.course.pdf.lbl.candidate.receipt"), Element.ALIGN_CENTER);
        document.add(table);
        document.close();
    }

    private void addBackgroudImage(PdfWriter writer) throws DocumentException, MalformedURLException, IOException {
        URL location = FormCoursePdfTemplate.class.getProtectionDomain().getCodeSource().getLocation();
        String path = Paths.get(location.getPath() + "/logo_ctvv.png").toString();
        PdfContentByte canvas = writer.getDirectContentUnder();
        Image image = Image.getInstance(path);
        image.scaleAbsolute(PageSize.A4);
        image.setAbsolutePosition(0, 90);
        image.scaleToFit(PageSize.A4.getWidth(), PageSize.A4.getHeight());
        canvas.saveState();
        PdfGState state = new PdfGState();
        state.setFillOpacity(0.06f);
        canvas.setGState(state);
        canvas.addImage(image);
        canvas.restoreState();
    }

    private String buildMessagePurpose(Candidate model) {
        String gunPermite = " ";
        String acquisition = " ";
        String registrationRenewal = " ";
        String freeCourse = " ";
        String practicarShooting = " ";
        if (model.getCourseType() == null) {
            return ResourceMessages.getMessage("form.course.pdf.lbl.course.purpose", gunPermite, acquisition, registrationRenewal, freeCourse, practicarShooting);
        }
        switch (model.getCourseType()) {
        case GUN_PERMIT:
            gunPermite = "X";
            break;
        case ACQUISITION:
            acquisition = "X";
            break;
        case REGISTRATION_RENEWAL:
            registrationRenewal = "X";
            break;
        case FREE_COURSE:
            freeCourse = "X";
            break;
        case PRACTICAL_SHOOTING:
            practicarShooting = "X";
            break;
        default:
            practicarShooting = "X";
            break;
        }
        return ResourceMessages.getMessage("form.course.pdf.lbl.course.purpose", gunPermite, acquisition, registrationRenewal, freeCourse, practicarShooting);
    }

    private String buildMessageDocs(Candidate model) {
        String psychological = (model.getPsychologicalReport() == null) || (model.getPsychologicalReport() == false) ? " " : "X";
        String criminal = (model.getCriminalRecord() == null) || (model.getCriminalRecord() == false) ? " " : "X";
        String ctvv = (model.getCtvvAssociated() == null) || (model.getCtvvAssociated() == false) ? " " : "X";
        String cardNumber = model.getCardNumber() == null ? "" : model.getCardNumber().toString();
        return ResourceMessages.getMessage("form.course.pdf.lbl.docs", psychological, criminal, ctvv, cardNumber);
    }

    private void createContentCell(PdfPTable table, String content, int align) {
        Font font = new Font(FontFamily.TIMES_ROMAN, 12, Font.NORMAL, GrayColor.GRAYBLACK);
        PdfPCell infoCell = new PdfPCell(new Phrase(content, font));
        infoCell.setHorizontalAlignment(align);
        infoCell.setColspan(TOTAL_COLUMNS);
        infoCell.setPaddingBottom(5);
        infoCell.setPaddingRight(5);
        infoCell.setPaddingLeft(5);
        table.addCell(infoCell);
    }

    private void createContentCellWithoutBorder(PdfPTable table, String content, int align) {
        Font font = new Font(FontFamily.TIMES_ROMAN, 12, Font.NORMAL, GrayColor.GRAYBLACK);
        PdfPCell infoCell = new PdfPCell(new Phrase(content, font));
        infoCell.setHorizontalAlignment(align);
        infoCell.setColspan(TOTAL_COLUMNS);
        infoCell.setPaddingBottom(5);
        infoCell.setPaddingRight(5);
        infoCell.setPaddingLeft(5);
        infoCell.setBorder(0);
        table.addCell(infoCell);
    }

    private void createDoubleContentCell(PdfPTable table, int colspan1, String tag1, int colspanContent1, String content1, int colspan2, String tag2, String content2) {
        int colspanContent2 = TOTAL_COLUMNS - colspan1 - colspanContent1 - colspan2;
        Font fontBold = new Font(FontFamily.TIMES_ROMAN, 12, Font.BOLD, GrayColor.GRAYBLACK);
        Font fontNormal = new Font(FontFamily.TIMES_ROMAN, 12, Font.NORMAL, GrayColor.GRAYBLACK);

        PdfPCell tag1Cell = new PdfPCell(new Phrase(tag1, fontBold));
        tag1Cell.setHorizontalAlignment(Element.ALIGN_LEFT);
        tag1Cell.setPaddingBottom(5);
        tag1Cell.setBorderWidthRight(0);
        tag1Cell.setColspan(colspan1);

        PdfPCell content1Cell = new PdfPCell(new Phrase(content1, fontNormal));
        content1Cell.setHorizontalAlignment(Element.ALIGN_LEFT);
        content1Cell.setPaddingBottom(5);
        content1Cell.setBorderWidthLeft(0);
        content1Cell.setBorderWidthRight(0);
        content1Cell.setColspan(colspanContent1);

        PdfPCell tag2Cell = new PdfPCell(new Phrase(tag2, fontBold));
        tag2Cell.setHorizontalAlignment(Element.ALIGN_LEFT);
        tag2Cell.setPaddingBottom(5);
        tag2Cell.setBorderWidthLeft(0);
        tag2Cell.setBorderWidthRight(0);
        tag2Cell.setColspan(colspan2);

        PdfPCell content2Cell = new PdfPCell(new Phrase(content2, fontNormal));
        content2Cell.setHorizontalAlignment(Element.ALIGN_LEFT);
        content2Cell.setPaddingBottom(5);
        content2Cell.setBorderWidthLeft(0);
        content2Cell.setColspan(colspanContent2);

        table.addCell(tag1Cell);
        table.addCell(content1Cell);
        table.addCell(tag2Cell);
        table.addCell(content2Cell);
    }

    private void createSingleContentCell(PdfPTable table, int colspan, String tag, String content) {
        Font fontBold = new Font(FontFamily.TIMES_ROMAN, 12, Font.BOLD, GrayColor.GRAYBLACK);
        Font fontNormal = new Font(FontFamily.TIMES_ROMAN, 12, Font.NORMAL, GrayColor.GRAYBLACK);
        PdfPCell tagCell = new PdfPCell(new Phrase(tag, fontBold));
        tagCell.setHorizontalAlignment(Element.ALIGN_LEFT);
        tagCell.setPaddingBottom(5);
        tagCell.setBorderWidthRight(0);
        tagCell.setColspan(colspan);

        PdfPCell contentCell = new PdfPCell(new Phrase(content, fontNormal));
        contentCell.setHorizontalAlignment(Element.ALIGN_LEFT);
        contentCell.setPaddingBottom(5);
        contentCell.setBorderWidthLeft(0);
        contentCell.setColspan(TOTAL_COLUMNS - colspan);

        table.addCell(tagCell);
        table.addCell(contentCell);
    }

    private void createBlankCell(PdfPTable table) {
        PdfPCell infoCell = new PdfPCell(new Phrase("   "));
        infoCell.setColspan(TOTAL_COLUMNS);
        infoCell.setBorderWidthRight(0);
        infoCell.setBorderWidthLeft(0);
        infoCell.setBorder(0);
        table.addCell(infoCell);
    }

    private void createInfoCell(PdfPTable table, String infoText, int align) {
        Font infoFont = new Font(FontFamily.TIMES_ROMAN, 12, Font.BOLD, GrayColor.GRAYBLACK);
        PdfPCell infoCell = new PdfPCell(new Phrase(infoText, infoFont));
        infoCell.setHorizontalAlignment(align);
        infoCell.setColspan(TOTAL_COLUMNS);
        infoCell.setPaddingBottom(5);
        infoCell.setBackgroundColor(BaseColor.LIGHT_GRAY);
        table.addCell(infoCell);
    }

    private void createPlaceCell(PdfPTable table, String placeText, String cityText, Date placeDate) {
        int colspanTagPlace = 6;
        int colspanTagDate = 5;
        int colspanDate = 7;
        int colspanTagCity = 2;
        int colspanCity = 12;
        int colspanPlace = TOTAL_COLUMNS - colspanTagPlace - colspanTagDate - colspanDate - colspanTagCity - colspanCity;
        Font fontBold = new Font(FontFamily.TIMES_ROMAN, 12, Font.BOLD, GrayColor.GRAYBLACK);
        Font fontNormal = new Font(FontFamily.TIMES_ROMAN, 12, Font.NORMAL, GrayColor.GRAYBLACK);
        PdfPCell placeCell = new PdfPCell(new Phrase(ResourceMessages.getMessage("form.course.pdf.lbl.tag.place"), fontBold));
        placeCell.setHorizontalAlignment(Element.ALIGN_LEFT);
        placeCell.setPaddingBottom(5);
        placeCell.setBorderWidthRight(0);
        placeCell.setColspan(colspanTagPlace);

        PdfPCell placeTextCell = new PdfPCell(new Phrase(placeText, fontNormal));
        placeTextCell.setHorizontalAlignment(Element.ALIGN_LEFT);
        placeTextCell.setPaddingBottom(5);
        placeTextCell.setBorderWidthRight(0);
        placeTextCell.setBorderWidthLeft(0);
        placeTextCell.setColspan(colspanPlace);

        PdfPCell cityCell = new PdfPCell(new Phrase(" - ", fontBold));
        cityCell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cityCell.setPaddingBottom(5);
        cityCell.setBorderWidthRight(0);
        cityCell.setBorderWidthLeft(0);
        cityCell.setColspan(colspanTagCity);

        PdfPCell cityTextCell = new PdfPCell(new Phrase(cityText, fontNormal));
        cityTextCell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cityTextCell.setPaddingBottom(5);
        cityTextCell.setBorderWidthRight(0);
        cityTextCell.setBorderWidthLeft(0);
        cityTextCell.setColspan(colspanCity);

        PdfPCell dateCell = new PdfPCell(new Phrase(ResourceMessages.getMessage("form.course.pdf.lbl.tag.date"), fontBold));
        dateCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
        dateCell.setPaddingBottom(5);
        dateCell.setBorderWidthRight(0);
        dateCell.setBorderWidthLeft(0);
        dateCell.setColspan(colspanTagDate);

        PdfPCell dateTextCell = new PdfPCell(new Phrase(this.formatDate(placeDate), fontNormal));
        dateTextCell.setHorizontalAlignment(Element.ALIGN_LEFT);
        dateTextCell.setPaddingBottom(5);
        dateTextCell.setBorderWidthLeft(0);
        dateTextCell.setColspan(colspanDate);

        table.addCell(placeCell);
        table.addCell(placeTextCell);
        table.addCell(cityCell);
        table.addCell(cityTextCell);
        table.addCell(dateCell);
        table.addCell(dateTextCell);
    }

    private String formatDate(Date date) {
        return new SimpleDateFormat("dd/MM/yyyy").format(date);
    }

    private void createHeaderCell(PdfPTable table, String headerText) {
        Font headerFont = new Font(FontFamily.TIMES_ROMAN, 13, Font.BOLD, GrayColor.GRAYBLACK);
        PdfPCell headerCell = new PdfPCell(new Phrase(headerText, headerFont));
        headerCell.setHorizontalAlignment(Element.ALIGN_CENTER);
        headerCell.setBorder(0);
        headerCell.setColspan(TOTAL_COLUMNS);
        headerCell.setPaddingBottom(5);
        table.addCell(headerCell);
    }
}

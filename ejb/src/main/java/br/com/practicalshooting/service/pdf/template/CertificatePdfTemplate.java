package br.com.practicalshooting.service.pdf.template;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Font.FontFamily;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.GrayColor;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

import br.com.practicalshooting.model.Candidate;
import br.com.practicalshooting.model.WeaponOfCourse;
import br.com.practicalshooting.util.ResourceMessages;

public class CertificatePdfTemplate {

    public static final int TOTAL_COLUMNS = 54;
    private final Candidate candidate;
    private final WeaponOfCourse weaponOfCourse;

    public CertificatePdfTemplate(Candidate candidate, WeaponOfCourse weaponOfCourse) {
        this.candidate = candidate;
        this.weaponOfCourse = weaponOfCourse;
    }

    public void createCertificatePdf(ByteArrayOutputStream baos) throws DocumentException, IOException {
        Document document = new Document(PageSize.A4);
        PdfWriter.getInstance(document, baos);
        document.open();
        PdfPTable table = new PdfPTable(TOTAL_COLUMNS);
        table.setWidthPercentage(85);
        table.getDefaultCell().setUseAscender(true);
        table.getDefaultCell().setUseDescender(true);
        this.createBlankCell(table);
        this.createBlankCell(table);
        this.createBlankCell(table);
        this.createBlankCell(table);
        this.createBlankCell(table);
        this.createBlankCell(table);
        this.createBlankCell(table);
        this.createBlankCell(table);
        this.createContentCellWithoutBorder(table, ResourceMessages.getMessage("cert.course.title"), Element.ALIGN_CENTER, Font.NORMAL);
        this.createContentCellWithoutBorder(table, ResourceMessages.getMessage("cert.course.subtitle"), Element.ALIGN_CENTER, Font.BOLD);
        this.createContentCellWithoutBorder(table, ResourceMessages.getMessage("cert.course.candidate.data", this.candidate.getUser().getName(), this.candidate.getRg(), this.candidate.getCpf(), this.candidate.formattedAddress().replaceAll("<br/>", ". ")), Element.ALIGN_LEFT, Font.NORMAL);
        this.createBlankCell(table);
        this.createContentCellWithoutBorder(table, ResourceMessages.getMessage("cert.course.tag.gun.especification"), Element.ALIGN_LEFT, Font.NORMAL);
        this.createContentCellWithoutBorder(table, ResourceMessages.getMessage("cert.course.tag.gun.type", ResourceMessages.getMessage(this.weaponOfCourse.getWeaponType().name())), Element.ALIGN_LEFT, Font.NORMAL);
        this.createContentCellWithoutBorder(table, ResourceMessages.getMessage("cert.course.tag.gun.caliber", this.weaponOfCourse.getCaliber()), Element.ALIGN_LEFT, Font.NORMAL);
        this.createContentCellWithoutBorder(table, this.buildMessageResult(this.candidate), Element.ALIGN_LEFT, Font.NORMAL);
        this.createBlankCell(table);
        this.createBlankCell(table);
        this.createContentCellWithoutBorder(table, ResourceMessages.getMessage("cert.course.tag.substantiation"), Element.ALIGN_LEFT, Font.NORMAL);
        this.createContentCellWithoutBorder(table, ResourceMessages.getMessage("cert.course.theorical.test.note", this.candidate.getPunctuationTheoretical()), Element.ALIGN_LEFT, Font.NORMAL);
        this.createContentCellWithoutBorder(table, ResourceMessages.getMessage("cert.course.practical.test.note", this.weaponOfCourse.getPracticeNote()), Element.ALIGN_LEFT, Font.NORMAL);
        this.createBlankCell(table);
        this.createBlankCell(table);
        this.createBlankCell(table);
        this.createBlankCell(table);
        this.createContentCellWithoutBorder(table, ResourceMessages.getMessage("cert.course.local.date", this.formatDate(this.candidate.getRealization().toDate())), Element.ALIGN_LEFT, Font.NORMAL);
        this.createBlankCell(table);
        this.createBlankCell(table);
        this.createBlankCell(table);
        this.createBlankCell(table);
        this.createContentCellWithoutBorder(table, "________________________________", Element.ALIGN_CENTER, Font.BOLD);
        this.createContentCellWithoutBorder(table, this.candidate.getInstructor().getUser().getName(), Element.ALIGN_CENTER, Font.BOLD);
        this.createContentCellWithoutBorder(table, ResourceMessages.getMessage("cert.course.theorical.test.place", this.candidate.getInstructor().getInstitution()), Element.ALIGN_CENTER, Font.NORMAL);
        this.createContentCellWithoutBorder(table, ResourceMessages.getMessage("cert.course.practical.test.place", this.candidate.getInstructor().getInstitution()), Element.ALIGN_CENTER, Font.NORMAL);
        document.add(table);
        document.close();
    }

    private String buildMessageResult(Candidate model) {
        String able = (model.getUnable() == null) || (model.getUnable() == false) ? " " : "X";
        String unable = (model.getUnable() == null) || (model.getUnable() == false) ? "X" : " ";
        return ResourceMessages.getMessage("cert.course.tag.result", able, unable);
    }

    private String formatDate(Date date) {
        return new SimpleDateFormat("dd/MM/yyyy").format(date);
    }

    private void createContentCellWithoutBorder(PdfPTable table, String content, int align, int fontType) {
        Font font = new Font(FontFamily.TIMES_ROMAN, 12, fontType, GrayColor.GRAYBLACK);
        PdfPCell infoCell = new PdfPCell(new Phrase(content, font));
        infoCell.setHorizontalAlignment(align);
        infoCell.setColspan(TOTAL_COLUMNS);
        infoCell.setPaddingBottom(5);
        infoCell.setPaddingRight(5);
        infoCell.setPaddingLeft(5);
        infoCell.setBorder(0);
        table.addCell(infoCell);
    }

    private void createBlankCell(PdfPTable table) {
        PdfPCell infoCell = new PdfPCell(new Phrase("   "));
        infoCell.setColspan(TOTAL_COLUMNS);
        infoCell.setBorderWidthRight(0);
        infoCell.setBorderWidthLeft(0);
        infoCell.setBorder(0);
        table.addCell(infoCell);
    }
}

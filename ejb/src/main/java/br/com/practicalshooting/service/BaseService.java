package br.com.practicalshooting.service;

import java.io.Serializable;

import javax.ejb.Stateless;

import org.apache.log4j.Logger;

import br.com.practicalshooting.util.LoggerUtil;

@Stateless
public abstract class BaseService implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -3608920823903046619L;
    protected static final Logger LOGGER = Logger.getLogger(LoggerUtil.LOGGER_INFO);
}

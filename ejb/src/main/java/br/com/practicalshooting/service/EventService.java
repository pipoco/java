package br.com.practicalshooting.service;

import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;
import org.joda.time.LocalDate;
import org.joda.time.LocalTime;

import br.com.practicalshooting.dao.EventDAO;
import br.com.practicalshooting.dao.EventDayDAO;
import br.com.practicalshooting.dao.EventModalityDAO;
import br.com.practicalshooting.dao.ModalityDAO;
import br.com.practicalshooting.dao.ParticipantDAO;
import br.com.practicalshooting.dao.TypeParticipationDAO;
import br.com.practicalshooting.model.Event;
import br.com.practicalshooting.model.EventDay;
import br.com.practicalshooting.model.EventModality;
import br.com.practicalshooting.model.Modality;
import br.com.practicalshooting.model.TypeParticipation;
import br.com.practicalshooting.model.enumeration.Situation;
import br.com.practicalshooting.util.ResourceMessages;
import br.com.practicalshooting.util.Util;
import br.com.practicalshooting.util.exception.EventsException;
import br.com.practicalshooting.util.exception.MultipleEventsException;

@Stateless
public class EventService extends BaseService {

    /**
     * 
     */
    private static final long serialVersionUID = -4752053596280461584L;

    @Inject
    private EventDAO eventDAO;
    @Inject
    private EventDayDAO eventDayDAO;
    @Inject
    private ModalityDAO modalityDAO;
    @Inject
    private ParticipantDAO participantDAO;
    @Inject
    private TypeParticipationDAO typeParticipationDAO;
    @Inject
    private EventModalityDAO eventModalityDAO;

    public void save(Event event) {
        if (event.getEventDays() == null || event.getEventDays().isEmpty()) {
            MultipleEventsException errors = new MultipleEventsException();
            errors.add(new EventsException(ResourceMessages.getMessage("rn.event.require.day")));
            throw errors;
        }
        if (event.getModalities() == null || event.getModalities().isEmpty()) {
            MultipleEventsException errors = new MultipleEventsException();
            errors.add(new EventsException(ResourceMessages.getMessage("rn.event.require.modality")));
            throw errors;
        }
        this.sortDays(event.getEventDays());
        event.setDays(event.formattedDays());
        event.setFirstDay(event.getEventDays().get(0).getDay());
        this.findRelationForCascade(event);
        if (Util.isPersistentEntity(event)) {
            this.update(event);
            return;
        }
        this.insert(event);
    }

    private void insert(Event event) {
        this.validateData(event);
        event.setSituation(Situation.ACTIVE);
        this.eventDAO.persist(event);
    }

    private void update(Event event) {
        this.validateData(event);
        this.eventDAO.merge(event);
        this.saveDays(event);
        this.saveTypeModalities(event);
    }

    private void findRelationForCascade(Event event) {
        for (EventModality modality : event.getModalities()) {
            modality.setModality(this.modalityDAO.findById(modality.getModality().getId()));
        }
        for (EventDay eventDay : event.getEventDays()) {
            if (eventDay.getParticipations() != null && !eventDay.getParticipations().isEmpty()) {
                for (TypeParticipation participation : eventDay.getParticipations()) {
                    participation.setType(this.participantDAO.findById(participation.getType().getId()));
                }
            }
        }
    }

    private void saveTypeModalities(Event event) {
        if (event.getRemovedModalities() != null && !event.getRemovedModalities().isEmpty()) {
            for (EventModality modality : event.getRemovedModalities()) {
                if (Util.isPersistentEntity(modality)) {
                    this.eventModalityDAO.remove(modality);
                }
            }
        }
    }

    private void saveDays(Event event) {
        if (event.getRemovedEventDays() != null && !event.getRemovedEventDays().isEmpty()) {
            for (EventDay eventDay : event.getRemovedEventDays()) {
                if (Util.isPersistentEntity(eventDay)) {
                    this.eventDayDAO.remove(eventDay);
                }
            }
        }
        for (EventDay eventDay : event.getEventDays()) {
            if (Util.isPersistentEntity(eventDay)) {
                this.saveParticipantDays(eventDay);
            }
        }
    }

    private void saveParticipantDays(EventDay eventDay) {
        if (eventDay.getRemovedParticipations() != null && !eventDay.getRemovedParticipations().isEmpty()) {
            for (TypeParticipation typeParticipation : eventDay.getRemovedParticipations()) {
                if (Util.isPersistentEntity(typeParticipation)) {
                    this.typeParticipationDAO.remove(typeParticipation);
                }
            }
        }
    }

    private void validateData(Event event) {
        MultipleEventsException errors = new MultipleEventsException();
        if (StringUtils.isBlank(event.getName())) {
            errors.add(new EventsException(ResourceMessages.getMessage("rn.field.x.empty", ResourceMessages.getMessage("lbl.name"))));
        }
        if (event.getEventType() == null) {
            errors.add(new EventsException(ResourceMessages.getMessage("rn.field.x.empty", ResourceMessages.getMessage("lbl.type"))));
        }
        if (!Util.isPersistentEntity(event.getClub())) {
            errors.add(new EventsException(ResourceMessages.getMessage("rn.field.x.empty", ResourceMessages.getMessage("lbl.club"))));
        }
        errors.existsErros();
    }

    private void sortDays(List<EventDay> days) {
        Collections.sort(days, new Comparator<EventDay>() {
            @Override
            public int compare(EventDay p1, EventDay p2) {
                return p1.getDay().toDate().compareTo(p2.getDay().toDate());
            }
        });
    }

    public void buildNewEventDay(Event event, Date dayEvent, Date hourStart, Date hourEnd) {
        if (dayEvent == null || hourStart == null || hourEnd == null) {
            throw new EventsException(ResourceMessages.getMessage("rn.add.event.day.error.empty.fields"));
        }
        EventDay eventDay = new EventDay();
        eventDay.setDay(new LocalDate(dayEvent));
        eventDay.setStart(new LocalTime(hourStart));
        eventDay.setEnd(new LocalTime(hourEnd));
        boolean dayExists = false;
        event.initEventDays();
        for (EventDay item : event.getEventDays()) {
            if (item.getDay().equals(eventDay.getDay())) {
                dayExists = true;
                break;
            }
        }
        if (dayExists) {
            throw new EventsException(ResourceMessages.getMessage("rn.add.event.day.exists"));
        }
        event.addEventDay(eventDay);
    }

    public void buildNewModality(Event event, Modality modality) {
        if (!Util.isPersistentEntity(modality)) {
            throw new EventsException(ResourceMessages.getMessage("rn.add.modality.empty.field"));
        }
        EventModality eventModality = new EventModality();
        eventModality.setModality(modality);
        boolean modalityExists = false;
        event.initModalities();
        for (EventModality item : event.getModalities()) {
            if (item.getModality().equals(eventModality.getModality())) {
                modalityExists = true;
                break;
            }
        }
        if (modalityExists) {
            throw new EventsException(ResourceMessages.getMessage("rn.add.modality.exists"));
        }
        event.addModalitie(eventModality);
    }
}

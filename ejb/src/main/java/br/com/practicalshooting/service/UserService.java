package br.com.practicalshooting.service;

import java.net.URISyntaxException;
import java.util.UUID;

import javax.ejb.Stateless;
import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;
import org.joda.time.LocalDate;

import br.com.practicalshooting.dao.ProfileDAO;
import br.com.practicalshooting.dao.UserDAO;
import br.com.practicalshooting.dao.UserProfileDAO;
import br.com.practicalshooting.model.Associate;
import br.com.practicalshooting.model.User;
import br.com.practicalshooting.model.enumeration.Situation;
import br.com.practicalshooting.security.model.Profile;
import br.com.practicalshooting.security.model.UserProfile;
import br.com.practicalshooting.util.EmailTemplateRead;
import br.com.practicalshooting.util.HashUtil;
import br.com.practicalshooting.util.ResourceMessages;
import br.com.practicalshooting.util.Util;
import br.com.practicalshooting.util.exception.EventsException;
import br.com.practicalshooting.util.exception.MultipleEventsException;

@Stateless
public class UserService extends BaseService {

    /**
     *
     */
    private static final long serialVersionUID = -2940674218463125404L;

    @Inject
    private UserDAO userDAO;
    @Inject
    private ProfileDAO profileDAO;
    @Inject
    private UserProfileDAO userProfileDAO;

    @Inject
    private AssociateService associateService;
    @Inject
    private MailQueueService mailQueueService;

    public void save(User user) {
        if (Util.isPersistentEntity(user)) {
            this.update(user);
            return;
        }
        this.insert(user);
    }

    private void insert(User user) {
        this.validateData(user);
        this.validateRulesForInsert(user);
        user.setSituation(Situation.PENDING);
        user.setPassword(HashUtil.SHA1(user.getPassword()));
        user.setConfirmRegisterToken(UUID.randomUUID().toString());
        user.setRequestTokensDate(LocalDate.now());
        user.clearRelationships();
        this.userDAO.persist(user);
    }

    private void update(User user) {
        this.validateData(user);
        this.validateRulesForUpdate(user);
        user.clearRelationships();
        this.userDAO.merge(user);
    }

    private void validateRulesForInsert(User user) {
        User tempUser = this.userDAO.findByEmailForRegisterOrUpdate(user.getEmail());
        if (Util.isPersistentEntity(tempUser)) {
            MultipleEventsException errors = new MultipleEventsException();
            errors.add(new EventsException(ResourceMessages.getMessage("rn.register.exists.with.x", ResourceMessages.getMessage("lbl.email"))));
            throw errors;
        }
    }

    private void validateRulesForUpdate(User user) {
        User tempUser = this.userDAO.findByEmailForRegisterOrUpdate(user.getEmail());
        if (Util.isPersistentEntity(tempUser) && !tempUser.getId().equals(user.getId())) {
            MultipleEventsException errors = new MultipleEventsException();
            errors.add(new EventsException(ResourceMessages.getMessage("rn.register.exists.with.x", ResourceMessages.getMessage("lbl.email"))));
            throw errors;
        }
    }

    private void validateData(User user) {
        MultipleEventsException errors = new MultipleEventsException();
        if (StringUtils.isBlank(user.getName())) {
            errors.add(new EventsException(ResourceMessages.getMessage("rn.field.x.empty", ResourceMessages.getMessage("lbl.name"))));
        }
        if (StringUtils.isBlank(user.getEmail())) {
            errors.add(new EventsException(ResourceMessages.getMessage("rn.field.x.empty", ResourceMessages.getMessage("lbl.email"))));
        }
        if (StringUtils.isBlank(user.getPassword())) {
            errors.add(new EventsException(ResourceMessages.getMessage("rn.field.x.empty", ResourceMessages.getMessage("lbl.password"))));
        } else if (!Util.emailValido(user.getEmail())) {
            errors.add(new EventsException(ResourceMessages.getMessage("rn.field.x.invalid", ResourceMessages.getMessage("lbl.password"))));
        }
        errors.existsErros();
    }

    public void changePassword(Long userSku, String currentPassword, String passwordChange, String confirmPasswordChange) {
        this.validateChangePasswordRules(userSku, currentPassword, passwordChange, confirmPasswordChange);
        MultipleEventsException errors = new MultipleEventsException();
        User user = this.userDAO.findById(userSku);
        if (!Util.isPersistentEntity(user)) {
            errors.add(new EventsException(ResourceMessages.getMessage("rn.change.password.user.invalid")));
            throw errors;
        }
        if (!user.getPassword().equals(HashUtil.SHA1(currentPassword))) {
            errors.add(new EventsException(ResourceMessages.getMessage("rn.change.current.password.invalid")));
            throw errors;
        }
        user.setPassword(HashUtil.SHA1(passwordChange));
        user.setConfirmPasswordToken(null);
        user.setConfirmRegisterToken(null);
        user.setRequestTokensDate(null);
        this.userDAO.merge(user);
    }

    private void validateChangePasswordRules(Long userSku, String currentPassword, String passwordChange, String confirmPasswordChange) {
        MultipleEventsException errors = new MultipleEventsException();
        if ((userSku == null) || StringUtils.isBlank(currentPassword) || StringUtils.isBlank(passwordChange) || StringUtils.isBlank(confirmPasswordChange)) {
            errors.add(new EventsException(ResourceMessages.getMessage("rn.change.password.field.blank")));
            throw errors;
        }
        if (!passwordChange.equals(confirmPasswordChange)) {
            errors.add(new EventsException(ResourceMessages.getMessage("rn.change.password.new.not.equals")));
            throw errors;
        }
    }

    public void updateProfile(User user) {
        MultipleEventsException errors = new MultipleEventsException();
        if (!Util.isPersistentEntity(user)) {
            errors.add(new EventsException(ResourceMessages.getMessage("rn.update.profile.user.invalid")));
            throw errors;
        }
        if (StringUtils.isBlank(user.getName())) {
            errors.add(new EventsException(ResourceMessages.getMessage("rn.update.profile.field.name.blank")));
            throw errors;
        }
        if (!Util.isPersistentEntity(user.getAssociate())) {
            user.setAssociate(null);
        }
        this.userDAO.merge(user);
    }

    public void saveAssociateData(User user) {
        user.getAssociate().setUser(this.userDAO.findById(user.getId()));
        this.associateService.save(user.getAssociate());
    }

    public void removeAssociateData(Associate associate) {
        this.associateService.remove(associate);
    }

    public void forgetPassword(String login) throws URISyntaxException {
        User user = this.userDAO.findActiveUserByEmail(login);
        if (!Util.isPersistentEntity(user)) {
            throw new EventsException(ResourceMessages.getMessage("error.email.forget.password"));
        }
        user.setConfirmPasswordToken(UUID.randomUUID().toString());
        user.setRequestTokensDate(LocalDate.now());
        this.userDAO.merge(user);
        this.sendMailToForgetPassword(user);
    }

    private void sendMailToForgetPassword(User user) throws URISyntaxException {
        this.mailQueueService.mailQueue(ResourceMessages.getMessage("title.forget.password"),
                EmailTemplateRead.readEmailTemplate(user.getName(), ResourceMessages.getMessage("body.forget.password", EmailTemplateRead.getAbsoluteApplicationUrl(), user.getConfirmPasswordToken()), ResourceMessages.getMessage("facebook_link"), ResourceMessages.getMessage("instagram_link")),
                user.getEmail());
    }

    public void register(User user) throws URISyntaxException {
        this.save(user);
        this.sendMailToRegister(user);
    }

    private void sendMailToRegister(User user) throws URISyntaxException {
        this.mailQueueService.mailQueue(ResourceMessages.getMessage("title.register.new.user"),
                EmailTemplateRead.readEmailTemplate(user.getName(), ResourceMessages.getMessage("body.register.new.user", EmailTemplateRead.getAbsoluteApplicationUrl(), user.getConfirmRegisterToken()), ResourceMessages.getMessage("facebook_link"), ResourceMessages.getMessage("instagram_link")),
                user.getEmail());
    }

    public void forgetPassword(Long userSku, String passwordChange, String confirmPasswordChange) {
        this.validateForgetPasswordRules(userSku, passwordChange, confirmPasswordChange);
        MultipleEventsException errors = new MultipleEventsException();
        User user = this.userDAO.findById(userSku);
        if (!Util.isPersistentEntity(user)) {
            errors.add(new EventsException(ResourceMessages.getMessage("rn.change.password.user.invalid")));
            throw errors;
        }
        user.setPassword(HashUtil.SHA1(passwordChange));
        user.setConfirmPasswordToken(null);
        user.setConfirmRegisterToken(null);
        user.setRequestTokensDate(null);
        this.userDAO.merge(user);
    }

    private void validateForgetPasswordRules(Long userSku, String passwordChange, String confirmPasswordChange) {
        MultipleEventsException errors = new MultipleEventsException();
        if ((userSku == null) || StringUtils.isBlank(passwordChange) || StringUtils.isBlank(confirmPasswordChange)) {
            errors.add(new EventsException(ResourceMessages.getMessage("rn.change.password.field.blank")));
            throw errors;
        }
        if (!passwordChange.equals(confirmPasswordChange)) {
            errors.add(new EventsException(ResourceMessages.getMessage("rn.change.password.new.not.equals")));
            throw errors;
        }
    }

    public void confirmRegister(Long userSku) {
        MultipleEventsException errors = new MultipleEventsException();
        User user = this.userDAO.findById(userSku);
        if (!Util.isPersistentEntity(user)) {
            errors.add(new EventsException(ResourceMessages.getMessage("rn.confirm.register.user.invalid")));
            throw errors;
        }
        user.setSituation(Situation.ACTIVE);
        user.setConfirmPasswordToken(null);
        user.setConfirmRegisterToken(null);
        user.setRequestTokensDate(null);
        this.userDAO.merge(user);
    }

    public User login(String login, String password) {
        User user = this.userDAO.findByEmailAndPassword(login, HashUtil.SHA1(password));
        if (Util.isPersistentEntity(user)) {
            Profile profile = this.userProfileDAO.findProfileByUser(user.getId());
            if (Util.isPersistentEntity(profile)) {
                user.setProfile(profile);
                return user;
            }
            profile = this.profileDAO.findByName("Usuário");
            UserProfile userProfile = new UserProfile();
            userProfile.setUser(user);
            userProfile.setProfile(profile);
            this.userProfileDAO.persist(userProfile);
            profile = this.userProfileDAO.findProfileByUser(user.getId());
            user.setProfile(profile);
        }
        return user;
    }
}

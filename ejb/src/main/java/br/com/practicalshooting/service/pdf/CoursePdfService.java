package br.com.practicalshooting.service.pdf;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.Serializable;

import javax.ejb.Stateless;
import javax.inject.Inject;

import com.itextpdf.text.DocumentException;

import br.com.practicalshooting.dao.CandidateDAO;
import br.com.practicalshooting.dao.WeaponOfCourseDAO;
import br.com.practicalshooting.model.Candidate;
import br.com.practicalshooting.model.WeaponOfCourse;
import br.com.practicalshooting.service.pdf.template.CertificatePdfTemplate;
import br.com.practicalshooting.service.pdf.template.FormCoursePdfTemplate;

@Stateless
public class CoursePdfService implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -4752053596280461584L;

    @Inject
    private CandidateDAO candidateDAO;
    @Inject
    private WeaponOfCourseDAO weaponOfCourseDAO;

    public void genarateFormCourse(Long candidateSku, ByteArrayOutputStream baos) throws DocumentException, IOException {
        Candidate candidate = this.candidateDAO.findByIdFetchUserInstructorContactsAndWeapons(candidateSku);
        FormCoursePdfTemplate formCoursePdf = new FormCoursePdfTemplate(candidate);
        formCoursePdf.createCoursePdf(baos);
    }

    public void genarateCertificateCourse(Long candidateSku, Long weaponSku, ByteArrayOutputStream baos) throws DocumentException, IOException {
        Candidate candidate = this.candidateDAO.findByIdFetchUserInstructorContactsAndWeapons(candidateSku);
        WeaponOfCourse weaponOfCourse = this.weaponOfCourseDAO.findById(weaponSku);
        CertificatePdfTemplate certificatePdfTemplate = new CertificatePdfTemplate(candidate, weaponOfCourse);
        certificatePdfTemplate.createCertificatePdf(baos);
    }
}

package br.com.practicalshooting.service;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;

import br.com.practicalshooting.dao.CandidateContactDAO;
import br.com.practicalshooting.dao.CandidateDAO;
import br.com.practicalshooting.dao.InstructorDAO;
import br.com.practicalshooting.dao.UserDAO;
import br.com.practicalshooting.dao.WeaponOfCourseDAO;
import br.com.practicalshooting.model.Candidate;
import br.com.practicalshooting.model.CandidateContact;
import br.com.practicalshooting.model.WeaponOfCourse;
import br.com.practicalshooting.model.enumeration.ContactType;
import br.com.practicalshooting.model.enumeration.SituationCourse;
import br.com.practicalshooting.util.EmailTemplateRead;
import br.com.practicalshooting.util.ResourceMessages;
import br.com.practicalshooting.util.Util;
import br.com.practicalshooting.util.exception.EventsException;
import br.com.practicalshooting.util.exception.MultipleEventsException;

@Stateless
public class CandidateService extends BaseService {

    /**
     *
     */
    private static final long serialVersionUID = -4752053596280461584L;

    @Inject
    private CandidateDAO candidateDAO;
    @Inject
    private CandidateContactDAO candidateContactDAO;
    @Inject
    private WeaponOfCourseDAO weaponOfCourseDAO;
    @Inject
    private UserDAO userDAO;
    @Inject
    private InstructorDAO instructorDAO;
    @Inject
    private MailQueueService mailQueueService;

    public void save(Candidate candidate, List<CandidateContact> candidateContactsRemoved, List<WeaponOfCourse> weaponsOfCourseRemoved) {
        if (Util.isPersistentEntity(candidate)) {
            this.update(candidate);
            this.removeContacts(candidateContactsRemoved);
            this.removeWeapons(weaponsOfCourseRemoved);
            return;
        }
        this.insert(candidate);
        this.removeContacts(candidateContactsRemoved);
        this.removeWeapons(weaponsOfCourseRemoved);
    }

    private void removeWeapons(List<WeaponOfCourse> weaponsOfCourseRemoved) {
        for (WeaponOfCourse weaponOfCourse : weaponsOfCourseRemoved) {
            this.weaponOfCourseDAO.remove(this.weaponOfCourseDAO.findById(weaponOfCourse.getId()));
        }
    }

    private void removeContacts(List<CandidateContact> candidateContactsRemoved) {
        for (CandidateContact candidateContact : candidateContactsRemoved) {
            this.candidateContactDAO.remove(this.candidateContactDAO.findById(candidateContact.getId()));
        }
    }

    private void insert(Candidate candidate) {
        this.validateData(candidate);
        this.validateRelationshipRules(candidate);
        candidate.setSituation(SituationCourse.PENDIND);
        this.candidateDAO.persist(candidate);
    }

    private void update(Candidate candidate) {
        this.validateData(candidate);
        this.validateRelationshipRules(candidate);
        this.candidateDAO.merge(candidate);
    }

    private void validateRelationshipRules(Candidate candidate) {
        MultipleEventsException errors = new MultipleEventsException();
        if (!Util.isPersistentEntity(candidate.getUser())) {
            errors.add(new EventsException(ResourceMessages.getMessage("lbl.user")));
        }
        if (!Util.isPersistentEntity(candidate.getInstructor())) {
            errors.add(new EventsException(ResourceMessages.getMessage("rn.field.x.empty", ResourceMessages.getMessage("lbl.instructor"))));
        }
        errors.existsErros();
    }

    private void validateData(Candidate candidate) {
        MultipleEventsException errors = new MultipleEventsException();
        Util.appendErrorIfExists(errors, StringUtils.isBlank(candidate.getFatherName()), ResourceMessages.getMessage("rn.field.x.empty", ResourceMessages.getMessage("lbl.father.name")));
        Util.appendErrorIfExists(errors, StringUtils.isBlank(candidate.getMotherName()), ResourceMessages.getMessage("rn.field.x.empty", ResourceMessages.getMessage("lbl.mother.name")));
        Util.appendErrorIfExists(errors, StringUtils.isBlank(candidate.getRg()), ResourceMessages.getMessage("rn.field.x.empty", ResourceMessages.getMessage("lbl.rg")));
        Util.appendErrorIfExists(errors, StringUtils.isBlank(candidate.getCpf()), ResourceMessages.getMessage("rn.field.x.empty", ResourceMessages.getMessage("lbl.cpf")));
        Util.appendErrorIfExists(errors, StringUtils.isBlank(candidate.getAddress().getPlace()), ResourceMessages.getMessage("rn.field.x.empty", ResourceMessages.getMessage("lbl.address")));
        Util.appendErrorIfExists(errors, StringUtils.isBlank(candidate.getAddress().getZipCode()), ResourceMessages.getMessage("rn.field.x.empty", ResourceMessages.getMessage("lbl.zipcode")));
        Util.appendErrorIfExists(errors, StringUtils.isBlank(candidate.getAddress().getDistrict()), ResourceMessages.getMessage("rn.field.x.empty", ResourceMessages.getMessage("lbl.district")));
        Util.appendErrorIfExists(errors, StringUtils.isBlank(candidate.getAddress().getCity()), ResourceMessages.getMessage("rn.field.x.empty", ResourceMessages.getMessage("lbl.city")));
        Util.appendErrorIfExists(errors, StringUtils.isBlank(candidate.getAddress().getState()), ResourceMessages.getMessage("rn.field.x.empty", ResourceMessages.getMessage("lbl.state")));
        Util.appendErrorIfExists(errors, candidate.getCourseType() == null, ResourceMessages.getMessage("rn.field.x.empty", ResourceMessages.getMessage("lbl.course.purpose")));
        errors.existsErros();
    }

    private void validateContactCandidateData(String contactDescription, ContactType contactType) {
        MultipleEventsException errors = new MultipleEventsException();
        if (StringUtils.isBlank(contactDescription)) {
            errors.add(new EventsException(ResourceMessages.getMessage("rn.field.x.empty", ResourceMessages.getMessage("lbl.contact.description"))));
        }
        if (contactType == null) {
            errors.add(new EventsException(ResourceMessages.getMessage("rn.field.x.empty", ResourceMessages.getMessage("lbl.contact.type"))));
        }
        errors.existsErros();
        if (contactType.equals(ContactType.EMAIL) && !Util.emailValido(contactDescription)) {
            errors.add(new EventsException(ResourceMessages.getMessage("error.invalid.email")));
        }
        errors.existsErros();
    }

    public CandidateContact buildNewContact(Candidate candidate, String contactDescription, ContactType contactType) {
        this.validateContactCandidateData(contactDescription, contactType);
        MultipleEventsException errors = new MultipleEventsException();
        CandidateContact candidateContact = new CandidateContact(candidate, contactDescription, contactType);
        if (candidate.containsContact(candidateContact)) {
            errors.add(new EventsException(ResourceMessages.getMessage("rn.contact.exists.in.candidate")));
        }
        errors.existsErros();
        return candidateContact;
    }

    public void register(Candidate candidate, String candidateEmail, String candidatePhone) {
        this.validateRelationshipRules(candidate);
        candidate.getContacts().add(this.buildNewContact(candidate, candidateEmail, ContactType.EMAIL));
        candidate.getContacts().add(this.buildNewContact(candidate, candidatePhone, ContactType.PHONE));
        candidate.setUser(this.userDAO.findById(candidate.getUser().getId()));
        candidate.setInstructor(this.instructorDAO.findByIdWithUser(candidate.getInstructor().getId()));
        this.save(candidate, new ArrayList<CandidateContact>(), new ArrayList<WeaponOfCourse>());
        this.sendEmailToCandidate(candidate, candidateEmail, candidatePhone);
        this.sendEmailToInstructor(candidate, candidateEmail, candidatePhone);
    }

    private void sendEmailToCandidate(Candidate candidate, String candidateEmail, String candidatePhone) {
        String ctvvAssociated = ResourceMessages.getMessage("lbl.no");
        String cardNumber = "";
        if ((candidate.getCtvvAssociated() != null) && candidate.getCtvvAssociated()) {
            ctvvAssociated = ResourceMessages.getMessage("lbl.yes");
            cardNumber = candidate.getCardNumber().toString();
        }
        this.mailQueueService.mailQueue(ResourceMessages.getMessage("title.register.candidate"),
                EmailTemplateRead.readEmailTemplate(candidate.getUser().getName(), ResourceMessages.getMessage("body.register.candidate", candidate.getUser().getName(), candidateEmail, candidatePhone, candidate.getFatherName(), candidate.getMotherName(), candidate.getRg(), candidate.getCpf(),
                        candidate.formattedAddress(), candidate.getInstructor().getUser().getName(), ResourceMessages.getMessage(candidate.getCourseType().name()), ctvvAssociated, cardNumber), ResourceMessages.getMessage("facebook_link"), ResourceMessages.getMessage("instagram_link")),
                candidateEmail);
    }

    private void sendEmailToInstructor(Candidate candidate, String candidateEmail, String candidatePhone) {
        String ctvvAssociated = ResourceMessages.getMessage("lbl.no");
        String cardNumber = "";
        if ((candidate.getCtvvAssociated() != null) && candidate.getCtvvAssociated()) {
            ctvvAssociated = ResourceMessages.getMessage("lbl.yes");
            cardNumber = candidate.getCardNumber().toString();
        }
        this.mailQueueService.mailQueue(ResourceMessages.getMessage("title.register.new.candidate"),
                EmailTemplateRead.readEmailTemplate(candidate.getInstructor().getUser().getName(), ResourceMessages.getMessage("body.register.new.candidate", candidate.getUser().getName(), candidateEmail, candidatePhone, candidate.getFatherName(), candidate.getMotherName(), candidate.getRg(),
                        candidate.getCpf(), candidate.formattedAddress(), ResourceMessages.getMessage(candidate.getCourseType().name()), ctvvAssociated, cardNumber), ResourceMessages.getMessage("facebook_link"), ResourceMessages.getMessage("instagram_link")),
                candidate.getInstructor().getUser().getEmail());
    }

    public Boolean hasCourseRegistred(Long userSKU) {
        MultipleEventsException errors = new MultipleEventsException();
        if (userSKU == null) {
            errors.add(new EventsException(ResourceMessages.getMessage("rn.field.x.empty", ResourceMessages.getMessage("lbl.user"))));
        }
        errors.existsErros();
        int totalCourses = this.candidateDAO.countByUserId(userSKU);
        return totalCourses > 0;
    }

    public void finalizeCourse(Long candidateSku) {
        Candidate candidate = this.candidateDAO.findByIdFetchUserInstructorContactsAndWeapons(candidateSku);
        this.validateRulesFinalizeCourse(candidate);
        candidate.setSituation(SituationCourse.FINALIZED);
        this.candidateDAO.merge(candidate);
    }

    private void validateRulesFinalizeCourse(Candidate candidate) {
        MultipleEventsException errors = new MultipleEventsException();
        if (candidate.getCriminalRecord() == null) {
            candidate.setCriminalRecord(false);
        }
        if (candidate.getPsychologicalReport() == null) {
            candidate.setPsychologicalReport(false);
        }
        if (candidate.getUnable() == null) {
            candidate.setUnable(false);
        }
        Util.appendErrorIfExists(errors, StringUtils.isBlank(candidate.getFatherName()), ResourceMessages.getMessage("rn.field.x.empty", ResourceMessages.getMessage("lbl.father.name")));
        Util.appendErrorIfExists(errors, StringUtils.isBlank(candidate.getMotherName()), ResourceMessages.getMessage("rn.field.x.empty", ResourceMessages.getMessage("lbl.mother.name")));
        Util.appendErrorIfExists(errors, StringUtils.isBlank(candidate.getRg()), ResourceMessages.getMessage("rn.field.x.empty", ResourceMessages.getMessage("lbl.rg")));
        Util.appendErrorIfExists(errors, StringUtils.isBlank(candidate.getCpf()), ResourceMessages.getMessage("rn.field.x.empty", ResourceMessages.getMessage("lbl.cpf")));
        Util.appendErrorIfExists(errors, StringUtils.isBlank(candidate.getAddress().getPlace()), ResourceMessages.getMessage("rn.field.x.empty", ResourceMessages.getMessage("lbl.address")));
        Util.appendErrorIfExists(errors, StringUtils.isBlank(candidate.getAddress().getZipCode()), ResourceMessages.getMessage("rn.field.x.empty", ResourceMessages.getMessage("lbl.zipcode")));
        Util.appendErrorIfExists(errors, StringUtils.isBlank(candidate.getAddress().getDistrict()), ResourceMessages.getMessage("rn.field.x.empty", ResourceMessages.getMessage("lbl.district")));
        Util.appendErrorIfExists(errors, StringUtils.isBlank(candidate.getAddress().getCity()), ResourceMessages.getMessage("rn.field.x.empty", ResourceMessages.getMessage("lbl.city")));
        Util.appendErrorIfExists(errors, StringUtils.isBlank(candidate.getAddress().getState()), ResourceMessages.getMessage("rn.field.x.empty", ResourceMessages.getMessage("lbl.state")));
        Util.appendErrorIfExists(errors, candidate.getCourseType() == null, ResourceMessages.getMessage("rn.field.x.empty", ResourceMessages.getMessage("lbl.course.purpose")));
        Util.appendErrorIfExists(errors, candidate.getTheoreticalNote() == null, ResourceMessages.getMessage("rn.field.x.empty", ResourceMessages.getMessage("lbl.note")));
        Util.appendErrorIfExists(errors, candidate.getReceiptReports() == null, ResourceMessages.getMessage("rn.field.x.empty", ResourceMessages.getMessage("lbl.receipt.date")));
        Util.appendErrorIfExists(errors, candidate.getRealization() == null, ResourceMessages.getMessage("rn.field.x.empty", ResourceMessages.getMessage("lbl.realization.date")));
        Util.appendErrorIfExists(errors, (candidate.getWeapons() == null) || candidate.getWeapons().isEmpty(), ResourceMessages.getMessage("rn.weapon.required"));
        if (errors.hasErros()) {
            errors.getExceptions().add(0, new EventsException(ResourceMessages.getMessage("msg.error.finalize.course.required.fields")));
            errors.existsErros();
        }
    }
}

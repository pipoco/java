package br.com.practicalshooting.service;

import javax.ejb.Stateless;
import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;
import org.quartz.SchedulerException;

import com.google.gson.JsonArray;

import br.com.practicalshooting.dao.MailQueueDAO;
import br.com.practicalshooting.model.MailQueue;
import br.com.practicalshooting.model.enumeration.MailStatus;
import br.com.practicalshooting.quartz.configuration.JobConfig;
import br.com.practicalshooting.util.ResourceMessages;
import br.com.practicalshooting.util.Util;
import br.com.practicalshooting.util.exception.EventsException;
import br.com.practicalshooting.util.exception.MultipleEventsException;

@Stateless
public class MailQueueService extends BaseService {

    /**
     *
     */
    private static final long serialVersionUID = -2940674218463125404L;

    @Inject
    private MailQueueDAO mailQueueDAO;
    @Inject
    private JobConfig jobConfig;

    public void save(MailQueue mailQueue) {
        if (Util.isPersistentEntity(mailQueue)) {
            this.update(mailQueue);
            return;
        }
        this.insert(mailQueue);
        try {
            this.jobConfig.executarJobAgora("Mail Queue");
        } catch (ClassNotFoundException | SchedulerException e) {
            LOGGER.error("", e);
        }
    }

    private void insert(MailQueue mailQueue) {
        // JsonArray to = new JsonArray();
        // to.add("julioeffgen@gmail.com");
        // mailQueue.setJsonTo(to.toString());
        this.validateData(mailQueue);
        mailQueue.setStatus(MailStatus.PENDING);
        this.mailQueueDAO.persist(mailQueue);
    }

    private void update(MailQueue mailQueue) {
        this.validateData(mailQueue);
        this.mailQueueDAO.merge(mailQueue);
    }

    private void validateData(MailQueue mailQueue) {
        MultipleEventsException errors = new MultipleEventsException();
        if (StringUtils.isBlank(mailQueue.getSubject())) {
            errors.add(new EventsException(ResourceMessages.getMessage("rn.field.x.empty", ResourceMessages.getMessage("lbl.subject"))));
        }
        if (StringUtils.isBlank(mailQueue.getJsonTo())) {
            errors.add(new EventsException(ResourceMessages.getMessage("rn.field.x.empty", ResourceMessages.getMessage("lbl.to"))));
        }
        if (StringUtils.isBlank(mailQueue.getContent())) {
            errors.add(new EventsException(ResourceMessages.getMessage("rn.field.x.empty", ResourceMessages.getMessage("lbl.content"))));
        }
        errors.existsErros();
    }

    public void mailQueue(String subject, String messageContent, String... recipientes) {
        if (recipientes == null) {
            throw new EventsException("error.invalid.param.recipients");
        }
        MailQueue mailQueue = new MailQueue();
        mailQueue.setSubject(subject);
        JsonArray to = new JsonArray();
        for (int i = 0; i < recipientes.length; i++) {
            to.add(recipientes[i]);
        }
        mailQueue.setJsonTo(to.toString());
        mailQueue.setContent(messageContent);
        this.save(mailQueue);
    }
}

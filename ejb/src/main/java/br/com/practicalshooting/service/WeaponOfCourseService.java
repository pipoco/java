package br.com.practicalshooting.service;

import javax.ejb.Stateless;
import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;

import br.com.practicalshooting.dao.CandidateDAO;
import br.com.practicalshooting.dao.WeaponOfCourseDAO;
import br.com.practicalshooting.model.WeaponOfCourse;
import br.com.practicalshooting.util.ResourceMessages;
import br.com.practicalshooting.util.Util;
import br.com.practicalshooting.util.exception.EventsException;
import br.com.practicalshooting.util.exception.MultipleEventsException;

@Stateless
public class WeaponOfCourseService extends BaseService {

    /**
     * 
     */
    private static final long serialVersionUID = -4752053596280461584L;

    @Inject
    private CandidateDAO candidateDAO;
    @Inject
    private WeaponOfCourseDAO weaponOfCourseDAO;

    public void save(WeaponOfCourse weaponOfCourse) {
        if (Util.isPersistentEntity(weaponOfCourse)) {
            this.update(weaponOfCourse);
            return;
        }
        this.insert(weaponOfCourse);
    }

    private void insert(WeaponOfCourse model) {
        this.validateData(model);
        this.validateRelationshipRules(model);
        model.setCandidate(this.candidateDAO.findById(model.getCandidate().getId()));
        this.weaponOfCourseDAO.persist(model);
    }

    private void update(WeaponOfCourse model) {
        this.validateData(model);
        this.validateRelationshipRules(model);
        model.setCandidate(this.candidateDAO.findById(model.getCandidate().getId()));
        this.weaponOfCourseDAO.merge(model);
    }

    private void validateRelationshipRules(WeaponOfCourse model) {
        MultipleEventsException errors = new MultipleEventsException();
        if (!Util.isPersistentEntity(model.getCandidate())) {
            errors.add(new EventsException(ResourceMessages.getMessage("rn.field.x.empty", ResourceMessages.getMessage("lbl.candidate"))));
        }
        errors.existsErros();
    }

    private void validateData(WeaponOfCourse model) {
        MultipleEventsException errors = new MultipleEventsException();
        if (StringUtils.isBlank(model.getCaliber())) {
            errors.add(new EventsException(ResourceMessages.getMessage("rn.field.x.empty", ResourceMessages.getMessage("lbl.caliber"))));
        }
        if (model.getPunctuationPractice() == null) {
            errors.add(new EventsException(ResourceMessages.getMessage("rn.field.x.empty", ResourceMessages.getMessage("lbl.note"))));
        }
        if (model.getPracticeNote() == null) {
            errors.add(new EventsException(ResourceMessages.getMessage("rn.field.x.empty", ResourceMessages.getMessage("lbl.punctuation"))));
        }
        if (model.getWeaponType() == null) {
            errors.add(new EventsException(ResourceMessages.getMessage("rn.field.x.empty", ResourceMessages.getMessage("lbl.gun"))));
        }
        errors.existsErros();
    }
}

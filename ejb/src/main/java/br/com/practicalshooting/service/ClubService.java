package br.com.practicalshooting.service;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;

import br.com.practicalshooting.dao.ClubContactDAO;
import br.com.practicalshooting.dao.ClubDAO;
import br.com.practicalshooting.model.Club;
import br.com.practicalshooting.model.ClubContact;
import br.com.practicalshooting.model.enumeration.ContactType;
import br.com.practicalshooting.util.ResourceMessages;
import br.com.practicalshooting.util.Util;
import br.com.practicalshooting.util.exception.EventsException;
import br.com.practicalshooting.util.exception.MultipleEventsException;

@Stateless
public class ClubService extends BaseService {

    /**
     * 
     */
    private static final long serialVersionUID = -4752053596280461584L;

    @Inject
    private ClubDAO clubDAO;
    @Inject
    private ClubContactDAO clubContactDAO;

    public void save(Club club, List<ClubContact> clubContactsRemoved) {
        if (Util.isPersistentEntity(club)) {
            this.update(club);
            this.removeContacts(clubContactsRemoved);
            return;
        }
        this.insert(club);
        this.removeContacts(clubContactsRemoved);
    }

    private void removeContacts(List<ClubContact> clubContactsRemoved) {
        for (ClubContact clubContact : clubContactsRemoved) {
            this.clubContactDAO.remove(this.clubContactDAO.findById(clubContact.getId()));
        }
    }

    private void insert(Club club) {
        this.validateData(club);
        this.validateRulesForInsert(club);
        this.clubDAO.persist(club);
    }

    private void update(Club club) {
        this.validateData(club);
        this.validateRulesForUpdate(club);
        this.clubDAO.merge(club);
    }

    private void validateRulesForInsert(Club club) {
        Club tempClub = this.clubDAO.findByName(club.getName());
        if (Util.isPersistentEntity(tempClub)) {
            MultipleEventsException errors = new MultipleEventsException();
            errors.add(new EventsException(ResourceMessages.getMessage("rn.register.exists.with.x", ResourceMessages.getMessage("lbl.name"))));
            throw errors;
        }
    }

    private void validateRulesForUpdate(Club club) {
        Club tempClub = this.clubDAO.findByName(club.getName());
        if (Util.isPersistentEntity(tempClub) && !tempClub.getId().equals(club.getId())) {
            MultipleEventsException errors = new MultipleEventsException();
            errors.add(new EventsException(ResourceMessages.getMessage("rn.register.exists.with.x", ResourceMessages.getMessage("lbl.name"))));
            throw errors;
        }
    }

    private void validateData(Club club) {
        MultipleEventsException errors = new MultipleEventsException();
        if (StringUtils.isBlank(club.getName())) {
            errors.add(new EventsException(ResourceMessages.getMessage("rn.field.x.empty", ResourceMessages.getMessage("lbl.name"))));
        }
        errors.existsErros();
    }

    private void validateContactClubData(String contactDescription, ContactType contactType) {
        MultipleEventsException errors = new MultipleEventsException();
        errors.existsErros();
        if (StringUtils.isBlank(contactDescription)) {
            errors.add(new EventsException(ResourceMessages.getMessage("rn.field.x.empty", ResourceMessages.getMessage("lbl.contact.description"))));
        }
        if (contactType == null) {
            errors.add(new EventsException(ResourceMessages.getMessage("rn.field.x.empty", ResourceMessages.getMessage("lbl.contact.type"))));
        }
        errors.existsErros();
        if (contactType.equals(ContactType.EMAIL) && !Util.emailValido(contactDescription)) {
            errors.add(new EventsException(ResourceMessages.getMessage("error.invalid.email")));
        }
        errors.existsErros();
    }

    public ClubContact buildNewContact(Club club, String contactDescription, ContactType contactType) {
        this.validateContactClubData(contactDescription, contactType);
        ClubContact clubContact = new ClubContact(club, contactDescription, contactType);
        if (club.containsContact(clubContact)) {
            MultipleEventsException errors = new MultipleEventsException();
            errors.add(new EventsException(ResourceMessages.getMessage("rn.contact.exists.in.club")));
        }
        return clubContact;
    }
}

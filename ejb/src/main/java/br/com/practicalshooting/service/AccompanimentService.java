package br.com.practicalshooting.service;

import javax.ejb.Stateless;
import javax.inject.Inject;

import br.com.practicalshooting.dao.AccompanimentDAO;
import br.com.practicalshooting.dao.EventDAO;
import br.com.practicalshooting.dao.UserDAO;
import br.com.practicalshooting.model.Accompaniment;
import br.com.practicalshooting.util.Util;

@Stateless
public class AccompanimentService extends BaseService {

    /**
     * 
     */
    private static final long serialVersionUID = -2940674218463125404L;

    @Inject
    private AccompanimentDAO accompanimentDAO;
    @Inject
    private UserDAO userDAO;
    @Inject
    private EventDAO eventDAO;

    public void save(Accompaniment accompaniment) {
        if (Util.isPersistentEntity(accompaniment)) {
            this.update(accompaniment);
            return;
        }
        this.insert(accompaniment);
    }

    private void insert(Accompaniment accompaniment) {
        // TODO: Incluir validações
        accompaniment.setUser(this.userDAO.findById(accompaniment.getUser().getId()));
        accompaniment.setEvent(this.eventDAO.findById(accompaniment.getEvent().getId()));
        this.accompanimentDAO.persist(accompaniment);
    }

    private void update(Accompaniment accompaniment) {
        // TODO: Incluir Validações
        this.accompanimentDAO.merge(accompaniment);
    }
}

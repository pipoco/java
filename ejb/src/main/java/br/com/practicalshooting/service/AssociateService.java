package br.com.practicalshooting.service;

import javax.ejb.Stateless;
import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;

import br.com.practicalshooting.dao.AssociateDAO;
import br.com.practicalshooting.dao.ClubDAO;
import br.com.practicalshooting.dao.UserDAO;
import br.com.practicalshooting.model.Associate;
import br.com.practicalshooting.model.enumeration.SituationAssociate;
import br.com.practicalshooting.util.ResourceMessages;
import br.com.practicalshooting.util.Util;
import br.com.practicalshooting.util.exception.EventsException;
import br.com.practicalshooting.util.exception.MultipleEventsException;

@Stateless
public class AssociateService extends BaseService {

    /**
     * 
     */
    private static final long serialVersionUID = -2940674218463125404L;

    @Inject
    private AssociateDAO associateDAO;

    @Inject
    private UserDAO userDAO;

    @Inject
    private ClubDAO clubDAO;

    public void save(Associate associate) {
        if (Util.isPersistentEntity(associate)) {
            this.update(associate);
            return;
        }
        this.insert(associate);
    }

    private void insert(Associate associate) {
        this.validateData(associate);
        associate.setClub(this.clubDAO.findById(associate.getClub().getId()));
        associate.setUser(this.userDAO.findById(associate.getUser().getId()));
        associate.setSituationAssociate(SituationAssociate.PENDIND);
        this.associateDAO.persist(associate);
    }

    private void update(Associate associate) {
        this.validateData(associate);
        associate.setClub(this.clubDAO.findById(associate.getClub().getId()));
        associate.setUser(this.userDAO.findById(associate.getUser().getId()));
        this.associateDAO.merge(associate);
    }

    private void validateData(Associate associate) {
        MultipleEventsException errors = new MultipleEventsException();
        if (!Util.isPersistentEntity(associate.getUser())) {
            errors.add(new EventsException(ResourceMessages.getMessage("rn.field.x.empty", ResourceMessages.getMessage("lbl.user"))));
        }
        if (!Util.isPersistentEntity(associate.getClub())) {
            errors.add(new EventsException(ResourceMessages.getMessage("rn.field.x.empty", ResourceMessages.getMessage("lbl.club"))));
        }
        if (StringUtils.isBlank(associate.getCpf())) {
            errors.add(new EventsException(ResourceMessages.getMessage("rn.field.x.empty", ResourceMessages.getMessage("lbl.cpf"))));
        }
        if (associate.getRegistration() == null) {
            errors.add(new EventsException(ResourceMessages.getMessage("rn.field.x.empty", ResourceMessages.getMessage("lbl.registration"))));
        }
        if (associate.getFiliationDate() == null) {
            errors.add(new EventsException(ResourceMessages.getMessage("rn.field.x.empty", ResourceMessages.getMessage("lbl.filiation.date"))));
        }
        if (associate.getBirthDate() == null) {
            errors.add(new EventsException(ResourceMessages.getMessage("rn.field.x.empty", ResourceMessages.getMessage("lbl.birth.date"))));
        }
        errors.existsErros();
    }

    public void remove(Associate associate) {
        this.associateDAO.remove(associate);
    }
}

package br.com.practicalshooting.dao;

import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.Query;

import br.com.practicalshooting.model.ClubContact;

@Stateless
public class ClubContactDAO extends BaseDAO<ClubContact, Long> {

    /**
     * 
     */
    private static final long serialVersionUID = 388550194255638493L;

    @SuppressWarnings("unchecked")
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public List<ClubContact> searchByClub(Long clubSku) {
        StringBuilder hql = new StringBuilder(" SELECT CLCO FROM ClubContact CLCO ");
        hql.append(" WHERE CLCO.club.id = :clubSku ");
        Query query = super.entityManager.createQuery(hql.toString());
        query.setParameter("clubSku", clubSku);
        return query.getResultList();
    }
}

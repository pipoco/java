package br.com.practicalshooting.dao;

import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.Query;

import br.com.practicalshooting.model.MediaEvent;

@Stateless
public class MediaEventDAO extends BaseDAO<MediaEvent, Long> {

    /**
     * 
     */
    private static final long serialVersionUID = 388550194255638493L;

    @SuppressWarnings("unchecked")
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public List<MediaEvent> searchByEvent(Long eventSku) {
        StringBuilder hql = new StringBuilder(" SELECT MEEV FROM MediaEvent MEEV ");
        hql.append(" WHERE MEEV.event.id = :eventSku ");
        Query query = super.entityManager.createQuery(hql.toString());
        query.setParameter("eventSku", eventSku);
        return query.getResultList();
    }
}

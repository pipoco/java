package br.com.practicalshooting.dao;

import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.Query;

import br.com.practicalshooting.model.MailQueue;
import br.com.practicalshooting.model.enumeration.MailStatus;

@Stateless
public class MailQueueDAO extends BaseDAO<MailQueue, Long> {

    /**
     * 
     */
    private static final long serialVersionUID = -8912331516049797620L;

    @SuppressWarnings("unchecked")
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public List<MailQueue> listAllToSend() {
        StringBuilder hql = new StringBuilder(" SELECT MAQU FROM MailQueue MAQU ");
        hql.append(" WHERE MAQU.status = :mailStatus ");
        Query query = super.entityManager.createQuery(hql.toString());
        query.setParameter("mailStatus", MailStatus.PENDING);
        return query.getResultList();
    }
}

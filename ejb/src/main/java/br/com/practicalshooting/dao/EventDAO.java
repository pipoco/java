package br.com.practicalshooting.dao;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import org.apache.commons.lang3.StringUtils;
import org.joda.time.LocalDate;

import br.com.practicalshooting.model.Event;
import br.com.practicalshooting.model.EventDay;
import br.com.practicalshooting.model.dto.NextEventsDTO;
import br.com.practicalshooting.model.enumeration.EventType;
import br.com.practicalshooting.model.enumeration.SortOrderWrapper;
import br.com.practicalshooting.util.Util;

@Stateless
public class EventDAO extends BaseDAO<Event, Long> {

    /**
     *
     */
    private static final long serialVersionUID = -1760171677333156961L;

    @Inject
    private EventDayDAO eventDayDAO;
    @Inject
    private EventModalityDAO eventModalityDAO;
    @Inject
    private TypeParticipationDAO typeParticipationDAO;
    @Inject
    private ClubContactDAO clubContactDAO;
    @Inject
    private MediaEventDAO mediaEventDAO;

    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public Event findByName(String eventName) {
        StringBuilder hql = new StringBuilder(" SELECT C FROM Event C ");
        hql.append(" WHERE C.name = :eventName ");
        Query query = super.entityManager.createQuery(hql.toString());
        query.setParameter("eventName", eventName);
        try {
            return (Event) query.getSingleResult();
        } catch (NoResultException e) {
            LOGGER.info("expected exception", e);
        }
        return null;
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public Integer count() {
        StringBuilder hql = new StringBuilder(" SELECT COUNT(C.id) FROM Event C ");
        Query query = super.entityManager.createQuery(hql.toString());
        Long count = (Long) query.getSingleResult();
        return count == null ? 0 : count.intValue();
    }

    @SuppressWarnings("unchecked")
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public List<Event> list(Integer first, Integer pageSize, String sortField, SortOrderWrapper sortOrder) {
        StringBuilder hql = new StringBuilder(" SELECT C FROM Event C ");
        hql.append(" JOIN FETCH C.club CLUB ");
        String sortType = sortOrder == null ? "ASC" : sortOrder.name();
        String field = StringUtils.isBlank(sortField) ? "days" : sortField;
        hql.append(" ORDER BY C.").append(field).append(" ").append(sortType);
        Query query = super.entityManager.createQuery(hql.toString());
        query.setMaxResults(pageSize);
        query.setFirstResult(first);
        return query.getResultList();
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public Event findByIdFetchClub(Long eventSKU) {
        StringBuilder hql = new StringBuilder(" SELECT C FROM Event C ");
        hql.append(" JOIN FETCH C.club CLUB ");
        hql.append(" WHERE C.id = :eventSKU ");
        Query query = super.entityManager.createQuery(hql.toString());
        query.setParameter("eventSKU", eventSKU);
        try {
            Event event = (Event) query.getSingleResult();
            event.setEventDays(this.eventDayDAO.listByEvent(event.getId()));
            event.setModalities(this.eventModalityDAO.listByEvent(event.getId()));
            for (EventDay eventDay : event.getEventDays()) {
                eventDay.setParticipations(this.typeParticipationDAO.listByEventDay(eventDay.getId()));
            }
            return event;
        } catch (NoResultException e) {
            LOGGER.info("expected exception", e);
        }
        return null;
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public NextEventsDTO findByIdForDetail(Long eventSKU) {
        StringBuilder hql = new StringBuilder(" SELECT EVEN FROM Event EVEN ");
        hql.append(" JOIN FETCH EVEN.club CLUB ");
        hql.append(" WHERE EVEN.id = :eventSKU ");
        Query query = super.entityManager.createQuery(hql.toString());
        query.setParameter("eventSKU", eventSKU);
        try {
            Event event = (Event) query.getSingleResult();
            for (EventDay eventDay : event.getEventDays()) {
                eventDay.setParticipations(this.typeParticipationDAO.listByEventDay(eventDay.getId()));
            }
            NextEventsDTO eventDTO = new NextEventsDTO(event, this.eventModalityDAO.listByEvent(event.getId()), this.clubContactDAO.searchByClub(event.getClub().getId()), this.mediaEventDAO.searchByEvent(event.getId()));
            return eventDTO;
        } catch (NoResultException e) {
            LOGGER.info("expected exception", e);
        }
        return null;
    }

    public int countNextEvents() {
        StringBuilder hql = new StringBuilder(" SELECT COUNT(EVEN.id) FROM Event EVEN ");
        hql.append(" WHERE EXISTS ( ");
        hql.append("     SELECT CURDATE() FROM EventDay EVDA ");
        hql.append("     WHERE EVDA.event.id = EVEN.id ");
        hql.append("     AND EVDA.day >= DATE(CURDATE()) ");
        hql.append(" ) ");
        Query query = super.entityManager.createQuery(hql.toString());
        Long count = (Long) query.getSingleResult();
        return count == null ? 0 : count.intValue();
    }

    @SuppressWarnings("unchecked")
    public List<NextEventsDTO> listNextEvents(int first, int pageSize, String filterName, Long filterClub, EventType filterType, LocalDate filterDayFrom, LocalDate filterDayTo) {
        StringBuilder hql = new StringBuilder(" SELECT EVEN FROM Event EVEN ");
        hql.append(" JOIN FETCH EVEN.club CLUB ");
        hql.append(" WHERE EXISTS ( ");
        hql.append("     SELECT CURDATE() FROM EventDay EVDA ");
        hql.append("     WHERE EVDA.event.id = EVEN.id ");
        Util.appendValueInQuery(hql, filterDayFrom != null, " AND EVDA.day >= :filterDayFrom ");
        Util.appendValueInQuery(hql, filterDayTo != null, " AND EVDA.day <= :filterDayTo ");
        Util.appendValueInQuery(hql, (filterDayFrom == null) && (filterDayTo == null), " AND EVDA.day >= DATE(CURDATE()) ");
        hql.append(" ) ");
        Util.appendValueInQuery(hql, StringUtils.isNotBlank(filterName), " AND LOWER(EVEN.name) LIKE :filterName ");
        Util.appendValueInQuery(hql, (filterClub != null) && (filterClub > 0), " AND CLUB.id = :filterClub ");
        Util.appendValueInQuery(hql, filterType != null, " AND EVEN.eventType = :filterType ");
        hql.append(" ORDER BY EVEN.firstDay ASC ");
        Query query = super.entityManager.createQuery(hql.toString());
        Util.appendParamInQuery(query, hql, "filterName", filterName);
        Util.appendParamInQuery(query, hql, "filterClub", filterClub);
        Util.appendParamInQuery(query, hql, "filterType", filterType);
        Util.appendParamInQuery(query, hql, "filterDayFrom", filterDayFrom);
        Util.appendParamInQuery(query, hql, "filterDayTo", filterDayTo);
        query.setMaxResults(pageSize);
        query.setFirstResult(first);
        List<NextEventsDTO> results = new ArrayList<NextEventsDTO>();
        List<Event> events = query.getResultList();
        for (Event event : events) {
            results.add(new NextEventsDTO(event, this.eventModalityDAO.listByEvent(event.getId()), this.clubContactDAO.searchByClub(event.getClub().getId()), this.mediaEventDAO.searchByEvent(event.getId())));
        }
        return results;
    }

    @SuppressWarnings("unchecked")
    public List<NextEventsDTO> listAllEvents(String filterName, Long filterClub, EventType filterType, LocalDate filterDayFrom, LocalDate filterDayTo) {
        StringBuilder hql = new StringBuilder(" SELECT EVEN FROM Event EVEN ");
        hql.append(" JOIN FETCH EVEN.club CLUB ");
        hql.append(" WHERE EXISTS ( ");
        hql.append("     SELECT CURDATE() FROM EventDay EVDA ");
        hql.append("     WHERE EVDA.event.id = EVEN.id ");
        Util.appendValueInQuery(hql, filterDayFrom != null, " AND EVDA.day >= :filterDayFrom ");
        Util.appendValueInQuery(hql, filterDayTo != null, " AND EVDA.day <= :filterDayTo ");
        Util.appendValueInQuery(hql, (filterDayFrom == null) && (filterDayTo == null), " AND EVDA.day >= DATE(CURDATE()) ");
        hql.append(" ) ");
        Util.appendValueInQuery(hql, StringUtils.isNotBlank(filterName), " AND LOWER(EVEN.name) LIKE :filterName ");
        Util.appendValueInQuery(hql, (filterClub != null) && (filterClub > 0), " AND CLUB.id = :filterClub ");
        Util.appendValueInQuery(hql, filterType != null, " AND EVEN.eventType = :filterType ");
        hql.append(" ORDER BY EVEN.firstDay ASC ");
        Query query = super.entityManager.createQuery(hql.toString());
        Util.appendParamInQuery(query, hql, "filterName", "%" + filterName + "%");
        Util.appendParamInQuery(query, hql, "filterClub", filterClub);
        Util.appendParamInQuery(query, hql, "filterType", filterType);
        Util.appendParamInQuery(query, hql, "filterDayFrom", filterDayFrom);
        Util.appendParamInQuery(query, hql, "filterDayTo", filterDayTo);
        List<NextEventsDTO> results = new ArrayList<NextEventsDTO>();
        List<Event> events = query.getResultList();
        for (Event event : events) {
            results.add(new NextEventsDTO(event, this.eventModalityDAO.listByEvent(event.getId()), this.clubContactDAO.searchByClub(event.getClub().getId()), this.mediaEventDAO.searchByEvent(event.getId())));
        }
        return results;
    }
}

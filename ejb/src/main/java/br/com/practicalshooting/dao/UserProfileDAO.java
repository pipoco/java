package br.com.practicalshooting.dao;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import br.com.practicalshooting.security.model.Profile;
import br.com.practicalshooting.security.model.UserProfile;

@Stateless
public class UserProfileDAO extends BaseDAO<UserProfile, Long> {

    /**
     *
     */
    private static final long serialVersionUID = 5017464348506670039L;

    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public Profile findProfileByUser(Long userSku) {
        StringBuilder hql = new StringBuilder(" SELECT PROF FROM UserProfile USPR ");
        hql.append(" JOIN USPR.user USER ");
        hql.append(" JOIN USPR.profile PROF ");
        hql.append(" WHERE USER.id = :userSku ");
        Query query = super.entityManager.createQuery(hql.toString());
        query.setParameter("userSku", userSku);
        try {
            return (Profile) query.getSingleResult();
        } catch (NoResultException e) {
            LOGGER.info("expected exception", e);
        }
        return null;
    }
}

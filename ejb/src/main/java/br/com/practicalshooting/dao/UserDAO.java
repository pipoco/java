package br.com.practicalshooting.dao;

import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import org.apache.commons.lang3.StringUtils;

import br.com.practicalshooting.model.User;
import br.com.practicalshooting.model.enumeration.Situation;
import br.com.practicalshooting.model.enumeration.SortOrderWrapper;

@Stateless
public class UserDAO extends BaseDAO<User, Long> {

    /**
     *
     */
    private static final long serialVersionUID = -7644324404463462767L;

    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public User findByName(String userName) {
        StringBuilder hql = new StringBuilder(" SELECT USER FROM User USER ");
        hql.append(" LEFT JOIN FETCH USER.associate ASSO ");
        hql.append(" WHERE USER.name = :userName ");
        hql.append(" AND USER.situation = :situUser ");
        Query query = super.entityManager.createQuery(hql.toString());
        query.setParameter("userName", userName);
        query.setParameter("situUser", Situation.ACTIVE);
        try {
            return (User) query.getSingleResult();
        } catch (NoResultException e) {
            LOGGER.info("expected exception", e);
        }
        return null;
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public User findActiveUserByEmail(String userEmail) {
        StringBuilder hql = new StringBuilder(" SELECT USER FROM User USER ");
        hql.append(" LEFT JOIN FETCH USER.associate ASSO ");
        hql.append(" WHERE USER.email = :userEmail ");
        hql.append(" AND USER.situation = :situUser ");
        Query query = super.entityManager.createQuery(hql.toString());
        query.setParameter("userEmail", userEmail);
        query.setParameter("situUser", Situation.ACTIVE);
        try {
            return (User) query.getSingleResult();
        } catch (NoResultException e) {
            LOGGER.info("expected exception", e);
        }
        return null;
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public User findByEmailForRegisterOrUpdate(String userEmail) {
        StringBuilder hql = new StringBuilder(" SELECT USER FROM User USER ");
        hql.append(" LEFT JOIN FETCH USER.associate ASSO ");
        hql.append(" WHERE USER.email = :userEmail ");
        // hql.append(" AND (USER.situation = :situUserActive OR USER.situation
        // = :situUserInactive) ");
        Query query = super.entityManager.createQuery(hql.toString());
        query.setParameter("userEmail", userEmail);
        // query.setParameter("situUserActive", Situation.ACTIVE);
        // query.setParameter("situUserInactive", Situation.INACTIVE);
        try {
            return (User) query.getSingleResult();
        } catch (NoResultException e) {
            LOGGER.info("expected exception", e);
        }
        return null;
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public User findByEmail(String userEmail) {
        StringBuilder hql = new StringBuilder(" SELECT USER FROM User USER ");
        hql.append(" LEFT JOIN FETCH USER.associate ASSO ");
        hql.append(" WHERE USER.email = :userEmail ");
        Query query = super.entityManager.createQuery(hql.toString());
        query.setParameter("userEmail", userEmail);
        try {
            return (User) query.getSingleResult();
        } catch (NoResultException e) {
            LOGGER.info("expected exception", e);
        }
        return null;
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public User findByEmailAndPassword(String userEmail, String userPassword) {
        if (StringUtils.isBlank(userEmail) || StringUtils.isBlank(userPassword)) {
            return null;
        }
        StringBuilder hql = new StringBuilder(" SELECT USER FROM User USER ");
        hql.append(" LEFT JOIN FETCH USER.associate ASSO ");
        hql.append(" LEFT JOIN FETCH ASSO.club CLUB ");
        hql.append(" WHERE USER.email = :userEmail ");
        hql.append(" AND USER.password = :userPassword ");
        hql.append(" AND USER.situation = :situUser ");
        Query query = super.entityManager.createQuery(hql.toString());
        query.setParameter("userEmail", userEmail);
        query.setParameter("userPassword", userPassword);
        query.setParameter("situUser", Situation.ACTIVE);
        try {
            return (User) query.getSingleResult();
        } catch (NoResultException e) {
            LOGGER.info("expected exception", e);
        }
        return null;
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public Integer count() {
        StringBuilder hql = new StringBuilder(" SELECT COUNT(USER.id) FROM User USER ");
        Query query = super.entityManager.createQuery(hql.toString());
        Long count = (Long) query.getSingleResult();
        return count == null ? 0 : count.intValue();
    }

    @SuppressWarnings("unchecked")
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public List<User> list(Integer first, Integer pageSize, String sortField, SortOrderWrapper sortOrder) {
        StringBuilder hql = new StringBuilder(" SELECT USER FROM User USER ");
        hql.append(" LEFT JOIN FETCH USER.associate ASSO ");
        hql.append(" LEFT JOIN FETCH ASSO.club CLUB ");
        hql.append(" WHERE USER.situation != :situUser ");
        String sortType = sortOrder == null ? "ASC" : sortOrder.name();
        String field = StringUtils.isBlank(sortField) ? "name" : sortField;
        hql.append(" ORDER BY USER.").append(field).append(" ").append(sortType);
        Query query = super.entityManager.createQuery(hql.toString());
        query.setParameter("situUser", Situation.CANCELED);
        query.setMaxResults(pageSize);
        query.setFirstResult(first);
        return query.getResultList();
    }

    public User findByIdFetchAssociate(Long userSKU) {
        StringBuilder hql = new StringBuilder(" SELECT USER FROM User USER ");
        hql.append(" LEFT JOIN FETCH USER.associate ASSO ");
        hql.append(" LEFT JOIN FETCH ASSO.club CLUB ");
        hql.append(" WHERE USER.id = :userSKU ");
        hql.append(" AND USER.situation = :situUser ");
        Query query = super.entityManager.createQuery(hql.toString());
        query.setParameter("userSKU", userSKU);
        query.setParameter("situUser", Situation.ACTIVE);
        try {
            return (User) query.getSingleResult();
        } catch (NoResultException e) {
            LOGGER.info("expected exception", e);
        }
        return null;
    }

    public User selectUserByPasswordToken(String confirmPasswordToken) {
        StringBuilder hql = new StringBuilder(" SELECT USER FROM User USER ");
        hql.append(" LEFT JOIN FETCH USER.associate ASSO ");
        hql.append(" WHERE USER.confirmPasswordToken = :confirmPasswordToken ");
        hql.append(" AND (USER.situation = :situUserActive OR USER.situation = :situUserInactive) ");
        Query query = super.entityManager.createQuery(hql.toString());
        query.setParameter("confirmPasswordToken", confirmPasswordToken);
        query.setParameter("situUserActive", Situation.ACTIVE);
        query.setParameter("situUserInactive", Situation.INACTIVE);
        try {
            return (User) query.getSingleResult();
        } catch (NoResultException e) {
            LOGGER.info("expected exception", e);
        }
        return null;
    }

    public User selectUserByRegisterToken(String confirmRegisterToken) {
        StringBuilder hql = new StringBuilder(" SELECT USER FROM User USER ");
        hql.append(" LEFT JOIN FETCH USER.associate ASSO ");
        hql.append(" WHERE USER.confirmRegisterToken = :confirmRegisterToken ");
        hql.append(" AND USER.situation = :situUserPending ");
        Query query = super.entityManager.createQuery(hql.toString());
        query.setParameter("confirmRegisterToken", confirmRegisterToken);
        query.setParameter("situUserPending", Situation.PENDING);
        try {
            return (User) query.getSingleResult();
        } catch (NoResultException e) {
            LOGGER.info("expected exception", e);
        }
        return null;
    }
}

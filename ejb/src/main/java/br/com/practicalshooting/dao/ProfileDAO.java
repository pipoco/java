package br.com.practicalshooting.dao;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import br.com.practicalshooting.security.model.Profile;

@Stateless
public class ProfileDAO extends BaseDAO<Profile, Long> {

    /**
     *
     */
    private static final long serialVersionUID = 5017464348506670039L;

    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public Profile findByName(String name) {
        StringBuilder hql = new StringBuilder(" SELECT PROF FROM Profile PROF ");
        hql.append(" WHERE PROF.name = :name ");
        Query query = super.entityManager.createQuery(hql.toString());
        query.setParameter("name", name);
        try {
            return (Profile) query.getSingleResult();
        } catch (NoResultException e) {
            LOGGER.info("expected exception", e);
        }
        return null;
    }
}

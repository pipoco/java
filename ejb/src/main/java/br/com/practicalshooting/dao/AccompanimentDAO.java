package br.com.practicalshooting.dao;

import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import org.apache.commons.lang3.StringUtils;

import br.com.practicalshooting.model.Accompaniment;
import br.com.practicalshooting.model.enumeration.SortOrderWrapper;

@Stateless
public class AccompanimentDAO extends BaseDAO<Accompaniment, Long> {

    /**
     * 
     */
    private static final long serialVersionUID = -1760171677333156961L;

    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public Accompaniment findByUser(Long userSKU) {
        StringBuilder hql = new StringBuilder(" SELECT C FROM Accompaniment C ");
        hql.append(" JOIN FETCH C.event EVEN ");
        hql.append(" WHERE C.user.id = :userSKU ");
        Query query = super.entityManager.createQuery(hql.toString());
        query.setParameter("userSKU", userSKU);
        try {
            return (Accompaniment) query.getSingleResult();
        } catch (NoResultException e) {
            LOGGER.info("expected exception", e);
        }
        return null;
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public Integer count(Long userSKU) {
        StringBuilder hql = new StringBuilder(" SELECT COUNT(C.id) FROM Accompaniment C ");
        hql.append(" WHERE C.user.id = :userSKU ");
        Query query = super.entityManager.createQuery(hql.toString());
        query.setParameter("userSKU", userSKU);
        Long count = (Long) query.getSingleResult();
        return count == null ? 0 : count.intValue();
    }

    @SuppressWarnings("unchecked")
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public List<Accompaniment> list(Integer first, Integer pageSize, String sortField, SortOrderWrapper sortOrder, Long userSKU) {
        StringBuilder hql = new StringBuilder(" SELECT C FROM Accompaniment C ");
        hql.append(" JOIN FETCH C.event EVEN ");
        hql.append(" WHERE C.user.id = :userSKU ");
        String sortType = sortOrder == null ? "ASC" : sortOrder.name();
        String field = StringUtils.isBlank(sortField) ? "id" : sortField;
        hql.append(" ORDER BY C.").append(field).append(" ").append(sortType);
        Query query = super.entityManager.createQuery(hql.toString());
        query.setParameter("userSKU", userSKU);
        query.setMaxResults(pageSize);
        query.setFirstResult(first);
        return query.getResultList();
    }

}

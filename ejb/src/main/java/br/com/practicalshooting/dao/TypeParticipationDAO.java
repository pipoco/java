package br.com.practicalshooting.dao;

import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.Query;

import br.com.practicalshooting.model.TypeParticipation;

@Stateless
public class TypeParticipationDAO extends BaseDAO<TypeParticipation, Long> {

    /**
     * 
     */
    private static final long serialVersionUID = -1760171677333156961L;

    @SuppressWarnings("unchecked")
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public List<TypeParticipation> listByEventDay(Long eventSku) {
        StringBuilder hql = new StringBuilder(" SELECT ED FROM TypeParticipation ED ");
        hql.append(" JOIN FETCH ED.type TYPA ");
        hql.append(" WHERE ED.eventDay.id = :eventSku ");
        Query query = super.entityManager.createQuery(hql.toString());
        query.setParameter("eventSku", eventSku);
        return query.getResultList();
    }

}

package br.com.practicalshooting.dao;

import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.Query;

import br.com.practicalshooting.model.WeaponOfCourse;

@Stateless
public class WeaponOfCourseDAO extends BaseDAO<WeaponOfCourse, Long> {

    /**
     * 
     */
    private static final long serialVersionUID = -1760171677333156961L;

    @SuppressWarnings("unchecked")
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public List<WeaponOfCourse> listByCandidate(Long candidateSku) {
        StringBuilder hql = new StringBuilder(" SELECT WECO FROM WeaponOfCourse WECO ");
        hql.append(" WHERE WECO.candidate.id = :candidateSku ");
        Query query = super.entityManager.createQuery(hql.toString());
        query.setParameter("candidateSku", candidateSku);
        return query.getResultList();
    }
}

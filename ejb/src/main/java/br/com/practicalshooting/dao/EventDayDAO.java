package br.com.practicalshooting.dao;

import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.Query;

import br.com.practicalshooting.model.EventDay;

@Stateless
public class EventDayDAO extends BaseDAO<EventDay, Long> {

    /**
     * 
     */
    private static final long serialVersionUID = -1760171677333156961L;

    @SuppressWarnings("unchecked")
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public List<EventDay> listByEvent(Long eventSku) {
        StringBuilder hql = new StringBuilder(" SELECT ED FROM EventDay ED ");
        hql.append(" WHERE ED.event.id = :eventSku ");
        Query query = super.entityManager.createQuery(hql.toString());
        query.setParameter("eventSku", eventSku);
        return query.getResultList();
    }
}

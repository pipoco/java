package br.com.practicalshooting.dao;

import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.Query;

import br.com.practicalshooting.model.InstructorContact;

@Stateless
public class InstructorContactDAO extends BaseDAO<InstructorContact, Long> {

    /**
     * 
     */
    private static final long serialVersionUID = 388550194255638493L;

    @SuppressWarnings("unchecked")
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public List<InstructorContact> searchByClub(Long instructorSku) {
        StringBuilder hql = new StringBuilder(" SELECT INCO FROM InstructorContact INCO ");
        hql.append(" WHERE INCO.instructor.id = :instructorSku ");
        Query query = super.entityManager.createQuery(hql.toString());
        query.setParameter("instructorSku", instructorSku);
        return query.getResultList();
    }
}

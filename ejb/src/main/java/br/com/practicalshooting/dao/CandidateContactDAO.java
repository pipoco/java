package br.com.practicalshooting.dao;

import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.Query;

import br.com.practicalshooting.model.CandidateContact;

@Stateless
public class CandidateContactDAO extends BaseDAO<CandidateContact, Long> {

    /**
     * 
     */
    private static final long serialVersionUID = 574301658025888204L;

    @SuppressWarnings("unchecked")
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public List<CandidateContact> searchByCandidate(Long candidateSku) {
        StringBuilder hql = new StringBuilder(" SELECT CACO FROM CandidateContact CACO ");
        hql.append(" WHERE CACO.candidate.id = :candidateSku ");
        Query query = super.entityManager.createQuery(hql.toString());
        query.setParameter("candidateSku", candidateSku);
        return query.getResultList();
    }
}

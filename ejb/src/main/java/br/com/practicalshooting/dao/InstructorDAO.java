package br.com.practicalshooting.dao;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import br.com.practicalshooting.model.Instructor;
import br.com.practicalshooting.model.dto.InstructorDTO;

@Stateless
public class InstructorDAO extends BaseDAO<Instructor, Long> {

    /**
     * 
     */
    private static final long serialVersionUID = 5419716796640190827L;

    @Inject
    private InstructorContactDAO instructorContactDAO;

    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public Instructor findByIdWithUser(Long instructorSku) {
        StringBuilder hql = new StringBuilder();
        hql.append(" SELECT INST FROM Instructor INST ");
        hql.append(" JOIN FETCH INST.user USER ");
        hql.append(" WHERE INST.id = :instructorSku ");
        Query query = super.entityManager.createQuery(hql.toString());
        query.setParameter("instructorSku", instructorSku);
        try {
            return (Instructor) query.getSingleResult();
        } catch (NoResultException e) {
            LOGGER.info("expected exception", e);
        }
        return null;
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public Instructor findByUserIdFetchUser(Long userSku) {
        StringBuilder hql = new StringBuilder();
        hql.append(" SELECT INST FROM Instructor INST ");
        hql.append(" JOIN FETCH INST.user USER ");
        hql.append(" WHERE USER.id = :userSku ");
        Query query = super.entityManager.createQuery(hql.toString());
        query.setParameter("userSku", userSku);
        try {
            return (Instructor) query.getSingleResult();
        } catch (NoResultException e) {
            LOGGER.info("expected exception", e);
        }
        return null;
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public Integer count() {
        StringBuilder hql = new StringBuilder(" SELECT COUNT(INST.id) FROM Instructor INST ");
        hql.append(" JOIN INST.user USER ");
        Query query = super.entityManager.createQuery(hql.toString());
        Long count = (Long) query.getSingleResult();
        return count == null ? 0 : count.intValue();
    }

    @SuppressWarnings("unchecked")
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public List<InstructorDTO> list(Integer first, Integer pageSize) {
        StringBuilder hql = new StringBuilder(" SELECT INST FROM Instructor INST ");
        hql.append(" JOIN FETCH INST.user USER ");
        hql.append(" LEFT JOIN FETCH INST.address ADDR ");
        hql.append(" ORDER BY USER.name ASC ");
        Query query = super.entityManager.createQuery(hql.toString());
        query.setMaxResults(pageSize);
        query.setFirstResult(first);
        List<InstructorDTO> results = new ArrayList<InstructorDTO>();
        List<Instructor> instructors = query.getResultList();
        for (Instructor instructor : instructors) {
            results.add(new InstructorDTO(instructor, this.instructorContactDAO.searchByClub(instructor.getId())));
        }
        return results;
    }

    @SuppressWarnings("unchecked")
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public List<InstructorDTO> listAllWithContacts() {
        StringBuilder hql = new StringBuilder(" SELECT INST FROM Instructor INST ");
        hql.append(" JOIN FETCH INST.user USER ");
        hql.append(" LEFT JOIN FETCH INST.address ADDR ");
        hql.append(" ORDER BY USER.name ASC ");
        Query query = super.entityManager.createQuery(hql.toString());
        List<InstructorDTO> results = new ArrayList<InstructorDTO>();
        List<Instructor> instructors = query.getResultList();
        for (Instructor instructor : instructors) {
            results.add(new InstructorDTO(instructor, this.instructorContactDAO.searchByClub(instructor.getId())));
        }
        return results;
    }
}

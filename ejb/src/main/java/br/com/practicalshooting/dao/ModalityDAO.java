package br.com.practicalshooting.dao;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import org.apache.commons.lang3.StringUtils;

import br.com.practicalshooting.model.Modality;

@Stateless
public class ModalityDAO extends BaseDAO<Modality, Long> {

    /**
     * 
     */
    private static final long serialVersionUID = -1760171677333156961L;

    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public Modality findByName(String modalityName) {
        StringBuilder hql = new StringBuilder(" SELECT C FROM Modality C ");
        hql.append(" WHERE C.name = :modalityName ");
        Query query = super.entityManager.createQuery(hql.toString());
        query.setParameter("modalityName", modalityName);
        try {
            return (Modality) query.getSingleResult();
        } catch (NoResultException e) {
            LOGGER.info("expected exception", e);
        }
        return null;
    }

    @SuppressWarnings("unchecked")
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public List<Modality> listAllContainsNameFetchOnlyNameAndSku(String partOfName) {
        if (StringUtils.isBlank(partOfName)) {
            return new ArrayList<Modality>();
        }
        StringBuilder hql = new StringBuilder(" SELECT C FROM Modality C ");
        // TODO: Lembrar de criar um indice no banco de dados com o nome
        // convertido
        hql.append(" WHERE LOWER(C.name) like :partOfName OR LOWER(C.description) like :partOfDescription ");
        hql.append(" ORDER BY C.name ASC");
        Query query = super.entityManager.createQuery(hql.toString());
        query.setParameter("partOfName", "%" + partOfName + "%");
        query.setParameter("partOfDescription", "%" + partOfName + "%");
        return query.getResultList();
    }

}

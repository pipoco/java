package br.com.practicalshooting.dao;

import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.Query;

import br.com.practicalshooting.security.model.Permission;

@Stateless
public class PermissionDAO extends BaseDAO<Permission, Long> {

    /**
     *
     */
    private static final long serialVersionUID = 128186921492932723L;

    @SuppressWarnings("unchecked")
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public List<Permission> listPermissionByUser(Long perfSku) {
        StringBuilder hql = new StringBuilder(" SELECT PERM FROM Permission PERM ");
        hql.append(" JOIN FETCH PERM.profile PROF ");
        hql.append(" JOIN FETCH PERM.action ACTI ");
        hql.append(" JOIN FETCH ACTI.function FUNC ");
        hql.append(" WHERE PROF.id = :perfSku ");
        Query query = super.entityManager.createQuery(hql.toString());
        query.setParameter("perfSku", perfSku);
        return query.getResultList();
    }
}

package br.com.practicalshooting.dao;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.Query;

import br.com.practicalshooting.model.QuartzConfiguration;

@Stateless
public class QuartzConfigurationDAO extends BaseDAO<QuartzConfiguration, Long> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3542587259715098120L;

	@SuppressWarnings("unchecked")
	public List<QuartzConfiguration> listAllAutoJobs() {
		StringBuilder hql = new StringBuilder();
		hql.append(" SELECT QTCO FROM QuartzConfiguration QTCO ");
		hql.append(" WHERE QTCO.autoConfig = :flgConfig ");
		Query query = super.entityManager.createQuery(hql.toString());
		query.setParameter("flgConfig", Boolean.TRUE);
		return query.getResultList();
	}

	public QuartzConfiguration getByName(String nomeJob) {
		StringBuilder hql = new StringBuilder();
		hql.append(" SELECT QTCO FROM QuartzConfiguration QTCO ");
		hql.append(" WHERE QTCO.name = :nomeJob ");
		Query query = super.entityManager.createQuery(hql.toString());
		query.setParameter("nomeJob", nomeJob);
		return (QuartzConfiguration) query.getSingleResult();
	}

	public QuartzConfiguration getByClass(String nameClass) {
		StringBuilder hql = new StringBuilder();
		hql.append(" SELECT QTCO FROM QuartzConfiguration QTCO ");
		hql.append(" WHERE QTCO.jobClassName = :nameClass ");
		Query query = super.entityManager.createQuery(hql.toString());
		query.setParameter("nameClass", nameClass);
		return (QuartzConfiguration) query.getSingleResult();
	}
}

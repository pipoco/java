package br.com.practicalshooting.dao;

import javax.ejb.Stateless;

import br.com.practicalshooting.model.ErrorWrapper;

@Stateless
public class ErrorWrapperDAO extends BaseDAO<ErrorWrapper, Long> {

    private static final long serialVersionUID = -5486161750267222139L;

}

package br.com.practicalshooting.dao;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import org.apache.commons.lang3.StringUtils;

import br.com.practicalshooting.model.Candidate;
import br.com.practicalshooting.model.enumeration.SortOrderWrapper;

@Stateless
public class CandidateDAO extends BaseDAO<Candidate, Long> {

    /**
     * 
     */
    private static final long serialVersionUID = 3235544598615597063L;
    @Inject
    private CandidateContactDAO candidateContactDAO;
    @Inject
    private WeaponOfCourseDAO weaponOfCourseDAO;

    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public Candidate findByIdFetchUserInstructorContactsAndWeapons(Long candidateSku) {
        StringBuilder hql = new StringBuilder();
        hql.append(" SELECT CAND FROM Candidate CAND ");
        hql.append(" JOIN FETCH CAND.user USER ");
        hql.append(" JOIN FETCH CAND.instructor INST ");
        hql.append(" JOIN FETCH INST.user INUS ");
        hql.append(" WHERE CAND.id = :candidateSku ");
        Query query = super.entityManager.createQuery(hql.toString());
        query.setParameter("candidateSku", candidateSku);
        try {
            Candidate candidate = (Candidate) query.getSingleResult();
            candidate.setContacts(this.candidateContactDAO.searchByCandidate(candidate.getId()));
            candidate.setWeapons(this.weaponOfCourseDAO.listByCandidate(candidate.getId()));
            return candidate;
        } catch (NoResultException e) {
            LOGGER.info("expected exception", e);
        }
        return null;
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public Candidate findByName(String candidateName) {
        StringBuilder hql = new StringBuilder(" SELECT CAND FROM Candidate CAND ");
        hql.append(" JOIN FETCH CAND.user USER ");
        hql.append(" JOIN FETCH CAND.instructor INST ");
        hql.append(" JOIN FETCH INST.user INUS ");
        hql.append(" WHERE USER.name = :candidateName ");
        Query query = super.entityManager.createQuery(hql.toString());
        query.setParameter("candidateName", candidateName);
        try {
            return (Candidate) query.getSingleResult();
        } catch (NoResultException e) {
            LOGGER.info("expected exception", e);
        }
        return null;
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public Candidate findByIdFetchUserAndInstructor(Long candSKU) {
        StringBuilder hql = new StringBuilder(" SELECT CAND FROM Candidate CAND ");
        hql.append(" JOIN FETCH CAND.user USER ");
        hql.append(" JOIN FETCH CAND.instructor INST ");
        hql.append(" JOIN FETCH INST.user INUS ");
        hql.append(" WHERE CAND.id = :candSKU ");
        Query query = super.entityManager.createQuery(hql.toString());
        query.setParameter("candSKU", candSKU);
        try {
            return (Candidate) query.getSingleResult();
        } catch (NoResultException e) {
            LOGGER.info("expected exception", e);
        }
        return null;
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public Integer count() {
        StringBuilder hql = new StringBuilder(" SELECT COUNT(CAND.id) FROM Candidate CAND ");
        hql.append(" JOIN CAND.user USER ");
        hql.append(" JOIN CAND.instructor INST ");
        Query query = super.entityManager.createQuery(hql.toString());
        Long count = (Long) query.getSingleResult();
        return count == null ? 0 : count.intValue();
    }

    @SuppressWarnings("unchecked")
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public List<Candidate> list(Integer first, Integer pageSize, String sortField, SortOrderWrapper sortOrder) {
        StringBuilder hql = new StringBuilder(" SELECT CAND FROM Candidate CAND ");
        hql.append(" JOIN FETCH CAND.user USER ");
        hql.append(" JOIN FETCH CAND.instructor INST ");
        hql.append(" JOIN FETCH INST.user INUS ");
        String sortType = sortOrder == null ? "ASC" : sortOrder.name();
        String field = StringUtils.isBlank(sortField) ? "user.name" : sortField;
        hql.append(" ORDER BY CAND.").append(field).append(" ").append(sortType);
        Query query = super.entityManager.createQuery(hql.toString());
        query.setMaxResults(pageSize);
        query.setFirstResult(first);
        return query.getResultList();
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public int countByUserId(Long userSKU) {
        if (userSKU == null) {
            return 0;
        }
        StringBuilder hql = new StringBuilder(" SELECT COUNT(CAND.id) FROM Candidate CAND ");
        hql.append(" JOIN CAND.user USER ");
        hql.append(" JOIN CAND.instructor INST ");
        hql.append(" WHERE USER.id = :userSKU ");
        Query query = super.entityManager.createQuery(hql.toString());
        query.setParameter("userSKU", userSKU);
        Long count = (Long) query.getSingleResult();
        return count == null ? 0 : count.intValue();
    }

    @SuppressWarnings("unchecked")
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public List<Candidate> listByUserIdFetchUserAndInstructor(Integer first, Integer pageSize, String sortField, SortOrderWrapper sortOrder, Long userSKU) {
        if (userSKU == null) {
            return new ArrayList<Candidate>();
        }
        StringBuilder hql = new StringBuilder(" SELECT CAND FROM Candidate CAND ");
        hql.append(" JOIN FETCH CAND.user USER ");
        hql.append(" JOIN FETCH CAND.instructor INST ");
        hql.append(" JOIN FETCH INST.user INUS ");
        hql.append(" WHERE USER.id = :userSKU ");
        String sortType = sortOrder == null ? "DESC" : sortOrder.name();
        String field = StringUtils.isBlank(sortField) ? "creationDate" : sortField;
        hql.append(" ORDER BY CAND.").append(field).append(" ").append(sortType);
        Query query = super.entityManager.createQuery(hql.toString());
        query.setParameter("userSKU", userSKU);
        query.setMaxResults(pageSize);
        query.setFirstResult(first);
        return query.getResultList();
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public int countByInstructorId(Long instructorSKU) {
        if (instructorSKU == null) {
            return 0;
        }
        StringBuilder hql = new StringBuilder(" SELECT COUNT(CAND.id) FROM Candidate CAND ");
        hql.append(" JOIN CAND.user USER ");
        hql.append(" JOIN CAND.instructor INST ");
        hql.append(" JOIN INST.user INUS ");
        hql.append(" WHERE INST.id = :instructorSKU ");
        Query query = super.entityManager.createQuery(hql.toString());
        query.setParameter("instructorSKU", instructorSKU);
        Long count = (Long) query.getSingleResult();
        return count == null ? 0 : count.intValue();
    }

    @SuppressWarnings("unchecked")
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public List<Candidate> listByInstructorIdFetchUserAndInstructor(int first, int pageSize, String sortField, SortOrderWrapper sortOrder, Long instructorSKU) {
        if (instructorSKU == null) {
            return new ArrayList<Candidate>();
        }
        StringBuilder hql = new StringBuilder(" SELECT CAND FROM Candidate CAND ");
        hql.append(" JOIN FETCH CAND.user USER ");
        hql.append(" JOIN FETCH CAND.instructor INST ");
        hql.append(" JOIN FETCH INST.user INUS ");
        hql.append(" WHERE INST.id = :instructorSKU ");
        String sortType = sortOrder == null ? "DESC" : sortOrder.name();
        String field = StringUtils.isBlank(sortField) ? "creationDate" : sortField;
        hql.append(" ORDER BY CAND.").append(field).append(" ").append(sortType);
        Query query = super.entityManager.createQuery(hql.toString());
        query.setParameter("instructorSKU", instructorSKU);
        query.setMaxResults(pageSize);
        query.setFirstResult(first);
        return query.getResultList();
    }
}

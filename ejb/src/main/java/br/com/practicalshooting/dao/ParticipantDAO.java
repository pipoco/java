package br.com.practicalshooting.dao;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import org.apache.commons.lang3.StringUtils;

import br.com.practicalshooting.model.Participant;

@Stateless
public class ParticipantDAO extends BaseDAO<Participant, Long> {

    /**
     * 
     */
    private static final long serialVersionUID = -1760171677333156961L;

    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public Participant findByName(String participantName) {
        StringBuilder hql = new StringBuilder(" SELECT C FROM Participant C ");
        hql.append(" WHERE C.name = :participantName ");
        Query query = super.entityManager.createQuery(hql.toString());
        query.setParameter("participantName", participantName);
        try {
            return (Participant) query.getSingleResult();
        } catch (NoResultException e) {
            LOGGER.info("expected exception", e);
        }
        return null;
    }

    @SuppressWarnings("unchecked")
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public List<Participant> listAllContainsNameFetchOnlyNameAndSku(String partOfName) {
        if (StringUtils.isBlank(partOfName)) {
            return new ArrayList<Participant>();
        }
        StringBuilder hql = new StringBuilder(" SELECT C FROM Participant C ");
        // TODO: Lembrar de criar um indice no banco de dados com o nome
        // convertido
        hql.append(" WHERE LOWER(C.name) like :partOfName OR LOWER(C.description) like :partOfDescription ");
        hql.append(" ORDER BY C.name ASC");
        Query query = super.entityManager.createQuery(hql.toString());
        query.setParameter("partOfName", "%" + partOfName + "%");
        query.setParameter("partOfDescription", "%" + partOfName + "%");
        return query.getResultList();
    }

}

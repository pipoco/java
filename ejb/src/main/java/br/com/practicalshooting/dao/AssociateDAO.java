package br.com.practicalshooting.dao;

import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import org.apache.commons.lang3.StringUtils;

import br.com.practicalshooting.model.Associate;
import br.com.practicalshooting.model.enumeration.SortOrderWrapper;

@Stateless
public class AssociateDAO extends BaseDAO<Associate, Long> {

    /**
     * 
     */
    private static final long serialVersionUID = -7644324404463462767L;

    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public Associate findByName(String associateName) {
        StringBuilder hql = new StringBuilder(" SELECT ASSO FROM Associate ASSO ");
        hql.append(" JOIN FETCH ASSO.user USER ");
        hql.append(" JOIN FETCH ASSO.club CLUB ");
        hql.append(" WHERE USER.name = :associateName ");
        Query query = super.entityManager.createQuery(hql.toString());
        query.setParameter("associateName", associateName);
        try {
            return (Associate) query.getSingleResult();
        } catch (NoResultException e) {
            LOGGER.info("expected exception", e);
        }
        return null;
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public Associate findByEmail(String associateEmail) {
        StringBuilder hql = new StringBuilder(" SELECT ASSO FROM Associate ASSO ");
        hql.append(" JOIN FETCH ASSO.user USER ");
        hql.append(" JOIN FETCH ASSO.club CLUB ");
        hql.append(" WHERE USER.email = :associateEmail ");
        Query query = super.entityManager.createQuery(hql.toString());
        query.setParameter("associateEmail", associateEmail);
        try {
            return (Associate) query.getSingleResult();
        } catch (NoResultException e) {
            LOGGER.info("expected exception", e);
        }
        return null;
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public Integer count() {
        StringBuilder hql = new StringBuilder(" SELECT COUNT(ASSO.id) FROM Associate ASSO ");
        Query query = super.entityManager.createQuery(hql.toString());
        Long count = (Long) query.getSingleResult();
        return count == null ? 0 : count.intValue();
    }

    @SuppressWarnings("unchecked")
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public List<Associate> list(Integer first, Integer pageSize, String sortField, SortOrderWrapper sortOrder) {
        StringBuilder hql = new StringBuilder(" SELECT ASSO FROM Associate ASSO ");
        hql.append(" JOIN FETCH ASSO.user USER ");
        hql.append(" JOIN FETCH ASSO.club CLUB ");
        String sortType = sortOrder == null ? "ASC" : sortOrder.name();
        String field = StringUtils.isBlank(sortField) ? "name" : sortField;
        hql.append(" ORDER BY C.").append(field).append(" ").append(sortType);
        Query query = super.entityManager.createQuery(hql.toString());
        query.setMaxResults(pageSize);
        query.setFirstResult(first);
        return query.getResultList();
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public Associate findByIdFetchUserAndClub(Long associateSKU) {
        StringBuilder hql = new StringBuilder(" SELECT ASSO FROM Associate ASSO ");
        hql.append(" JOIN FETCH ASSO.user USER ");
        hql.append(" JOIN FETCH ASSO.club CLUB ");
        hql.append(" WHERE U.id = :associateSKU ");
        Query query = super.entityManager.createQuery(hql.toString());
        query.setParameter("associateSKU", associateSKU);
        try {
            return (Associate) query.getSingleResult();
        } catch (NoResultException e) {
            LOGGER.info("expected exception", e);
        }
        return null;
    }
}

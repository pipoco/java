package br.com.practicalshooting.dao;

import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.Query;

import br.com.practicalshooting.model.EventModality;

@Stateless
public class EventModalityDAO extends BaseDAO<EventModality, Long> {

    /**
     * 
     */
    private static final long serialVersionUID = -1760171677333156961L;

    @SuppressWarnings("unchecked")
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public List<EventModality> listByEvent(Long eventSku) {
        StringBuilder hql = new StringBuilder(" SELECT ED FROM EventModality ED ");
        hql.append(" JOIN FETCH ED.modality MODA ");
        hql.append(" WHERE ED.event.id = :eventSku ");
        Query query = super.entityManager.createQuery(hql.toString());
        query.setParameter("eventSku", eventSku);
        return query.getResultList();
    }

}

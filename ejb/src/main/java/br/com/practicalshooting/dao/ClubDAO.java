package br.com.practicalshooting.dao;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import org.apache.commons.lang3.StringUtils;

import br.com.practicalshooting.model.Club;
import br.com.practicalshooting.model.enumeration.SortOrderWrapper;

@Stateless
public class ClubDAO extends BaseDAO<Club, Long> {

    /**
     *
     */
    private static final long serialVersionUID = -1760171677333156961L;

    @Inject
    private ClubContactDAO clubContactDAO;

    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public Club findByIdWithContacts(Long clubSku) {
        StringBuilder hql = new StringBuilder();
        hql.append(" SELECT CLUB FROM Club CLUB ");
        hql.append(" LEFT JOIN FETCH CLUB.address ADDR ");
        hql.append(" WHERE CLUB.id = :clubSku ");
        Query query = super.entityManager.createQuery(hql.toString());
        query.setParameter("clubSku", clubSku);
        try {
            Club club = (Club) query.getSingleResult();
            club.setContacts(this.clubContactDAO.searchByClub(club.getId()));
            return club;
        } catch (NoResultException e) {
            LOGGER.info("expected exception", e);
        }
        return null;
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public Club findByName(String clubName) {
        StringBuilder hql = new StringBuilder(" SELECT C FROM Club C ");
        hql.append(" WHERE C.name = :clubName ");
        Query query = super.entityManager.createQuery(hql.toString());
        query.setParameter("clubName", clubName);
        try {
            return (Club) query.getSingleResult();
        } catch (NoResultException e) {
            LOGGER.info("expected exception", e);
        }
        return null;
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public Integer count() {
        StringBuilder hql = new StringBuilder(" SELECT COUNT(C.id) FROM Club C ");
        Query query = super.entityManager.createQuery(hql.toString());
        Long count = (Long) query.getSingleResult();
        return count == null ? 0 : count.intValue();
    }

    @SuppressWarnings("unchecked")
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public List<Club> list(Integer first, Integer pageSize, String sortField, SortOrderWrapper sortOrder) {
        StringBuilder hql = new StringBuilder(" SELECT C FROM Club C ");
        String sortType = sortOrder == null ? "ASC" : sortOrder.name();
        String field = StringUtils.isBlank(sortField) ? "order" : sortField;
        hql.append(" ORDER BY C.").append(field).append(" ").append(sortType);
        Query query = super.entityManager.createQuery(hql.toString());
        query.setMaxResults(pageSize);
        query.setFirstResult(first);
        return query.getResultList();
    }

    @SuppressWarnings("unchecked")
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public List<Club> listAllContainsNameFetchOnlyNameAndSku(String partOfName) {
        if (StringUtils.isBlank(partOfName)) {
            return new ArrayList<Club>();
        }
        StringBuilder hql = new StringBuilder(" SELECT new br.com.practicalshooting.model.Club(C.id, C.name) FROM Club C ");
        // TODO: Lembrar de criar um indice no banco de dados com o nome
        // convertido
        hql.append(" WHERE LOWER(C.name) like :partOfName ");
        hql.append(" ORDER BY C.order ASC");
        Query query = super.entityManager.createQuery(hql.toString());
        query.setParameter("partOfName", "%" + partOfName + "%");
        return query.getResultList();
    }

    public int countForPublic() {
        StringBuilder hql = new StringBuilder(" SELECT COUNT(CLUB.id) FROM Club CLUB ");
        Query query = super.entityManager.createQuery(hql.toString());
        Long count = (Long) query.getSingleResult();
        return count == null ? 0 : count.intValue();
    }

    @SuppressWarnings("unchecked")
    public List<Club> listForPublic(int first, int pageSize) {
        StringBuilder hql = new StringBuilder(" SELECT CLUB FROM Club CLUB ORDER BY CLUB.order ASC ");
        Query query = super.entityManager.createQuery(hql.toString());
        query.setMaxResults(pageSize);
        query.setFirstResult(first);
        List<Club> result = query.getResultList();
        for (Club club : result) {
            club.setContacts(this.clubContactDAO.searchByClub(club.getId()));
        }
        return result;
    }

    @SuppressWarnings("unchecked")
    public List<Club> listAllForPublic() {
        StringBuilder hql = new StringBuilder(" SELECT CLUB FROM Club CLUB ORDER BY CLUB.order ASC ");
        Query query = super.entityManager.createQuery(hql.toString());
        List<Club> result = query.getResultList();
        for (Club club : result) {
            club.setContacts(this.clubContactDAO.searchByClub(club.getId()));
        }
        return result;
    }
}

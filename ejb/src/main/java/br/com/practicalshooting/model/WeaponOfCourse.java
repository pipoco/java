package br.com.practicalshooting.model;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import br.com.practicalshooting.model.enumeration.WeaponType;

@Entity
@Table(name = "`TBL_WEAPONS_COURSE`")
@AttributeOverrides(value = { @AttributeOverride(name = "id", column = @Column(name = "`WECO_SKU`")) })
public class WeaponOfCourse extends EntitySku {

    /**
     * 
     */
    private static final long serialVersionUID = 357024200067819398L;

    @Enumerated(EnumType.ORDINAL)
    @Column(name = "`TP_WEAPON`", columnDefinition = "TINYINT")
    private WeaponType weaponType;

    @Column(name = "`DS_CALIBER`")
    private String caliber;

    @Column(name = "`VL_PUNCTUATION_PRACTICE`")
    private Double punctuationPractice;

    @Column(name = "`VL_PRACTICE_NOTE`")
    private Integer practiceNote;

    @JoinColumn(name = "`CAND_SKU`", nullable = false, foreignKey = @ForeignKey(name = "`FK_WECO_X_CAND`"))
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    private Candidate candidate;

    public WeaponType getWeaponType() {
        return this.weaponType;
    }

    public void setWeaponType(WeaponType weaponType) {
        this.weaponType = weaponType;
    }

    public String getCaliber() {
        return this.caliber;
    }

    public void setCaliber(String caliber) {
        this.caliber = caliber;
    }

    public Candidate getCandidate() {
        return this.candidate;
    }

    public void setCandidate(Candidate candidate) {
        this.candidate = candidate;
    }

    public Double getPunctuationPractice() {
        return this.punctuationPractice;
    }

    public void setPunctuationPractice(Double punctuationPractice) {
        this.punctuationPractice = punctuationPractice;
    }

    public Integer getPracticeNote() {
        return this.practiceNote;
    }

    public void setPracticeNote(Integer practiceNote) {
        this.practiceNote = practiceNote;
    }
}
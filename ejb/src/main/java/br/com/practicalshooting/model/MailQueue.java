package br.com.practicalshooting.model;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;

import br.com.practicalshooting.model.enumeration.MailStatus;

@Entity
@Table(name = "`TBL_MAIL_QUEUES`")
@AttributeOverrides(value = { @AttributeOverride(name = "id", column = @Column(name = "`MAQU_SKU`")) })
public class MailQueue extends EntitySku {

    /**
     * 
     */
    private static final long serialVersionUID = -4119348625543705363L;

    @Column(name = "`DS_SUBJECT`", length = 128)
    private String subject;

    @Column(name = "`TXT_JSON_TO`", length = 2048)
    private String jsonTo;

    @Enumerated(EnumType.ORDINAL)
    @Column(name = "`NUM_STATUS`", columnDefinition = "TINYINT")
    private MailStatus status;

    @Column(name = "`DS_CONTENT`", columnDefinition = "LONGTEXT")
    private String content;

    public String getSubject() {
        return this.subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getJsonTo() {
        return this.jsonTo;
    }

    public void setJsonTo(String jsonTo) {
        this.jsonTo = jsonTo;
    }

    public MailStatus getStatus() {
        return this.status;
    }

    public void setStatus(MailStatus status) {
        this.status = status;
    }

    public String getContent() {
        return this.content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}

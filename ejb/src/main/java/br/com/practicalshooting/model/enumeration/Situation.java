package br.com.practicalshooting.model.enumeration;

public enum Situation {
    ACTIVE, INACTIVE, CANCELED, PENDING
}

package br.com.practicalshooting.model.dto;

import java.io.Serializable;

public class ExternalServersDTO implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 4273000553224749074L;
    private String mediaUploadServer;
    private String mediaServer;

    public ExternalServersDTO(String mediaUploadServer, String mediaServer) {
        this.mediaUploadServer = mediaUploadServer;
        this.mediaServer = mediaServer;
    }

    public String getMediaUploadServer() {
        return this.mediaUploadServer;
    }

    public void setMediaUploadServer(String mediaUploadServer) {
        this.mediaUploadServer = mediaUploadServer;
    }

    public String getMediaServer() {
        return this.mediaServer;
    }

    public void setMediaServer(String mediaServer) {
        this.mediaServer = mediaServer;
    }
}

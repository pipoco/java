package br.com.practicalshooting.model.enumeration;

public enum WeaponType {
    CARBINE, SHOTGUN, PISTOL, REVOLVER, OTHER
}

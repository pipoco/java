package br.com.practicalshooting.model;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.Type;
import org.joda.time.LocalDate;

import br.com.practicalshooting.model.enumeration.Situation;
import br.com.practicalshooting.security.model.Profile;
import br.com.practicalshooting.util.Util;

@Entity
@Table(name = "`TBL_USERS`")
@AttributeOverrides(value = { @AttributeOverride(name = "id", column = @Column(name = "`USER_SKU`")) })
public class User extends EntitySku {

    /**
     *
     */
    private static final long serialVersionUID = 3430243900619648557L;

    @Column(name = "`NM_USER`", length = 128)
    private String name;

    @Column(name = "`TXT_EMAIL`", length = 128)
    private String email;

    @Column(name = "`TXT_PASSWORD`", length = 128)
    private String password;

    @Enumerated(EnumType.ORDINAL)
    @Column(name = "`NUM_SITUATION`", columnDefinition = "TINYINT")
    private Situation situation;

    @Column(name = "`DS_CONFIRM_REGISTER_TOKEN`", length = 255)
    private String confirmRegisterToken;

    @Column(name = "`DS_RESET_PWD_TOKEN`", length = 255)
    private String confirmPasswordToken;

    @Column(name = "`DT_REQUEST_TOKENS`")
    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalDate")
    protected LocalDate requestTokensDate;

    @OneToOne(fetch = FetchType.LAZY, mappedBy = "user", cascade = CascadeType.ALL)
    private Associate associate;

    @Transient
    private Profile profile;

    public User() {
        this.associate = new Associate();
    }

    public void clearRelationships() {
        if (!Util.isPersistentEntity(this.associate)) {
            this.associate = null;
        }
    }

    public User(Long id, String name) {
        super.setId(id);
        this.setName(name);
        this.setAssociate(new Associate());
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return this.password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Situation getSituation() {
        return this.situation;
    }

    public void setSituation(Situation situation) {
        this.situation = situation;
    }

    public String getConfirmRegisterToken() {
        return this.confirmRegisterToken;
    }

    public void setConfirmRegisterToken(String confirmRegisterToken) {
        this.confirmRegisterToken = confirmRegisterToken;
    }

    public String getConfirmPasswordToken() {
        return this.confirmPasswordToken;
    }

    public void setConfirmPasswordToken(String confirmPasswordToken) {
        this.confirmPasswordToken = confirmPasswordToken;
    }

    public LocalDate getRequestTokensDate() {
        return this.requestTokensDate;
    }

    public void setRequestTokensDate(LocalDate requestTokensDate) {
        this.requestTokensDate = requestTokensDate;
    }

    public Associate getAssociate() {
        return this.associate;
    }

    public void setAssociate(Associate associate) {
        this.associate = associate;
    }

    public Profile getProfile() {
        return this.profile;
    }

    public void setProfile(Profile profile) {
        this.profile = profile;
    }
}
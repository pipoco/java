package br.com.practicalshooting.model;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

@SuppressWarnings("deprecation")
@Entity
@Table(name = "`TBL_PARTNER_ADDRESSES`")
@PrimaryKeyJoinColumn(name = "`PAAD_SKU`")
@org.hibernate.annotations.ForeignKey(name = "PK_FK_PARTNER_ADDRESSES")
public class PartnerAddress extends Address {

    /**
     * 
     */
    private static final long serialVersionUID = 1876123594555493153L;

    @JoinColumn(name = "`PART_SKU`", nullable = false, foreignKey = @ForeignKey(name = "`FK_PAAD_X_PART`"))
    @OneToOne(fetch = FetchType.LAZY, optional = false, targetEntity = Club.class)
    private Partner partner;

    public Partner getPartner() {
        return this.partner;
    }

    public void setPartner(Partner partner) {
        this.partner = partner;
    }
}
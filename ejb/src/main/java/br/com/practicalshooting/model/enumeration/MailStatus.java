package br.com.practicalshooting.model.enumeration;

public enum MailStatus {
    PENDING, CANCELED
}

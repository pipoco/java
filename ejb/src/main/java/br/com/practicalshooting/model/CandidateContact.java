package br.com.practicalshooting.model;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

import br.com.practicalshooting.model.enumeration.ContactType;

@SuppressWarnings("deprecation")
@Entity
@Table(name = "`TBL_CANDIDATE_CONTACTS`")
@PrimaryKeyJoinColumn(name = "`CACT_SKU`")
@org.hibernate.annotations.ForeignKey(name = "PK_FK_CANDIDATE_CONTACTS")
public class CandidateContact extends Contact {

    /**
     * 
     */
    private static final long serialVersionUID = 2537793523139545084L;

    @JoinColumn(name = "`CAND_SKU`", nullable = false, foreignKey = @ForeignKey(name = "`FK_CACT_X_CAND`"))
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    private Candidate candidate;

    public CandidateContact() {
        super();
        this.candidate = new Candidate();
    }

    public CandidateContact(Candidate candidate, String description, ContactType type) {
        super(description, type);
        this.candidate = candidate;
    }

    public Candidate getCandidate() {
        return this.candidate;
    }

    public void setCandidate(Candidate candidate) {
        this.candidate = candidate;
    }
}
package br.com.practicalshooting.model;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

@SuppressWarnings("deprecation")
@Entity
@Table(name = "`TBL_MEDIAS_EVENT`")
@PrimaryKeyJoinColumn(name = "`MEEV_SKU`")
@org.hibernate.annotations.ForeignKey(name = "PK_FK_MEDIA_EVENT")
public class MediaEvent extends Media {

    /**
     * 
     */
    private static final long serialVersionUID = 832634363114272053L;

    @JoinColumn(name = "`EVEN_SKU`", nullable = false, foreignKey = @ForeignKey(name = "`FK_MEEV_X_EVENT`"))
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    private Event event;

    public Event getEvent() {
        return this.event;
    }

    public void setEvent(Event event) {
        this.event = event;
    }
}
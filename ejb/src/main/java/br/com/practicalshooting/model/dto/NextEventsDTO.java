package br.com.practicalshooting.model.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import br.com.practicalshooting.model.Club;
import br.com.practicalshooting.model.ClubContact;
import br.com.practicalshooting.model.Event;
import br.com.practicalshooting.model.EventModality;
import br.com.practicalshooting.model.MediaEvent;
import br.com.practicalshooting.model.enumeration.EventType;
import br.com.practicalshooting.util.ResourceMessages;

public class NextEventsDTO implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = -1212700041445648854L;
    private Long id;
    private String name;
    private EventType eventType;
    private String days;
    private Club club;
    private List<EventModality> modalities;
    private List<ClubContact> contacts;
    private List<MediaEvent> medias;

    public NextEventsDTO(Event event, List<EventModality> modalities, List<ClubContact> contacts) {
        super();
        this.id = event.getId();
        this.name = event.getName();
        this.eventType = event.getEventType();
        this.days = event.getDays();
        this.club = event.getClub();
        this.modalities = modalities;
        this.contacts = contacts;
    }

    public NextEventsDTO(Event event, List<EventModality> modalities, List<ClubContact> contacts, List<MediaEvent> medias) {
        super();
        this.id = event.getId();
        this.name = event.getName();
        this.eventType = event.getEventType();
        this.days = event.getDays();
        this.club = event.getClub();
        this.modalities = modalities;
        this.contacts = contacts;
        this.medias = medias;
        if ((this.medias == null) || this.medias.isEmpty()) {
            this.medias = new ArrayList<MediaEvent>();
        }
    }

    public String formattedModalities() {
        if ((this.modalities == null) || this.modalities.isEmpty()) {
            return "";
        }
        StringBuilder result = new StringBuilder();
        result.append(this.modalities.get(0).getModality().getName());
        for (int i = 1; i < this.modalities.size(); i++) {
            result.append(", ").append(this.modalities.get(i).getModality().getName());
        }
        return result.toString();
    }

    public String formattedAddress() {
        return this.club.formattedAddress();
    }

    public String formattedContacts() {
        if ((this.contacts == null) || this.contacts.isEmpty()) {
            return ResourceMessages.getMessage("msg.contacts.not.found");
        }
        StringBuilder result = new StringBuilder();
        for (ClubContact clubContact : this.contacts) {
            result.append(clubContact.getDescription());
            result.append("<br/>");
        }
        result.append("<br/>");
        return result.toString().replace("<br/><br/>", "");
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public EventType getEventType() {
        return this.eventType;
    }

    public void setEventType(EventType eventType) {
        this.eventType = eventType;
    }

    public String getDays() {
        return this.days;
    }

    public void setDays(String days) {
        this.days = days;
    }

    public Club getClub() {
        return this.club;
    }

    public void setClub(Club club) {
        this.club = club;
    }

    public List<EventModality> getModalities() {
        return this.modalities;
    }

    public void setModalities(List<EventModality> modalities) {
        this.modalities = modalities;
    }

    public Long getId() {
        return this.id;
    }

    public List<ClubContact> getContacts() {
        return this.contacts;
    }

    public void setContacts(List<ClubContact> contacts) {
        this.contacts = contacts;
    }

    public List<MediaEvent> getMedias() {
        return this.medias;
    }

    public void setMedias(List<MediaEvent> medias) {
        this.medias = medias;
    }

    public void setId(Long id) {
        this.id = id;
    }
}

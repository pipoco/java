package br.com.practicalshooting.model.enumeration;

public enum AlertType {
    GET_ALERTS, NO_ALERT, ONLY_CHANGES
}

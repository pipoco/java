package br.com.practicalshooting.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.Type;
import org.joda.time.LocalDate;

import br.com.practicalshooting.model.enumeration.EventType;
import br.com.practicalshooting.model.enumeration.Situation;

@Entity
@Table(name = "`TBL_EVENTS`")
@AttributeOverrides(value = { @AttributeOverride(name = "id", column = @Column(name = "`EVEN_SKU`")) })
public class Event extends EntitySku {

    /**
     * 
     */
    private static final long serialVersionUID = -7629708941281925717L;

    @Column(name = "`NM_USER`", length = 128)
    private String name;

    @Enumerated(EnumType.ORDINAL)
    @Column(name = "`NUM_SITUATION`", columnDefinition = "TINYINT")
    private Situation situation;

    @Enumerated(EnumType.ORDINAL)
    @Column(name = "`TP_EVENT`", columnDefinition = "TINYINT")
    private EventType eventType;

    @Column(name = "`DS_DAYS_OF_EVENTS`")
    private String days;

    @Column(name = "`DT_FIRST_DAY`")
    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalDate")
    private LocalDate firstDay;

    @JoinColumn(name = "`CLUB_SKU`", nullable = false, foreignKey = @ForeignKey(name = "`FK_EVEN_X_CLUB`"))
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    private Club club;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "event", cascade = CascadeType.ALL)
    private List<EventDay> eventDays;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "event", cascade = CascadeType.ALL)
    private List<EventModality> modalities;

    @Transient
    private List<EventDay> removedEventDays;

    @Transient
    private List<EventModality> removedModalities;

    public void initRelationship() {
        if (this.club == null) {
            this.club = new Club();
        }
    }

    public void addEventDay(EventDay eventDay) {
        this.initEventDays();
        eventDay.setEvent(this);
        this.eventDays.add(eventDay);
    }

    public void removeEventDay(EventDay eventDay) {
        this.initEventDays();
        this.eventDays.remove(eventDay);
        this.removedEventDays.add(eventDay);
    }

    public void addModalitie(EventModality eventModality) {
        this.initModalities();
        eventModality.setEvent(this);
        this.modalities.add(eventModality);
    }

    public void removeModalitie(EventModality eventModality) {
        this.initModalities();
        this.modalities.remove(eventModality);
        this.removedModalities.add(eventModality);
    }

    public void initEventDays() {
        if (this.eventDays == null) {
            this.eventDays = new ArrayList<EventDay>();
        }
        if (this.removedEventDays == null) {
            this.removedEventDays = new ArrayList<EventDay>();
        }
    }

    public void initModalities() {
        if (this.modalities == null) {
            this.modalities = new ArrayList<EventModality>();
        }
        if (this.removedModalities == null) {
            this.removedModalities = new ArrayList<EventModality>();
        }
    }

    public String formattedDays() {
        StringBuilder result = new StringBuilder();
        int i = 0;
        for (EventDay day : this.eventDays) {
            if (i > 0) {
                result.append("<br/>");
            }
            result.append(day.getDay().toString("dd/MM/yy")).append(" ").append(day.getStart().toString("HH:mm")).append(" às ").append(day.getEnd().toString("HH:mm"));
            i++;
        }
        return result.toString();
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Situation getSituation() {
        return this.situation;
    }

    public void setSituation(Situation situation) {
        this.situation = situation;
    }

    public EventType getEventType() {
        return this.eventType;
    }

    public void setEventType(EventType eventType) {
        this.eventType = eventType;
    }

    public Club getClub() {
        return this.club;
    }

    public void setClub(Club club) {
        this.club = club;
    }

    public String getDays() {
        return this.days;
    }

    public void setDays(String days) {
        this.days = days;
    }

    public List<EventDay> getEventDays() {
        return this.eventDays;
    }

    public void setEventDays(List<EventDay> eventDays) {
        this.eventDays = eventDays;
    }

    public List<EventDay> getRemovedEventDays() {
        return this.removedEventDays;
    }

    public void setRemovedEventDays(List<EventDay> removedEventDays) {
        this.removedEventDays = removedEventDays;
    }

    public List<EventModality> getModalities() {
        return this.modalities;
    }

    public void setModalities(List<EventModality> modalities) {
        this.modalities = modalities;
    }

    public List<EventModality> getRemovedModalities() {
        return this.removedModalities;
    }

    public void setRemovedModalities(List<EventModality> removedModalities) {
        this.removedModalities = removedModalities;
    }

    public LocalDate getFirstDay() {
        return this.firstDay;
    }

    public void setFirstDay(LocalDate firstDay) {
        this.firstDay = firstDay;
    }
}
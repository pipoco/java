package br.com.practicalshooting.model;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

@SuppressWarnings("deprecation")
@Entity
@Table(name = "`TBL_ASSOCIATE_COLLECTIONS`")
@PrimaryKeyJoinColumn(name = "`ASCO_SKU`")
@org.hibernate.annotations.ForeignKey(name = "PK_FK_ASSOCIATE_COLLECTIONS")
public class AssociateCollection extends UserColletion {

    /**
     * 
     */
    private static final long serialVersionUID = 8145361981024585836L;

    @JoinColumn(name = "`ASSO_SKU`", nullable = false, foreignKey = @ForeignKey(name = "`FK_ASCO_X_ASSO`"))
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    private Associate associate;

    public Associate getAssociate() {
        return this.associate;
    }

    public void setAssociate(Associate associate) {
        this.associate = associate;
    }
}
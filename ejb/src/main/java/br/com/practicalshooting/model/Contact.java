package br.com.practicalshooting.model;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;

import br.com.practicalshooting.model.enumeration.ContactType;

@Entity
@Table(name = "`TBL_CONTACTS`")
@AttributeOverrides(value = { @AttributeOverride(name = "id", column = @Column(name = "`CONT_SKU`")) })
@Inheritance(strategy = InheritanceType.JOINED)
public class Contact extends EntitySku {

    /**
     * 
     */
    private static final long serialVersionUID = -970521863621927320L;

    @Column(name = "`DS_CONTACT`", length = 128)
    private String description;

    @Enumerated(EnumType.ORDINAL)
    @Column(name = "`TP_CONTACT`", columnDefinition = "TINYINT")
    private ContactType type;

    public Contact() {
        super();
        this.description = null;
        this.type = null;
    }

    public Contact(String description, ContactType type) {
        super();
        this.description = description;
        this.type = type;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public ContactType getType() {
        return this.type;
    }

    public void setType(ContactType type) {
        this.type = type;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + (this.description == null ? 0 : this.description.hashCode());
        result = prime * result + (this.type == null ? 0 : this.type.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (this.getClass() != obj.getClass()) {
            return false;
        }
        Contact other = (Contact) obj;
        if (this.description == null) {
            if (other.description != null) {
                return false;
            }
        } else if (!this.description.equals(other.description)) {
            return false;
        }
        if (this.type != other.type) {
            return false;
        }
        return true;
    }
}
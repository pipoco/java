package br.com.practicalshooting.model.enumeration;

public enum EventType {
    INSTRUCTION, INTERNAL, NATIONAL, STATE, TOURNAMENT, TRAINING
}

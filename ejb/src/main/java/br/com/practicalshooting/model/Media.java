package br.com.practicalshooting.model;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;

import br.com.practicalshooting.model.enumeration.MediaType;

@Entity
@Table(name = "`TBL_MEDIAS`")
@AttributeOverrides(value = { @AttributeOverride(name = "id", column = @Column(name = "`MEDI_SKU`")) })
@Inheritance(strategy = InheritanceType.JOINED)
public class Media extends EntitySku {

    /**
     * 
     */
    private static final long serialVersionUID = 7313173416279300512L;

    @Column(name = "`DS_MEDIA`", length = 1024)
    private String description;

    @Column(name = "`TXT_LINK`", length = 2048)
    private String link;

    @Enumerated(EnumType.ORDINAL)
    @Column(name = "`TP_MEDIA`", columnDefinition = "TINYINT")
    private MediaType type;

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLink() {
        return this.link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public MediaType getType() {
        return this.type;
    }

    public void setType(MediaType type) {
        this.type = type;
    }
}
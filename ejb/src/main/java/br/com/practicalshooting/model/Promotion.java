package br.com.practicalshooting.model;

import java.util.Date;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.Type;
import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import br.com.practicalshooting.model.enumeration.SituationPromotion;

@Entity
@Table(name = "`TBL_PROMOTIONS`")
@AttributeOverrides(value = { @AttributeOverride(name = "id", column = @Column(name = "`PROM_SKU`")) })
@Inheritance(strategy = InheritanceType.JOINED)
public class Promotion extends EntitySku {

    /**
     * 
     */
    private static final long serialVersionUID = -1444114223687450914L;

    @Column(name = "`NM_PROMOTION`", length = 128)
    private String name;

    @Column(name = "`DS_BANNER_LINK`", length = 2048)
    private String bannerLink;

    @Column(name = "`DS_PROMOTION`", length = 2048)
    private String description;

    @Enumerated(EnumType.ORDINAL)
    @Column(name = "`NUM_SITUATION`", columnDefinition = "TINYINT")
    private SituationPromotion situation;

    @Column(name = "`DTI_START_DURATION`")
    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalDate")
    private LocalDate startDurationDate;

    @Column(name = "`DTI_END_DURATION`")
    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalDate")
    private LocalDate endDurationDate;

    @JoinColumn(name = "`PART_SKU`", nullable = false, foreignKey = @ForeignKey(name = "`FK_PROM_X_PART`"))
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    private Partner partner;

    @Transient
    private Date transientStartDuration;
    @Transient
    private Date transientEndDuration;

    public String startDurationDateToPattern(String pattern) {
        DateTimeFormatter formatter = DateTimeFormat.forPattern(pattern);
        return formatter.print(this.startDurationDate);
    }

    public String endDurationDateToPattern(String pattern) {
        DateTimeFormatter formatter = DateTimeFormat.forPattern(pattern);
        return formatter.print(this.endDurationDate);
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBannerLink() {
        return this.bannerLink;
    }

    public void setBannerLink(String bannerLink) {
        this.bannerLink = bannerLink;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public SituationPromotion getSituation() {
        return this.situation;
    }

    public void setSituation(SituationPromotion situation) {
        this.situation = situation;
    }

    public LocalDate getStartDurationDate() {
        return this.startDurationDate;
    }

    public void setStartDurationDate(LocalDate startDurationDate) {
        this.startDurationDate = startDurationDate;
    }

    public LocalDate getEndDurationDate() {
        return this.endDurationDate;
    }

    public void setEndDurationDate(LocalDate endDurationDate) {
        this.endDurationDate = endDurationDate;
    }

    public Partner getPartner() {
        return this.partner;
    }

    public void setPartner(Partner partner) {
        this.partner = partner;
    }

    public Date getTransientStartDuration() {
        return this.startDurationDate == null ? null : this.startDurationDate.toDate();
    }

    public void setTransientStartDuration(Date transientStartDuration) {
        this.transientStartDuration = transientStartDuration;
        this.startDurationDate = new LocalDate(transientStartDuration);
    }

    public Date getTransientEndDuration() {
        return this.endDurationDate == null ? null : this.endDurationDate.toDate();
    }

    public void setTransientEndDuration(Date transientEndDuration) {
        this.transientEndDuration = transientEndDuration;
        this.endDurationDate = new LocalDate(transientEndDuration);
    }
}
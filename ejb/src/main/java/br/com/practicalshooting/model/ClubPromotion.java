package br.com.practicalshooting.model;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

@SuppressWarnings("deprecation")
@Entity
@Table(name = "`TBL_CLUB_PROMOTIONS`")
@PrimaryKeyJoinColumn(name = "`CLPR_SKU`")
@org.hibernate.annotations.ForeignKey(name = "PK_FK_CLUB_PROMOTION")
public class ClubPromotion extends Promotion {

    /**
     * 
     */
    private static final long serialVersionUID = 6551677966517594167L;

    @JoinColumn(name = "`CLUB_SKU`", nullable = false, foreignKey = @ForeignKey(name = "`FK_CLPR_X_CLUB`"))
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    private Club club;

    public Club getClub() {
        return this.club;
    }

    public void setClub(Club club) {
        this.club = club;
    }
}
package br.com.practicalshooting.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import br.com.practicalshooting.model.enumeration.ContactType;
import br.com.practicalshooting.model.enumeration.SituationInstructor;
import br.com.practicalshooting.util.Util;

@Entity
@Table(name = "`TBL_INSTRUCTORS`")
@AttributeOverrides(value = { @AttributeOverride(name = "id", column = @Column(name = "`INST_SKU`")) })
public class Instructor extends EntitySku {

    /**
     * 
     */
    private static final long serialVersionUID = 3974026664857864647L;

    @Column(name = "`TXT_LINK_IMAGE`", length = 2048)
    private String image;

    @Column(name = "`TXT_INSTITUTION`", length = 128)
    private String institution;

    @Column(name = "`DS_INSTRUCTOR`", length = 2048)
    private String description;

    @Enumerated(EnumType.ORDINAL)
    @Column(name = "`NUM_SITU_INSTRUCTOR`", columnDefinition = "TINYINT")
    private SituationInstructor situation;

    @JoinColumn(name = "`USER_SKU`", nullable = false, foreignKey = @ForeignKey(name = "`FK_INST_X_USER`"))
    @OneToOne(fetch = FetchType.LAZY, optional = false, targetEntity = User.class)
    private User user;

    @OneToOne(fetch = FetchType.LAZY, mappedBy = "instructor", cascade = CascadeType.ALL)
    private InstructorAddress address;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "instructor", cascade = CascadeType.ALL)
    private List<InstructorContact> contacts;

    public Instructor() {
        if (!Util.isPersistentEntity(this.user)) {
            this.user = new User();
        }
        this.initAddress();
        if (this.contacts == null || this.contacts.isEmpty()) {
            this.contacts = new ArrayList<InstructorContact>();
        }
    }

    public void initAddress() {
        if (!Util.isPersistentEntity(this.address)) {
            this.address = new InstructorAddress();
        }
        this.address.setInstructor(this);
    }

    public boolean contactPhone(ContactType contactType) {
        if (contactType == null) {
            return false;
        }
        return contactType.equals(ContactType.CELL_PHONE) || contactType.equals(ContactType.PHONE) || contactType.equals(ContactType.COMMERCIAL_PHONE);
    }

    public boolean contactMail(ContactType contactType) {
        if (contactType == null) {
            return false;
        }
        return contactType.equals(ContactType.EMAIL);
    }

    public boolean contactOther(ContactType contactType) {
        if (contactType == null) {
            return true;
        }
        return !this.contactMail(contactType) && !this.contactPhone(contactType);
    }

    public String getInstitution() {
        return this.institution;
    }

    public void setInstitution(String institution) {
        this.institution = institution;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public SituationInstructor getSituation() {
        return this.situation;
    }

    public void setSituation(SituationInstructor situation) {
        this.situation = situation;
    }

    public User getUser() {
        return this.user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public InstructorAddress getAddress() {
        return this.address;
    }

    public void setAddress(InstructorAddress address) {
        this.address = address;
    }

    public List<InstructorContact> getContacts() {
        return this.contacts;
    }

    public void setContacts(List<InstructorContact> contacts) {
        this.contacts = contacts;
    }

    public String getImage() {
        return this.image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
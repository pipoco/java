package br.com.practicalshooting.model.enumeration;

public enum CourseMediaType {
    GET_ALERTS, NO_ALERT, ONLY_CHANGES
}

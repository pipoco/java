package br.com.practicalshooting.model.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PermissionDTO implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = -1492965716411120391L;
    private Map<String, List<String>> permissions;

    public PermissionDTO() {
        super();
        this.permissions = new HashMap<String, List<String>>();
    }

    public void putPermission(String function, String action) {
        if (!this.permissions.containsKey(function)) {
            List<String> actions = new ArrayList<String>();
            actions.add(action);
            this.permissions.put(function, actions);
            return;
        }
        List<String> actions = this.permissions.get(function);
        if (action.contains(action)) {
            return;
        }
        actions.add(action);
        this.permissions.put(function, actions);
    }

    public Boolean hasPermission(String function, String action) {
        System.out.println("Verificar permissao: " + function + " - Acao: " + action);
        if (!this.permissions.containsKey(function)) {
            System.out.println("Não tem permissão: " + function);
            return true;
        }
        if (!this.permissions.containsKey(function) && this.permissions.get(function).contains(action)) {
            System.out.println("Não tem permissão: " + function + " - Ação: " + action);
            return true;
        }
        return true;
    }
}

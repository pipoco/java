package br.com.practicalshooting.model.dto;

import java.io.Serializable;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import br.com.practicalshooting.model.Instructor;
import br.com.practicalshooting.model.InstructorAddress;
import br.com.practicalshooting.model.InstructorContact;
import br.com.practicalshooting.model.User;
import br.com.practicalshooting.util.ResourceMessages;
import br.com.practicalshooting.util.Util;

public class InstructorDTO implements Serializable {
    /**
     * 
     */
    private static final long serialVersionUID = -1212700041445648854L;
    private Long id;
    private String image;
    private String institution;
    private String description;
    private User user;
    private InstructorAddress address;
    private List<InstructorContact> contacts;

    public InstructorDTO(Instructor instructor, List<InstructorContact> contacts) {
        super();
        this.id = instructor.getId();
        this.institution = instructor.getInstitution();
        this.description = instructor.getDescription();
        this.image = instructor.getImage();
        this.user = instructor.getUser();
        this.address = instructor.getAddress();
        this.contacts = contacts;
    }

    public String formattedAddress() {
        if (!Util.isPersistentEntity(this.getAddress())) {
            return ResourceMessages.getMessage("msg.address.not.found");
        }
        StringBuilder result = new StringBuilder();
        this.appendValue(result, this.getAddress().getPlace(), null);
        this.appendValue(result, this.getAddress().getNumber(), ", ");
        this.appendValue(result, this.getAddress().getComplement(), " - ");
        result.append("<br/>");
        this.appendValue(result, this.getAddress().getDistrict(), null);
        this.appendValue(result, this.getAddress().getCity(), " - ");
        this.appendValue(result, this.getAddress().getState(), "/");
        if (StringUtils.isNotBlank(this.getAddress().getZipCode())) {
            this.appendValue(result, ResourceMessages.getMessage("lbl.zipcode"), ". ");
            this.appendValue(result, this.getAddress().getZipCode(), ": ");
        }
        this.appendValue(result, ".", null);
        return result.toString();
    }

    public String formattedContacts() {
        if (this.contacts == null || this.contacts.isEmpty()) {
            return ResourceMessages.getMessage("msg.contacts.not.found");
        }
        StringBuilder result = new StringBuilder();
        for (InstructorContact instructorContact : this.contacts) {
            result.append(ResourceMessages.getMessage(instructorContact.getType().name())).append(": ").append(instructorContact.getDescription());
            result.append("<br/>");
        }
        result.append("<br/>");
        return result.toString().replace("<br/><br/>", "");
    }

    private void appendValue(StringBuilder result, String value, String separator) {
        if (StringUtils.isNotBlank(separator) && StringUtils.isNotBlank(value)) {
            result.append(separator);
        }
        if (StringUtils.isNotBlank(value)) {
            result.append(value);
        }
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getInstitution() {
        return this.institution;
    }

    public void setInstitution(String institution) {
        this.institution = institution;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public User getUser() {
        return this.user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public InstructorAddress getAddress() {
        return this.address;
    }

    public void setAddress(InstructorAddress address) {
        this.address = address;
    }

    public List<InstructorContact> getContacts() {
        return this.contacts;
    }

    public void setContacts(List<InstructorContact> contacts) {
        this.contacts = contacts;
    }

    public String getImage() {
        return this.image;
    }

    public void setImage(String image) {
        this.image = image;
    }

}

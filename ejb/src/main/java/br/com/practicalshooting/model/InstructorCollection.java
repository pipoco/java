package br.com.practicalshooting.model;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

@SuppressWarnings("deprecation")
@Entity
@Table(name = "`TBL_INSTRUCTOR_COLLECTIONS`")
@PrimaryKeyJoinColumn(name = "`INCO_SKU`")
@org.hibernate.annotations.ForeignKey(name = "PK_FK_INSTRUCTOR_COLLECTIONS")
public class InstructorCollection extends UserColletion {

    /**
     * 
     */
    private static final long serialVersionUID = -1599111432056022326L;

    @JoinColumn(name = "`INST_SKU`", nullable = false, foreignKey = @ForeignKey(name = "`FK_INCO_X_COLL`"))
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    private Instructor instructor;

    public Instructor getInstructor() {
        return this.instructor;
    }

    public void setInstructor(Instructor instructor) {
        this.instructor = instructor;
    }
}
package br.com.practicalshooting.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.EntityListeners;
import javax.persistence.MappedSuperclass;
import javax.persistence.Transient;
import javax.persistence.Version;

import org.apache.log4j.Logger;
import org.hibernate.annotations.Type;
import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import br.com.practicalshooting.util.LoggerUtil;
import br.com.practicalshooting.util.TimeStampEntityListener;

@MappedSuperclass
@EntityListeners(TimeStampEntityListener.class)
public abstract class Entity implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -2288353963766246777L;

    @Transient
    private static final Logger LOGGER = Logger.getLogger(LoggerUtil.LOGGER_INFO);

    @Version
    @Column(name = "`NR_VERSION`")
    protected Integer version = 0;

    @Column(name = "`DT_CREATION`")
    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalDate")
    protected LocalDate creationDate;

    @Column(name = "`DT_UPDATED`")
    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalDate")
    protected LocalDate updatedDate;

    public String creationDateToPattern(String pattern) {
        DateTimeFormatter formatter = DateTimeFormat.forPattern(pattern);
        return formatter.print(this.creationDate);
    }

    public String updatedDateToPattern(String pattern) {
        DateTimeFormatter formatter = DateTimeFormat.forPattern(pattern);
        return formatter.print(this.updatedDate);
    }

    public LocalDate getCreationDate() {
        return this.creationDate;
    }

    public void setCreationDate(LocalDate creationDate) {
        this.creationDate = creationDate;
    }

    public Integer getVersion() {
        return this.version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public LocalDate getUpdatedDate() {
        return this.updatedDate;
    }

    public void setUpdatedDate(LocalDate updatedDate) {
        this.updatedDate = updatedDate;
    }

    public static Logger getLogger() {
        return LOGGER;
    }
}
package br.com.practicalshooting.model;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

@SuppressWarnings("deprecation")
@Entity
@Table(name = "`TBL_INSTRUCTOR_ADDRESSES`")
@PrimaryKeyJoinColumn(name = "`INAD_SKU`")
@org.hibernate.annotations.ForeignKey(name = "PK_FK_INSTRUCTOR_ADDRESSES")
public class InstructorAddress extends Address {

    /**
     * 
     */
    private static final long serialVersionUID = 1221787827374849459L;

    @JoinColumn(name = "`INST_SKU`", nullable = false, foreignKey = @ForeignKey(name = "`FK_INAD_X_INST`"))
    @OneToOne(fetch = FetchType.LAZY, optional = false, targetEntity = Instructor.class)
    private Instructor instructor;

    public Instructor getInstructor() {
        return this.instructor;
    }

    public void setInstructor(Instructor instructor) {
        this.instructor = instructor;
    }
}
package br.com.practicalshooting.model;

import java.net.InetAddress;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name = "`TBL_QUAR_CONFIGURATION`", uniqueConstraints = { @UniqueConstraint(name = "`UK_QUAR_NAME_01`", columnNames = { "`NM_PROCESS`" }) })
@AttributeOverrides(value = { @AttributeOverride(name = "id", column = @Column(name = "`QUAR_SKU`")) })
public class QuartzConfiguration extends EntitySku {
    /**
     *
     */
    private static final long serialVersionUID = -800441088188706388L;

    @Column(name = "`NM_PROCESS`", nullable = false, length = 128)
    private String name;

    @Column(name = "`TXT_JOB_CLASS_NAME`", nullable = false)
    private String jobClassName;

    @Column(name = "`TXT_CRON_EXPRESSION`", nullable = false, length = 80)
    private String cronExpression;

    @Column(name = "`TXT_SERVER_EXECUTION`", nullable = false, length = 100)
    private String serverExecution;

    @Column(name = "`FL_AUTO_CONFIG`", nullable = false, columnDefinition = "TINYINT")
    private Boolean autoConfig;

    public Boolean executeOnServer() {
        try {
            if (this.serverExecution.equalsIgnoreCase(InetAddress.getLocalHost().getHostName())) {
                return Boolean.TRUE;
            }
        } catch (Exception e) {
            super.getLogger().error("ERROR ON SERVER NAME", e);
        }
        return Boolean.FALSE;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getJobClassName() {
        return this.jobClassName;
    }

    public void setJobClassName(String jobClassName) {
        this.jobClassName = jobClassName;
    }

    public String getCronExpression() {
        return this.cronExpression;
    }

    public void setCronExpression(String cronExpression) {
        this.cronExpression = cronExpression;
    }

    public Boolean getAutoConfig() {
        return this.autoConfig;
    }

    public void setAutoConfig(Boolean autoConfig) {
        this.autoConfig = autoConfig;
    }

    public String getServerExecution() {
        return this.serverExecution;
    }

    public void setServerExecution(String serverExecution) {
        this.serverExecution = serverExecution;
    }
}
package br.com.practicalshooting.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.Type;
import org.joda.time.LocalDate;
import org.joda.time.LocalTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

@Entity
@Table(name = "`TBL_EVENT_DAYS`")
@AttributeOverrides(value = { @AttributeOverride(name = "id", column = @Column(name = "`EVDA_SKU`")) })
public class EventDay extends EntitySku {

    /**
     * 
     */
    private static final long serialVersionUID = 6390723211342213263L;

    @Column(name = "`DT_EVENT`")
    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalDate")
    private LocalDate day;

    @Column(name = "`HR_START`")
    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalTime")
    private LocalTime start;

    @Column(name = "`HR_END`")
    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalTime")
    private LocalTime end;

    @JoinColumn(name = "`EVEN_SKU`", nullable = false, foreignKey = @ForeignKey(name = "`FK_EVDA_X_EVENT`"))
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    private Event event;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "eventDay", cascade = CascadeType.ALL)
    private List<TypeParticipation> participations;

    @Transient
    private List<TypeParticipation> removedParticipations;

    @Transient
    private Participant participant;

    public void addParticipation(TypeParticipation modelParticipation) {
        this.initParticipationList();
        this.participations.add(modelParticipation);
    }

    public void removeParticipation(TypeParticipation modelParticipation) {
        this.initParticipationList();
        this.participations.remove(modelParticipation);
        this.removedParticipations.add(modelParticipation);
    }

    public void initParticipationList() {
        if (this.participations == null) {
            this.participations = new ArrayList<TypeParticipation>();
        }
        if (this.removedParticipations == null) {
            this.removedParticipations = new ArrayList<TypeParticipation>();
        }
    }

    public String dayToPattern(String pattern) {
        DateTimeFormatter formatter = DateTimeFormat.forPattern(pattern);
        return formatter.print(this.day);
    }

    public String startToPattern(String pattern) {
        DateTimeFormatter formatter = DateTimeFormat.forPattern(pattern);
        return formatter.print(this.start);
    }

    public String endToPattern(String pattern) {
        DateTimeFormatter formatter = DateTimeFormat.forPattern(pattern);
        return formatter.print(this.end);
    }

    public LocalDate getDay() {
        return this.day;
    }

    public void setDay(LocalDate day) {
        this.day = day;
    }

    public LocalTime getStart() {
        return this.start;
    }

    public void setStart(LocalTime start) {
        this.start = start;
    }

    public LocalTime getEnd() {
        return this.end;
    }

    public void setEnd(LocalTime end) {
        this.end = end;
    }

    public Event getEvent() {
        return this.event;
    }

    public void setEvent(Event event) {
        this.event = event;
    }

    public List<TypeParticipation> getParticipations() {
        return this.participations;
    }

    public void setParticipations(List<TypeParticipation> participations) {
        this.participations = participations;
    }

    public List<TypeParticipation> getRemovedParticipations() {
        return this.removedParticipations;
    }

    public void setRemovedParticipations(List<TypeParticipation> removedParticipations) {
        this.removedParticipations = removedParticipations;
    }

    public Participant getParticipant() {
        return this.participant;
    }

    public void setParticipant(Participant participant) {
        this.participant = participant;
    }
}
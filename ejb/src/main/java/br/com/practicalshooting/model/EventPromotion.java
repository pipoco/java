package br.com.practicalshooting.model;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

@SuppressWarnings("deprecation")
@Entity
@Table(name = "`TBL_EVENT_PROMOTIONS`")
@PrimaryKeyJoinColumn(name = "`EVPR_SKU`")
@org.hibernate.annotations.ForeignKey(name = "PK_FK_EVUB_PROMOTION")
public class EventPromotion extends Promotion {

    /**
     * 
     */
    private static final long serialVersionUID = -1071338982112436456L;

    @JoinColumn(name = "`EVEN_SKU`", nullable = false, foreignKey = @ForeignKey(name = "`FK_EVPR_X_EVEN`"))
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    private Event event;

    public Event getEvent() {
        return this.event;
    }

    public void setEvent(Event event) {
        this.event = event;
    }
}
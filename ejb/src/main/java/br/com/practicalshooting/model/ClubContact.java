package br.com.practicalshooting.model;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

import br.com.practicalshooting.model.enumeration.ContactType;

@SuppressWarnings("deprecation")
@Entity
@Table(name = "`TBL_CLUB_CONTACTS`")
@PrimaryKeyJoinColumn(name = "`CLCT_SKU`")
@org.hibernate.annotations.ForeignKey(name = "PK_FK_CLUB_CONTACTS")
public class ClubContact extends Contact {

    /**
     * 
     */
    private static final long serialVersionUID = 8898439720788551340L;

    @JoinColumn(name = "`CLUB_SKU`", nullable = false, foreignKey = @ForeignKey(name = "`FK_CLCT_X_CLUB`"))
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    private Club club;

    public ClubContact() {
        super();
        this.club = new Club();
    }

    public ClubContact(Club club, String description, ContactType type) {
        super(description, type);
        this.club = club;
    }

    public Club getClub() {
        return this.club;
    }

    public void setClub(Club club) {
        this.club = club;
    }
}
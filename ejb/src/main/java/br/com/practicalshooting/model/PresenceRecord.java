package br.com.practicalshooting.model;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Type;
import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import br.com.practicalshooting.model.enumeration.EventType;

@Entity
@Table(name = "`TBL_PRESENCE_RECORDS`")
@AttributeOverrides(value = { @AttributeOverride(name = "id", column = @Column(name = "`PRRE_SKU`")) })
public class PresenceRecord extends EntitySku {

    /**
     * 
     */
    private static final long serialVersionUID = 6494733830877036807L;

    @Enumerated(EnumType.ORDINAL)
    @Column(name = "`TP_EVENT`", columnDefinition = "TINYINT")
    private EventType eventType;

    @Column(name = "`DT_RECORD`")
    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalDate")
    private LocalDate recordDate;

    @JoinColumn(name = "`ASSO_SKU`", nullable = false, foreignKey = @ForeignKey(name = "`FK_PRRE_X_ASSO`"))
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    private Associate associate;

    public String recordDateToPattern(String pattern) {
        DateTimeFormatter formatter = DateTimeFormat.forPattern(pattern);
        return formatter.print(this.recordDate);
    }

    public EventType getEventType() {
        return this.eventType;
    }

    public void setEventType(EventType eventType) {
        this.eventType = eventType;
    }

    public LocalDate getRecordDate() {
        return this.recordDate;
    }

    public void setRecordDate(LocalDate recordDate) {
        this.recordDate = recordDate;
    }

    public Associate getAssociate() {
        return this.associate;
    }

    public void setAssociate(Associate associate) {
        this.associate = associate;
    }
}
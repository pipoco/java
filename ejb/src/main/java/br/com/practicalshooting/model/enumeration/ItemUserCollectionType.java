package br.com.practicalshooting.model.enumeration;

public enum ItemUserCollectionType {
    GET_ALERTS, NO_ALERT, ONLY_CHANGES
}

package br.com.practicalshooting.model;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Type;
import org.joda.time.DateTime;

@Entity
@Table(name = "`TBL_QUARTZ_LOG`")
@AttributeOverrides(value = { @AttributeOverride(name = "id", column = @Column(name = "`QULO_SKU`")) })
public class QuartzLog extends EntitySku {

    /**
     * 
     */
    private static final long serialVersionUID = -4702839022200409380L;

    @Column(name = "`NM_PROCESS`", length = 128)
    private String processName;

    @Column(name = "`DTI_START`")
    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime")
    private DateTime startDate;

    @Column(name = "`DTI_END`")
    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime")
    private DateTime endDate;

    @Column(name = "`USER_SKU`", nullable = true)
    private Long userExecution;

    @JoinColumn(name = "`QUAR_SKU`", nullable = false, foreignKey = @ForeignKey(name = "`FK_QUAR_QUARTZ_CONFIGURATION`"))
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    private QuartzConfiguration quartzConfiguration;

    public String getProcessName() {
        return this.processName;
    }

    public void setProcessName(String processName) {
        this.processName = processName;
    }

    public DateTime getStartDate() {
        return this.startDate;
    }

    public void setStartDate(DateTime startDate) {
        this.startDate = startDate;
    }

    public DateTime getEndDate() {
        return this.endDate;
    }

    public void setEndDate(DateTime endDate) {
        this.endDate = endDate;
    }

    public Long getUserExecution() {
        return this.userExecution;
    }

    public void setUserExecution(Long userExecution) {
        this.userExecution = userExecution;
    }

    public QuartzConfiguration getQuartzConfiguration() {
        return this.quartzConfiguration;
    }

    public void setQuartzConfiguration(QuartzConfiguration quartzConfiguration) {
        this.quartzConfiguration = quartzConfiguration;
    }

}

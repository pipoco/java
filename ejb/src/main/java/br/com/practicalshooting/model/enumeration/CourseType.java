package br.com.practicalshooting.model.enumeration;

public enum CourseType {
    PRACTICAL_SHOOTING, GUN_PERMIT, ACQUISITION, REGISTRATION_RENEWAL, FREE_COURSE
}

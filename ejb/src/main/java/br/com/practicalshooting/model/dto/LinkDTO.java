package br.com.practicalshooting.model.dto;

import java.io.Serializable;

public class LinkDTO implements Serializable {
    /**
     * 
     */
    private static final long serialVersionUID = 4268759211243279257L;

    private String uri;
    private String name;
    private String content;

    public LinkDTO(String name, String content, String uri) {
        this.name = name;
        this.content = content;
        this.uri = uri;
    }

    public String getUri() {
        return this.uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getContent() {
        return this.content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}

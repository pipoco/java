package br.com.practicalshooting.model;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

@SuppressWarnings("deprecation")
@Entity
@Table(name = "`TBL_INSTRUCTOR_CONTACTS`")
@PrimaryKeyJoinColumn(name = "`INCT_SKU`")
@org.hibernate.annotations.ForeignKey(name = "PK_FK_INSTRUTOR_CONTACTS")
public class InstructorContact extends Contact {

    /**
     * 
     */
    private static final long serialVersionUID = 7415486901799812417L;

    @JoinColumn(name = "`INST_SKU`", nullable = false, foreignKey = @ForeignKey(name = "`FK_INCT_X_INST`"))
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    private Instructor instructor;

    public Instructor getInstructor() {
        return this.instructor;
    }

    public void setInstructor(Instructor instructor) {
        this.instructor = instructor;
    }
}
package br.com.practicalshooting.model;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "`TBL_EVENT_INVITATIONS`")
@AttributeOverrides(value = { @AttributeOverride(name = "id", column = @Column(name = "`EVIN_SKU`")) })
public class EventInvitation extends EntitySku {

    /**
     * 
     */
    private static final long serialVersionUID = -1429638147523180521L;

    @Column(name = "`DS_DESCRIPTION`", length = 2048)
    private String description;

    @Column(name = "`DS_BANNER_LINK`", length = 2048)
    private String bannerLink;

    @Column(name = "`DS_EARLY_REGISTRATION`", length = 2048)
    private String earlyRegistration;

    @Column(name = "`NUM_SHOTS`")
    private Integer amountShots;

    @Column(name = "`NUM_STAGES`")
    private Integer amountStages;

    @Column(name = "`DS_DAYS`", length = 128)
    private String days;

    @JoinColumn(name = "`EVEN_SKU`", nullable = false, foreignKey = @ForeignKey(name = "`FK_EVIN_X_EVENT`"))
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    private Event event;

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getBannerLink() {
        return this.bannerLink;
    }

    public void setBannerLink(String bannerLink) {
        this.bannerLink = bannerLink;
    }

    public String getEarlyRegistration() {
        return this.earlyRegistration;
    }

    public void setEarlyRegistration(String earlyRegistration) {
        this.earlyRegistration = earlyRegistration;
    }

    public Integer getAmountShots() {
        return this.amountShots;
    }

    public void setAmountShots(Integer amountShots) {
        this.amountShots = amountShots;
    }

    public Integer getAmountStages() {
        return this.amountStages;
    }

    public void setAmountStages(Integer amountStages) {
        this.amountStages = amountStages;
    }

    public String getDays() {
        return this.days;
    }

    public void setDays(String days) {
        this.days = days;
    }

    public Event getEvent() {
        return this.event;
    }

    public void setEvent(Event event) {
        this.event = event;
    }
}
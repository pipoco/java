package br.com.practicalshooting.model;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

@SuppressWarnings("deprecation")
@Entity
@Table(name = "`TBL_CANDIDATE_ADDRESSES`")
@PrimaryKeyJoinColumn(name = "`CAAD_SKU`")
@org.hibernate.annotations.ForeignKey(name = "PK_FK_CANDIDATE_ADDRESSES")
public class CandidateAddress extends Address {

    /**
     * 
     */
    private static final long serialVersionUID = 6503477038956775793L;

    @JoinColumn(name = "`CAND_SKU`", nullable = false, foreignKey = @ForeignKey(name = "`FK_CAAD_X_CAND`"))
    @OneToOne(fetch = FetchType.LAZY, optional = false, targetEntity = Candidate.class)
    private Candidate candidate;

    public Candidate getCandidate() {
        return this.candidate;
    }

    public void setCandidate(Candidate candidate) {
        this.candidate = candidate;
    }

}
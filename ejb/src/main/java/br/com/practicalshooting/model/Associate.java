package br.com.practicalshooting.model;

import java.util.Date;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.Type;
import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import br.com.practicalshooting.model.enumeration.SituationAssociate;
import br.com.practicalshooting.util.Util;

@Entity
@Table(name = "`TBL_ASSOCIATED`")
@AttributeOverrides(value = { @AttributeOverride(name = "id", column = @Column(name = "`ASSO_SKU`")) })
public class Associate extends EntitySku {

    /**
     * 
     */
    private static final long serialVersionUID = 3802396593125326500L;

    @Column(name = "`NUM_REGISTRATION`")
    private Long registration;

    @Column(name = "`DT_BIRTH_DATE`")
    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalDate")
    private LocalDate birthDate;

    @Column(name = "`TXT_CPF`", length = 15)
    private String cpf;

    @Column(name = "`DT_FILIATION`")
    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalDate")
    private LocalDate filiationDate;

    @Column(name = "`DT_LAST_RENOVATION`")
    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalDate")
    private LocalDate lastRenovationDate;

    @Enumerated(EnumType.ORDINAL)
    @Column(name = "`NUM_SITU_ASSOCIATE`", columnDefinition = "TINYINT")
    private SituationAssociate situationAssociate;

    @JoinColumn(name = "`CLUB_SKU`", nullable = true, foreignKey = @ForeignKey(name = "`FK_ASSO_X_CLUB`"))
    @ManyToOne(fetch = FetchType.LAZY, optional = true)
    private Club club;

    @JoinColumn(name = "`USER_SKU`", nullable = false, foreignKey = @ForeignKey(name = "`FK_ASSO_X_USER`"))
    @OneToOne(fetch = FetchType.LAZY, optional = false, targetEntity = User.class)
    private User user;

    @Transient
    private Date transientBirthDate;
    @Transient
    private Date transientFiliationDate;
    @Transient
    private Date transientLastRenovationDate;

    public Associate() {
        if (!Util.isPersistentEntity(this.club)) {
            this.club = new Club();
        }
    }

    public String birthDateToPattern(String pattern) {
        if (this.birthDate == null) {
            return "";
        }
        DateTimeFormatter formatter = DateTimeFormat.forPattern(pattern);
        return formatter.print(this.birthDate);
    }

    public String filiationDateToPattern(String pattern) {
        if (this.filiationDate == null) {
            return "";
        }
        DateTimeFormatter formatter = DateTimeFormat.forPattern(pattern);
        return formatter.print(this.filiationDate);
    }

    public String lastRenovationDateToPattern(String pattern) {
        if (this.lastRenovationDate == null) {
            return "";
        }
        DateTimeFormatter formatter = DateTimeFormat.forPattern(pattern);
        return formatter.print(this.lastRenovationDate);
    }

    public Long getRegistration() {
        return this.registration;
    }

    public void setRegistration(Long registration) {
        this.registration = registration;
    }

    public LocalDate getBirthDate() {
        return this.birthDate;
    }

    public void setBirthDate(LocalDate birthDate) {
        this.birthDate = birthDate;
    }

    public String getCpf() {
        return this.cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public LocalDate getFiliationDate() {
        return this.filiationDate;
    }

    public void setFiliationDate(LocalDate filiationDate) {
        this.filiationDate = filiationDate;
    }

    public LocalDate getLastRenovationDate() {
        return this.lastRenovationDate;
    }

    public void setLastRenovationDate(LocalDate lastRenovationDate) {
        this.lastRenovationDate = lastRenovationDate;
    }

    public SituationAssociate getSituationAssociate() {
        return this.situationAssociate;
    }

    public void setSituationAssociate(SituationAssociate situationAssociate) {
        this.situationAssociate = situationAssociate;
    }

    public Club getClub() {
        return this.club;
    }

    public void setClub(Club club) {
        this.club = club;
    }

    public User getUser() {
        return this.user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Date getTransientBirthDate() {
        return this.birthDate == null ? null : this.birthDate.toDate();
    }

    public void setTransientBirthDate(Date transientBirthDate) {
        this.transientBirthDate = transientBirthDate;
        this.birthDate = new LocalDate(transientBirthDate);
    }

    public Date getTransientFiliationDate() {
        return this.filiationDate == null ? null : this.filiationDate.toDate();
    }

    public void setTransientFiliationDate(Date transientFiliationDate) {
        this.transientFiliationDate = transientFiliationDate;
        this.filiationDate = new LocalDate(transientFiliationDate);
    }

    public Date getTransientLastRenovationDate() {
        return this.transientLastRenovationDate;
    }

    public void setTransientLastRenovationDate(Date transientLastRenovationDate) {
        this.transientLastRenovationDate = transientLastRenovationDate;
    }
}
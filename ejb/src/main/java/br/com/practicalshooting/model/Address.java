package br.com.practicalshooting.model;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;

@Entity
@Table(name = "`TBL_ADDRESSES`")
@AttributeOverrides(value = { @AttributeOverride(name = "id", column = @Column(name = "`ADDR_SKU`")) })
@Inheritance(strategy = InheritanceType.JOINED)
public class Address extends EntitySku {

    /**
     * 
     */
    private static final long serialVersionUID = -3930693581146527772L;

    @Column(name = "`TXT_PLACE`", length = 255)
    private String place;

    @Column(name = "`TXT_ZIP_CODE`", length = 10)
    private String zipCode;

    @Column(name = "`TXT_NUMBER`", length = 30)
    private String number;

    @Column(name = "`TXT_COMPLEMENT`", length = 128)
    private String complement;

    @Column(name = "`TXT_DISTRICT`", length = 128)
    private String district;

    @Column(name = "`TXT_CITY`", length = 128)
    private String city;

    @Column(name = "`TXT_STATE`", length = 2)
    private String state;

    public String getPlace() {
        return this.place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public String getZipCode() {
        return this.zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getNumber() {
        return this.number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getComplement() {
        return this.complement;
    }

    public void setComplement(String complement) {
        this.complement = complement;
    }

    public String getDistrict() {
        return this.district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getState() {
        return this.state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCity() {
        return this.city;
    }

    public void setCity(String city) {
        this.city = city;
    }
}
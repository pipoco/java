package br.com.practicalshooting.model;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import br.com.practicalshooting.model.enumeration.AlertType;

@Entity
@Table(name = "`TBL_ACCOMPANIMENTS`")
@AttributeOverrides(value = { @AttributeOverride(name = "id", column = @Column(name = "`ACCO_SKU`")) })
public class Accompaniment extends EntitySku {

    /**
     * 
     */
    private static final long serialVersionUID = 2170571232943095453L;

    @Enumerated(EnumType.ORDINAL)
    @Column(name = "`TP_ALERT`", columnDefinition = "TINYINT")
    private AlertType receiveAlert;

    @Column(name = "`NUM_DAYS_BEFORE`")
    private Integer daysBefore;

    @JoinColumn(name = "`USER_SKU`", nullable = false, foreignKey = @ForeignKey(name = "`FK_ACCO_X_USER`"))
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    private User user;

    @JoinColumn(name = "`EVEN_SKU`", nullable = false, foreignKey = @ForeignKey(name = "`FK_ACCO_X_EVENT`"))
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    private Event event;

    public AlertType getReceiveAlert() {
        return this.receiveAlert;
    }

    public void setReceiveAlert(AlertType receiveAlert) {
        this.receiveAlert = receiveAlert;
    }

    public Integer getDaysBefore() {
        return this.daysBefore;
    }

    public void setDaysBefore(Integer daysBefore) {
        this.daysBefore = daysBefore;
    }

    public User getUser() {
        return this.user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Event getEvent() {
        return this.event;
    }

    public void setEvent(Event event) {
        this.event = event;
    }
}
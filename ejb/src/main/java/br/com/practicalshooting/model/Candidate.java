package br.com.practicalshooting.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.annotations.Type;
import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import br.com.practicalshooting.model.enumeration.ContactType;
import br.com.practicalshooting.model.enumeration.CourseType;
import br.com.practicalshooting.model.enumeration.SituationCourse;
import br.com.practicalshooting.util.ResourceMessages;
import br.com.practicalshooting.util.Util;

@Entity
@Table(name = "`TBL_CANDIDATES`")
@AttributeOverrides(value = { @AttributeOverride(name = "id", column = @Column(name = "`CAND_SKU`")) })
public class Candidate extends EntitySku {

    /**
     *
     */
    private static final long serialVersionUID = 6962000711976236895L;

    @Column(name = "`NM_FATHER`", length = 128)
    private String fatherName;

    @Column(name = "`NM_MOTHER`", length = 128)
    private String motherName;

    @Column(name = "`TXT_RG`", length = 30)
    private String rg;

    @Column(name = "`TXT_CPF`", length = 15)
    private String cpf;

    @Column(name = "`TXT_OBSERVATIONS`", length = 200)
    private String observations;

    @Column(name = "`FL_PSYCHOLOGICAL_REPORT`", columnDefinition = "TINYINT")
    private Boolean psychologicalReport;

    @Column(name = "`FL_CRIMINAL_RECORD`", columnDefinition = "TINYINT")
    private Boolean criminalRecord;

    @Column(name = "`FL_CTVV_ASSOCIATED`", columnDefinition = "TINYINT")
    private Boolean ctvvAssociated;

    @Column(name = "`NUM_CARD_NUMBER`")
    private Integer cardNumber;

    @Column(name = "`VL_PUNCTUATION_THEORETICAL`")
    private Integer punctuationTheoretical;

    @Column(name = "`VL_THEORETICAL_NOTE`")
    private Double theoreticalNote;

    @Enumerated(EnumType.ORDINAL)
    @Column(name = "`NUM_SITU_COURSE`", columnDefinition = "TINYINT")
    private SituationCourse situation;

    @Enumerated(EnumType.ORDINAL)
    @Column(name = "`NUM_COURSE_TYPE`", columnDefinition = "TINYINT")
    private CourseType courseType;

    @Column(name = "`DT_REALIZATION`")
    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalDate")
    private LocalDate realization;

    @Column(name = "`DT_RECEIPT_REPORTS`")
    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalDate")
    private LocalDate receiptReports;

    @Column(name = "`FL_UNABLE`")
    private Boolean unable;

    @JoinColumn(name = "`USER_SKU`", nullable = false, foreignKey = @ForeignKey(name = "`FK_CAND_X_USER`"))
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    private User user;

    @JoinColumn(name = "`INST_SKU`", nullable = false, foreignKey = @ForeignKey(name = "`FK_CAND_X_INST`"))
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    private Instructor instructor;

    @OneToOne(fetch = FetchType.LAZY, mappedBy = "candidate", cascade = CascadeType.ALL)
    private CandidateAddress address;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "candidate", cascade = CascadeType.ALL)
    private List<CandidateContact> contacts;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "candidate", cascade = CascadeType.ALL)
    private List<WeaponOfCourse> weapons;

    @Transient
    private Date realizationDate;

    @Transient
    private Date receiptReportsDate;

    @Transient
    private Integer pointsPractice;

    public Candidate() {
        if (!Util.isPersistentEntity(this.user)) {
            this.user = new User();
        }
        this.initAddress();
        if ((this.contacts == null) || this.contacts.isEmpty()) {
            this.contacts = new ArrayList<CandidateContact>();
        }
        if ((this.weapons == null) || this.weapons.isEmpty()) {
            this.weapons = new ArrayList<WeaponOfCourse>();
        }
    }

    public void initAddress() {
        if (!Util.isPersistentEntity(this.address)) {
            this.address = new CandidateAddress();
        }
        this.address.setCandidate(this);
    }

    public boolean contactPhone(ContactType contactType) {
        if (contactType == null) {
            return false;
        }
        return contactType.equals(ContactType.CELL_PHONE) || contactType.equals(ContactType.PHONE) || contactType.equals(ContactType.COMMERCIAL_PHONE);
    }

    public boolean contactMail(ContactType contactType) {
        if (contactType == null) {
            return false;
        }
        return contactType.equals(ContactType.EMAIL);
    }

    public boolean contactOther(ContactType contactType) {
        if (contactType == null) {
            return true;
        }
        return !this.contactMail(contactType) && !this.contactPhone(contactType);
    }

    public String formattedAddress() {
        if (!Util.isPersistentEntity(this.getAddress())) {
            return ResourceMessages.getMessage("msg.address.not.found");
        }
        StringBuilder result = new StringBuilder();
        this.appendValue(result, this.getAddress().getPlace(), null);
        this.appendValue(result, this.getAddress().getNumber(), ", ");
        this.appendValue(result, this.getAddress().getComplement(), " - ");
        result.append("<br/>");
        this.appendValue(result, this.getAddress().getDistrict(), null);
        this.appendValue(result, this.getAddress().getCity(), " - ");
        this.appendValue(result, this.getAddress().getState(), "/");
        if (StringUtils.isNotBlank(this.getAddress().getZipCode())) {
            this.appendValue(result, ResourceMessages.getMessage("lbl.zipcode"), ". ");
            this.appendValue(result, this.getAddress().getZipCode(), ": ");
        }
        this.appendValue(result, ".", null);
        return result.toString();
    }

    public String formattedContacts() {
        if ((this.contacts == null) || this.contacts.isEmpty()) {
            return ResourceMessages.getMessage("msg.contacts.not.found");
        }
        StringBuilder result = new StringBuilder();
        for (CandidateContact candidateContact : this.contacts) {
            result.append(ResourceMessages.getMessage(candidateContact.getType().name())).append(": ").append(candidateContact.getDescription());
            result.append("<br/>");
        }
        result.append("<br/>");
        return result.toString().replace("<br/><br/>", "");
    }

    private void appendValue(StringBuilder result, String value, String separator) {
        if (StringUtils.isNotBlank(separator) && StringUtils.isNotBlank(value)) {
            result.append(separator);
        }
        if (StringUtils.isNotBlank(value)) {
            result.append(value);
        }
    }

    public String getRg() {
        return this.rg;
    }

    public void setRg(String rg) {
        this.rg = rg;
    }

    public String getCpf() {
        return this.cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public Boolean getPsychologicalReport() {
        return this.psychologicalReport;
    }

    public void setPsychologicalReport(Boolean psychologicalReport) {
        this.psychologicalReport = psychologicalReport;
    }

    public Boolean getCriminalRecord() {
        return this.criminalRecord;
    }

    public void setCriminalRecord(Boolean criminalRecord) {
        this.criminalRecord = criminalRecord;
    }

    public SituationCourse getSituation() {
        return this.situation;
    }

    public void setSituation(SituationCourse situation) {
        this.situation = situation;
    }

    public User getUser() {
        return this.user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Instructor getInstructor() {
        return this.instructor;
    }

    public void setInstructor(Instructor instructor) {
        this.instructor = instructor;
    }

    public String realizationDateToPattern(String pattern) {
        if (this.realization == null) {
            return "";
        }
        DateTimeFormatter formatter = DateTimeFormat.forPattern(pattern);
        return formatter.print(this.realization);
    }

    public LocalDate getReceiptReports() {
        return this.receiptReports;
    }

    public void setReceiptReports(LocalDate receiptReports) {
        this.receiptReports = receiptReports;
    }

    @Override
    public String creationDateToPattern(String pattern) {
        if (this.creationDate == null) {
            return "";
        }
        DateTimeFormatter formatter = DateTimeFormat.forPattern(pattern);
        return formatter.print(super.creationDate);
    }

    public Date getRealizationDate() {
        this.realizationDate = this.realization == null ? null : this.realization.toDate();
        return this.realizationDate;
    }

    public void setRealizationDate(Date realizationDate) {
        this.realizationDate = realizationDate;
        this.realization = this.realizationDate == null ? null : new LocalDate(realizationDate);
    }

    public Date getReceiptReportsDate() {
        this.receiptReportsDate = this.receiptReports == null ? null : this.receiptReports.toDate();
        return this.receiptReportsDate;
    }

    public void setReceiptReportsDate(Date receiptReportsDate) {
        this.receiptReportsDate = receiptReportsDate;
        this.receiptReports = this.receiptReportsDate == null ? null : new LocalDate(receiptReportsDate);
    }

    public String getFatherName() {
        return this.fatherName;
    }

    public void setFatherName(String fatherName) {
        this.fatherName = fatherName;
    }

    public String getMotherName() {
        return this.motherName;
    }

    public void setMotherName(String motherName) {
        this.motherName = motherName;
    }

    public LocalDate getRealization() {
        return this.realization;
    }

    public void setRealization(LocalDate realization) {
        this.realization = realization;
    }

    public CandidateAddress getAddress() {
        return this.address;
    }

    public void setAddress(CandidateAddress address) {
        this.address = address;
    }

    public List<CandidateContact> getContacts() {
        return this.contacts;
    }

    public void setContacts(List<CandidateContact> contacts) {
        this.contacts = contacts;
    }

    public List<WeaponOfCourse> getWeapons() {
        return this.weapons;
    }

    public void setWeapons(List<WeaponOfCourse> weapons) {
        this.weapons = weapons;
    }

    public Integer getPunctuationTheoretical() {
        return this.punctuationTheoretical;
    }

    public void setPunctuationTheoretical(Integer punctuationTheoretical) {
        this.punctuationTheoretical = punctuationTheoretical;
    }

    public Double getTheoreticalNote() {
        return this.theoreticalNote;
    }

    public void setTheoreticalNote(Double theoreticalNote) {
        this.theoreticalNote = theoreticalNote;
    }

    public Integer getPointsPractice() {
        return this.pointsPractice;
    }

    public void setPointsPractice(Integer pointsPractice) {
        this.pointsPractice = pointsPractice;
    }

    public boolean containsContact(CandidateContact candidateContact) {
        if (this.contacts == null) {
            this.contacts = new ArrayList<CandidateContact>();
            return false;
        }
        boolean exists = false;
        // TODO: IMPLEMENTAR EQUALS
        for (CandidateContact item : this.contacts) {
            if (item.getDescription().equalsIgnoreCase(candidateContact.getDescription()) && item.getType().equals(candidateContact.getType())) {
                exists = true;
                break;
            }
        }
        return exists;
    }

    public String messageToRemove() {
        StringBuilder result = new StringBuilder();
        result.append(this.creationDateToPattern("dd/MM/yyyy")).append(" - ").append(ResourceMessages.getMessage(this.courseType.name())).append(" - ").append(this.instructor.getUser().getName());
        return result.toString();
    }

    public Boolean getCtvvAssociated() {
        return this.ctvvAssociated;
    }

    public void setCtvvAssociated(Boolean ctvvAssociated) {
        this.ctvvAssociated = ctvvAssociated;
    }

    public CourseType getCourseType() {
        return this.courseType;
    }

    public void setCourseType(CourseType courseType) {
        this.courseType = courseType;
    }

    public Integer getCardNumber() {
        return this.cardNumber;
    }

    public void setCardNumber(Integer cardNumber) {
        this.cardNumber = cardNumber;
    }

    public Boolean getUnable() {
        return this.unable;
    }

    public void setUnable(Boolean unable) {
        this.unable = unable;
    }

    public String getObservations() {
        return this.observations;
    }

    public void setObservations(String observations) {
        this.observations = observations;
    }

    public void addNewWeapon(WeaponOfCourse model) {
        if (this.weapons == null) {
            this.weapons = new ArrayList<WeaponOfCourse>();
        }
        this.weapons.add(model);
    }

    public String popPhone() {
        String phone = null;
        if (Util.isPersistentEntity(this)) {
            for (CandidateContact studentContact : this.getContacts()) {
                if (studentContact.getType().equals(ContactType.PHONE)) {
                    phone = studentContact.getDescription();
                    break;
                }
            }
        }
        return phone;
    }

    public String popMail() {
        String mail = null;
        if (Util.isPersistentEntity(this)) {
            for (CandidateContact studentContact : this.getContacts()) {
                if (studentContact.getType().equals(ContactType.EMAIL)) {
                    mail = studentContact.getDescription();
                    break;
                }
            }
        }
        return mail;
    }
}
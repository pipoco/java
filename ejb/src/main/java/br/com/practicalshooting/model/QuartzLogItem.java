package br.com.practicalshooting.model;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Type;
import org.joda.time.DateTime;

import br.com.practicalshooting.model.enumeration.QuartzLogType;
import br.com.practicalshooting.util.Util;

@Entity
@Table(name = "`TBL_QUARTZ_LOG_ITEM`")
@AttributeOverrides(value = { @AttributeOverride(name = "id", column = @Column(name = "`QLIT_SKU`")) })
public class QuartzLogItem extends EntitySku {
    /**
     * 
     */
    private static final long serialVersionUID = -336506358227593459L;

    @Column(name = "`TXT_LOG`", columnDefinition = "LONGTEXT")
    private String content;

    @Column(name = "`DTI_LOG`")
    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime")
    private DateTime moment;

    @Enumerated(EnumType.ORDINAL)
    @Column(name = "`TP_LOG`", columnDefinition = "TINYINT")
    private QuartzLogType type;

    @JoinColumn(name = "`QULO_SKU`", nullable = false, foreignKey = @ForeignKey(name = "`FK_QLIT_LOG_QUARTZ`"))
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    private QuartzLog quartzLog;

    public QuartzLogItem() {
        super();
        this.quartzLog = new QuartzLog();
    }

    public void checkRelationship() {
        this.quartzLog = Util.isPersistentEntity(this.quartzLog) ? this.quartzLog : null;
    }

    public String getContent() {
        return this.content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public DateTime getMoment() {
        return this.moment;
    }

    public void setMoment(DateTime moment) {
        this.moment = moment;
    }

    public QuartzLogType getType() {
        return this.type;
    }

    public void setType(QuartzLogType type) {
        this.type = type;
    }

    public QuartzLog getQuartzLog() {
        return this.quartzLog;
    }

    public void setQuartzLog(QuartzLog quartzLog) {
        this.quartzLog = quartzLog;
    }
}

package br.com.practicalshooting.model.enumeration;

public enum MediaType {
    IMAGE, VIDEO
}

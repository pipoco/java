package br.com.practicalshooting.model.enumeration;

public enum NotionType {
    CELL_PHONE, COMMERCIAL_PHONE, PHONE, EMAIL, FACEBOOK, LINKEDIN
}

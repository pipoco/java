package br.com.practicalshooting.model;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "`TBL_TYPES_PARTICIPATION`")
@AttributeOverrides(value = { @AttributeOverride(name = "id", column = @Column(name = "`TYPA_SKU`")) })
public class TypeParticipation extends EntitySku {

    /**
     * 
     */
    private static final long serialVersionUID = -6953707039614966238L;

    @JoinColumn(name = "`PART_SKU`", nullable = false, foreignKey = @ForeignKey(name = "`FK_TYPA_X_PARTICIPANT`"))
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    private Participant type;

    @JoinColumn(name = "`EVDA_SKU`", nullable = false, foreignKey = @ForeignKey(name = "`FK_TYPA_X_EVDA`"))
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    private EventDay eventDay;

    public TypeParticipation() {
        this.type = new Participant();
    }

    public Participant getType() {
        return this.type;
    }

    public void setType(Participant type) {
        this.type = type;
    }

    public EventDay getEventDay() {
        return this.eventDay;
    }

    public void setEventDay(EventDay eventDay) {
        this.eventDay = eventDay;
    }
}
package br.com.practicalshooting.model;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;

import br.com.practicalshooting.model.enumeration.Situation;

@Entity
@Table(name = "`TBL_COLLECTIONS`")
@AttributeOverrides(value = { @AttributeOverride(name = "id", column = @Column(name = "`COLL_SKU`")) })
@Inheritance(strategy = InheritanceType.JOINED)
public class UserColletion extends EntitySku {

    /**
     * 
     */
    private static final long serialVersionUID = -1083342155773701579L;

    @Column(name = "`NM_COLLECTION`", length = 128)
    private String name;

    @Enumerated(EnumType.ORDINAL)
    @Column(name = "`NUM_SITUATION`", columnDefinition = "TINYINT")
    private Situation situation;

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Situation getSituation() {
        return this.situation;
    }

    public void setSituation(Situation situation) {
        this.situation = situation;
    }
}
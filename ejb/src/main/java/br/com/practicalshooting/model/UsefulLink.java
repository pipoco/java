package br.com.practicalshooting.model;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "`TBL_USEFUL_LINKS`")
@AttributeOverrides(value = { @AttributeOverride(name = "id", column = @Column(name = "`USLI_SKU`")) })
public class UsefulLink extends EntitySku {

    /**
     * 
     */
    private static final long serialVersionUID = 7424971309592332578L;

    @Column(name = "`NM_LINK`", length = 128)
    private String name;

    @Column(name = "`TXT_LINK`", length = 2048)
    private String link;

    @Column(name = "`DS_LINK`", length = 1024)
    private String description;

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLink() {
        return this.link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
package br.com.practicalshooting.model.enumeration;

import java.io.Serializable;

public enum SortOrderWrapper implements Serializable {
    ASC, DESC;

    public static SortOrderWrapper fromPrimeFaces(String sortOrder) {
        if ("ASCENDING".equalsIgnoreCase(sortOrder)) {
            return SortOrderWrapper.ASC;
        } else if ("DESCENDING".equalsIgnoreCase(sortOrder)) {
            return SortOrderWrapper.DESC;
        }
        return null;
    }

    public boolean isAscending() {
        return this.equals(SortOrderWrapper.ASC);
    }
}

package br.com.practicalshooting.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.apache.commons.lang3.StringUtils;

import br.com.practicalshooting.model.enumeration.ContactType;
import br.com.practicalshooting.model.enumeration.SituationAdministrator;
import br.com.practicalshooting.util.ResourceMessages;
import br.com.practicalshooting.util.Util;

@Entity
@Table(name = "`TBL_CLUBS`")
@AttributeOverrides(value = { @AttributeOverride(name = "id", column = @Column(name = "`CLUB_SKU`")) })
public class Club extends EntitySku {

    /**
     *
     */
    private static final long serialVersionUID = 9049619392142980884L;

    @Column(name = "`CD_CLUB`", length = 20)
    private String code;

    @Column(name = "`NM_NAME`", length = 128)
    private String name;

    @Column(name = "`TXT_LINK_IMAGE`", length = 2048)
    private String image;

    @Column(name = "`TXT_REFERENCE`", length = 128)
    private String reference;

    @Column(name = "`TXT_MAP_LINK`", length = 2048)
    private String mapLink;

    @Column(name = "`NM_PRESIDENT`", length = 128)
    private String president;

    @Column(name = "`VL_ORDER`")
    private int order;

    @Enumerated(EnumType.ORDINAL)
    @Column(name = "`NUM_SITU_ADMINISTRATOR`", columnDefinition = "TINYINT")
    private SituationAdministrator administration;

    @JoinColumn(name = "`USER_SKU`", nullable = true, foreignKey = @ForeignKey(name = "`FK_CLUB_X_USER`"))
    @ManyToOne(fetch = FetchType.LAZY, optional = true)
    private User administrator;

    @OneToOne(fetch = FetchType.LAZY, mappedBy = "club", cascade = CascadeType.ALL)
    private ClubAddress address;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "club", cascade = CascadeType.ALL)
    private List<ClubContact> contacts;

    public Club() {
        this.initAddress();
        if ((this.contacts == null) || this.contacts.isEmpty()) {
            this.contacts = new ArrayList<ClubContact>();
        }
    }

    public Club(Long id, String name) {
        super.setId(id);
        this.setName(name);
        this.initAddress();
        if ((this.contacts == null) || this.contacts.isEmpty()) {
            this.contacts = new ArrayList<ClubContact>();
        }
    }

    public void initAddress() {
        if (!Util.isPersistentEntity(this.address)) {
            this.address = new ClubAddress();
        }
        this.address.setClub(this);
    }

    public boolean contactPhone(ContactType contactType) {
        if (contactType == null) {
            return false;
        }
        return contactType.equals(ContactType.CELL_PHONE) || contactType.equals(ContactType.PHONE) || contactType.equals(ContactType.COMMERCIAL_PHONE);
    }

    public boolean contactMail(ContactType contactType) {
        if (contactType == null) {
            return false;
        }
        return contactType.equals(ContactType.EMAIL);
    }

    public boolean contactOther(ContactType contactType) {
        if (contactType == null) {
            return true;
        }
        return !this.contactMail(contactType) && !this.contactPhone(contactType);
    }

    public String formattedAddress() {
        if (!Util.isPersistentEntity(this.getAddress())) {
            return ResourceMessages.getMessage("msg.address.not.found");
        }
        StringBuilder result = new StringBuilder();
        this.appendValue(result, this.getAddress().getPlace(), null);
        this.appendValue(result, this.getAddress().getNumber(), ", ");
        this.appendValue(result, this.getAddress().getComplement(), " - ");
        result.append("<br/>");
        this.appendValue(result, this.getAddress().getDistrict(), null);
        this.appendValue(result, this.getAddress().getCity(), " - ");
        this.appendValue(result, this.getAddress().getState(), "/");
        if (StringUtils.isNotBlank(this.getAddress().getZipCode())) {
            this.appendValue(result, ResourceMessages.getMessage("lbl.zipcode"), ". ");
            this.appendValue(result, this.getAddress().getZipCode(), ": ");
        }
        this.appendValue(result, ".", null);
        return result.toString();
    }

    public String formattedContacts() {
        if ((this.contacts == null) || this.contacts.isEmpty()) {
            return ResourceMessages.getMessage("msg.contacts.not.found");
        }
        StringBuilder result = new StringBuilder();
        for (ClubContact clubContact : this.contacts) {
            result.append(ResourceMessages.getMessage(clubContact.getType().name())).append(": ").append(clubContact.getDescription());
            result.append("<br/>");
        }
        result.append("<br/>");
        return result.toString().replace("<br/><br/>", "");
    }

    private void appendValue(StringBuilder result, String value, String separator) {
        if (StringUtils.isNotBlank(separator) && StringUtils.isNotBlank(value)) {
            result.append(separator);
        }
        if (StringUtils.isNotBlank(value)) {
            result.append(value);
        }
    }

    public String getCode() {
        return this.code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return this.image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getReference() {
        return this.reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public String getMapLink() {
        return this.mapLink;
    }

    public void setMapLink(String mapLink) {
        this.mapLink = mapLink;
    }

    public String getPresident() {
        return this.president;
    }

    public void setPresident(String president) {
        this.president = president;
    }

    public SituationAdministrator getAdministration() {
        return this.administration;
    }

    public void setAdministration(SituationAdministrator administration) {
        this.administration = administration;
    }

    public User getAdministrator() {
        return this.administrator;
    }

    public void setAdministrator(User administrator) {
        this.administrator = administrator;
    }

    public ClubAddress getAddress() {
        return this.address;
    }

    public void setAddress(ClubAddress address) {
        this.address = address;
    }

    public List<ClubContact> getContacts() {
        return this.contacts;
    }

    public void setContacts(List<ClubContact> contacts) {
        this.contacts = contacts;
    }

    public int getOrder() {
        return this.order;
    }

    public void setOrder(int order) {
        this.order = order;
    }

    public boolean containsContact(ClubContact clubContact) {
        if (this.contacts == null) {
            this.contacts = new ArrayList<ClubContact>();
            return false;
        }
        boolean exists = false;
        // TODO: IMPLEMENTAR EQUALS
        for (ClubContact item : this.contacts) {
            if (item.getDescription().equalsIgnoreCase(clubContact.getDescription()) && item.getType().equals(clubContact.getType())) {
                exists = true;
                break;
            }
        }
        return exists;
    }
}
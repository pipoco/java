package br.com.practicalshooting.model;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.annotations.Type;
import org.joda.time.LocalDate;

@Entity
@Table(name = "`TBL_ERROS`")
@AttributeOverrides(value = { @AttributeOverride(name = "id", column = @Column(name = "`ERRO_SKU`")) })
public class ErrorWrapper extends EntitySku {

    /**
     * 
     */
    private static final long serialVersionUID = -626291650605469575L;

    @Column(name = "`DT_MOMENT`")
    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalDate")
    private LocalDate moment;

    @Column(name = "`DS_ERROR_MESSAGE`")
    private String message;

    @Column(name = "`DS_STACKTRACE`", columnDefinition = "LONGTEXT")
    private String stackTrace;

    @Column(name = "`NM_VIEW`", length = 128)
    private String viewId;

    @Column(name = "`DS_DETAILS`", length = 1024)
    private String details;

    @Column(name = "`USER_SKU`")
    private Long userSku;

    public LocalDate getMoment() {
        return this.moment;
    }

    public void setMoment(LocalDate moment) {
        this.moment = moment;
    }

    public String getMessage() {
        return this.message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStackTrace() {
        return this.stackTrace;
    }

    public void setStackTrace(String stackTrace) {
        this.stackTrace = stackTrace;
    }

    public String getViewId() {
        return this.viewId;
    }

    public void setViewId(String viewId) {
        this.viewId = viewId;
    }

    public String getDetails() {
        return this.details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public Long getUserSku() {
        return this.userSku;
    }

    public void setUserSku(Long userSku) {
        this.userSku = userSku;
    }

}
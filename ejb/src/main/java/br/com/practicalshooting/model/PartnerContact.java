package br.com.practicalshooting.model;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

@SuppressWarnings("deprecation")
@Entity
@Table(name = "`TBL_PARTNER_CONTACTS`")
@PrimaryKeyJoinColumn(name = "`PACT_SKU`")
@org.hibernate.annotations.ForeignKey(name = "PK_FK_PARTNER_CONTACTS")
public class PartnerContact extends Contact {
    /**
     * 
     */
    private static final long serialVersionUID = 2169069239860269008L;

    @JoinColumn(name = "`PART_SKU`", nullable = false, foreignKey = @ForeignKey(name = "`FK_PACT_X_PART`"))
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    private Partner partner;

    public Partner getPartner() {
        return this.partner;
    }

    public void setPartner(Partner partner) {
        this.partner = partner;
    }
}
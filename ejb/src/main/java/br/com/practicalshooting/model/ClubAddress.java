package br.com.practicalshooting.model;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

@SuppressWarnings("deprecation")
@Entity
@Table(name = "`TBL_CLUB_ADDRESSES`")
@PrimaryKeyJoinColumn(name = "`CLAD_SKU`")
@org.hibernate.annotations.ForeignKey(name = "PK_FK_CLUB_ADDRESSES")
public class ClubAddress extends Address {

    /**
     * 
     */
    private static final long serialVersionUID = -644545999368098000L;

    @JoinColumn(name = "`CLUB_SKU`", nullable = false, foreignKey = @ForeignKey(name = "`FK_CLAD_X_CLUB`"))
    @OneToOne(fetch = FetchType.LAZY, optional = false, targetEntity = Club.class)
    private Club club;

    public Club getClub() {
        return this.club;
    }

    public void setClub(Club club) {
        this.club = club;
    }
}
package br.com.practicalshooting.model;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.apache.commons.lang3.StringUtils;

@Entity
@Table(name = "`TBL_PARTICIPANTS`", uniqueConstraints = { @UniqueConstraint(name = "`UK_PART_NAME_01`", columnNames = { "`NM_PUBLIC`" }) })
@AttributeOverrides(value = { @AttributeOverride(name = "id", column = @Column(name = "`PART_SKU`")) })
public class Participant extends EntitySku {

    /**
     * 
     */
    private static final long serialVersionUID = 709573432917123681L;

    @Column(name = "`NM_PUBLIC`", length = 128)
    private String name;

    @Column(name = "`DS_PUBLIC`", length = 1024)
    private String description;

    public String key() {
        if (StringUtils.isNotBlank(this.name) && StringUtils.isNotBlank(this.name)) {
            return this.name + " - " + this.description;
        }
        return "";
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
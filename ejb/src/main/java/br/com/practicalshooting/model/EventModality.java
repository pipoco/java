package br.com.practicalshooting.model;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "`TBL_EVENT_MODALITIES`")
@AttributeOverrides(value = { @AttributeOverride(name = "id", column = @Column(name = "`EVMO_SKU`")) })
public class EventModality extends EntitySku {

    /**
     * 
     */
    private static final long serialVersionUID = 8963910874089568606L;

    @JoinColumn(name = "`MODA_SKU`", nullable = false, foreignKey = @ForeignKey(name = "`FK_EVMO_X_MODALITY`"))
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    private Modality modality;

    @JoinColumn(name = "`EVEN_SKU`", nullable = false, foreignKey = @ForeignKey(name = "`FK_EVMO_X_EVENT`"))
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    private Event event;

    public Modality getModality() {
        return this.modality;
    }

    public void setModality(Modality modality) {
        this.modality = modality;
    }

    public Event getEvent() {
        return this.event;
    }

    public void setEvent(Event event) {
        this.event = event;
    }
}
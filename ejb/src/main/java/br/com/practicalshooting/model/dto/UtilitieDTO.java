package br.com.practicalshooting.model.dto;

import java.io.Serializable;

public class UtilitieDTO implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 5917913735854661105L;

    private String title;
    private String subtitle;
    private String path;
    private String imagePath;

    public UtilitieDTO(String title, String subtitle, String path, String imagePath) {
        this.title = title;
        this.subtitle = subtitle;
        this.path = path;
        this.imagePath = imagePath;
    }

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSubtitle() {
        return this.subtitle;
    }

    public void setSubtitle(String subtitle) {
        this.subtitle = subtitle;
    }

    public String getPath() {
        return this.path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getImagePath() {
        return this.imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }
}

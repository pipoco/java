package br.com.practicalshooting.model.enumeration;

public enum ContactType {
    CELL_PHONE, COMMERCIAL_PHONE, PHONE, EMAIL, FACEBOOK, LINKEDIN, SITE
}

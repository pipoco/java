package br.com.practicalshooting.model.enumeration;

public enum QuartzLogType {
    ERROR, INFO, WARN
}

package br.com.practicalshooting.model;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import br.com.practicalshooting.model.enumeration.SituationPartner;
import br.com.practicalshooting.util.Util;

@Entity
@Table(name = "`TBL_PARTNERS`")
@AttributeOverrides(value = { @AttributeOverride(name = "id", column = @Column(name = "`PART_SKU`")) })
public class Partner extends EntitySku {

    /**
     * 
     */
    private static final long serialVersionUID = 6457322906335907003L;

    @Column(name = "`NM_PARTNER`", length = 128)
    private String name;

    @Column(name = "`DS_CNPJ`", length = 20)
    private String cnpj;

    @Column(name = "`DS_PARTNER`", length = 2048)
    private String description;

    @Column(name = "`NM_PARTNER_CONTACT`", length = 128)
    private String partnerContact;

    @Enumerated(EnumType.ORDINAL)
    @Column(name = "`NUM_SITU_PARTNER`", columnDefinition = "TINYINT")
    private SituationPartner situation;

    @Column(name = "`DS_IMAGE_LINK`", length = 2048)
    private String imageLink;

    @JoinColumn(name = "`USER_SKU`", nullable = false, foreignKey = @ForeignKey(name = "`FK_PART_X_USER`"))
    @OneToOne(fetch = FetchType.LAZY, optional = false, targetEntity = User.class)
    private User user;

    public Partner() {
        if (!Util.isPersistentEntity(this.user)) {
            this.user = new User();
        }
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCnpj() {
        return this.cnpj;
    }

    public void setCnpj(String cnpj) {
        this.cnpj = cnpj;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPartnerContact() {
        return this.partnerContact;
    }

    public void setPartnerContact(String partnerContact) {
        this.partnerContact = partnerContact;
    }

    public SituationPartner getSituation() {
        return this.situation;
    }

    public void setSituation(SituationPartner situation) {
        this.situation = situation;
    }

    public String getImageLink() {
        return this.imageLink;
    }

    public void setImageLink(String imageLink) {
        this.imageLink = imageLink;
    }

    public User getUser() {
        return this.user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
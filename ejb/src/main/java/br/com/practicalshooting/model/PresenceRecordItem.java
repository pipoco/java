package br.com.practicalshooting.model;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "`TBL_RECORD_ITEMS`")
@AttributeOverrides(value = { @AttributeOverride(name = "id", column = @Column(name = "`REIT_SKU`")) })
public class PresenceRecordItem extends EntitySku {

    /**
     * 
     */
    private static final long serialVersionUID = 2195819773840792661L;

    @Column(name = "`NUM_SHOTS`")
    private Integer amountOfShots;

    @JoinColumn(name = "`PRRE_SKU`", nullable = false, foreignKey = @ForeignKey(name = "`FK_REIT_X_PRRE`"))
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    private PresenceRecord presenceRecord;

    @JoinColumn(name = "`IUCO_SKU`", nullable = false, foreignKey = @ForeignKey(name = "`FK_REIT_X_IUCO`"))
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    private ItemUserCollection itemUserCollection;

    public Integer getAmountOfShots() {
        return this.amountOfShots;
    }

    public void setAmountOfShots(Integer amountOfShots) {
        this.amountOfShots = amountOfShots;
    }

    public PresenceRecord getPresenceRecord() {
        return this.presenceRecord;
    }

    public void setPresenceRecord(PresenceRecord presenceRecord) {
        this.presenceRecord = presenceRecord;
    }

    public ItemUserCollection getItemUserCollection() {
        return this.itemUserCollection;
    }

    public void setItemUserCollection(ItemUserCollection itemUserCollection) {
        this.itemUserCollection = itemUserCollection;
    }
}
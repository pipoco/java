package br.com.practicalshooting.model;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

@SuppressWarnings("deprecation")
@Entity
@Table(name = "`TBL_CLUB_COLLECTIONS`")
@PrimaryKeyJoinColumn(name = "`CLCO_SKU`")
@org.hibernate.annotations.ForeignKey(name = "PK_FK_CLUB_COLLECTIONS")
public class ClubCollection extends UserColletion {

    /**
     * 
     */
    private static final long serialVersionUID = 5503952185953815865L;

    @JoinColumn(name = "`CLUB_SKU`", nullable = false, foreignKey = @ForeignKey(name = "`FK_CLCO_X_CLUB`"))
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    private Club club;

    public Club getClub() {
        return this.club;
    }

    public void setClub(Club club) {
        this.club = club;
    }
}
package br.com.practicalshooting.model;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Type;
import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import br.com.practicalshooting.model.enumeration.ItemUserCollectionType;
import br.com.practicalshooting.model.enumeration.Situation;

@Entity
@Table(name = "`TBL_ITEMS_USER_COLLECTION`")
@AttributeOverrides(value = { @AttributeOverride(name = "id", column = @Column(name = "`IUCO_SKU`")) })
public class ItemUserCollection extends EntitySku {

    /**
     * 
     */
    private static final long serialVersionUID = 210313476136942687L;

    @Column(name = "`NM_ITEM`", length = 128)
    private String name;

    @Column(name = "`DS_DESCRIPTION`", length = 1024)
    private String description;

    @Enumerated(EnumType.ORDINAL)
    @Column(name = "`TP_USER_COLLECION`", columnDefinition = "TINYINT")
    private ItemUserCollectionType itemUserCollectionType;

    @Column(name = "`DS_MANUFACTURER`", length = 128)
    private String manufacturer;

    @Column(name = "`DS_MODEL`", length = 128)
    private String model;

    @Column(name = "`DS_CALIBER`", length = 20)
    private String caliber;

    @Column(name = "`DS_SERIAL_NUMBER`", length = 128)
    private String serialNumber;

    @Column(name = "`DT_RENOVATION`")
    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalDate")
    private LocalDate renovationDate;

    @Enumerated(EnumType.ORDINAL)
    @Column(name = "`NUM_SITUATION`", columnDefinition = "TINYINT")
    private Situation situation;

    @JoinColumn(name = "`COLL_SKU`", nullable = false, foreignKey = @ForeignKey(name = "`FK_IUCO_X_COLL`"))
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    private UserColletion userColletion;

    public String renovationDateToPattern(String pattern) {
        DateTimeFormatter formatter = DateTimeFormat.forPattern(pattern);
        return formatter.print(this.renovationDate);
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public ItemUserCollectionType getItemUserCollectionType() {
        return this.itemUserCollectionType;
    }

    public void setItemUserCollectionType(ItemUserCollectionType itemUserCollectionType) {
        this.itemUserCollectionType = itemUserCollectionType;
    }

    public String getManufacturer() {
        return this.manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public String getModel() {
        return this.model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getCaliber() {
        return this.caliber;
    }

    public void setCaliber(String caliber) {
        this.caliber = caliber;
    }

    public String getSerialNumber() {
        return this.serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public LocalDate getRenovationDate() {
        return this.renovationDate;
    }

    public void setRenovationDate(LocalDate renovationDate) {
        this.renovationDate = renovationDate;
    }

    public Situation getSituation() {
        return this.situation;
    }

    public void setSituation(Situation situation) {
        this.situation = situation;
    }

    public UserColletion getUserColletion() {
        return this.userColletion;
    }

    public void setUserColletion(UserColletion userColletion) {
        this.userColletion = userColletion;
    }
}
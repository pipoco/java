package br.com.practicalshooting.quartz.configuration;

import java.io.Serializable;

import org.apache.log4j.Logger;
import org.quartz.Job;

import br.com.practicalshooting.util.LoggerUtil;

public abstract class BaseJob implements Job, Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -5416741291292169625L;
    protected static final Logger LOGGER = Logger.getLogger(LoggerUtil.LOGGER_INFO);

}

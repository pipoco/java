package br.com.practicalshooting.quartz.jobs;

import java.util.List;

import javax.inject.Inject;
import javax.mail.MessagingException;
import javax.mail.internet.AddressException;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import br.com.practicalshooting.dao.MailQueueDAO;
import br.com.practicalshooting.model.MailQueue;
import br.com.practicalshooting.model.enumeration.MailStatus;
import br.com.practicalshooting.quartz.configuration.BaseJob;
import br.com.practicalshooting.util.Mail;
import br.com.practicalshooting.util.MailMessage;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

public class MailJob extends BaseJob {

    /**
     * 
     */
    private static final long serialVersionUID = -5536150881035759192L;
    @Inject
    private MailQueueDAO queueDAO;

    @Inject
    private Mail mail;

    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
        List<MailQueue> mailQueues = this.queueDAO.listAllToSend();
        for (MailQueue mailQueue : mailQueues) {
            this.sendEmail(mailQueue);
        }
    }

    private void sendEmail(MailQueue mailQueue) {
        boolean error = false;
        try {
            List<String> to = new Gson().fromJson(mailQueue.getJsonTo(), new TypeToken<List<String>>() {
            }.getType());
            MailMessage mailMessage = new MailMessage().remetente("no-reply.shooting@gmail.com").assunto(mailQueue.getSubject()).para(to.toArray()).mensagem(mailQueue.getContent());
            this.mail.send(mailMessage);
            this.queueDAO.remove(mailQueue);
        } catch (AddressException e) {
            LOGGER.error("", e);
            error = true;
        } catch (MessagingException e) {
            LOGGER.error("", e);
            error = true;
        } catch (Exception e) {
            LOGGER.error("", e);
            error = true;
        } finally {
            if (error) {
                mailQueue.setStatus(MailStatus.CANCELED);
                this.queueDAO.merge(mailQueue);
            }
        }
    }
}

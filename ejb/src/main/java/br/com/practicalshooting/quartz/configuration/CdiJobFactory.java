package br.com.practicalshooting.quartz.configuration;

import javax.enterprise.inject.Any;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;

import org.apache.log4j.Logger;
import org.quartz.Job;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.spi.JobFactory;
import org.quartz.spi.TriggerFiredBundle;

import br.com.practicalshooting.util.LoggerUtil;

public class CdiJobFactory implements JobFactory {

    protected static final Logger LOGGER = Logger.getLogger(LoggerUtil.LOGGER_INFO);

    @Inject
    @Any
    private Instance<Job> jobs;

    @Override
    public Job newJob(TriggerFiredBundle triggerFiredBundle, Scheduler scheduler) throws SchedulerException {
        final JobDetail jobDetail = triggerFiredBundle.getJobDetail();
        final Class<? extends Job> jobClass = jobDetail.getJobClass();

        for (Job job : this.jobs) {
            if (job.getClass().isAssignableFrom(jobClass)) {
                return job;
            }
        }

        LOGGER.error("Cannot create a Job of type " + jobClass);
        throw new RuntimeException("Cannot create a Job of type " + jobClass);
    }
}

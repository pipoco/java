package br.com.practicalshooting.quartz.configuration;

import static org.quartz.TriggerBuilder.newTrigger;

import java.io.Serializable;
import java.net.InetAddress;
import java.text.ParseException;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.inject.Inject;

import org.apache.log4j.Logger;
import org.quartz.CronExpression;
import org.quartz.CronScheduleBuilder;
import org.quartz.CronTrigger;
import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.Trigger;
import org.quartz.TriggerKey;
import org.quartz.impl.StdSchedulerFactory;

import br.com.practicalshooting.dao.QuartzConfigurationDAO;
import br.com.practicalshooting.model.QuartzConfiguration;
import br.com.practicalshooting.util.LoggerUtil;
import br.com.practicalshooting.util.Util;

@Startup
@Singleton
public class JobConfig implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -8856857795155257898L;

    private static final Logger LOGGER = Logger.getLogger(LoggerUtil.LOGGER_INFO);

    private Scheduler scheduler;

    @Inject
    private QuartzConfigurationDAO quartzConfigurationDAO;

    @Inject
    private CdiJobFactory cdiJobFactory;

    @PostConstruct
    public void startup() {
        try {
            StdSchedulerFactory schedulerFactory = new StdSchedulerFactory();
            schedulerFactory.initialize();
            this.scheduler = schedulerFactory.getScheduler();
            this.scheduler.setJobFactory(this.cdiJobFactory);
            this.scheduler.start();
        } catch (SchedulerException e) {
            LOGGER.error("QUARTZ CONFIG ERROR", e);
        }
        this.configAutoJobs();
    }

    @PreDestroy
    public void stop() {
        try {
            this.scheduler.shutdown();
        } catch (SchedulerException e) {
            LOGGER.error("QUARTZ STOP ERROR", e);
        }
    }

    public void configAutoJobs() {
        try {
            List<QuartzConfiguration> quartzConfigurationList = this.quartzConfigurationDAO.listAllAutoJobs();
            for (QuartzConfiguration quartzConfiguration : quartzConfigurationList) {
                if (quartzConfiguration.executeOnServer()) {
                    this.configurarJob(quartzConfiguration);
                } else {
                    LOGGER.info("EXECUÇÃO DO JOB [" + quartzConfiguration.getName() + "] NÃO CONFIGURADA PARA O SERVIDOR " + InetAddress.getLocalHost().getHostName());
                }
            }
        } catch (Exception e) {
            LOGGER.error("CONFIG QUARTZ ERROR", e);
        }
    }

    private void configurarJob(QuartzConfiguration quartzConfiguration) {
        try {
            Class<?> job = Class.forName(quartzConfiguration.getJobClassName());
            if (this.isJob(job)) {
                this.schedule(quartzConfiguration);
                LOGGER.info("Job scheduled: " + quartzConfiguration.getJobClassName());
            }
        } catch (ClassNotFoundException e) {
            LOGGER.warn("Job class not found: " + quartzConfiguration.getJobClassName(), e);
        } catch (ParseException | SchedulerException e) {
            LOGGER.warn("Error schedule job: " + quartzConfiguration.getJobClassName(), e);
        }
    }

    private void schedule(final QuartzConfiguration quartzConfiguration) throws ParseException, SchedulerException, ClassNotFoundException {
        Class<? extends BaseJob> baseJobClazz = this.getJobClass(quartzConfiguration);
        JobDetail jobDetail = JobBuilder.newJob(baseJobClazz).withIdentity(quartzConfiguration.getName(), Scheduler.DEFAULT_GROUP).build();
        Trigger trigger = this.createCronTrigger(quartzConfiguration);
        if (trigger != null) {
            this.scheduler.scheduleJob(jobDetail, trigger);
        }
    }

    private CronTrigger createCronTrigger(final QuartzConfiguration quartzConfiguration) throws ParseException {
        return newTrigger().withIdentity(quartzConfiguration.getName()).withSchedule(CronScheduleBuilder.cronSchedule(new CronExpression(quartzConfiguration.getCronExpression()))).build();
    }

    @SuppressWarnings("unchecked")
    private Class<? extends BaseJob> getJobClass(final QuartzConfiguration quartzConfiguration) throws ClassNotFoundException {
        String jobClassName = quartzConfiguration.getJobClassName();
        try {
            return (Class<? extends BaseJob>) Class.forName(jobClassName);
        } catch (ClassNotFoundException e) {
            throw e;
        }
    }

    private boolean isJob(final Class<?> clazz) {
        Class<?> superClass = clazz.getSuperclass();
        if (superClass != null) {
            if (superClass.isAssignableFrom(Object.class)) {
                return false;
            } else if (superClass.isAssignableFrom(BaseJob.class)) {
                return true;
            }
            return this.isJob(superClass);
        }
        return false;
    }

    private void fireJobNow(QuartzConfiguration job) throws ClassNotFoundException, SchedulerException {
        Class<? extends BaseJob> baseJobClazz = this.getJobClass(job);
        JobDetail jobDetail = JobBuilder.newJob(baseJobClazz).withIdentity(job.getName() + "_IMMEDIATE", "JOBS_IMMEDIATE").build();
        Trigger trigger = newTrigger().withIdentity(job.getName() + "Trigger_IMMEDIATE", "JOBS_IMMEDIATE").startNow().build();
        this.scheduler.scheduleJob(jobDetail, trigger);
    }

    public void pararJob(String nomejob) throws SchedulerException {
        TriggerKey triggerKey = new TriggerKey(nomejob, Scheduler.DEFAULT_GROUP);
        this.scheduler.unscheduleJob(triggerKey);
    }

    public void executarJobAgora(String nomeJob) throws ClassNotFoundException, SchedulerException {
        QuartzConfiguration jobConf = this.quartzConfigurationDAO.getByName(nomeJob);
        this.fireJobNow(jobConf);
    }

    public void reconfigurarJob(String nomeJob) throws ClassNotFoundException, SchedulerException, ParseException {
        QuartzConfiguration jobConf = this.quartzConfigurationDAO.getByName(nomeJob);
        if (Util.isPersistentEntity(jobConf)) {
            Class<? extends BaseJob> baseJobClazz = this.getJobClass(jobConf);
            JobDetail jobDetail = JobBuilder.newJob(baseJobClazz).withIdentity(jobConf.getName(), Scheduler.DEFAULT_GROUP).build();
            Trigger triggerKey = newTrigger().withIdentity(jobConf.getName(), Scheduler.DEFAULT_GROUP).startNow().build();
            if (this.scheduler.checkExists(jobDetail.getKey())) {
                Trigger trigger = this.createCronTrigger(jobConf);
                this.scheduler.rescheduleJob(triggerKey.getKey(), trigger);
            } else {
                this.schedule(jobConf);
            }
        }
    }

    public void atualizarCronDoJob(String nomeJob, String novaCron) throws ClassNotFoundException, SchedulerException, ParseException {
        QuartzConfiguration jobConf = this.quartzConfigurationDAO.getByName(nomeJob);
        if (Util.isPersistentEntity(jobConf)) {
            jobConf.setCronExpression(novaCron);
            this.quartzConfigurationDAO.merge(jobConf);
            this.reconfigurarJob(nomeJob);
        }
    }

    public void atualizarExecucaoAutomaticaDoJob(String nomeJob, boolean executarAutomaticamente) throws ClassNotFoundException, SchedulerException, ParseException {
        QuartzConfiguration jobConf = this.quartzConfigurationDAO.getByName(nomeJob);
        if (Util.isPersistentEntity(jobConf)) {
            jobConf.setAutoConfig(executarAutomaticamente);
            this.quartzConfigurationDAO.merge(jobConf);
            if (executarAutomaticamente) {
                this.reconfigurarJob(nomeJob);
            } else {
                this.pararJob(nomeJob);
            }
        }
    }
}

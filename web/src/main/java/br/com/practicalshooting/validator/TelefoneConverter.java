package br.com.practicalshooting.validator;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;

import org.apache.commons.lang3.StringUtils;

@FacesConverter(value = "telefoneConverter")
public class TelefoneConverter implements Converter {

	@Override
	public Object getAsObject(FacesContext arg0, UIComponent arg1, String arg2) {
		String value = arg2.replaceAll("\\(|\\)|\\-", "");
		if (!StringUtils.isBlank(value) && value.length() < 10) {
			throw new ConverterException();
		}
		return value;
	}

	@Override
	public String getAsString(FacesContext arg0, UIComponent arg1, Object arg2) {
		String value = String.valueOf(arg2);
		if (StringUtils.isBlank(value)) {
			return value;
		}
		return value.replaceAll("\\(|\\)|\\-", "");
	}

}
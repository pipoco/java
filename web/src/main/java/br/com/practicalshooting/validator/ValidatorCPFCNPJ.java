package br.com.practicalshooting.validator;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;

import org.apache.commons.lang3.StringUtils;

import br.com.practicalshooting.util.ResourceMessages;
import br.com.practicalshooting.util.Util;

@FacesValidator("validatorCPFCNPJ")
public class ValidatorCPFCNPJ implements Validator {

    @Override
    public void validate(FacesContext context, UIComponent toValidate, Object value) {

        String cpfCnpj = (String) value;
        cpfCnpj = cpfCnpj.replace(".", "").replace(".", "").replaceAll("/", "").replaceAll("-", "");
        if (!StringUtils.isBlank(cpfCnpj)) {
            if (cpfCnpj.length() <= 11) {
                if (!Util.validaCPF(cpfCnpj)) {
                    ((UIInput) toValidate).setValid(false);
                    FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, ResourceMessages.getMessage("msg.cpf.invalido"), "");
                    context.addMessage(toValidate.getClientId(context), message);
                }
            } else {
                if (!Util.isValidCNPJ(cpfCnpj)) {
                    ((UIInput) toValidate).setValid(false);
                    FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, ResourceMessages.getMessage("msg.cnpj.invalido"), "");
                    context.addMessage(toValidate.getClientId(context), message);
                }
            }
        }
    }

}

package br.com.practicalshooting.validator;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import org.apache.commons.lang3.StringUtils;

@FacesConverter(value = "numeroDuasCasasDecimais")
public class NumeroDuasCasasDecimaisConverter implements Converter {

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        try {
            if (!StringUtils.isBlank(value)) {
                String temp = value.replace(".", "").replace(",", ".");
                return new Double(temp);
            }
        } catch (NumberFormatException e) {
            throw e;
        }
        return null;
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object object) {
        if (object == null) {
            return null;
        } else {
            Number temp = (Number) object;
            return this.formataNumero(temp.doubleValue());
        }
    }

    private String formataNumero(Double vlr) {
        DecimalFormatSymbols formatSymbols = new DecimalFormatSymbols();
        formatSymbols.setDecimalSeparator(',');
        formatSymbols.setPerMill('.');

        DecimalFormat df = new DecimalFormat("###.###", formatSymbols);
        df.setMaximumFractionDigits(2);
        df.setMinimumFractionDigits(2);
        String retorno = df.format(vlr);
        if (retorno.charAt(0) == ',') {
            return "0" + retorno;
        } else {
            return retorno;
        }
    }
}
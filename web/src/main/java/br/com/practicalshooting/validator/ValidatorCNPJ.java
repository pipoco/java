package br.com.practicalshooting.validator;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;

import org.apache.commons.lang3.StringUtils;

import br.com.practicalshooting.util.ResourceMessages;
import br.com.practicalshooting.util.Util;

@FacesValidator("validatorCNPJ")
public class ValidatorCNPJ implements Validator {

    @Override
    public void validate(FacesContext context, UIComponent toValidate, Object value) {
        String cnpj = (String) value;
        if (!StringUtils.isBlank(cnpj)) {
            cnpj = cnpj.replace(".", "");
            cnpj = cnpj.replace("/", "");
            cnpj = cnpj.replace("-", "");
            if (!Util.isValidCNPJ(cnpj)) {
                ((UIInput) toValidate).setValid(false);
                FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, ResourceMessages.getMessage("msg.cnpj.invalido"), "");
                context.addMessage(toValidate.getClientId(context), message);
            }
        }
    }
}

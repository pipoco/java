package br.com.practicalshooting.validator;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import org.apache.commons.lang3.StringUtils;

@FacesConverter(value = "numberWithCommaConverter")
public class NumberWithCommaConverter implements Converter {

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        try {
            if (!StringUtils.isBlank(value)) {
                String temp = value.replace(".", "").replace(",", ".");
                return new Double(temp);
            }
        } catch (NumberFormatException e) {
            throw e;
        }
        return null;
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object object) {
        if (object == null) {
            return null;
        } else {
            Number temp = (Number) object;
            return temp.toString().replace(".", ",");
        }
    }
}
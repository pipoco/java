package br.com.practicalshooting.validator;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;

import br.com.practicalshooting.util.ResourceMessages;
import br.com.practicalshooting.util.Util;

@FacesValidator("validatorEmail")
public class ValidatorEmail implements Validator {

    @Override
    public void validate(FacesContext context, UIComponent toValidate, Object value) {
        String email = (String) value;
        if (!Util.emailValido(email)) {
            ((UIInput) toValidate).setValid(false);
            FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, ResourceMessages.getMessage("error.invalid.email"), "");
            context.addMessage(toValidate.getClientId(context), message);
            return;
        }
    }

}

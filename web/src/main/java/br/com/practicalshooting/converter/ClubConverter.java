package br.com.practicalshooting.converter;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;
import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;

import br.com.practicalshooting.dao.ClubDAO;
import br.com.practicalshooting.model.Club;
import br.com.practicalshooting.util.Util;

@FacesConverter("clubConverter")
public class ClubConverter implements Converter {

    @Inject
    private ClubDAO clubDAO;

    @Override
    public Object getAsObject(FacesContext fc, UIComponent uic, String value) {
        if (StringUtils.isNotBlank(value)) {
            try {
                return this.clubDAO.findById(Long.valueOf(value));
            } catch (NumberFormatException e) {
                throw new ConverterException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Conversion Error", "Not a valid club."));
            }
        } else {
            return null;
        }
    }

    @Override
    public String getAsString(FacesContext fc, UIComponent uic, Object object) {
        if (object != null && Util.isPersistentEntity((Club) object)) {
            return ((Club) object).getId().toString();
        }
        return null;
    }
}

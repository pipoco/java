package br.com.practicalshooting.converter;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;
import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;

import br.com.practicalshooting.dao.ParticipantDAO;
import br.com.practicalshooting.model.Participant;
import br.com.practicalshooting.util.Util;

@FacesConverter("participantConverter")
public class ParticipantConverter implements Converter {

    @Inject
    private ParticipantDAO participantDAO;

    @Override
    public Object getAsObject(FacesContext fc, UIComponent uic, String value) {
        if (StringUtils.isNotBlank(value)) {
            try {
                return this.participantDAO.findById(Long.valueOf(value));
            } catch (NumberFormatException e) {
                throw new ConverterException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Conversion Error", "Not a valid participant."));
            }
        } else {
            return null;
        }
    }

    @Override
    public String getAsString(FacesContext fc, UIComponent uic, Object object) {
        if (object != null && Util.isPersistentEntity((Participant) object)) {
            return ((Participant) object).getId().toString();
        }
        return null;
    }
}

package br.com.practicalshooting.controller.datamodel;

import java.util.List;
import java.util.Map;

import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

import br.com.practicalshooting.dao.InstructorDAO;
import br.com.practicalshooting.model.dto.InstructorDTO;

@SuppressWarnings("unchecked")
public class InstructorDataModel extends LazyDataModel<InstructorDTO> {

    /**
     * 
     */
    private static final long serialVersionUID = -1704075775288809394L;

    private static final int DEFAULT_PAGINATION = 5;
    private final InstructorDAO instructorDAO;
    private InstructorDTO[] selectedList;

    public InstructorDataModel(InstructorDAO instructorDAO) {
        this.instructorDAO = instructorDAO;
    }

    @Override
    public InstructorDTO getRowData(String rowKey) {
        InstructorDTO objetctSelected = null;
        for (InstructorDTO item : (List<InstructorDTO>) this.getWrappedData()) {
            if (item.getId().equals(Long.valueOf(rowKey))) {
                objetctSelected = item;
                break;
            }
        }
        return objetctSelected;
    }

    @Override
    public Object getRowKey(InstructorDTO instructor) {
        return instructor.getId();
    }

    public void list() {
        this.setRowCount(this.instructorDAO.count());
        this.selectedList = null;
        this.setPageSize(DEFAULT_PAGINATION);
    }

    @Override
    public List<InstructorDTO> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {
        return this.instructorDAO.list(first, pageSize);
    }

    public InstructorDTO[] getSelectedList() {
        return this.selectedList;
    }

    public void setSelectedList(InstructorDTO[] selectedList) {
        this.selectedList = selectedList;
    }
}

package br.com.practicalshooting.controller;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ConversationScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import br.com.practicalshooting.dao.UserDAO;
import br.com.practicalshooting.model.User;
import br.com.practicalshooting.service.UserService;
import br.com.practicalshooting.util.ResourceMessages;
import br.com.practicalshooting.util.Util;

@Named
@ConversationScoped
public class ConfirmRegisterController extends BaseBean {

    /**
     * 
     */
    private static final long serialVersionUID = 7037305311302446963L;

    @Inject
    private UserService userService;
    @Inject
    private UserDAO userDAO;

    private User user;
    private String infoUser;
    private Boolean userInvalid;
    private Boolean userValid;
    private String errorMessage;

    @PostConstruct
    public void init() {
        this.user = new User();
        this.userInvalid = Boolean.TRUE;
        this.userValid = Boolean.FALSE;
        this.infoUser = ResourceMessages.getMessage("lbl.find.register");
        String token = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("cct");
        this.user = this.userDAO.selectUserByRegisterToken(token);
        if (Util.isPersistentEntity(this.user)) {
            this.infoUser = ResourceMessages.getMessage("lbl.info.confirm.register", this.user.getName());
            this.userInvalid = Boolean.FALSE;
            this.userValid = Boolean.TRUE;
        } else {
            this.errorMessage = ResourceMessages.getMessage("error.token.confirm.register.invalid");
        }
    }

    public void userIsNotValid() {
        if (this.userInvalid) {
            this.userInvalid = Boolean.FALSE;
            ResourceMessages.addErrorMessage(this.errorMessage);
            this.infoUser = null;
            return;
        }
    }

    public void confirmRegister() {
        this.userService.confirmRegister(this.user.getId());
        ResourceMessages.addInfoMessage("default.success.confirm.register");
        this.infoUser = null;
        this.userValid = Boolean.FALSE;
    }

    public String getInfoUser() {
        return this.infoUser;
    }

    public void setInfoUser(String infoUser) {
        this.infoUser = infoUser;
    }

    public User getUser() {
        return this.user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public UserService getUserService() {
        return this.userService;
    }

    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    public Boolean getUserInvalid() {
        return this.userInvalid;
    }

    public void setUserInvalid(Boolean userInvalid) {
        this.userInvalid = userInvalid;
    }

    public String getErrorMessage() {
        return this.errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public Boolean getUserValid() {
        return this.userValid;
    }

    public void setUserValid(Boolean userValid) {
        this.userValid = userValid;
    }
}

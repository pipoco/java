package br.com.practicalshooting.controller;

import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ConversationScoped;
import javax.inject.Inject;
import javax.inject.Named;

import br.com.practicalshooting.model.User;
import br.com.practicalshooting.service.UserService;
import br.com.practicalshooting.util.PrimeFacesUtil;
import br.com.practicalshooting.util.ResourceMessages;
import br.com.practicalshooting.util.Util;

@Named
@ConversationScoped
public class LoginController extends BaseBean {

    /**
     *
     */
    private static final long serialVersionUID = 2523468347917660568L;

    @Inject
    private UserService userService;

    private String login;
    private String password;

    @PostConstruct
    public void init() {
        this.login = "";
        this.password = "";
    }

    public String login() {
        User user = this.userService.login(this.login, this.password);
        if (Util.isPersistentEntity(user)) {
            ResourceMessages.addInfoMessage("default.success.login", user.getName());
            this.login = "";
            this.password = "";
            this.sessionBean.putLoggedUser(user);
        } else {
            this.password = "";
            ResourceMessages.addErrorMessage("error.login.invalid");
        }
        return "/home.xhtml?faces-redirect=true";
    }

    public void forgetPassword() throws URISyntaxException {
        List<String> formUpdate = new ArrayList<String>();
        formUpdate.add("formForgetPassword");
        this.userService.forgetPassword(this.login);
        PrimeFacesUtil.fecharDialogo("dialogForgetPassword");
        formUpdate.add("formLogin");
        ResourceMessages.addInfoMessage("default.success.forget.password");
        PrimeFacesUtil.update(formUpdate);
    }

    public String getLogin() {
        return this.login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return this.password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}

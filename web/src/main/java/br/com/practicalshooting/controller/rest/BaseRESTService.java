package br.com.practicalshooting.controller.rest;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.apache.log4j.Logger;

import br.com.practicalshooting.util.LoggerUtil;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

public class BaseRESTService implements Serializable {

    protected static final Logger LOGGER = Logger.getLogger(LoggerUtil.LOGGER_INFO);

    private static final long serialVersionUID = -4164283956689942937L;

    private final List<String> errors = new ArrayList<String>();

    protected Gson gson;

    public BaseRESTService() {
        super();
        final GsonBuilder builder = new GsonBuilder();
        builder.serializeNulls();
        builder.excludeFieldsWithoutExposeAnnotation();
        this.gson = builder.create();
    }

    @Context
    private HttpServletRequest httpRequest;

    protected Response error(Status httpErrorCode) {
        return this.error(httpErrorCode, this.errors);
    }

    protected Response error(Status httpErrorCode, String... errorList) {
        return this.error(httpErrorCode, Arrays.asList(errorList));
    }

    protected Response error(Status httpErrorCode, List<String> errorList) {
        JsonArray jsonArray = new JsonArray();
        for (String str : errorList) {
            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("message", str);
            jsonArray.add(jsonObject);
        }
        JsonObject errorWrapper = new JsonObject();
        errorWrapper.add("errors", jsonArray);
        return Response.status(httpErrorCode).entity(errorWrapper.toString()).build();
    }

    protected void addError(String message) {
        this.errors.add(message);
    }

    protected boolean hasError() {
        return !this.errors.isEmpty();
    }
}

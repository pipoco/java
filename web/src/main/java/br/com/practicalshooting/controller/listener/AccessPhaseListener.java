package br.com.practicalshooting.controller.listener;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.faces.FacesException;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.event.PhaseEvent;
import javax.faces.event.PhaseId;
import javax.faces.event.PhaseListener;

import br.com.practicalshooting.controller.SessionBean;
import br.com.practicalshooting.util.ResourceMessages;

public class AccessPhaseListener implements PhaseListener {
    private static final long serialVersionUID = 8907028710750653269L;

    private final List<String> urlsPermitidas;

    public AccessPhaseListener() {
        this.urlsPermitidas = new ArrayList<String>();
        this.urlsPermitidas.add("index.html");
        this.urlsPermitidas.add("/login.xhtml");
        this.urlsPermitidas.add("/login.jsp");
        this.urlsPermitidas.add("/home.xhtml");
        this.urlsPermitidas.add("/pages/event/detail.xhtml");
        this.urlsPermitidas.add("/pages/club/detail.xhtml");
        this.urlsPermitidas.add("/pages/event/list.xhtml");
        this.urlsPermitidas.add("/pages/club/list.xhtml");
        this.urlsPermitidas.add("/pages/instructor/list.xhtml");
        this.urlsPermitidas.add("/pages/partner/list.xhtml");
        this.urlsPermitidas.add("/pages/register/show.xhtml");
        this.urlsPermitidas.add("/pages/register/changePwd.xhtml");
        this.urlsPermitidas.add("/pages/register/confirme.xhtml");
        this.urlsPermitidas.add("/pages/candidate/show.xhtml");
        this.urlsPermitidas.add("/templates/error.xhtml");
        this.urlsPermitidas.add("/templates/error404.xhtml");
    }

    @Override
    public void afterPhase(PhaseEvent phaseEvent) {
        FacesContext facesContext = phaseEvent.getFacesContext();

        String pagina = facesContext.getViewRoot().getViewId();
        if ((pagina == null) || this.urlsPermitidas.contains(pagina)) {
            return;
        }

        SessionBean currentSession = facesContext.getApplication().evaluateExpressionGet(facesContext, "#{sessionBean}", SessionBean.class);
        if ((currentSession == null) || !currentSession.isLoggedIn()) {
            ExternalContext extContext = facesContext.getExternalContext();
            String url = extContext.encodeActionURL(facesContext.getApplication().getViewHandler().getActionURL(facesContext, "/login.jsf"));
            ResourceMessages.addWarnMessage("msg.acesso.negado");
            ResourceMessages.persistirMensagens();
            try {
                extContext.redirect(url);
            } catch (IOException ioe) {
                throw new FacesException(ioe);
            }
        }
    }

    @Override
    public PhaseId getPhaseId() {
        return PhaseId.RESTORE_VIEW;
    }

    @Override
    public void beforePhase(PhaseEvent arg0) {
        // Não precisa fazer nada
    }

}

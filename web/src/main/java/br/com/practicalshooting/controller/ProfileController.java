package br.com.practicalshooting.controller;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ConversationScoped;
import javax.inject.Inject;
import javax.inject.Named;

import br.com.practicalshooting.dao.ClubDAO;
import br.com.practicalshooting.dao.UserDAO;
import br.com.practicalshooting.model.Associate;
import br.com.practicalshooting.model.Club;
import br.com.practicalshooting.model.User;
import br.com.practicalshooting.model.enumeration.SituationAssociate;
import br.com.practicalshooting.service.UserService;
import br.com.practicalshooting.util.ResourceMessages;
import br.com.practicalshooting.util.PrimeFacesUtil;
import br.com.practicalshooting.util.Util;

@Named
@ConversationScoped
public class ProfileController extends BaseBean {

    /**
     * 
     */
    private static final long serialVersionUID = 1504292715786026699L;

    @Inject
    private SessionBean sessionBean;

    @Inject
    private UserService userService;
    @Inject
    private UserDAO userDAO;
    @Inject
    private ClubDAO clubDAO;
    @Inject
    private DialogoConfirmacaoBean dialogoConfirmacaoBean;

    private User user;
    private String currentPassword;
    private String passwordChange;
    private String confirmPasswordChange;
    private Boolean editingMode;

    @PostConstruct
    public void init() {
        this.user = this.userDAO.findByIdFetchAssociate(this.sessionBean.getLoggedUser().getId());
        this.sessionBean.setLoggedUser(this.user);
        this.editingMode = Boolean.FALSE;
        if (!Util.isPersistentEntity(this.user)) {
            this.user = new User();
        }
    }

    public void changePassword() {
        this.userService.changePassword(this.user.getId(), this.currentPassword, this.passwordChange, this.confirmPasswordChange);
        this.user = this.userDAO.findByIdFetchAssociate(this.user.getId());
        this.sessionBean.setLoggedUser(this.user);
        List<String> formUpdate = new ArrayList<String>();
        formUpdate.add("formChangePassword");
        PrimeFacesUtil.fecharDialogo("dialogChangePassword");
        formUpdate.add("formProfile");
        ResourceMessages.addInfoMessage("default.success.change.password");
        PrimeFacesUtil.update(formUpdate);
    }

    public void initChangePassword() {
        PrimeFacesUtil.abrirDialogo("dialogChangePassword");
        PrimeFacesUtil.update("formChangePassword");
    }

    public void enableEditingMode() {
        this.user = this.userDAO.findByIdFetchAssociate(this.user.getId());
        this.editingMode = Boolean.TRUE;
    }

    public void cancelEditingMode() {
        this.user = this.userDAO.findByIdFetchAssociate(this.user.getId());
        this.editingMode = Boolean.FALSE;
    }

    public void save() {
        this.userService.updateProfile(this.user);
        this.user = this.userDAO.findByIdFetchAssociate(this.user.getId());
        this.sessionBean.setLoggedUser(this.user);
        this.editingMode = Boolean.FALSE;
        ResourceMessages.addInfoMessage("default.success.update.profile");
        PrimeFacesUtil.update("formChangePassword");
    }

    public void preUpdateAssociateData() {
        this.user = this.userDAO.findByIdFetchAssociate(this.user.getId());
        if (!Util.isPersistentEntity(this.user.getAssociate())) {
            this.user.setAssociate(new Associate());
        }
        PrimeFacesUtil.update("formAssociateData");
        PrimeFacesUtil.abrirDialogo("dialogAssociateData");
    }

    public void saveAssociateData() {
        this.userService.saveAssociateData(this.user);
        this.user = this.userDAO.findByIdFetchAssociate(this.user.getId());
        this.sessionBean.setLoggedUser(this.user);
        this.editingMode = Boolean.FALSE;
        ResourceMessages.addInfoMessage("default.success.update.associate.data");
        PrimeFacesUtil.update("formProfile");
        PrimeFacesUtil.fecharDialogo("dialogAssociateData");
    }

    public List<Club> clubFilter(String query) {
        return this.clubDAO.listAllContainsNameFetchOnlyNameAndSku(query.toLowerCase());
    }

    public void cancelDeleteAssociate() {
        this.dialogoConfirmacaoBean.cleanConfiguration();
    }

    public void confirmDeleteAssociate() {
        try {
            this.userService.removeAssociateData(this.user.getAssociate());
            this.user = this.userDAO.findByIdFetchAssociate(this.user.getId());
            this.sessionBean.setLoggedUser(this.user);
            this.editingMode = Boolean.FALSE;
            ResourceMessages.addInfoMessage("default.success.delete.associate.data");
        } finally {
            this.dialogoConfirmacaoBean.cleanConfiguration();
            PrimeFacesUtil.update("formProfile");
        }
    }

    public void removeAssociate() {
        this.dialogoConfirmacaoBean.changeActionOnCancelButton("#{profileController.cancelDeleteAssociate()}");
        this.dialogoConfirmacaoBean.changeActionOnConfirmButton("#{profileController.confirmDeleteAssociate()}");
        this.dialogoConfirmacaoBean.changeHeader(ResourceMessages.getMessage("act.delete.associate"));
        this.dialogoConfirmacaoBean.changeMessage(ResourceMessages.getMessage("msg.remove.my.associate.data"));
        this.dialogoConfirmacaoBean.show();
    }

    public Boolean associateRegistred() {
        return Util.isPersistentEntity(this.user.getAssociate());
    }

    public Boolean associateNotRegistred() {
        return !this.associateRegistred();
    }

    public Boolean allowChangeAssociateData() {
        return this.associateRegistred() && this.user.getAssociate().getSituationAssociate().equals(SituationAssociate.PENDIND);
    }

    public String getCurrentPassword() {
        return this.currentPassword;
    }

    public void setCurrentPassword(String currentPassword) {
        this.currentPassword = currentPassword;
    }

    public String getPasswordChange() {
        return this.passwordChange;
    }

    public void setPasswordChange(String passwordChange) {
        this.passwordChange = passwordChange;
    }

    public String getConfirmPasswordChange() {
        return this.confirmPasswordChange;
    }

    public void setConfirmPasswordChange(String confirmPasswordChange) {
        this.confirmPasswordChange = confirmPasswordChange;
    }

    public User getUser() {
        return this.user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Boolean getEditingMode() {
        return this.editingMode;
    }

    public void setEditingMode(Boolean editingMode) {
        this.editingMode = editingMode;
    }
}

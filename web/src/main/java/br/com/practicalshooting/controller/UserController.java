package br.com.practicalshooting.controller;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ConversationScoped;
import javax.inject.Inject;
import javax.inject.Named;

import br.com.practicalshooting.controller.datamodel.UserDataModel;
import br.com.practicalshooting.dao.UserDAO;
import br.com.practicalshooting.model.User;
import br.com.practicalshooting.model.enumeration.Situation;
import br.com.practicalshooting.service.UserService;
import br.com.practicalshooting.util.PrimeFacesUtil;
import br.com.practicalshooting.util.ResourceMessages;

@Named
@ConversationScoped
public class UserController extends BaseBean {

    /**
     *
     */
    private static final long serialVersionUID = 1504292715786026699L;
    private static String FUNCTION = "USUÁRIOS";
    @Inject
    private UserService userService;
    @Inject
    private UserDAO userDAO;
    @Inject
    private DialogoConfirmacaoBean dialogoConfirmacaoBean;

    private User user;
    private UserDataModel userList;

    @PostConstruct
    public void init() {
        this.user = new User();
        this.userList = new UserDataModel(this.userDAO, super.hasPermisson(FUNCTION, ACTION_READ));
        this.userList.list();
    }

    private void save() {
        this.userService.save(this.user);
    }

    public void resetForm() {
        this.user = new User();
    }

    public void cancelDelete() {
        this.user = new User();
        this.dialogoConfirmacaoBean.cleanConfiguration();
    }

    public void confirmDelete() {
        super.verifyIfHasPermisson(FUNCTION, ACTION_DELETE);
        try {
            this.user.setSituation(Situation.CANCELED);
            this.save();
            ResourceMessages.addInfoMessage("default.success.delete.register", this.user.getName());
            this.resetForm();
        } finally {
            this.user = new User();
            this.dialogoConfirmacaoBean.cleanConfiguration();
            PrimeFacesUtil.update("formUsers");
        }
    }

    public void openDelete(Long userSKU) {
        super.verifyIfHasPermisson(FUNCTION, ACTION_DELETE);
        this.user = this.userDAO.findById(userSKU);
        this.dialogoConfirmacaoBean.changeActionOnCancelButton("#{userController.cancelDelete()}");
        this.dialogoConfirmacaoBean.changeActionOnConfirmButton("#{userController.confirmDelete()}");
        this.dialogoConfirmacaoBean.changeHeader(ResourceMessages.getMessage("act.delete.user"));
        this.dialogoConfirmacaoBean.changeMessage(ResourceMessages.getMessage("msg.delete.register.x", this.user.getName()));
        this.dialogoConfirmacaoBean.show();
    }

    public void cancelSuspend() {
        this.user = new User();
        this.dialogoConfirmacaoBean.cleanConfiguration();
    }

    public void confirmSuspend() {
        super.verifyIfHasPermisson(FUNCTION, ACTION_UPDATE);
        try {
            this.user.setSituation(Situation.INACTIVE);
            this.save();
            ResourceMessages.addInfoMessage("default.success.suspend.register", this.user.getName());
            this.resetForm();
        } finally {
            this.user = new User();
            this.dialogoConfirmacaoBean.cleanConfiguration();
            PrimeFacesUtil.update("formUsers");
        }
    }

    public void openSuspend(Long userSKU) {
        super.verifyIfHasPermisson(FUNCTION, ACTION_UPDATE);
        this.user = this.userDAO.findById(userSKU);
        this.dialogoConfirmacaoBean.changeActionOnCancelButton("#{userController.cancelSuspend()}");
        this.dialogoConfirmacaoBean.changeActionOnConfirmButton("#{userController.confirmSuspend()}");
        this.dialogoConfirmacaoBean.changeHeader(ResourceMessages.getMessage("act.suspend.user"));
        this.dialogoConfirmacaoBean.changeMessage(ResourceMessages.getMessage("msg.suspend.register.x", this.user.getName()));
        this.dialogoConfirmacaoBean.show();
    }

    public void cancelActivate() {
        this.user = new User();
        this.dialogoConfirmacaoBean.cleanConfiguration();
    }

    public void confirmActivate() {
        super.verifyIfHasPermisson(FUNCTION, ACTION_UPDATE);
        try {
            this.user.setSituation(Situation.ACTIVE);
            this.save();
            ResourceMessages.addInfoMessage("default.success.activate.register", this.user.getName());
            this.resetForm();
        } finally {
            this.user = new User();
            this.dialogoConfirmacaoBean.cleanConfiguration();
            PrimeFacesUtil.update("formUsers");
        }
    }

    public void openActivate(Long userSKU) {
        super.verifyIfHasPermisson(FUNCTION, ACTION_UPDATE);
        this.user = this.userDAO.findById(userSKU);
        this.dialogoConfirmacaoBean.changeActionOnCancelButton("#{userController.cancelActivate()}");
        this.dialogoConfirmacaoBean.changeActionOnConfirmButton("#{userController.confirmActivate()}");
        this.dialogoConfirmacaoBean.changeHeader(ResourceMessages.getMessage("act.activate.user"));
        this.dialogoConfirmacaoBean.changeMessage(ResourceMessages.getMessage("msg.activate.register.x", this.user.getName()));
        this.dialogoConfirmacaoBean.show();
    }

    public Boolean userActive(User model) {
        return model.getSituation().equals(Situation.ACTIVE);
    }

    public Boolean userInactive(User model) {
        return model.getSituation().equals(Situation.INACTIVE);
    }

    public User getUser() {
        return this.user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public UserDataModel getUserList() {
        return this.userList;
    }

    public void setUserList(UserDataModel userList) {
        this.userList = userList;
    }
}

package br.com.practicalshooting.controller;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;

import br.com.practicalshooting.util.ResourceMessages;

@Named
@ApplicationScoped
public class MessageUtilBean {

    public String messageForKey(String key, String params) {
        return ResourceMessages.getMessage(key, ResourceMessages.getMessage(params));
    }

    public String requiredField(String key) {
        return this.messageForKey("rn.field.x.empty", key);
    }
}

package br.com.practicalshooting.controller.rest;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;

import org.apache.commons.lang3.StringUtils;

/**
 * Servlet Filter implementation class ApplicationRestFilter
 */
@WebFilter("/rest/*")
public class ApplicationRestFilter implements Filter {

    // @Inject
    // private UserDAO userDAO;

    private final List<String> publicEndpoints = new ArrayList<String>();

    public ApplicationRestFilter() {

    }

    @Override
    public void destroy() {
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) request;

        String path = req.getRequestURI();
        if (path == null) {
            chain.doFilter(request, response);
            return;
        }

        path = path.toLowerCase();
        if (this.isPublicEndpoint(path)) {
            chain.doFilter(request, response);
            return;
        }

        String authHeader = req.getHeader(HttpHeaders.AUTHORIZATION);

        if (StringUtils.isBlank(authHeader)) {
            HttpServletResponse httpResponse = (HttpServletResponse) response;
            httpResponse.sendError(HttpServletResponse.SC_FORBIDDEN);
            httpResponse.setContentType("application/json");
            httpResponse.setCharacterEncoding("utf-8");
            return;
        }

        // User user = this.userDAO.selectUser(authHeader);
        //
        // request.setAttribute("afiliado", user);
        response.setCharacterEncoding("UTF-8");
        response.setContentType(MediaType.APPLICATION_JSON);
        chain.doFilter(request, response);
    }

    private boolean isPublicEndpoint(String path) {
        return this.publicEndpoints.contains(path);
    }

    @Override
    public void init(FilterConfig fConfig) throws ServletException {
        this.publicEndpoints.add("/events/rest/media");
    }

}
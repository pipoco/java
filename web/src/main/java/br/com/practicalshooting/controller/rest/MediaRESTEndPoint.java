package br.com.practicalshooting.controller.rest;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.UUID;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;

import com.google.gson.JsonObject;

@Path("/media")
public class MediaRESTEndPoint extends BaseRESTService {

    private static final long serialVersionUID = -7507228725749645739L;

    private static final String PATH = "/Library/WebServer/Documents/events/files/";

    @POST
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    public Response uploadFile(@FormDataParam("file") InputStream fileInputStream, @FormDataParam("file") FormDataContentDisposition contentDispositionHeader) throws Exception {

        String filePath = PATH;
        String fileExtension = contentDispositionHeader.getFileName().split("\\.")[1];

        if (filePath.endsWith(File.separator) == false) {
            filePath += File.separator;
        }
        String uuid = UUID.randomUUID().toString() + "." + fileExtension;

        filePath += uuid;

        // save the file to the server
        this.saveFile(fileInputStream, filePath);

        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("fileServer", uuid);

        return Response.status(Status.OK).entity(this.gson.toJson(jsonObject)).build();
    }

    // save uploaded file to a defined location on the server
    private void saveFile(InputStream uploadedInputStream, String serverLocation) throws Exception {
        OutputStream outpuStream = new FileOutputStream(new File(serverLocation));
        int read = 0;
        byte[] bytes = new byte[1024];

        while ((read = uploadedInputStream.read(bytes)) != -1) {
            outpuStream.write(bytes, 0, read);
        }
        outpuStream.flush();
        outpuStream.close();
    }
}

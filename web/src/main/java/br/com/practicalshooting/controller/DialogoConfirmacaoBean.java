package br.com.practicalshooting.controller;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ConversationScoped;
import javax.inject.Named;

import org.primefaces.component.commandbutton.CommandButton;
import org.primefaces.component.hotkey.Hotkey;

import br.com.practicalshooting.util.FacesContextUtil;
import br.com.practicalshooting.util.ResourceMessages;
import br.com.practicalshooting.util.PrimeFacesUtil;

/**
 * Bean de controle de diálogos de confirmação.
 *
 * @author Hermann Miertschink Neto
 */
@Named
@ConversationScoped
public class DialogoConfirmacaoBean extends BaseBean {

    /**
     *
     */
    private static final long serialVersionUID = 7375123321710270950L;
    private static final String BASENAME = "ConfirmacaoAcao";
    private String defaultHeader;
    private String message;
    private CommandButton confirmButton;
    private CommandButton cancelButton;
    private Hotkey cancelHotKey;

    @PostConstruct
    public void cleanConfiguration() {
        this.defaultHeader = ResourceMessages.getMessage("act.confirmar");
        this.message = "Sem Mensagem Definida";
        this.confirmButton = new CommandButton();
        this.confirmButton.setActionExpression(null);
        this.confirmButton.setOnclick("PF('" + this.dialogName() + "')" + ".hide()");
        this.confirmButton.setAjax(Boolean.TRUE);
        this.cancelButton = new CommandButton();
        this.cancelButton.setActionExpression(null);
        this.cancelButton.setAjax(Boolean.TRUE);
        this.cancelButton.setProcess("@this");
        this.cancelButton.setImmediate(Boolean.TRUE);
        this.cancelButton.setOnclick("PF('" + this.dialogName() + "')" + ".hide()");
        this.cancelHotKey = new Hotkey();
        this.cancelHotKey.setActionExpression(null);
        this.cancelHotKey.setOnstart("PF('" + this.dialogName() + "')" + ".hide()");
    }

    public void changeHeader(String header) {
        String jQueryExpression = "jQuery('#" + this.dialogId() + " > div > a > span[class=\"ui-dialog-title\"]').html('" + header + "');";
        PrimeFacesUtil.execute(jQueryExpression);
    }

    public void changeMessage(String message) {
        this.message = message;
    }

    public void changeActionOnConfirmButton(String action) {
        if (action == null) {
            this.confirmButton.setActionExpression(null);
        } else {
            this.confirmButton.setActionExpression(FacesContextUtil.createMethodExpression(action));
        }
    }

    public void changeActionOnCancelButton(String action) {
        if (action == null) {
            this.cancelButton.setActionExpression(null);
            this.cancelHotKey.setActionExpression(null);
        } else {
            this.cancelButton.setActionExpression(FacesContextUtil.createMethodExpression(action));
            this.cancelHotKey.setActionExpression(FacesContextUtil.createMethodExpression(action));
        }
    }

    public void show() {
        if (this.cancelButton.getActionExpression() == null) {
            this.cancelButton.setOnclick("PF('" + this.dialogName() + "')" + ".hide()");
            this.cancelHotKey.setOnsuccess("PF('" + this.dialogName() + "')" + ".hide()");
        }

        if (this.confirmButton.getActionExpression() == null) {
            PrimeFacesUtil.execute("alert('O diálogo de confirmação não pode ser exibido pois não teve os parâmetros obrigatórios preenchidos!')");
        } else {
            PrimeFacesUtil.execute("PF('" + this.dialogName() + "')" + ".show();");
            PrimeFacesUtil.update(this.formName());
        }
    }

    public void hide() {
        PrimeFacesUtil.execute("PF('" + this.dialogName() + "')" + ".hide();");
        PrimeFacesUtil.update(this.formName());
    }

    public String dialogId() {
        return "id" + BASENAME;
    }

    public String dialogName() {
        return "dialogo" + BASENAME;
    }

    public String formName() {
        return "form" + BASENAME;
    }

    public String getMessage() {
        return this.message;
    }

    public String getDefaultHeader() {
        return this.defaultHeader;
    }

    public CommandButton getConfirmButton() {
        return this.confirmButton;
    }

    public void setConfirmButton(CommandButton confirmButton) {
        this.confirmButton = confirmButton;
    }

    public CommandButton getCancelButton() {
        return this.cancelButton;
    }

    public void setCancelButton(CommandButton cancelButton) {
        this.cancelButton = cancelButton;
    }

    public Hotkey getCancelHotKey() {
        return this.cancelHotKey;
    }

    public void setCancelHotKey(Hotkey cancelHotKey) {
        this.cancelHotKey = cancelHotKey;
    }
}
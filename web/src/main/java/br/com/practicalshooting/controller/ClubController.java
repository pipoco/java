package br.com.practicalshooting.controller;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ConversationScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import org.apache.commons.lang3.StringUtils;

import br.com.practicalshooting.controller.datamodel.ClubDataModel;
import br.com.practicalshooting.dao.ClubDAO;
import br.com.practicalshooting.model.Club;
import br.com.practicalshooting.model.ClubContact;
import br.com.practicalshooting.model.enumeration.ContactType;
import br.com.practicalshooting.service.ClubService;
import br.com.practicalshooting.util.PrimeFacesUtil;
import br.com.practicalshooting.util.ResourceMessages;
import br.com.practicalshooting.util.Util;

@Named
@ConversationScoped
public class ClubController extends BaseBean {

    /**
     *
     */
    private static final long serialVersionUID = 1504292715786026699L;
    private static String FUNCTION = "CLUBES";
    @Inject
    private ClubService clubService;
    @Inject
    private ClubDAO clubDAO;
    @Inject
    private DialogoConfirmacaoBean dialogoConfirmacaoBean;
    @Inject
    private MediaUploadController mediaUploadController;

    private Club club;
    private Club clubDetail;
    private ClubDataModel clubList;
    private List<Club> publicClubList;
    private Integer closeFormDetail;
    private Boolean editingMode;
    private String contactDescription;
    private ContactType contactType;
    private List<ClubContact> removedContacts;

    @PostConstruct
    public void init() {
        this.club = new Club();
        this.clubDetail = new Club();
        this.club.initAddress();
        this.clubDetail.initAddress();
        this.clubList = new ClubDataModel(this.clubDAO, super.hasPermisson(FUNCTION, ACTION_READ));
        this.closeFormDetail = 1;
        this.editingMode = Boolean.FALSE;
        this.clubList.list();
        this.publicClubList = this.clubDAO.listAllForPublic();
    }

    public String openClubDetail(Long clubSku) {
        this.clubDetail = this.clubDAO.findByIdWithContacts(clubSku);
        return "/pages/club/detail.xhtml?faces-redirect=true";
    }

    private void save() {
        this.clubService.save(this.club, this.removedContacts);
        this.club = this.clubDAO.findByIdWithContacts(this.club.getId());
        this.editingMode = Boolean.TRUE;
        PrimeFacesUtil.update("formClubs");
    }

    public void insert() {
        super.verifyIfHasPermisson(FUNCTION, ACTION_CREATE);
        this.save();
        ResourceMessages.addInfoMessage("default.success.insert.register", this.club.getName());
    }

    public void update() {
        super.verifyIfHasPermisson(FUNCTION, ACTION_UPDATE);
        this.save();
        ResourceMessages.addInfoMessage("default.success.update.register", this.club.getName());
    }

    public void uploadClubImage() {
        this.mediaUploadController.changeHeader(ResourceMessages.getMessage("act.upload.club.image"));
        this.mediaUploadController.changeActionOnCancelButton("#{clubController.uploadClubImageCancel()}");
        this.mediaUploadController.changeActionOnConfirmButton("#{clubController.uploadClubImageSuccess()}");
        this.mediaUploadController.show();
    }

    public void uploadClubImageSuccess() {
        this.mediaUploadController.hide();
        this.club.setImage(this.mediaUploadController.getMediaFile());
        PrimeFacesUtil.update("formClubs:detailsFormClub:clubImage");
    }

    public void uploadClubImageCancel() {
        this.mediaUploadController.hide();
    }

    public String clubImage() {
        if (StringUtils.isBlank(this.club.getImage())) {
            return FacesContext.getCurrentInstance().getExternalContext().getApplicationContextPath() + "/resources/images/placeholder.png";
        }
        return this.appInfoBean.getMediaServer() + this.club.getImage();
    }

    public String clubImage(Club model) {
        if (StringUtils.isBlank(model.getImage())) {
            return FacesContext.getCurrentInstance().getExternalContext().getApplicationContextPath() + "/resources/images/placeholder.png";
        }
        return this.appInfoBean.getMediaServer() + model.getImage();
    }

    public void initCreate() {
        this.club = new Club();
        this.club.initAddress();
        this.closeFormDetail = 0;
        this.editingMode = Boolean.TRUE;
        this.removedContacts = new ArrayList<ClubContact>();
    }

    public void resetForm() {
        this.club = new Club();
        this.closeFormDetail = 1;
        this.editingMode = Boolean.FALSE;
    }

    public void preEdit(Long clubSKU) {
        this.removedContacts = new ArrayList<ClubContact>();
        this.club = this.clubDAO.findByIdWithContacts(clubSKU);
        this.club.initAddress();
        this.closeFormDetail = 0;
        this.editingMode = Boolean.TRUE;
    }

    public void onClubContactChange() {
        LOGGER.info(this.contactType);
    }

    public void addNewContact() {
        ClubContact clubContact = this.clubService.buildNewContact(this.club, this.contactDescription, this.contactType);
        this.club.getContacts().add(clubContact);
        this.contactDescription = null;
        this.contactType = null;
    }

    public void removeClubContact(ClubContact model) {
        if (this.removedContacts == null) {
            this.removedContacts = new ArrayList<ClubContact>();
        }
        this.club.getContacts().remove(model);
        if (Util.isPersistentEntity(model)) {
            this.removedContacts.add(model);
        }
    }

    public void cancelDelete() {
        this.club = new Club();
        this.closeFormDetail = 1;
        this.dialogoConfirmacaoBean.cleanConfiguration();
    }

    public void confirmDelete() {
        super.verifyIfHasPermisson(FUNCTION, ACTION_DELETE);
        try {
            this.clubDAO.remove(this.club);
            ResourceMessages.addInfoMessage("default.success.delete.register", this.club.getName());
        } finally {
            this.club = new Club();
            this.closeFormDetail = 1;
            this.dialogoConfirmacaoBean.cleanConfiguration();
            PrimeFacesUtil.update("formClubs");
        }
    }

    public void openDelete(Long clubSKU) {
        super.verifyIfHasPermisson(FUNCTION, ACTION_DELETE);
        this.club = this.clubDAO.findById(clubSKU);
        this.closeFormDetail = 1;
        this.dialogoConfirmacaoBean.changeActionOnCancelButton("#{clubController.cancelDelete()}");
        this.dialogoConfirmacaoBean.changeActionOnConfirmButton("#{clubController.confirmDelete()}");
        this.dialogoConfirmacaoBean.changeHeader(ResourceMessages.getMessage("act.delete.club"));
        this.dialogoConfirmacaoBean.changeMessage(ResourceMessages.getMessage("msg.delete.club.x", this.club.getName()));
        this.dialogoConfirmacaoBean.show();
    }

    public Boolean isNewRegister() {
        return !Util.isPersistentEntity(this.club);
    }

    public Boolean isNotNewRegister() {
        return Util.isPersistentEntity(this.club);
    }

    public Club getClub() {
        return this.club;
    }

    public void setClub(Club club) {
        this.club = club;
    }

    public ClubDataModel getClubList() {
        return this.clubList;
    }

    public void setClubList(ClubDataModel clubList) {
        this.clubList = clubList;
    }

    public Integer getCloseFormDetail() {
        return this.closeFormDetail;
    }

    public void setCloseFormDetail(Integer closeFormDetail) {
        this.closeFormDetail = closeFormDetail;
    }

    public Boolean getEditingMode() {
        return this.editingMode;
    }

    public void setEditingMode(Boolean editingMode) {
        this.editingMode = editingMode;
    }

    public String getContactDescription() {
        return this.contactDescription;
    }

    public void setContactDescription(String contactDescription) {
        this.contactDescription = contactDescription;
    }

    public ContactType getContactType() {
        return this.contactType;
    }

    public void setContactType(ContactType contactType) {
        this.contactType = contactType;
    }

    public List<Club> getPublicClubList() {
        return this.publicClubList;
    }

    public void setPublicClubList(List<Club> publicClubList) {
        this.publicClubList = publicClubList;
    }

    public Club getClubDetail() {
        return this.clubDetail;
    }

    public void setClubDetail(Club clubDetail) {
        this.clubDetail = clubDetail;
    }
}

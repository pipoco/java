package br.com.practicalshooting.controller;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ConversationScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import br.com.practicalshooting.dao.UserDAO;
import br.com.practicalshooting.model.User;
import br.com.practicalshooting.service.UserService;
import br.com.practicalshooting.util.ResourceMessages;
import br.com.practicalshooting.util.Util;

@Named
@ConversationScoped
public class ChangePasswordController extends BaseBean {

    /**
     * 
     */
    private static final long serialVersionUID = 7037305311302446963L;

    @Inject
    private UserService userService;
    @Inject
    private UserDAO userDAO;

    private User user;
    private String infoUser;
    private String passwordChange;
    private String confirmPasswordChange;
    private Boolean userInvalid;
    private Boolean userValid;
    private String errorMessage;

    @PostConstruct
    public void init() {
        this.user = new User();
        this.userInvalid = Boolean.TRUE;
        this.userValid = Boolean.FALSE;
        this.infoUser = ResourceMessages.getMessage("lbl.find.register");
        String token = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("pct");
        this.user = this.userDAO.selectUserByPasswordToken(token);
        if (Util.isPersistentEntity(this.user)) {
            this.infoUser = ResourceMessages.getMessage("lbl.info.change.password", this.user.getName());
            this.userInvalid = Boolean.FALSE;
            this.userValid = Boolean.TRUE;
        } else {
            this.errorMessage = ResourceMessages.getMessage("error.token.change.password.invalid");
        }
    }

    public void changePassword() {
        this.userService.forgetPassword(this.user.getId(), this.passwordChange, this.confirmPasswordChange);
        ResourceMessages.addInfoMessage("default.success.forget.password");
        this.userValid = Boolean.FALSE;
        this.infoUser = "";
    }

    public void userIsNotValid() {
        if (this.userInvalid) {
            this.userInvalid = Boolean.FALSE;
            ResourceMessages.addErrorMessage(this.errorMessage);
            this.infoUser = null;
        }
    }

    public String getInfoUser() {
        return this.infoUser;
    }

    public void setInfoUser(String infoUser) {
        this.infoUser = infoUser;
    }

    public String getPasswordChange() {
        return this.passwordChange;
    }

    public void setPasswordChange(String passwordChange) {
        this.passwordChange = passwordChange;
    }

    public String getConfirmPasswordChange() {
        return this.confirmPasswordChange;
    }

    public void setConfirmPasswordChange(String confirmPasswordChange) {
        this.confirmPasswordChange = confirmPasswordChange;
    }

    public User getUser() {
        return this.user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public UserService getUserService() {
        return this.userService;
    }

    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    public Boolean getUserInvalid() {
        return this.userInvalid;
    }

    public void setUserInvalid(Boolean userInvalid) {
        this.userInvalid = userInvalid;
    }

    public String getErrorMessage() {
        return this.errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public Boolean getUserValid() {
        return this.userValid;
    }

    public void setUserValid(Boolean userValid) {
        this.userValid = userValid;
    }
}

package br.com.practicalshooting.controller;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;

import br.com.practicalshooting.model.enumeration.AlertType;
import br.com.practicalshooting.model.enumeration.ContactType;
import br.com.practicalshooting.model.enumeration.CourseType;
import br.com.practicalshooting.model.enumeration.EventType;
import br.com.practicalshooting.model.enumeration.WeaponType;

@Named
@ApplicationScoped
public class EnumBean extends BaseBean {

    /**
     * 
     */
    private static final long serialVersionUID = -2123978575181700657L;

    public EventType[] eventTypes() {
        return EventType.values();
    }

    public AlertType[] alertTypes() {
        return AlertType.values();
    }

    public ContactType[] contactTypes() {
        return ContactType.values();
    }

    public CourseType[] courseTypes() {
        return CourseType.values();
    }

    public WeaponType[] weaponTypes() {
        return WeaponType.values();
    }
}

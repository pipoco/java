package br.com.practicalshooting.controller;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ConversationScoped;
import javax.inject.Inject;
import javax.inject.Named;

import br.com.practicalshooting.controller.datamodel.AssociateDataModel;
import br.com.practicalshooting.dao.AssociateDAO;
import br.com.practicalshooting.model.Associate;
import br.com.practicalshooting.service.AssociateService;
import br.com.practicalshooting.util.ResourceMessages;
import br.com.practicalshooting.util.PrimeFacesUtil;
import br.com.practicalshooting.util.Util;

@Named
@ConversationScoped
public class AssociateController extends BaseBean {

    /**
     * 
     */
    private static final long serialVersionUID = 1504292715786026699L;

    @Inject
    private AssociateService associateService;
    @Inject
    private AssociateDAO associateDAO;
    @Inject
    private DialogoConfirmacaoBean dialogoConfirmacaoBean;

    private Associate associate;
    private AssociateDataModel associateList;
    private Integer closeFormDetail;
    private Boolean editingMode;

    @PostConstruct
    public void init() {
        this.associate = new Associate();
        this.associateList = new AssociateDataModel(this.associateDAO);
        this.closeFormDetail = 1;
        this.editingMode = Boolean.FALSE;
        this.associateList.list();
    }

    public void save() {
        this.associateService.save(this.associate);
        this.associate = this.associateDAO.findByIdFetchUserAndClub(this.associate.getId());
        this.editingMode = Boolean.TRUE;
        PrimeFacesUtil.update("formAssociates");
        ResourceMessages.addInfoMessage("default.success.update.register", this.associate.getUser().getName());
        this.associateList.list();
    }

    public void resetForm() {
        this.associate = new Associate();
        this.closeFormDetail = 1;
        this.editingMode = Boolean.FALSE;
    }

    public void preEdit(Long associateSKU) {
        this.associate = this.associateDAO.findByIdFetchUserAndClub(associateSKU);
        this.closeFormDetail = 0;
        this.editingMode = Boolean.TRUE;
    }

    public void cancelDelete() {
        this.associate = new Associate();
        this.closeFormDetail = 1;
        this.dialogoConfirmacaoBean.cleanConfiguration();
    }

    public void confirmDelete() {
        try {
            this.associateDAO.remove(this.associate);
            ResourceMessages.addInfoMessage("default.success.delete.register", this.associate.getUser().getName());
            this.associateList.list();
        } finally {
            this.associate = new Associate();
            this.closeFormDetail = 1;
            this.dialogoConfirmacaoBean.cleanConfiguration();
            PrimeFacesUtil.update("formAssociates");
        }
    }

    public void openDelete(Long associateSKU) {
        this.associate = this.associateDAO.findById(associateSKU);
        this.closeFormDetail = 1;
        this.dialogoConfirmacaoBean.changeActionOnCancelButton("#{associateController.cancelDelete()}");
        this.dialogoConfirmacaoBean.changeActionOnConfirmButton("#{associateController.confirmDelete()}");
        this.dialogoConfirmacaoBean.changeHeader(ResourceMessages.getMessage("act.delete.associate"));
        this.dialogoConfirmacaoBean.changeMessage(ResourceMessages.getMessage("msg.delete.associate.x", this.associate.getUser().getName()));
        this.dialogoConfirmacaoBean.show();
    }

    public Boolean isNewRegister() {
        return !Util.isPersistentEntity(this.associate);
    }

    public Boolean isNotNewRegister() {
        return Util.isPersistentEntity(this.associate);
    }

    public Associate getAssociate() {
        return this.associate;
    }

    public void setAssociate(Associate associate) {
        this.associate = associate;
    }

    public AssociateDataModel getAssociateList() {
        return this.associateList;
    }

    public void setAssociateList(AssociateDataModel associateList) {
        this.associateList = associateList;
    }

    public Integer getCloseFormDetail() {
        return this.closeFormDetail;
    }

    public void setCloseFormDetail(Integer closeFormDetail) {
        this.closeFormDetail = closeFormDetail;
    }

    public Boolean getEditingMode() {
        return this.editingMode;
    }

    public void setEditingMode(Boolean editingMode) {
        this.editingMode = editingMode;
    }
}

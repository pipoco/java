package br.com.practicalshooting.controller;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ConversationScoped;
import javax.inject.Inject;
import javax.inject.Named;

import br.com.practicalshooting.controller.datamodel.AccompanimentDataModel;
import br.com.practicalshooting.dao.AccompanimentDAO;
import br.com.practicalshooting.model.Accompaniment;
import br.com.practicalshooting.util.ResourceMessages;

@Named
@ConversationScoped
public class AccompanimentController extends BaseBean {

    /**
     * 
     */
    private static final long serialVersionUID = 1504292715786026699L;

    @Inject
    private AccompanimentDAO accompanimentDAO;
    @Inject
    private DialogoConfirmacaoBean dialogoConfirmacaoBean;

    private Accompaniment accompaniment;
    private AccompanimentDataModel accompanimentList;

    @PostConstruct
    public void init() {
        this.accompaniment = new Accompaniment();
        this.accompanimentList = new AccompanimentDataModel(this.accompanimentDAO, this.sessionBean.getLoggedUser());
        this.accompanimentList.list();
    }

    public void resetForm() {
        this.accompaniment = new Accompaniment();
        this.accompanimentList.list();
    }

    public void cancelDelete() {
        this.accompaniment = new Accompaniment();
        this.dialogoConfirmacaoBean.cleanConfiguration();
    }

    public void confirmDelete() {
        try {
            this.accompanimentDAO.remove(this.accompaniment);
            ResourceMessages.addInfoMessage("default.success.delete.register", ResourceMessages.getMessage(this.accompaniment.getReceiveAlert().name()));
            this.resetForm();
        } finally {
            this.accompaniment = new Accompaniment();
            this.dialogoConfirmacaoBean.cleanConfiguration();
        }
    }

    public void openDelete(Long accompanimentSKU) {
        this.accompaniment = this.accompanimentDAO.findById(accompanimentSKU);
        this.dialogoConfirmacaoBean.changeActionOnCancelButton("#{accompanimentController.cancelDelete()}");
        this.dialogoConfirmacaoBean.changeActionOnConfirmButton("#{accompanimentController.confirmDelete()}");
        this.dialogoConfirmacaoBean.changeHeader(ResourceMessages.getMessage("act.delete.accompaniment"));
        this.dialogoConfirmacaoBean.changeMessage(ResourceMessages.getMessage("msg.delete.register.x", ResourceMessages.getMessage(this.accompaniment.getReceiveAlert().name())));
        this.dialogoConfirmacaoBean.show();
    }

    public Accompaniment getAccompaniment() {
        return this.accompaniment;
    }

    public void setAccompaniment(Accompaniment accompaniment) {
        this.accompaniment = accompaniment;
    }

    public AccompanimentDataModel getAccompanimentList() {
        return this.accompanimentList;
    }

    public void setAccompanimentList(AccompanimentDataModel accompanimentList) {
        this.accompanimentList = accompanimentList;
    }
}

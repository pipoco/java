package br.com.practicalshooting.controller.datamodel;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

import br.com.practicalshooting.dao.CandidateDAO;
import br.com.practicalshooting.model.Candidate;
import br.com.practicalshooting.model.enumeration.SortOrderWrapper;

@SuppressWarnings("unchecked")
public class CandidateDataModel extends LazyDataModel<Candidate> {

    /**
     *
     */
    private static final long serialVersionUID = -1704075775288809394L;

    private static final int DEFAULT_PAGINATION = 5;
    private final CandidateDAO candidateDAO;
    private Candidate[] selectedList;
    private Long userSKU;
    private Long instructorSKU;
    private boolean usingInstructor;
    private boolean hasPermisson;

    public CandidateDataModel(CandidateDAO candidateDAO, boolean hasPermisson) {
        this.candidateDAO = candidateDAO;
        this.hasPermisson = hasPermisson;
    }

    @Override
    public Candidate getRowData(String rowKey) {
        Candidate objetctSelected = null;
        for (Candidate item : (List<Candidate>) this.getWrappedData()) {
            if (item.getId().equals(Long.valueOf(rowKey))) {
                objetctSelected = item;
                break;
            }
        }
        return objetctSelected;
    }

    @Override
    public Object getRowKey(Candidate candidate) {
        return candidate.getId();
    }

    public void list(Long userSKU) {
        this.userSKU = userSKU;
        this.setRowCount(0);
        this.selectedList = null;
        this.setPageSize(DEFAULT_PAGINATION);
        if (!this.hasPermisson) {
            return;
        }
        this.setRowCount(this.candidateDAO.countByUserId(this.userSKU));
    }

    public void listByInstructor(Long instructorSKU) {
        this.instructorSKU = instructorSKU;
        this.usingInstructor = true;
        this.setRowCount(0);
        this.selectedList = null;
        this.setPageSize(DEFAULT_PAGINATION);
        if (!this.hasPermisson) {
            return;
        }
        this.setRowCount(this.candidateDAO.countByInstructorId(instructorSKU));
    }

    @Override
    public List<Candidate> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {
        if (!this.hasPermisson) {
            return new ArrayList<Candidate>();
        }
        if (this.usingInstructor) {
            return this.candidateDAO.listByInstructorIdFetchUserAndInstructor(first, pageSize, sortField, SortOrderWrapper.fromPrimeFaces(sortOrder.name()), this.instructorSKU);
        } else {
            return this.candidateDAO.listByUserIdFetchUserAndInstructor(first, pageSize, sortField, SortOrderWrapper.fromPrimeFaces(sortOrder.name()), this.userSKU);
        }
    }

    public Candidate[] getSelectedList() {
        return this.selectedList;
    }

    public void setSelectedList(Candidate[] selectedList) {
        this.selectedList = selectedList;
    }

    public Long getUserSKU() {
        return this.userSKU;
    }

    public void setUserSKU(Long userSKU) {
        this.userSKU = userSKU;
    }

    public Long getInstructorSKU() {
        return this.instructorSKU;
    }

    public void setInstructorSKU(Long instructorSKU) {
        this.instructorSKU = instructorSKU;
    }
}

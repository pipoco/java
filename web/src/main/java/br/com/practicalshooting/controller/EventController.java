package br.com.practicalshooting.controller;

import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ConversationScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.joda.time.LocalDate;

import br.com.practicalshooting.controller.datamodel.EventDataModel;
import br.com.practicalshooting.dao.ClubDAO;
import br.com.practicalshooting.dao.EventDAO;
import br.com.practicalshooting.dao.ModalityDAO;
import br.com.practicalshooting.dao.ParticipantDAO;
import br.com.practicalshooting.model.Accompaniment;
import br.com.practicalshooting.model.Club;
import br.com.practicalshooting.model.Event;
import br.com.practicalshooting.model.EventDay;
import br.com.practicalshooting.model.EventModality;
import br.com.practicalshooting.model.Modality;
import br.com.practicalshooting.model.Participant;
import br.com.practicalshooting.model.TypeParticipation;
import br.com.practicalshooting.model.dto.NextEventsDTO;
import br.com.practicalshooting.model.enumeration.AlertType;
import br.com.practicalshooting.model.enumeration.EventType;
import br.com.practicalshooting.service.AccompanimentService;
import br.com.practicalshooting.service.EventService;
import br.com.practicalshooting.util.PrimeFacesUtil;
import br.com.practicalshooting.util.ResourceMessages;
import br.com.practicalshooting.util.Util;

@Named
@ConversationScoped
public class EventController extends BaseBean {

    /**
     *
     */
    private static final long serialVersionUID = 1504292715786026699L;
    private static String FUNCTION = "EVENTOS";
    @Inject
    private EventService eventService;
    @Inject
    private AccompanimentService accompanimentService;
    @Inject
    private EventDAO eventDAO;
    @Inject
    private ParticipantDAO participantDAO;
    @Inject
    private ModalityDAO modalityDAO;
    @Inject
    private ClubDAO clubDAO;
    @Inject
    private DialogoConfirmacaoBean dialogoConfirmacaoBean;

    private Event event;
    private NextEventsDTO eventDetail;
    private Event transientEvent;

    private EventDataModel eventList;
    private List<NextEventsDTO> nextEvents;
    private List<NextEventsDTO> nextEventsHome;

    private Integer closeFormDetail;
    private Boolean editingMode;

    private AlertType alertType;
    private Integer alertDays;
    private Date dayEvent;
    private Date hourStart;
    private Date hourEnd;
    private Participant participant;
    private Modality modality;

    private String filterName;
    private Club filterClub;
    private EventType filterType;
    private Date filterDayFrom;
    private Date filterDayTo;
    private boolean filterMode;
    private Integer closeFilterDetail;

    @PostConstruct
    public void init() {
        this.event = new Event();
        this.event.initRelationship();
        this.participant = new Participant();
        this.modality = new Modality();
        this.eventList = new EventDataModel(this.eventDAO, super.hasPermisson(FUNCTION, ACTION_READ));
        this.closeFormDetail = 1;
        this.editingMode = Boolean.FALSE;
        this.eventList.list();
        this.resetFilterOptions();
        this.nextEventsHome = this.eventDAO.listNextEvents(0, 3, null, null, null, null, null);
        this.nextEvents = this.eventDAO.listAllEvents(null, null, null, LocalDate.now(), null);
    }

    public String openEventDetail(Long eventSku) {
        this.eventDetail = this.eventDAO.findByIdForDetail(eventSku);
        return "/pages/event/detail.xhtml?faces-redirect=true";
    }

    private void save() {
        this.eventService.save(this.event);
        this.event = this.eventDAO.findByIdFetchClub(this.event.getId());
        this.event.initRelationship();
        this.editingMode = Boolean.TRUE;
        PrimeFacesUtil.update("formEvents");
    }

    public void insert() {
        super.verifyIfHasPermisson(FUNCTION, ACTION_CREATE);
        this.save();
        ResourceMessages.addInfoMessage("default.success.insert.register", this.event.getName());
    }

    public void update() {
        super.verifyIfHasPermisson(FUNCTION, ACTION_UPDATE);
        this.save();
        ResourceMessages.addInfoMessage("default.success.update.register", this.event.getName());
    }

    public void initCreate() {
        this.event = new Event();
        this.event.initRelationship();
        this.participant = new Participant();
        this.modality = new Modality();
        this.closeFormDetail = 0;
        this.editingMode = Boolean.TRUE;
    }

    public void resetForm() {
        this.event = new Event();
        this.event.initRelationship();
        this.participant = new Participant();
        this.modality = new Modality();
        this.closeFormDetail = 1;
        this.editingMode = Boolean.FALSE;
    }

    public void preEdit(Long eventSKU) {
        this.resetForm();
        this.event = this.eventDAO.findByIdFetchClub(eventSKU);
        this.closeFormDetail = 0;
        this.editingMode = Boolean.TRUE;
    }

    public void cancelDelete() {
        this.transientEvent = new Event();
        this.closeFormDetail = 1;
        this.dialogoConfirmacaoBean.cleanConfiguration();
    }

    public void confirmDelete() {
        super.verifyIfHasPermisson(FUNCTION, ACTION_DELETE);
        try {
            this.eventDAO.remove(this.transientEvent);
            ResourceMessages.addInfoMessage("default.success.delete.register", this.transientEvent.getName());
        } finally {
            this.transientEvent = new Event();
            this.closeFormDetail = 1;
            this.dialogoConfirmacaoBean.cleanConfiguration();
            PrimeFacesUtil.update("formEvents");
        }
    }

    public void searchEvents() {
        LocalDate dayFrom = null;
        LocalDate dayTo = null;
        Long clubSku = null;
        if (this.filterDayFrom != null) {
            dayFrom = LocalDate.fromDateFields(this.filterDayFrom);
        }
        if (this.filterDayTo != null) {
            dayTo = LocalDate.fromDateFields(this.filterDayTo);
        }
        if (Util.isPersistentEntity(this.filterClub)) {
            clubSku = this.filterClub.getId();
        }
        this.nextEvents = this.eventDAO.listAllEvents(this.filterName, clubSku, this.filterType, dayFrom, dayTo);
    }

    public void openDelete(Long eventSKU) {
        super.verifyIfHasPermisson(FUNCTION, ACTION_DELETE);
        this.transientEvent = this.eventDAO.findById(eventSKU);
        this.closeFormDetail = 1;
        this.dialogoConfirmacaoBean.changeActionOnCancelButton("#{eventController.cancelDelete()}");
        this.dialogoConfirmacaoBean.changeActionOnConfirmButton("#{eventController.confirmDelete()}");
        this.dialogoConfirmacaoBean.changeHeader(ResourceMessages.getMessage("act.delete.event"));
        this.dialogoConfirmacaoBean.changeMessage(ResourceMessages.getMessage("msg.delete.event.x", this.transientEvent.getName()));
        this.dialogoConfirmacaoBean.show();
    }

    public List<Club> clubFilter(String query) {
        return this.clubDAO.listAllContainsNameFetchOnlyNameAndSku(query.toLowerCase());
    }

    public List<Participant> participantFilter(String query) {
        return this.participantDAO.listAllContainsNameFetchOnlyNameAndSku(query.toLowerCase());
    }

    public List<Modality> modalityFilter(String query) {
        return this.modalityDAO.listAllContainsNameFetchOnlyNameAndSku(query.toLowerCase());
    }

    public void addNewDay() {
        this.eventService.buildNewEventDay(this.event, this.dayEvent, this.hourStart, this.hourEnd);
        this.dayEvent = null;
        this.hourStart = null;
        this.hourEnd = null;
    }

    public void removeEventDay(EventDay model) {
        this.event.removeEventDay(model);
    }

    public void addNewParticipant(EventDay modelDay) {
        TypeParticipation typeParticipation = new TypeParticipation();
        typeParticipation.setType(this.participantDAO.findById(modelDay.getParticipant().getId()));
        typeParticipation.setEventDay(modelDay);
        modelDay.addParticipation(typeParticipation);
        modelDay.setParticipant(new Participant());
    }

    public void removeParticipation(EventDay modelDay, TypeParticipation modelParticipation) {
        for (EventDay eventDay : this.event.getEventDays()) {
            if (modelDay.equals(eventDay)) {
                eventDay.removeParticipation(modelParticipation);
                break;
            }
        }
    }

    public void addEventModality() {
        this.eventService.buildNewModality(this.event, this.modality);
        this.modality = new Modality();
    }

    public void removeModality(EventModality model) {
        this.event.removeModalitie(model);
    }

    public void cancelFollowEvent() {
        this.transientEvent = new Event();
        this.alertDays = null;
        this.alertType = null;
    }

    public void confirmFollowEvent() {
        if (this.alertType == null) {
            ResourceMessages.addErrorMessage("rn.field.x.empty", ResourceMessages.getMessage("lbl.alert.type"));
            PrimeFacesUtil.update("formFollowEvent");
            return;
        }
        Accompaniment accompaniment = new Accompaniment();
        accompaniment.setEvent(this.transientEvent);
        accompaniment.setDaysBefore(this.alertDays);
        accompaniment.setReceiveAlert(this.alertType);
        accompaniment.setUser(this.sessionBean.getLoggedUser());
        this.accompanimentService.save(accompaniment);
        ResourceMessages.addInfoMessage("default.success.follow.event", ResourceMessages.getMessage(this.alertType.name()), this.transientEvent.getName());
        PrimeFacesUtil.fecharDialogo("dialogFollowEvent");
        PrimeFacesUtil.update("formEvents");
        this.transientEvent = new Event();
        this.alertDays = null;
        this.alertType = null;
    }

    public void openFollowEvent(Event model) {
        this.transientEvent = model;
        this.alertDays = null;
        this.alertType = null;
        PrimeFacesUtil.abrirDialogo("dialogFollowEvent");
        PrimeFacesUtil.update("formFollowEvent");
    }

    public Boolean receiveAlert() {
        return Util.isPersistentEntity(this.transientEvent) && (this.alertType != null) && this.alertType.equals(AlertType.GET_ALERTS);
    }

    public void onAlertTypeChange() {
        if ((this.alertType != null) && !this.alertType.equals(AlertType.GET_ALERTS)) {
            this.alertDays = null;
        }
    }

    public void openFilterOptions() {
        this.filterMode = true;
        this.closeFilterDetail = 0;
    }

    public void closeFilterOptions() {
        // this.nextEvents.filter(this.filterName, this.filterClub,
        // this.filterType, this.filterDayFrom, this.filterDayTo);
        // this.filterMode = false;
        // this.closeFilterDetail = 1;
    }

    public void filterAllOptions() {
        // this.resetFilterOptions();
        // this.nextEvents.filter(this.filterName, this.filterClub,
        // this.filterType, this.filterDayFrom, this.filterDayTo);
    }

    public void resetFilterOptions() {
        this.filterName = null;
        this.filterClub = new Club();
        this.filterType = null;
        this.filterDayFrom = null;
        this.filterDayTo = null;
        this.filterMode = false;
        this.closeFilterDetail = 1;
    }

    public Boolean isNewRegister() {
        return !Util.isPersistentEntity(this.event);
    }

    public Boolean isNotNewRegister() {
        return Util.isPersistentEntity(this.event);
    }

    public Event getEvent() {
        return this.event;
    }

    public void setEvent(Event event) {
        this.event = event;
    }

    public EventDataModel getEventList() {
        return this.eventList;
    }

    public void setEventList(EventDataModel eventList) {
        this.eventList = eventList;
    }

    public Integer getCloseFormDetail() {
        return this.closeFormDetail;
    }

    public void setCloseFormDetail(Integer closeFormDetail) {
        this.closeFormDetail = closeFormDetail;
    }

    public Boolean getEditingMode() {
        return this.editingMode;
    }

    public void setEditingMode(Boolean editingMode) {
        this.editingMode = editingMode;
    }

    public Date getDayEvent() {
        return this.dayEvent;
    }

    public void setDayEvent(Date dayEvent) {
        this.dayEvent = dayEvent;
    }

    public Date getHourStart() {
        return this.hourStart;
    }

    public void setHourStart(Date hourStart) {
        this.hourStart = hourStart;
    }

    public Date getHourEnd() {
        return this.hourEnd;
    }

    public void setHourEnd(Date hourEnd) {
        this.hourEnd = hourEnd;
    }

    public Participant getParticipant() {
        return this.participant;
    }

    public void setParticipant(Participant participant) {
        this.participant = participant;
    }

    public Modality getModality() {
        return this.modality;
    }

    public void setModality(Modality modality) {
        this.modality = modality;
    }

    public AlertType getAlertType() {
        return this.alertType;
    }

    public void setAlertType(AlertType alertType) {
        this.alertType = alertType;
    }

    public Integer getAlertDays() {
        return this.alertDays;
    }

    public void setAlertDays(Integer alertDays) {
        this.alertDays = alertDays;
    }

    public Event getTransientEvent() {
        return this.transientEvent;
    }

    public void setTransientEvent(Event transientEvent) {
        this.transientEvent = transientEvent;
    }

    public List<NextEventsDTO> getNextEvents() {
        return this.nextEvents;
    }

    public void setNextEvents(List<NextEventsDTO> nextEvents) {
        this.nextEvents = nextEvents;
    }

    public String getFilterName() {
        return this.filterName;
    }

    public void setFilterName(String filterName) {
        this.filterName = filterName;
    }

    public Club getFilterClub() {
        return this.filterClub;
    }

    public void setFilterClub(Club filterClub) {
        this.filterClub = filterClub;
    }

    public EventType getFilterType() {
        return this.filterType;
    }

    public void setFilterType(EventType filterType) {
        this.filterType = filterType;
    }

    public Date getFilterDayFrom() {
        return this.filterDayFrom;
    }

    public void setFilterDayFrom(Date filterDayFrom) {
        this.filterDayFrom = filterDayFrom;
    }

    public Date getFilterDayTo() {
        return this.filterDayTo;
    }

    public void setFilterDayTo(Date filterDayTo) {
        this.filterDayTo = filterDayTo;
    }

    public boolean getFilterMode() {
        return this.filterMode;
    }

    public void setFilterMode(boolean filterMode) {
        this.filterMode = filterMode;
    }

    public Integer getCloseFilterDetail() {
        return this.closeFilterDetail;
    }

    public void setCloseFilterDetail(Integer closeFilterDetail) {
        this.closeFilterDetail = closeFilterDetail;
    }

    public List<NextEventsDTO> getNextEventsHome() {
        return this.nextEventsHome;
    }

    public void setNextEventsHome(List<NextEventsDTO> nextEventsHome) {
        this.nextEventsHome = nextEventsHome;
    }

    public NextEventsDTO getEventDetail() {
        return this.eventDetail;
    }

    public void setEventDetail(NextEventsDTO eventDetail) {
        this.eventDetail = eventDetail;
    }
}

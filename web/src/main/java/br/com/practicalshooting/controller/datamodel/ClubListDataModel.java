package br.com.practicalshooting.controller.datamodel;

import java.util.List;
import java.util.Map;

import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

import br.com.practicalshooting.dao.ClubDAO;
import br.com.practicalshooting.model.Club;

@SuppressWarnings("unchecked")
public class ClubListDataModel extends LazyDataModel<Club> {

    /**
     * 
     */
    private static final long serialVersionUID = -1704075775288809394L;

    private static final int DEFAULT_PAGINATION = 5;
    private final ClubDAO clubDAO;

    public ClubListDataModel(ClubDAO clubDAO) {
        this.clubDAO = clubDAO;
    }

    @Override
    public Club getRowData(String rowKey) {
        Club objetctSelected = null;
        for (Club item : (List<Club>) this.getWrappedData()) {
            if (item.getId().equals(Long.valueOf(rowKey))) {
                objetctSelected = item;
                break;
            }
        }
        return objetctSelected;
    }

    @Override
    public Object getRowKey(Club club) {
        return club.getId();
    }

    public void list() {
        this.setRowCount(this.clubDAO.countForPublic());
        this.setPageSize(DEFAULT_PAGINATION);
    }

    @Override
    public List<Club> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {
        return this.clubDAO.listForPublic(first, pageSize);
    }
}

package br.com.practicalshooting.controller;

import java.io.IOException;
import java.lang.reflect.Type;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.enterprise.context.ApplicationScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;

import org.apache.log4j.Logger;

import br.com.practicalshooting.model.dto.ExternalServersDTO;
import br.com.practicalshooting.util.LoggerUtil;
import br.com.practicalshooting.util.ResourceMessages;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

@Named("appInfoBean")
@ApplicationScoped
public class ApplicationInfoBean {

    private final String dataTablePaginatorTemplate = "{CurrentPageReport} {FirstPageLink} {PreviousPageLink} {PageLinks} {NextPageLink} {LastPageLink} {RowsPerPageDropdown}";
    private final String dataTablePaginatorTemplateExporter = "{CurrentPageReport} {FirstPageLink} {PreviousPageLink} {PageLinks} {NextPageLink} {LastPageLink} {RowsPerPageDropdown} {Exporters}";

    private static final long TIMEOUTATUALIZACAOPARAMETROS = 1000L * 60 * 60;
    private static final Logger LOGGER = Logger.getLogger(LoggerUtil.LOGGER_INFO);
    private String mediaServer;
    private String mediaUploadServer;
    private ExternalServersDTO externalServersDTO;

    private Timer timer;
    private Map<String, String> titulosUnidadesFuncionais;

    @PostConstruct
    public void init() {
        this.initExternalServers();
        this.timer = new Timer();
        this.timer.schedule(new TimerTask() {
            @Override
            public void run() {
                // ApplicationInfoBean.this.atualizarParametros();
            }
        }, 0, TIMEOUTATUALIZACAOPARAMETROS);
    }

    private void initExternalServers() {
        try {
            String myPath = FacesContext.getCurrentInstance().getExternalContext().getRealPath("/");
            String content = new String(Files.readAllBytes(Paths.get(myPath + "/resources/contents/externalServers.txt")));
            Gson gson = new Gson();
            Type type = new TypeToken<ExternalServersDTO>() {
            }.getType();
            this.externalServersDTO = gson.fromJson(content, type);
            this.mediaUploadServer = this.externalServersDTO.getMediaUploadServer();
            this.mediaServer = this.externalServersDTO.getMediaServer();
        } catch (IOException e) {
            LOGGER.error("ERROR", e);
        }
    }

    @PreDestroy
    public void destroy() {
        this.timer.cancel();
    }

    public String tituloUf(String codigoUnidadeFuncional) {
        if (this.titulosUnidadesFuncionais == null) {
            this.init();
        }
        String titulo = this.titulosUnidadesFuncionais.get(codigoUnidadeFuncional.toUpperCase());
        if (titulo == null) {
            return codigoUnidadeFuncional.toUpperCase() + " - NÃO ENCONTRADO";
        }
        return titulo;
    }

    public String getDataTablePaginatorTemplate() {
        return this.dataTablePaginatorTemplate;
    }

    public String getDataTablePaginatorTemplateExporter() {
        return this.dataTablePaginatorTemplateExporter;
    }

    public String nomeSistema() {
        return ResourceMessages.getMessage("system.title");
    }

    public String getMediaServer() {
        return this.mediaServer;
    }

    public void setMediaServer(String mediaServer) {
        this.mediaServer = mediaServer;
    }

    public String getMediaUploadServer() {
        return this.mediaUploadServer;
    }

    public void setMediaUploadServer(String mediaUploadServer) {
        this.mediaUploadServer = mediaUploadServer;
    }
}

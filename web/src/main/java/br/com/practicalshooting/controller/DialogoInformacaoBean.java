package br.com.practicalshooting.controller;

import java.io.Serializable;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

import br.com.practicalshooting.util.PrimeFacesUtil;

@Named("dialogoInformacaoBean")
@RequestScoped
public class DialogoInformacaoBean implements Serializable {

    private static final long serialVersionUID = 6487119958330087565L;

    private String mensagem = "<<A Definir>>";

    public void changeHeaderAndShow(String header, String mensagem) {
        this.changeHeader(header);
        this.show(mensagem);
    }

    public void changeHeader(String header) {
        String jQueryExpression = "jQuery('#dialogoInfoBean > div > span[class=\"ui-dialog-title\"]').html('" + header + "');";
        PrimeFacesUtil.execute(jQueryExpression);
    }

    /**
     * Exibe o diálogo com uma determinada mensagem. A requisição de origem
     * precisa ser ajax.
     * 
     * @param mensagem
     *            Mensagem a ser exibida ao usuário em forma de diálogo
     */
    public void show(String mensagem) {
        this.mensagem = mensagem;
        // Atualizar Formulário
        PrimeFacesUtil.update("formInformacao");
        // Exibir Dialogo
        PrimeFacesUtil.execute("PF('dialogoInformacao')" + ".show()");
    }

    public void hide() {
        PrimeFacesUtil.execute("PF('dialogoInformacao')" + ".hide()");
    }

    public String getMensagem() {
        return this.mensagem;
    }
}

package br.com.practicalshooting.controller.rest;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import br.com.practicalshooting.util.ResourceMessages;

@Path("/event")
public class EventRESTEndPoint extends BaseRESTService {

    private static final long serialVersionUID = -7507228725749645739L;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getEvent() {
        return Response.ok().build();
    }

    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getEventWithId(@PathParam("id") Long id) {
        if (id == null || id <= 0) {
            this.addError(ResourceMessages.getMessage("msg.parametros.ausentes", ResourceMessages.getMessage("id")));
            return this.error(Status.BAD_REQUEST);
        }

        return Response.ok().build();
    }
}

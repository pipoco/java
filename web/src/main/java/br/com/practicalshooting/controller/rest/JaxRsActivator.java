package br.com.practicalshooting.controller.rest;

import java.util.HashSet;
import java.util.Set;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

import org.glassfish.jersey.media.multipart.MultiPartFeature;
import org.reflections.Reflections;

import br.com.practicalshooting.controller.rest.util.DefaultMessageBodyWriter;
import br.com.practicalshooting.util.exception.DefaultRestExceptionHandler;

@ApplicationPath("rest")
public class JaxRsActivator extends Application {

	private final static String REST_PACKAGE = "br.com.practicalshooting.controller.rest";

	@Override
	public Set<Class<?>> getClasses() {
		final Set<Class<?>> resources = new HashSet<Class<?>>();

		resources.add(DefaultMessageBodyWriter.class);

		Reflections reflections = new Reflections(REST_PACKAGE);
		Set<Class<? extends BaseRESTService>> implementations = reflections
				.getSubTypesOf(BaseRESTService.class);

		resources.addAll(implementations);

		// Add additional features such as support for Multipart.
		resources.add(MultiPartFeature.class);

		// Add Default Exception Handler
		resources.add(DefaultRestExceptionHandler.class);

		return resources;
	}
}

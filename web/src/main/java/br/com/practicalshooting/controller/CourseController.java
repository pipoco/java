package br.com.practicalshooting.controller;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ConversationScoped;
import javax.inject.Inject;
import javax.inject.Named;

import br.com.practicalshooting.controller.datamodel.CandidateDataModel;
import br.com.practicalshooting.dao.CandidateDAO;
import br.com.practicalshooting.model.Candidate;
import br.com.practicalshooting.model.CandidateContact;
import br.com.practicalshooting.model.WeaponOfCourse;
import br.com.practicalshooting.model.enumeration.ContactType;
import br.com.practicalshooting.model.enumeration.SituationCourse;
import br.com.practicalshooting.service.CandidateService;
import br.com.practicalshooting.util.PrimeFacesUtil;
import br.com.practicalshooting.util.ResourceMessages;
import br.com.practicalshooting.util.Util;

@Named
@ConversationScoped
public class CourseController extends BaseBean {

    /**
     *
     */
    private static final long serialVersionUID = 7037305311302446963L;

    private static String FUNCTION = "CURSOS";

    @Inject
    private CandidateService candidateService;
    @Inject
    private CandidateDAO candidateDAO;
    @Inject
    private DialogoConfirmacaoBean dialogoConfirmacaoBean;

    private CandidateDataModel candidateList;
    private Candidate candidate;
    private String phone;
    private Integer closeFormDetail;
    private Boolean editingMode;
    private String contactDescription;
    private ContactType contactType;
    private List<CandidateContact> removedContacts;

    @PostConstruct
    public void init() {
        this.candidateList = new CandidateDataModel(this.candidateDAO, super.hasPermisson(FUNCTION, ACTION_READ));
        this.candidate = new Candidate();
        this.closeFormDetail = 1;
        this.editingMode = Boolean.FALSE;
        if (Util.isPersistentEntity(this.sessionBean.getLoggedUser())) {
            this.candidateList.list(this.sessionBean.getLoggedUser().getId());
        } else {
            this.candidateList.list(null);
        }
    }

    private void save() {
        this.candidateService.save(this.candidate, this.removedContacts, new ArrayList<WeaponOfCourse>());
        this.candidate = this.candidateDAO.findByIdFetchUserInstructorContactsAndWeapons(this.candidate.getId());
        this.editingMode = Boolean.TRUE;
        PrimeFacesUtil.update("formCourses");
    }

    public void insert() {
        super.verifyIfHasPermisson(FUNCTION, ACTION_CREATE);
        this.save();
        ResourceMessages.addInfoMessage("default.success.insert.register", this.candidate.getUser().getName());
    }

    public void update() {
        super.verifyIfHasPermisson(FUNCTION, ACTION_UPDATE);
        this.putPhone();
        this.save();
        ResourceMessages.addInfoMessage("default.success.update.register", this.candidate.getUser().getName());
    }

    public void resetForm() {
        this.candidate = new Candidate();
        this.closeFormDetail = 1;
        this.editingMode = Boolean.FALSE;
    }

    public void preEdit(Long candidateSKU) {
        this.removedContacts = new ArrayList<CandidateContact>();
        this.candidate = this.candidateDAO.findByIdFetchUserInstructorContactsAndWeapons(candidateSKU);
        this.candidate.initAddress();
        this.popPhone();
        this.closeFormDetail = 0;
        this.editingMode = Boolean.TRUE;
    }

    public void popPhone() {
        if (Util.isPersistentEntity(this.candidate)) {
            for (CandidateContact candidateContact : this.candidate.getContacts()) {
                if (candidateContact.getType().equals(ContactType.PHONE)) {
                    this.phone = candidateContact.getDescription();
                }
            }
        }
    }

    public void putPhone() {
        if (Util.isPersistentEntity(this.candidate)) {
            for (CandidateContact candidateContact : this.candidate.getContacts()) {
                if (candidateContact.getType().equals(ContactType.PHONE)) {
                    candidateContact.setDescription(this.phone);
                }
            }
        }
    }

    public void onCandidateContactChange() {
        LOGGER.info(this.contactType);
    }

    public void addNewContact() {
        CandidateContact candidateContact = this.candidateService.buildNewContact(this.candidate, this.contactDescription, this.contactType);
        this.candidate.getContacts().add(candidateContact);
        this.contactDescription = null;
        this.contactType = null;
    }

    public void removeCandidateContact(CandidateContact model) {
        if (this.removedContacts == null) {
            this.removedContacts = new ArrayList<CandidateContact>();
        }
        this.candidate.getContacts().remove(model);
        if (Util.isPersistentEntity(model)) {
            this.removedContacts.add(model);
        }
    }

    public void cancelDelete() {
        this.candidate = new Candidate();
        this.closeFormDetail = 1;
        this.dialogoConfirmacaoBean.cleanConfiguration();
    }

    public void confirmDelete() {
        super.verifyIfHasPermisson(FUNCTION, ACTION_DELETE);
        try {
            this.candidateDAO.remove(this.candidate);
            ResourceMessages.addInfoMessage("default.success.delete.register", this.candidate.messageToRemove());
        } finally {
            this.candidate = new Candidate();
            this.closeFormDetail = 1;
            this.dialogoConfirmacaoBean.cleanConfiguration();
            PrimeFacesUtil.update("formCourses");
        }
    }

    public void openDelete(Long candidateSKU) {
        super.verifyIfHasPermisson(FUNCTION, ACTION_DELETE);
        this.candidate = this.candidateDAO.findByIdFetchUserAndInstructor(candidateSKU);
        this.closeFormDetail = 1;
        this.dialogoConfirmacaoBean.changeActionOnCancelButton("#{courseController.cancelDelete()}");
        this.dialogoConfirmacaoBean.changeActionOnConfirmButton("#{courseController.confirmDelete()}");
        this.dialogoConfirmacaoBean.changeHeader(ResourceMessages.getMessage("act.delete.register"));
        this.dialogoConfirmacaoBean.changeMessage(ResourceMessages.getMessage("msg.delete.register.x", this.candidate.messageToRemove()));
        this.dialogoConfirmacaoBean.show();
    }

    public Boolean blockEditing(Candidate model) {
        if (!super.hasPermisson(FUNCTION, ACTION_UPDATE)) {
            return true;
        }
        if (!Util.isPersistentEntity(model) || !model.getSituation().equals(SituationCourse.PENDIND)) {
            return true;
        }
        return false;
    }

    public Boolean blockRemoval(Candidate model) {
        if (!super.hasPermisson(FUNCTION, ACTION_DELETE)) {
            return true;
        }
        if (!Util.isPersistentEntity(model) || !model.getSituation().equals(SituationCourse.PENDIND)) {
            return true;
        }
        return false;
    }

    public Boolean hasRegisteredWeapon() {
        return (this.candidate.getWeapons() != null) && !this.candidate.getWeapons().isEmpty();
    }

    public Boolean isntRegisteredWeapon() {
        return !this.hasRegisteredWeapon();
    }

    public Boolean hasCourseRegistred() {
        if (!Util.isPersistentEntity(this.sessionBean.getLoggedUser())) {
            return false;
        }
        return this.candidateService.hasCourseRegistred(this.sessionBean.getLoggedUser().getId());
    }

    public Candidate getCandidate() {
        return this.candidate;
    }

    public void setCandidate(Candidate candidate) {
        this.candidate = candidate;
    }

    public CandidateDataModel getCandidateList() {
        return this.candidateList;
    }

    public void setCandidateList(CandidateDataModel candidateList) {
        this.candidateList = candidateList;
    }

    public Integer getCloseFormDetail() {
        return this.closeFormDetail;
    }

    public void setCloseFormDetail(Integer closeFormDetail) {
        this.closeFormDetail = closeFormDetail;
    }

    public Boolean getEditingMode() {
        return this.editingMode;
    }

    public void setEditingMode(Boolean editingMode) {
        this.editingMode = editingMode;
    }

    public String getContactDescription() {
        return this.contactDescription;
    }

    public void setContactDescription(String contactDescription) {
        this.contactDescription = contactDescription;
    }

    public ContactType getContactType() {
        return this.contactType;
    }

    public void setContactType(ContactType contactType) {
        this.contactType = contactType;
    }

    public List<CandidateContact> getRemovedContacts() {
        return this.removedContacts;
    }

    public void setRemovedContacts(List<CandidateContact> removedContacts) {
        this.removedContacts = removedContacts;
    }

    public String getPhone() {
        return this.phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
}

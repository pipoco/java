package br.com.practicalshooting.controller;

import java.net.URISyntaxException;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ConversationScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.apache.commons.lang3.StringUtils;

import br.com.practicalshooting.dao.InstructorDAO;
import br.com.practicalshooting.dao.UserDAO;
import br.com.practicalshooting.model.Candidate;
import br.com.practicalshooting.model.Instructor;
import br.com.practicalshooting.model.User;
import br.com.practicalshooting.model.dto.InstructorDTO;
import br.com.practicalshooting.service.CandidateService;
import br.com.practicalshooting.util.ResourceMessages;
import br.com.practicalshooting.util.Util;
import br.com.practicalshooting.util.exception.EventsException;

@Named
@ConversationScoped
public class CandidateController extends BaseBean {

    /**
     * 
     */
    private static final long serialVersionUID = 7037305311302446963L;

    @Inject
    private LoginController loginController;
    @Inject
    private RegisterController registerController;
    @Inject
    private CandidateService candidateService;
    @Inject
    private InstructorDAO instructorDAO;
    @Inject
    private UserDAO userDAO;

    private Candidate candidate;
    private User user;
    private Instructor instructor;
    private String userPwd;
    private String userPwdConfirm;
    private String email;
    private String phone;
    private boolean registredUser;
    private boolean registerSuccess;

    @PostConstruct
    public void init() {
        this.user = this.sessionBean.getLoggedUser();
        if (!Util.isPersistentEntity(this.user)) {
            this.user = new User();
            this.registredUser = false;
        } else {
            this.email = this.user.getEmail();
        }
        this.candidate = new Candidate();
    }

    public void register() throws URISyntaxException {
        if (this.userOnline()) {
            this.registerWithLoggedUser();
            ResourceMessages.addInfoMessage("default.success.candidate.register");
            this.registerSuccess = true;
            return;
        }
        if (this.isRegisteredUser()) {
            this.registerWithExistentUser();
            ResourceMessages.addInfoMessage("default.success.candidate.register");
            this.registerSuccess = true;
            return;
        }
        this.registerWithNewUser();
        ResourceMessages.addInfoMessage("default.success.candidate.register");
        this.registerSuccess = true;
    }

    private void registerWithLoggedUser() {
        this.candidate.setUser(this.user);
        this.candidate.setInstructor(this.instructor);
        this.candidateService.register(this.candidate, this.email, this.phone);
    }

    private void registerWithExistentUser() {
        this.loginController.setLogin(this.email);
        this.loginController.setPassword(this.userPwd);
        String loginResult = this.loginController.login();
        if (StringUtils.isBlank(loginResult)) {
            throw new EventsException(ResourceMessages.getMessage("rn.candidate.invalid.user"));
        }
        this.user = this.sessionBean.getLoggedUser();
        this.registerWithLoggedUser();
    }

    private void registerWithNewUser() throws URISyntaxException {
        this.user.setEmail(this.email);
        this.user.setPassword(this.userPwd);
        this.registerController.setUser(this.user);
        String registerResult = this.registerController.register();
        if (StringUtils.isBlank(registerResult)) {
            throw new EventsException(ResourceMessages.getMessage("rn.candidate.register.error"));
        }
        this.registredUser = true;
        this.user = this.userDAO.findByEmail(this.email);
        this.candidate.setUser(this.user);
        this.candidate.setInstructor(this.instructor);
        this.candidateService.register(this.candidate, this.email, this.phone);
    }

    public String createCandidateRegister(InstructorDTO instructorDTO) {
        this.instructor = this.instructorDAO.findByIdWithUser(instructorDTO.getId());
        this.registerSuccess = false;
        this.init();
        return "/pages/candidate/show.xhtml";
    }

    public Boolean userOfflineOrUnregistred() {
        return this.userOffline() && !this.registredUser;
    }

    public Boolean userOffline() {
        return !this.userOnline();
    }

    public Boolean userOnline() {
        return Util.isPersistentEntity(this.sessionBean.getLoggedUser());
    }

    public Boolean isRegisteredUser() {
        return this.userOffline() && this.registredUser;
    }

    public User getUser() {
        return this.user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public CandidateService getCandidateService() {
        return this.candidateService;
    }

    public void setCandidateService(CandidateService candidateService) {
        this.candidateService = candidateService;
    }

    public Candidate getCandidate() {
        return this.candidate;
    }

    public void setCandidate(Candidate candidate) {
        this.candidate = candidate;
    }

    public Instructor getInstructor() {
        return this.instructor;
    }

    public void setInstructor(Instructor instructor) {
        this.instructor = instructor;
    }

    public boolean isRegistredUser() {
        return this.registredUser;
    }

    public void setRegistredUser(boolean registredUser) {
        this.registredUser = registredUser;
    }

    public String getUserPwd() {
        return this.userPwd;
    }

    public void setUserPwd(String userPwd) {
        this.userPwd = userPwd;
    }

    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return this.phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getUserPwdConfirm() {
        return this.userPwdConfirm;
    }

    public void setUserPwdConfirm(String userPwdConfirm) {
        this.userPwdConfirm = userPwdConfirm;
    }

    public boolean isRegisterSuccess() {
        return this.registerSuccess;
    }

    public void setRegisterSuccess(boolean registerSuccess) {
        this.registerSuccess = registerSuccess;
    }

}

package br.com.practicalshooting.controller.datamodel;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

import br.com.practicalshooting.dao.UserDAO;
import br.com.practicalshooting.model.User;
import br.com.practicalshooting.model.enumeration.SortOrderWrapper;

@SuppressWarnings("unchecked")
public class UserDataModel extends LazyDataModel<User> {

    /**
     *
     */
    private static final long serialVersionUID = -1704075775288809394L;

    private static final int DEFAULT_PAGINATION = 5;
    private final UserDAO userDAO;
    private User[] selectedList;
    private boolean hasPermission;

    public UserDataModel(UserDAO userDAO, boolean hasPermission) {
        this.userDAO = userDAO;
        this.hasPermission = hasPermission;
    }

    @Override
    public User getRowData(String rowKey) {
        User objetctSelected = null;
        for (User item : (List<User>) this.getWrappedData()) {
            if (item.getId().equals(Long.valueOf(rowKey))) {
                objetctSelected = item;
                break;
            }
        }
        return objetctSelected;
    }

    @Override
    public Object getRowKey(User user) {
        return user.getId();
    }

    public void list() {
        this.setRowCount(0);
        this.selectedList = null;
        this.setPageSize(DEFAULT_PAGINATION);
        if (!this.hasPermission) {
            return;
        }
        this.setRowCount(this.userDAO.count());
    }

    @Override
    public List<User> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {
        if (!this.hasPermission) {
            return new ArrayList<User>();
        }
        return this.userDAO.list(first, pageSize, sortField, SortOrderWrapper.fromPrimeFaces(sortOrder.name()));
    }

    public User[] getSelectedList() {
        return this.selectedList;
    }

    public void setSelectedList(User[] selectedList) {
        this.selectedList = selectedList;
    }
}

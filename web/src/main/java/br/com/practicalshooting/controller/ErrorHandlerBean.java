package br.com.practicalshooting.controller;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.mail.MessagingException;
import javax.mail.internet.AddressException;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.joda.time.LocalDate;

import br.com.practicalshooting.dao.ErrorWrapperDAO;
import br.com.practicalshooting.model.ErrorWrapper;
import br.com.practicalshooting.model.User;
import br.com.practicalshooting.util.FacesContextUtil;
import br.com.practicalshooting.util.LoggerUtil;
import br.com.practicalshooting.util.Mail;
import br.com.practicalshooting.util.MailMessage;
import br.com.practicalshooting.util.ResourceMessages;
import br.com.practicalshooting.util.Util;

@Named
@SessionScoped
public class ErrorHandlerBean extends BaseBean {

    private static final Logger LOGGER = Logger.getLogger(LoggerUtil.LOGGER_INFO);

    private static final long serialVersionUID = -80735681667636119L;

    @Inject
    private SessionBean sessionBean;
    @Inject
    private ErrorWrapperDAO erroDAO;
    @Inject
    private Mail mailSender;

    private ErrorWrapper erro;
    private String emailPara;
    private String detalhes;
    private boolean receberCopia;

    @PostConstruct
    public void init() {
        this.erro = new ErrorWrapper();
        this.emailPara = null;
        this.detalhes = null;
        this.receberCopia = false;
    }

    public void iniciarContextoErrorHandler(Throwable throwable) {
        this.init();
        try {
            this.erro.setViewId(FacesContextUtil.getViewId());
            this.erro.setMoment(LocalDate.now());
            this.erro.setMessage(throwable.getMessage());
            this.erro.setStackTrace(Util.printStackToString(throwable));
            this.erro = this.erroDAO.merge(this.erro);
        } catch (Exception e) {
            LOGGER.error("IniciarContextoErrorHandler", e);
        }
    }

    public String sendReport() {
        try {
            this.erro.setDetails(this.detalhes);
            this.erro = this.erroDAO.merge(this.erro);
            ResourceMessages.addInfoMessage("msg.obrigado.pelo.report");
        } catch (Exception e) {
            LOGGER.error("sendReport (Other)", e);
        }
        return "/templates/blank.jsf";
    }

    public String sendEmailReport() {
        try {
            this.erro.setDetails(this.detalhes);
            this.erro = this.erroDAO.merge(this.erro);

            List<String> enviarEmailPara = new ArrayList<String>();

            boolean hasOneInvalid = false;
            if (this.receberCopia) {
                if (StringUtils.isBlank(this.emailPara)) {
                    ResourceMessages.addErrorMessage("msg.campo.obrigatorio", "e-mail");
                    hasOneInvalid = true;
                } else {
                    String[] vtr = this.emailPara.split(";");
                    for (String str : vtr) {
                        if (str.trim().isEmpty()) {
                            // Ignorar este.
                        } else if (Util.emailValido(str)) {
                            enviarEmailPara.add(str);
                        } else {
                            hasOneInvalid = true;
                            ResourceMessages.addErrorMessage("msg.email.invalido.completo", str);
                        }
                    }
                }
                if (hasOneInvalid) {
                    return null;
                }
                if (enviarEmailPara.isEmpty()) {
                    ResourceMessages.addErrorMessage("msg.campo.obrigatorio", "e-mail");
                    return null;
                }
            }
            ResourceMessages.addInfoMessage("msg.obrigado.pelo.report");

            enviarEmailPara.add("julioeffgen@gmail.com");

            if (!enviarEmailPara.isEmpty()) {
                MailMessage message;
                String titulo = "[Events] Error Report " + StringUtils.leftPad(this.erro.getId().toString(), 6, "0");
                // final String nomeUsuario = "{nomeUsuario}";
                // final String emailUsuario = "{emailUsuario}";
                final String detalhesReplace = "{detalhes}";
                final String mensagemErro = "{mensagemErro}";
                final String momento = "{momento}";

                String mensagem = "";
                if (StringUtils.isEmpty(mensagem)) {
                    mensagem = "Mensagem de erro não configurada.";
                } else {
                    User user = this.sessionBean.getLoggedUser();
                    if (user == null) {
                        user = new User();
                    }
                    // if (mensagem.contains(nomeUsuario)) {
                    // mensagem =
                    // mensagem.replaceAll(nomeUsuario.replaceAll("[{]",
                    // "[{]").replaceAll("[}]", "[}]"), user.getNome() != null ?
                    // user.getNome() : "Não Cadastrado");
                    // }
                    // if (mensagem.contains(emailUsuario)) {
                    // mensagem =
                    // mensagem.replaceAll(emailUsuario.replaceAll("[{]",
                    // "[{]").replaceAll("[}]", "[}]"), user.getEmail() != null
                    // ? user.getEmail() : "Não Cadastrado");
                    // }
                    if (this.erro.getDetails() != null && mensagem.contains(detalhesReplace)) {
                        mensagem = mensagem.replaceAll(detalhesReplace.replaceAll("[{]", "[{]").replaceAll("[}]", "[}]"), this.erro.getDetails());
                    }
                    if (this.erro.getMessage() != null && mensagem.contains(mensagemErro)) {
                        mensagem = mensagem.replaceAll(mensagemErro.replaceAll("[{]", "[{]").replaceAll("[}]", "[}]"), this.erro.getMessage());
                    }
                    if (this.erro.getMoment() != null && mensagem.contains(momento)) {
                        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
                        mensagem = mensagem.replaceAll(momento.replaceAll("[{]", "[{]").replaceAll("[}]", "[}]"), sdf.format(this.erro.getMoment()));
                    }
                }

                message = new MailMessage().assunto(titulo).para(enviarEmailPara.toArray()).mensagem(mensagem);

                this.mailSender.send(message);
                ResourceMessages.addInfoMessage("msg.obrigado.pelo.report.sucesso.email");
            }

        } catch (AddressException e) {
            LOGGER.error("sendReport (Invalid Email) ", e);
        } catch (MessagingException e) {
            LOGGER.error("sendReport (Error on send)", e);
        } catch (Exception e) {
            LOGGER.error("sendReport (Other)", e);
        }
        return "/templates/blank.jsf";
    }

    public ErrorWrapper getErro() {
        return this.erro;
    }

    public void setErro(ErrorWrapper erro) {
        this.erro = erro;
    }

    public String getEmailPara() {
        return this.emailPara;
    }

    public void setEmailPara(String emailPara) {
        this.emailPara = emailPara;
    }

    public String getDetalhes() {
        return this.detalhes;
    }

    public void setDetalhes(String detalhes) {
        this.detalhes = detalhes;
    }

    public boolean isReceberCopia() {
        return this.receberCopia;
    }

    public void setReceberCopia(boolean receberCopia) {
        this.receberCopia = receberCopia;
    }
}

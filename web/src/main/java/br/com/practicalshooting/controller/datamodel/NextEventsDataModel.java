package br.com.practicalshooting.controller.datamodel;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.joda.time.LocalDate;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

import br.com.practicalshooting.dao.EventDAO;
import br.com.practicalshooting.model.Club;
import br.com.practicalshooting.model.Event;
import br.com.practicalshooting.model.dto.NextEventsDTO;
import br.com.practicalshooting.model.enumeration.EventType;
import br.com.practicalshooting.util.Util;

@SuppressWarnings("unchecked")
public class NextEventsDataModel extends LazyDataModel<NextEventsDTO> {

    /**
     * 
     */
    private static final long serialVersionUID = -6907487562693762128L;

    private final EventDAO eventDAO;
    private String filterName;
    private Long filterClub;
    private EventType filterType;
    private LocalDate filterDayFrom;
    private LocalDate filterDayTo;

    public NextEventsDataModel(EventDAO eventDAO) {
        this.eventDAO = eventDAO;
        this.setRowCount(this.eventDAO.countNextEvents());
    }

    @Override
    public NextEventsDTO getRowData(String rowKey) {
        NextEventsDTO objetctSelected = null;
        for (Event item : (List<Event>) this.getWrappedData()) {
            if (item.getId().equals(Long.valueOf(rowKey))) {
                objetctSelected = new NextEventsDTO(item, null, null);
                break;
            }
        }
        return objetctSelected;
    }

    @Override
    public Object getRowKey(NextEventsDTO event) {
        return event.getId();
    }

    @Override
    public List<NextEventsDTO> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {
        return this.eventDAO.listNextEvents(first, pageSize, this.filterName, this.filterClub, this.filterType, this.filterDayFrom, this.filterDayTo);
    }

    public void filter(String filterName, Club filterClub, EventType filterType, Date filterDayFrom, Date filterDayTo) {
        this.filterName = filterName;
        this.filterClub = Util.isPersistentEntity(filterClub) ? filterClub.getId() : null;
        this.filterType = filterType;
        this.filterDayFrom = filterDayFrom == null ? null : LocalDate.fromDateFields(filterDayFrom);
        this.filterDayTo = filterDayTo == null ? null : LocalDate.fromDateFields(filterDayTo);
    }

    public String getFilterName() {
        return this.filterName;
    }

    public void setFilterName(String filterName) {
        this.filterName = filterName;
    }

    public Long getFilterClub() {
        return this.filterClub;
    }

    public void setFilterClub(Long filterClub) {
        this.filterClub = filterClub;
    }

    public EventType getFilterType() {
        return this.filterType;
    }

    public void setFilterType(EventType filterType) {
        this.filterType = filterType;
    }

    public LocalDate getFilterDayFrom() {
        return this.filterDayFrom;
    }

    public void setFilterDayFrom(LocalDate filterDayFrom) {
        this.filterDayFrom = filterDayFrom;
    }

    public LocalDate getFilterDayTo() {
        return this.filterDayTo;
    }

    public void setFilterDayTo(LocalDate filterDayTo) {
        this.filterDayTo = filterDayTo;
    }
}

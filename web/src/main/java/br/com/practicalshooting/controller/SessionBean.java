package br.com.practicalshooting.controller;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.enterprise.context.Conversation;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.apache.log4j.Logger;

import br.com.practicalshooting.dao.InstructorDAO;
import br.com.practicalshooting.dao.PermissionDAO;
import br.com.practicalshooting.model.Instructor;
import br.com.practicalshooting.model.User;
import br.com.practicalshooting.model.dto.PermissionDTO;
import br.com.practicalshooting.security.model.Permission;
import br.com.practicalshooting.util.FacesContextUtil;
import br.com.practicalshooting.util.LoggerUtil;
import br.com.practicalshooting.util.PrimeFacesUtil;
import br.com.practicalshooting.util.ResourceMessages;
import br.com.practicalshooting.util.Util;
import br.com.practicalshooting.util.exception.EventsException;

@Named
@SessionScoped
public class SessionBean implements Serializable {

    private static final long serialVersionUID = -62101873061793757L;

    private static final Logger LOGGER = Logger.getLogger(LoggerUtil.LOGGER_INFO);

    @Inject
    private Conversation conversation;
    @Inject
    private InstructorDAO instructorDAO;
    @Inject
    private PermissionDAO permissionDAO;

    private User loggedUser;
    private Instructor loggedInstructor;
    private PermissionDTO permissionDTO;

    private Integer dataTableRows;

    @PostConstruct
    public void init() {
        this.dataTableRows = 10;
        this.loggedUser = null;
        this.permissionDTO = new PermissionDTO();
    }

    public String logout() {
        this.init();
        FacesContextUtil.invalidateSession();
        return "/home.jsf?faces-redirect=true";
    }

    public String goHome() {
        return "/home.jsf?faces-redirect=true";
    }

    public boolean isLoggedIn() {
        return Util.isPersistentEntity(this.loggedUser);
    }

    public boolean hasPermisson(String function, String action) {
        return this.permissionDTO.hasPermission(function, action);
    }

    @PreDestroy
    public void logoutTimeout() {
        try {
            this.init();
            if (FacesContextUtil.isValidFacesSession()) {
                FacesContextUtil.invalidateSession();
                FacesContextUtil.redirecionar("/home.jsf");
            }
        } catch (Exception e) {
            e.printStackTrace();
            LOGGER.error("Ao efetuar o timeout  de sessão. Acesso: ", e);
        }
    }

    public void keepAlive() {
        // Deixado intencionalmente em branco.
    }

    public void logoutInatividade() {
        try {
            this.init();
            if (FacesContextUtil.isValidFacesSession()) {
                FacesContextUtil.invalidateSession();
                FacesContextUtil.redirecionar("/home.jsf");
            }
            PrimeFacesUtil.execute("RedirectTimeout()");
        } catch (Exception e) {
            LOGGER.error("Ao efetuar o timeout por inatividade de sessão. Acesso: ", e);
        }
    }

    public void initConversation() {
        if (this.conversation.isTransient()) {
            this.conversation.begin();
        }
    }

    public void endConversation() {
        if (!this.conversation.isTransient()) {
            this.conversation.end();
        }
    }

    public String goToPage(String page) {
        this.endConversation();
        return page + "?faces-redirect=true";
    }

    public String translateMessage(String key, String param) {
        return ResourceMessages.getMessage(key, ResourceMessages.getMessage(param));
    }

    public Conversation getConversation() {
        return this.conversation;
    }

    public void setConversation(Conversation conversation) {
        this.conversation = conversation;
    }

    public User getLoggedUser() {
        return this.loggedUser;
    }

    public void setLoggedUser(User loggedUser) {
        this.loggedUser = loggedUser;
    }

    public Integer getDataTableRows() {
        return this.dataTableRows;
    }

    public void setDataTableRows(Integer dataTableRows) {
        this.dataTableRows = dataTableRows;
    }

    public Instructor getLoggedInstructor() {
        return this.loggedInstructor;
    }

    public void setLoggedInstructor(Instructor loggedInstructor) {
        this.loggedInstructor = loggedInstructor;
    }

    public void putLoggedUser(User user) {
        this.setLoggedUser(user);
        this.setLoggedInstructor(this.instructorDAO.findByUserIdFetchUser(user.getId()));
        List<Permission> permissions = this.permissionDAO.listPermissionByUser(this.loggedUser.getProfile().getId());
        for (Permission permission : permissions) {
            this.permissionDTO.putPermission(permission.getAction().getFunction().getName(), permission.getAction().getCommand());
        }
    }

    public void verifyIfHasPermisson(String function, String action) {
        if (!this.hasPermisson(function, action)) {
            throw new EventsException(ResourceMessages.getMessage("sorry.unauthorized.access"));
        }
    }

}
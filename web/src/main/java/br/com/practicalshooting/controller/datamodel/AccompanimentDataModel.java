package br.com.practicalshooting.controller.datamodel;

import java.util.List;
import java.util.Map;

import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

import br.com.practicalshooting.dao.AccompanimentDAO;
import br.com.practicalshooting.model.Accompaniment;
import br.com.practicalshooting.model.User;
import br.com.practicalshooting.model.enumeration.SortOrderWrapper;

@SuppressWarnings("unchecked")
public class AccompanimentDataModel extends LazyDataModel<Accompaniment> {

    /**
     * 
     */
    private static final long serialVersionUID = -1704075775288809394L;

    private static final int DEFAULT_PAGINATION = 5;
    private final AccompanimentDAO accompanimentDAO;
    private final User user;
    private Accompaniment[] selectedList;

    public AccompanimentDataModel(AccompanimentDAO accompanimentDAO, User user) {
        this.accompanimentDAO = accompanimentDAO;
        this.user = user;
    }

    @Override
    public Accompaniment getRowData(String rowKey) {
        Accompaniment objetctSelected = null;
        for (Accompaniment item : (List<Accompaniment>) this.getWrappedData()) {
            if (item.getId().equals(Long.valueOf(rowKey))) {
                objetctSelected = item;
                break;
            }
        }
        return objetctSelected;
    }

    @Override
    public Object getRowKey(Accompaniment accompaniment) {
        return accompaniment.getId();
    }

    public void list() {
        this.setRowCount(this.accompanimentDAO.count(this.user.getId()));
        this.selectedList = null;
        this.setPageSize(DEFAULT_PAGINATION);
    }

    @Override
    public List<Accompaniment> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {
        return this.accompanimentDAO.list(first, pageSize, sortField, SortOrderWrapper.fromPrimeFaces(sortOrder.name()), this.user.getId());
    }

    public Accompaniment[] getSelectedList() {
        return this.selectedList;
    }

    public void setSelectedList(Accompaniment[] selectedList) {
        this.selectedList = selectedList;
    }
}

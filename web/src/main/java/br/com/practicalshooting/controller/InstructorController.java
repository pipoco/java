package br.com.practicalshooting.controller;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ConversationScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import org.apache.commons.lang3.StringUtils;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

import com.itextpdf.text.DocumentException;

import br.com.practicalshooting.controller.datamodel.CandidateDataModel;
import br.com.practicalshooting.dao.CandidateDAO;
import br.com.practicalshooting.dao.InstructorDAO;
import br.com.practicalshooting.dao.WeaponOfCourseDAO;
import br.com.practicalshooting.model.Candidate;
import br.com.practicalshooting.model.CandidateContact;
import br.com.practicalshooting.model.Instructor;
import br.com.practicalshooting.model.WeaponOfCourse;
import br.com.practicalshooting.model.dto.InstructorDTO;
import br.com.practicalshooting.model.enumeration.ContactType;
import br.com.practicalshooting.model.enumeration.SituationCourse;
import br.com.practicalshooting.service.CandidateService;
import br.com.practicalshooting.service.WeaponOfCourseService;
import br.com.practicalshooting.service.pdf.CoursePdfService;
import br.com.practicalshooting.util.PrimeFacesUtil;
import br.com.practicalshooting.util.ResourceMessages;
import br.com.practicalshooting.util.Util;
import br.com.practicalshooting.util.exception.EventsException;

@Named
@ConversationScoped
public class InstructorController extends BaseBean {

    /**
     *
     */
    private static final long serialVersionUID = 1504292715786026699L;
    private static final int questionWeight = 20;
    private static String FUNCTION = "INSTRUTORES";

    @Inject
    private InstructorDAO instructorDAO;
    @Inject
    private CandidateService studentService;
    @Inject
    private WeaponOfCourseService weaponOfCourseService;
    @Inject
    private CandidateDAO studentDAO;
    @Inject
    private WeaponOfCourseDAO weaponOfCourseDAO;
    @Inject
    private DialogoConfirmacaoBean dialogoConfirmacaoBean;
    @Inject
    private CoursePdfService coursePdfService;

    private List<InstructorDTO> instructors;
    private Instructor instructor;
    private String courseBasic;
    private String courseAdvanced;

    private CandidateDataModel studentList;
    private Candidate student;
    private Long weaponSku;
    private String phone;
    private Integer closeFormDetail;
    private Boolean editingMode;
    private String contactDescription;
    private ContactType contactType;
    private List<CandidateContact> removedContacts;

    private WeaponOfCourse weaponOfCourse;
    private Boolean editingWeaponMode;

    private StreamedContent formCourseFile;
    private StreamedContent certificateCourseFile;

    @PostConstruct
    public void init() {
        this.instructors = this.instructorDAO.listAllWithContacts();
        this.initCourseBasic();
        this.initCourseAdvanced();

        this.studentList = new CandidateDataModel(this.studentDAO, super.hasPermisson(FUNCTION, ACTION_READ));
        this.student = new Candidate();
        this.closeFormDetail = 1;
        this.editingMode = Boolean.FALSE;
        this.instructor = Util.isPersistentEntity(this.sessionBean.getLoggedUser()) ? this.instructorDAO.findByUserIdFetchUser(this.sessionBean.getLoggedUser().getId()) : new Instructor();
        if (!Util.isPersistentEntity(this.instructor)) {
            this.instructor = new Instructor();
        }
        this.studentList.listByInstructor(this.instructor.getId());

        this.weaponOfCourse = new WeaponOfCourse();
    }

    private void initCourseBasic() {
        try {
            String myPath = FacesContext.getCurrentInstance().getExternalContext().getRealPath("/");
            String content = new String(Files.readAllBytes(Paths.get(myPath + "/resources/contents/courseBasic.txt")));
            this.courseBasic = content;
        } catch (IOException e) {
            LOGGER.error("ERROR", e);
        }
    }

    private void initCourseAdvanced() {
        try {
            String myPath = FacesContext.getCurrentInstance().getExternalContext().getRealPath("/");
            String content = new String(Files.readAllBytes(Paths.get(myPath + "/resources/contents/courseAdvanced.txt")));
            this.courseAdvanced = content;
        } catch (IOException e) {
            LOGGER.error("ERROR", e);
        }
    }

    public String instructorImage(InstructorDTO model) {
        if (StringUtils.isBlank(model.getImage())) {
            return FacesContext.getCurrentInstance().getExternalContext().getApplicationContextPath() + "/resources/images/placeholder.png";
        }
        return this.appInfoBean.getMediaServer() + model.getImage();
    }

    private void save() {
        if (!Util.isPersistentEntity(this.instructor)) {
            throw new EventsException("rn.action.allowed.only.instructor");
        }
        this.studentService.save(this.student, this.removedContacts, new ArrayList<WeaponOfCourse>());
        this.student = this.studentDAO.findByIdFetchUserInstructorContactsAndWeapons(this.student.getId());
        this.editingMode = Boolean.TRUE;
        PrimeFacesUtil.update("formStudents");
    }

    public void insert() {
        super.verifyIfHasPermisson(FUNCTION, ACTION_CREATE);
        this.putPhone();
        this.save();
        ResourceMessages.addInfoMessage("default.success.insert.register", this.student.getUser().getName());
    }

    public void update() {
        super.verifyIfHasPermisson(FUNCTION, ACTION_UPDATE);
        this.putPhone();
        this.save();
        ResourceMessages.addInfoMessage("default.success.update.register", this.student.getUser().getName());
    }

    public void resetForm() {
        this.student = new Candidate();
        this.closeFormDetail = 1;
        this.editingMode = Boolean.FALSE;
    }

    public void preEdit(Long studentSKU) {
        this.removedContacts = new ArrayList<CandidateContact>();
        this.student = this.studentDAO.findByIdFetchUserInstructorContactsAndWeapons(studentSKU);
        this.student.initAddress();
        this.popPhone();
        this.closeFormDetail = 0;
        this.editingMode = Boolean.TRUE;
        this.weaponOfCourse = new WeaponOfCourse();
        this.editingWeaponMode = false;
    }

    public void preEditWeapon(Long weaponSku) {
        this.weaponOfCourse = this.weaponOfCourseDAO.findById(weaponSku);
        this.editingWeaponMode = true;
    }

    public void cancelDeleteWeapon() {
        this.weaponOfCourse = new WeaponOfCourse();
        this.editingWeaponMode = false;
    }

    public void confirmDeleteWeapon() {
        try {
            this.weaponOfCourseDAO.remove(this.weaponOfCourse);
            ResourceMessages.addInfoMessage("default.success.delete.weapon", ResourceMessages.getMessage(this.weaponOfCourse.getWeaponType().toString()));
        } finally {
            PrimeFacesUtil.update("formStudents:weaponList");
            PrimeFacesUtil.update("formStudents:weaponFormInsertUpdate");
            this.student.setWeapons(this.weaponOfCourseDAO.listByCandidate(this.student.getId()));
            this.weaponOfCourse = new WeaponOfCourse();
            this.editingWeaponMode = false;
        }
    }

    public void preDeleteWeapon(Long weaponSku) {
        this.weaponOfCourse = this.weaponOfCourseDAO.findById(weaponSku);
        this.editingWeaponMode = false;
        this.dialogoConfirmacaoBean.changeActionOnCancelButton("#{instructorController.cancelDeleteWeapon()}");
        this.dialogoConfirmacaoBean.changeActionOnConfirmButton("#{instructorController.confirmDeleteWeapon()}");
        this.dialogoConfirmacaoBean.changeHeader(ResourceMessages.getMessage("act.delete.register"));
        this.dialogoConfirmacaoBean.changeMessage(ResourceMessages.getMessage("msg.selected.record"));
        this.dialogoConfirmacaoBean.show();
    }

    public void saveWeapon() {
        String feedbackMessage = Util.isPersistentEntity(this.weaponOfCourse) ? "default.success.add.weapon" : "default.success.alt.weapon";
        this.weaponOfCourse.setCandidate(this.student);
        this.weaponOfCourseService.save(this.weaponOfCourse);
        ResourceMessages.addInfoMessage(feedbackMessage, ResourceMessages.getMessage(this.weaponOfCourse.getWeaponType().toString()));
        this.student.setWeapons(this.weaponOfCourseDAO.listByCandidate(this.student.getId()));
        this.weaponOfCourse = new WeaponOfCourse();
        this.editingWeaponMode = false;
    }

    public void cancelSaveWeapon() {
        this.weaponOfCourse = new WeaponOfCourse();
        this.editingWeaponMode = false;
    }

    public void preInsertWeapon() {
        this.weaponOfCourse = new WeaponOfCourse();
        this.editingWeaponMode = true;
    }

    public void popPhone() {
        if (Util.isPersistentEntity(this.student)) {
            for (CandidateContact studentContact : this.student.getContacts()) {
                if (studentContact.getType().equals(ContactType.PHONE)) {
                    this.phone = studentContact.getDescription();
                }
            }
        }
    }

    public void putPhone() {
        if (Util.isPersistentEntity(this.student)) {
            for (CandidateContact studentContact : this.student.getContacts()) {
                if (studentContact.getType().equals(ContactType.PHONE)) {
                    studentContact.setDescription(this.phone);
                }
            }
        }
    }

    public void onCandidateContactChange() {
        LOGGER.info(this.contactType);
    }

    public void addNewContact() {
        CandidateContact studentContact = this.studentService.buildNewContact(this.student, this.contactDescription, this.contactType);
        this.student.getContacts().add(studentContact);
        this.contactDescription = null;
        this.contactType = null;
    }

    public void removeCandidateContact(CandidateContact model) {
        if (this.removedContacts == null) {
            this.removedContacts = new ArrayList<CandidateContact>();
        }
        this.student.getContacts().remove(model);
        if (Util.isPersistentEntity(model)) {
            this.removedContacts.add(model);
        }
    }

    public void cancelDelete() {
        this.student = new Candidate();
        this.closeFormDetail = 1;
        this.dialogoConfirmacaoBean.cleanConfiguration();
    }

    public void confirmDelete() {
        super.verifyIfHasPermisson(FUNCTION, ACTION_DELETE);
        try {
            this.studentDAO.remove(this.student);
            ResourceMessages.addInfoMessage("default.success.delete.register", this.student.messageToRemove());
        } finally {
            this.student = new Candidate();
            this.closeFormDetail = 1;
            this.dialogoConfirmacaoBean.cleanConfiguration();
            PrimeFacesUtil.update("formStudents");
        }
    }

    public void cancelFinalizeCourse() {
        this.student = new Candidate();
        this.dialogoConfirmacaoBean.cleanConfiguration();
    }

    public void confirmFinalizeCourse() {
        super.verifyIfHasPermisson(FUNCTION, ACTION_UPDATE);
        try {
            this.studentService.finalizeCourse(this.student.getId());
        } finally {
            this.student = new Candidate();
            this.closeFormDetail = 1;
            this.dialogoConfirmacaoBean.cleanConfiguration();
            PrimeFacesUtil.update("formStudents");
        }
    }

    public void openFinalizeCourse(Long studentSKU) {
        super.verifyIfHasPermisson(FUNCTION, ACTION_UPDATE);
        this.student = this.studentDAO.findByIdFetchUserAndInstructor(studentSKU);
        this.closeFormDetail = 1;
        this.dialogoConfirmacaoBean.changeActionOnCancelButton("#{instructorController.cancelFinalizeCourse()}");
        this.dialogoConfirmacaoBean.changeActionOnConfirmButton("#{instructorController.confirmFinalizeCourse()}");
        this.dialogoConfirmacaoBean.changeHeader(ResourceMessages.getMessage("msg.alter.situation"));
        this.dialogoConfirmacaoBean.changeMessage(ResourceMessages.getMessage("msg.alter.situation.of.course", this.student.messageToRemove()));
        this.dialogoConfirmacaoBean.show();
    }

    public void cancelReturnPendingCandidate() {
        this.student = new Candidate();
        this.dialogoConfirmacaoBean.cleanConfiguration();
    }

    public void confirmReturnPendingCandidate() {
        super.verifyIfHasPermisson(FUNCTION, ACTION_UPDATE);
        try {
            this.student.setSituation(SituationCourse.PENDIND);
            this.studentDAO.merge(this.student);
        } finally {
            this.student = new Candidate();
            this.closeFormDetail = 1;
            this.dialogoConfirmacaoBean.cleanConfiguration();
            PrimeFacesUtil.update("formStudents");
        }
    }

    public void openReturnPendingCandidate(Long studentSKU) {
        super.verifyIfHasPermisson(FUNCTION, ACTION_UPDATE);
        this.student = this.studentDAO.findByIdFetchUserAndInstructor(studentSKU);
        this.closeFormDetail = 1;
        this.dialogoConfirmacaoBean.changeActionOnCancelButton("#{instructorController.cancelReturnPendingCandidate()}");
        this.dialogoConfirmacaoBean.changeActionOnConfirmButton("#{instructorController.confirmReturnPendingCandidate()}");
        this.dialogoConfirmacaoBean.changeHeader(ResourceMessages.getMessage("msg.alter.situation"));
        this.dialogoConfirmacaoBean.changeMessage(ResourceMessages.getMessage("msg.alter.situation.of.course", this.student.messageToRemove()));
        this.dialogoConfirmacaoBean.show();
    }

    public void cancelCancelCourse() {
        this.student = new Candidate();
        this.dialogoConfirmacaoBean.cleanConfiguration();
    }

    public void confirmCancelCourse() {
        super.verifyIfHasPermisson(FUNCTION, ACTION_UPDATE);
        try {
            this.student.setSituation(SituationCourse.CANCELED);
            this.studentDAO.merge(this.student);
        } finally {
            this.student = new Candidate();
            this.closeFormDetail = 1;
            this.dialogoConfirmacaoBean.cleanConfiguration();
            PrimeFacesUtil.update("formStudents");
        }
    }

    public void openCancelCourse(Long studentSKU) {
        super.verifyIfHasPermisson(FUNCTION, ACTION_UPDATE);
        this.student = this.studentDAO.findByIdFetchUserAndInstructor(studentSKU);
        this.closeFormDetail = 1;
        this.dialogoConfirmacaoBean.changeActionOnCancelButton("#{instructorController.cancelCancelCourse()}");
        this.dialogoConfirmacaoBean.changeActionOnConfirmButton("#{instructorController.confirmCancelCourse()}");
        this.dialogoConfirmacaoBean.changeHeader(ResourceMessages.getMessage("msg.alter.situation"));
        this.dialogoConfirmacaoBean.changeMessage(ResourceMessages.getMessage("msg.alter.situation.of.course", this.student.messageToRemove()));
        this.dialogoConfirmacaoBean.show();
    }

    public void openDelete(Long studentSKU) {
        super.verifyIfHasPermisson(FUNCTION, ACTION_DELETE);
        this.student = this.studentDAO.findByIdFetchUserAndInstructor(studentSKU);
        this.closeFormDetail = 1;
        this.dialogoConfirmacaoBean.changeActionOnCancelButton("#{instructorController.cancelDelete()}");
        this.dialogoConfirmacaoBean.changeActionOnConfirmButton("#{instructorController.confirmDelete()}");
        this.dialogoConfirmacaoBean.changeHeader(ResourceMessages.getMessage("act.delete.register"));
        this.dialogoConfirmacaoBean.changeMessage(ResourceMessages.getMessage("msg.delete.register.x", this.student.messageToRemove()));
        this.dialogoConfirmacaoBean.show();
    }

    public void downloadFormCourse() throws DocumentException, IOException {
        try {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            this.coursePdfService.genarateFormCourse(this.student.getId(), baos);
            this.formCourseFile = new DefaultStreamedContent(new ByteArrayInputStream(baos.toByteArray()), "application/pdf", "Formulario-" + this.student.getUser().getName() + ".pdf");
            PrimeFacesUtil.execute("$('.downloadFormCourse').trigger('click');");
        } finally {
            this.student = new Candidate();
            this.dialogoConfirmacaoBean.cleanConfiguration();
        }
    }

    public void cancelDownloadFormCourse() {
        this.student = new Candidate();
        this.dialogoConfirmacaoBean.cleanConfiguration();
    }

    public void openDownloadFormCourse(Long studentSKU) {
        super.verifyIfHasPermisson(FUNCTION, ACTION_UPDATE);
        this.student = this.studentDAO.findByIdFetchUserAndInstructor(studentSKU);
        this.dialogoConfirmacaoBean.changeActionOnCancelButton("#{instructorController.cancelDownloadFormCourse()}");
        this.dialogoConfirmacaoBean.changeActionOnConfirmButton("#{instructorController.downloadFormCourse()}");
        this.dialogoConfirmacaoBean.changeHeader(ResourceMessages.getMessage("act.download"));
        this.dialogoConfirmacaoBean.changeMessage(ResourceMessages.getMessage("msg.download.form.course.x", this.student.messageToRemove()));
        this.dialogoConfirmacaoBean.show();
    }

    public void openDownloadCertificateCourse(Long studentSKU, Long weaponSKU) {
        super.verifyIfHasPermisson(FUNCTION, ACTION_UPDATE);
        this.student = this.studentDAO.findByIdFetchUserAndInstructor(studentSKU);
        this.weaponSku = weaponSKU;
        this.dialogoConfirmacaoBean.changeActionOnCancelButton("#{instructorController.cancelDownloadCertificate()}");
        this.dialogoConfirmacaoBean.changeActionOnConfirmButton("#{instructorController.downloadCertificate()}");
        this.dialogoConfirmacaoBean.changeHeader(ResourceMessages.getMessage("act.download"));
        this.dialogoConfirmacaoBean.changeMessage(ResourceMessages.getMessage("msg.download.certificate.course.x", this.student.messageToRemove()));
        this.dialogoConfirmacaoBean.show();
    }

    public void cancelDownloadCertificate() {
        this.student = new Candidate();
        this.dialogoConfirmacaoBean.cleanConfiguration();
    }

    public void downloadCertificate() throws DocumentException, IOException {
        try {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            this.coursePdfService.genarateCertificateCourse(this.student.getId(), this.weaponSku, baos);
            this.certificateCourseFile = new DefaultStreamedContent(new ByteArrayInputStream(baos.toByteArray()), "application/pdf", "Certificate-" + this.student.getUser().getName() + ".pdf");
            PrimeFacesUtil.execute("$('.downloadCertificateCourse').trigger('click');");
        } finally {
            this.student = new Candidate();
            this.weaponSku = null;
            this.dialogoConfirmacaoBean.cleanConfiguration();
        }
    }

    public Boolean blockEditing(Candidate model) {
        if (!super.hasPermisson(FUNCTION, ACTION_UPDATE)) {
            return true;
        }
        if (!Util.isPersistentEntity(model) || !model.getSituation().equals(SituationCourse.PENDIND)) {
            return true;
        }
        return false;
    }

    public Boolean enableDownloads(Candidate model) {
        if (!super.hasPermisson(FUNCTION, ACTION_UPDATE)) {
            return false;
        }
        if (Util.isPersistentEntity(model) && model.getSituation().equals(SituationCourse.FINALIZED)) {
            return true;
        }
        return false;
    }

    public Boolean enableReturnPending(Candidate model) {
        if (!super.hasPermisson(FUNCTION, ACTION_UPDATE)) {
            return false;
        }
        if (Util.isPersistentEntity(model) && !model.getSituation().equals(SituationCourse.PENDIND)) {
            return true;
        }
        return false;
    }

    public Boolean enableCancelCourse(Candidate model) {
        if (!super.hasPermisson(FUNCTION, ACTION_UPDATE)) {
            return false;
        }
        if (Util.isPersistentEntity(model) && !model.getSituation().equals(SituationCourse.CANCELED)) {
            return true;
        }
        return false;
    }

    public Boolean enableFinalizeCourse(Candidate model) {
        if (!super.hasPermisson(FUNCTION, ACTION_UPDATE)) {
            return false;
        }
        if (Util.isPersistentEntity(model) && model.getSituation().equals(SituationCourse.PENDIND)) {
            return true;
        }
        return false;
    }

    public Boolean blockRemoval(Candidate model) {
        if (!super.hasPermisson(FUNCTION, ACTION_DELETE)) {
            return true;
        }
        if (!Util.isPersistentEntity(model) || !model.getSituation().equals(SituationCourse.PENDIND)) {
            return true;
        }
        return false;
    }

    public Boolean hasInstructorRegistred() {
        return Util.isPersistentEntity(this.sessionBean.getLoggedInstructor());
    }

    public Boolean hasRegisteredWeapon() {
        return (this.student.getWeapons() != null) && !this.student.getWeapons().isEmpty();
    }

    public Boolean isntRegisteredWeapon() {
        return !this.hasRegisteredWeapon();
    }

    public void calcPunctuation() {
        if (this.student.getTheoreticalNote() == null) {
            this.student.setPunctuationTheoretical(null);
            return;
        }
        Double total = this.student.getTheoreticalNote() * InstructorController.questionWeight;
        this.student.setPunctuationTheoretical(total.intValue());
    }

    public void calcWeaponPunctuation() {
        if (this.weaponOfCourse.getPunctuationPractice() == null) {
            this.weaponOfCourse.setPracticeNote(null);
            return;
        }
        Double total = this.weaponOfCourse.getPunctuationPractice() * InstructorController.questionWeight;
        this.weaponOfCourse.setPracticeNote(total.intValue());
    }

    public List<InstructorDTO> getInstructors() {
        return this.instructors;
    }

    public void setInstructors(List<InstructorDTO> instructors) {
        this.instructors = instructors;
    }

    public String getCourseBasic() {
        return this.courseBasic;
    }

    public void setCourseBasic(String courseBasic) {
        this.courseBasic = courseBasic;
    }

    public String getCourseAdvanced() {
        return this.courseAdvanced;
    }

    public void setCourseAdvanced(String courseAdvanced) {
        this.courseAdvanced = courseAdvanced;
    }

    public Instructor getInstructor() {
        return this.instructor;
    }

    public void setInstructor(Instructor instructor) {
        this.instructor = instructor;
    }

    public CandidateDataModel getStudentList() {
        return this.studentList;
    }

    public void setStudentList(CandidateDataModel studentList) {
        this.studentList = studentList;
    }

    public Candidate getStudent() {
        return this.student;
    }

    public void setStudent(Candidate student) {
        this.student = student;
    }

    public String getPhone() {
        return this.phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Integer getCloseFormDetail() {
        return this.closeFormDetail;
    }

    public void setCloseFormDetail(Integer closeFormDetail) {
        this.closeFormDetail = closeFormDetail;
    }

    public Boolean getEditingMode() {
        return this.editingMode;
    }

    public void setEditingMode(Boolean editingMode) {
        this.editingMode = editingMode;
    }

    public String getContactDescription() {
        return this.contactDescription;
    }

    public void setContactDescription(String contactDescription) {
        this.contactDescription = contactDescription;
    }

    public ContactType getContactType() {
        return this.contactType;
    }

    public void setContactType(ContactType contactType) {
        this.contactType = contactType;
    }

    public List<CandidateContact> getRemovedContacts() {
        return this.removedContacts;
    }

    public void setRemovedContacts(List<CandidateContact> removedContacts) {
        this.removedContacts = removedContacts;
    }

    public WeaponOfCourse getWeaponOfCourse() {
        return this.weaponOfCourse;
    }

    public void setWeaponOfCourse(WeaponOfCourse weaponOfCourse) {
        this.weaponOfCourse = weaponOfCourse;
    }

    public Boolean getEditingWeaponMode() {
        return this.editingWeaponMode;
    }

    public void setEditingWeaponMode(Boolean editingWeaponMode) {
        this.editingWeaponMode = editingWeaponMode;
    }

    public StreamedContent getFormCourseFile() {
        return this.formCourseFile;
    }

    public void setFormCourseFile(StreamedContent formCourseFile) {
        this.formCourseFile = formCourseFile;
    }

    public StreamedContent getCertificateCourseFile() {
        return this.certificateCourseFile;
    }

    public void setCertificateCourseFile(StreamedContent certificateCourseFile) {
        this.certificateCourseFile = certificateCourseFile;
    }
}

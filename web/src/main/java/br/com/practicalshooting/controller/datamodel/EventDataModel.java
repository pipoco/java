package br.com.practicalshooting.controller.datamodel;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

import br.com.practicalshooting.dao.EventDAO;
import br.com.practicalshooting.model.Event;
import br.com.practicalshooting.model.enumeration.SortOrderWrapper;

@SuppressWarnings("unchecked")
public class EventDataModel extends LazyDataModel<Event> {

    /**
     *
     */
    private static final long serialVersionUID = -1704075775288809394L;

    private static final int DEFAULT_PAGINATION = 5;
    private final EventDAO eventDAO;
    private Event[] selectedList;
    private boolean hasPermission;

    public EventDataModel(EventDAO eventDAO, boolean hasPermission) {
        this.eventDAO = eventDAO;
        this.hasPermission = hasPermission;
    }

    @Override
    public Event getRowData(String rowKey) {
        Event objetctSelected = null;
        for (Event item : (List<Event>) this.getWrappedData()) {
            if (item.getId().equals(Long.valueOf(rowKey))) {
                objetctSelected = item;
                break;
            }
        }
        return objetctSelected;
    }

    @Override
    public Object getRowKey(Event event) {
        return event.getId();
    }

    public void list() {
        this.setRowCount(0);
        this.selectedList = null;
        this.setPageSize(DEFAULT_PAGINATION);
        if (!this.hasPermission) {
            return;
        }
        this.setRowCount(this.eventDAO.count());
    }

    @Override
    public List<Event> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {
        if (!this.hasPermission) {
            return new ArrayList<Event>();
        }
        return this.eventDAO.list(first, pageSize, sortField, SortOrderWrapper.fromPrimeFaces(sortOrder.name()));
    }

    public Event[] getSelectedList() {
        return this.selectedList;
    }

    public void setSelectedList(Event[] selectedList) {
        this.selectedList = selectedList;
    }
}

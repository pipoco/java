package br.com.practicalshooting.controller;

import java.io.Serializable;

import javax.enterprise.context.ConversationScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.apache.log4j.Logger;

import br.com.practicalshooting.util.LoggerUtil;

@Named
@ConversationScoped
public class BaseBean implements Serializable {

    private static final long serialVersionUID = -8630228052045051532L;

    private static final String MSG_SEM_PERMISSAO = "msg.sem.permissao.acesso";

    protected static final Logger LOGGER = Logger.getLogger(LoggerUtil.LOGGER_INFO);

    protected static String ACTION_CREATE = "INSERT";
    protected static String ACTION_READ = "VIEW";
    protected static String ACTION_UPDATE = "UPDATE";
    protected static String ACTION_DELETE = "DELETE";

    @Inject
    protected ApplicationInfoBean appInfoBean;
    @Inject
    protected SessionBean sessionBean;

    protected boolean hasPermisson(String function, String action) {
        return this.sessionBean.hasPermisson(function, action);
    }

    protected void verifyIfHasPermisson(String function, String action) {
        this.sessionBean.verifyIfHasPermisson(function, action);
    }

    public static String getMsgSemPermissao() {
        return MSG_SEM_PERMISSAO;
    }
}

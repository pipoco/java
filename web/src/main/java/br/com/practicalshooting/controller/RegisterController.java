package br.com.practicalshooting.controller;

import java.net.URISyntaxException;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ConversationScoped;
import javax.inject.Inject;
import javax.inject.Named;

import br.com.practicalshooting.model.User;
import br.com.practicalshooting.service.UserService;
import br.com.practicalshooting.util.ResourceMessages;

@Named
@ConversationScoped
public class RegisterController extends BaseBean {

    /**
     * 
     */
    private static final long serialVersionUID = 7037305311302446963L;

    @Inject
    private UserService userService;

    private User user;

    @PostConstruct
    public void init() {
        this.user = new User();
    }

    public String register() throws URISyntaxException {
        this.userService.register(this.user);
        ResourceMessages.addInfoMessage("default.success.register");
        return "/home.xhtml?faces-redirect=true";
    }

    public User getUser() {
        return this.user;
    }

    public void setUser(User user) {
        this.user = user;
    }

}

package br.com.practicalshooting.controller.datamodel;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

import br.com.practicalshooting.dao.ClubDAO;
import br.com.practicalshooting.model.Club;
import br.com.practicalshooting.model.enumeration.SortOrderWrapper;

@SuppressWarnings("unchecked")
public class ClubDataModel extends LazyDataModel<Club> {

    /**
     *
     */
    private static final long serialVersionUID = -1704075775288809394L;

    private static final int DEFAULT_PAGINATION = 5;
    private final ClubDAO clubDAO;
    private Club[] selectedList;
    private boolean hasPermission;

    public ClubDataModel(ClubDAO clubDAO, boolean hasPermission) {
        this.clubDAO = clubDAO;
        this.hasPermission = hasPermission;
    }

    @Override
    public Club getRowData(String rowKey) {
        Club objetctSelected = null;
        for (Club item : (List<Club>) this.getWrappedData()) {
            if (item.getId().equals(Long.valueOf(rowKey))) {
                objetctSelected = item;
                break;
            }
        }
        return objetctSelected;
    }

    @Override
    public Object getRowKey(Club club) {
        return club.getId();
    }

    public void list() {
        this.setRowCount(0);
        this.selectedList = null;
        this.setPageSize(DEFAULT_PAGINATION);
        if (!this.hasPermission) {
            return;
        }
        this.setRowCount(this.clubDAO.count());
    }

    @Override
    public List<Club> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {
        if (!this.hasPermission) {
            return new ArrayList<Club>();
        }
        return this.clubDAO.list(first, pageSize, sortField, SortOrderWrapper.fromPrimeFaces(sortOrder.name()));
    }

    public Club[] getSelectedList() {
        return this.selectedList;
    }

    public void setSelectedList(Club[] selectedList) {
        this.selectedList = selectedList;
    }
}

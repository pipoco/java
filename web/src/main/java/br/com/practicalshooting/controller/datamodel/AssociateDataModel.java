package br.com.practicalshooting.controller.datamodel;

import java.util.List;
import java.util.Map;

import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

import br.com.practicalshooting.dao.AssociateDAO;
import br.com.practicalshooting.model.Associate;
import br.com.practicalshooting.model.enumeration.SortOrderWrapper;

@SuppressWarnings("unchecked")
public class AssociateDataModel extends LazyDataModel<Associate> {

    /**
     * 
     */
    private static final long serialVersionUID = -1704075775288809394L;

    private static final int DEFAULT_PAGINATION = 5;
    private final AssociateDAO associateDAO;
    private Associate[] selectedList;

    public AssociateDataModel(AssociateDAO associateDAO) {
        this.associateDAO = associateDAO;
    }

    @Override
    public Associate getRowData(String rowKey) {
        Associate objetctSelected = null;
        for (Associate item : (List<Associate>) this.getWrappedData()) {
            if (item.getId().equals(Long.valueOf(rowKey))) {
                objetctSelected = item;
                break;
            }
        }
        return objetctSelected;
    }

    @Override
    public Object getRowKey(Associate associate) {
        return associate.getId();
    }

    public void list() {
        this.setRowCount(this.associateDAO.count());
        this.selectedList = null;
        this.setPageSize(DEFAULT_PAGINATION);
    }

    @Override
    public List<Associate> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {
        return this.associateDAO.list(first, pageSize, sortField, SortOrderWrapper.fromPrimeFaces(sortOrder.name()));
    }

    public Associate[] getSelectedList() {
        return this.selectedList;
    }

    public void setSelectedList(Associate[] selectedList) {
        this.selectedList = selectedList;
    }
}

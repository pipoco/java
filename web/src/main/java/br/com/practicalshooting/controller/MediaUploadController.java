package br.com.practicalshooting.controller;

import java.io.IOException;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ConversationScoped;
import javax.inject.Named;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;

import org.glassfish.jersey.media.multipart.FormDataMultiPart;
import org.glassfish.jersey.media.multipart.MultiPartFeature;
import org.glassfish.jersey.media.multipart.file.StreamDataBodyPart;
import org.primefaces.component.commandbutton.CommandButton;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.json.JSONObject;

import br.com.practicalshooting.util.FacesContextUtil;
import br.com.practicalshooting.util.ResourceMessages;
import br.com.practicalshooting.util.PrimeFacesUtil;

/**
 * Bean de controle de diálogos de confirmação.
 * 
 * @author Hermann Miertschink Neto
 */
@Named
@ConversationScoped
public class MediaUploadController extends BaseBean {

    /**
     *
     */
    private static final long serialVersionUID = 7375123321710270950L;
    private static final String BASENAME = "MediaUpload";
    private static final int RESPONSE_OK = 200;
    private String defaultHeader;
    private CommandButton confirmButton;
    private CommandButton cancelButton;
    private String mediaFile;

    @PostConstruct
    public void cleanConfiguration() {
        this.defaultHeader = ResourceMessages.getMessage("act.media.upload");
        this.confirmButton = new CommandButton();
        this.confirmButton.setActionExpression(null);
        this.confirmButton.setOnclick("PF('" + this.dialogName() + "')" + ".hide()");
        this.confirmButton.setAjax(Boolean.TRUE);
        this.cancelButton = new CommandButton();
        this.cancelButton.setActionExpression(null);
        this.cancelButton.setAjax(Boolean.TRUE);
        this.cancelButton.setProcess("@this");
        this.cancelButton.setImmediate(Boolean.TRUE);
        this.cancelButton.setOnclick("PF('" + this.dialogName() + "')" + ".hide()");
    }

    public void handleFileUpload(FileUploadEvent event) throws IOException {
        Client client = ClientBuilder.newBuilder().register(MultiPartFeature.class).build();
        StreamDataBodyPart bodyPart = new StreamDataBodyPart("file", event.getFile().getInputstream(), event.getFile().getFileName());
        FormDataMultiPart formDataMultiPart = new FormDataMultiPart();
        FormDataMultiPart multipart = (FormDataMultiPart) formDataMultiPart.bodyPart(bodyPart);

        WebTarget target = client.target(this.appInfoBean.getMediaUploadServer());
        Response response = target.request().header("Authorization", "teste").post(Entity.entity(multipart, multipart.getMediaType()));
        formDataMultiPart.close();
        multipart.close();
        if (response.getStatus() == RESPONSE_OK) {
            JSONObject json = new JSONObject(response.readEntity(String.class));
            this.mediaFile = json.getString("fileServer");
            ResourceMessages.addInfoMessage("msg.upload.sucesso", this.appInfoBean.getMediaServer() + this.mediaFile);
        }
    }

    public void changeHeader(String header) {
        String jQueryExpression = "jQuery('#" + this.dialogId() + " > div > a > span[class=\"ui-dialog-title\"]').html('" + header + "');";
        PrimeFacesUtil.execute(jQueryExpression);
    }

    public void changeActionOnConfirmButton(String action) {
        if (action == null) {
            this.confirmButton.setActionExpression(null);
        } else {
            this.confirmButton.setActionExpression(FacesContextUtil.createMethodExpression(action));
        }
    }

    public void changeActionOnCancelButton(String action) {
        if (action == null) {
            this.cancelButton.setActionExpression(null);
        } else {
            this.cancelButton.setActionExpression(FacesContextUtil.createMethodExpression(action));
        }
    }

    public void show() {
        if (this.cancelButton.getActionExpression() == null) {
            this.cancelButton.setOnclick("PF('" + this.dialogName() + "')" + ".hide()");
        }

        if (this.confirmButton.getActionExpression() == null) {
            PrimeFacesUtil.execute(ResourceMessages.getMessage("error.open.media.upload"));
        } else {
            PrimeFacesUtil.execute("PF('" + this.dialogName() + "')" + ".show();");
            PrimeFacesUtil.update(this.formName());
        }
    }

    public void hide() {
        PrimeFacesUtil.execute("PF('" + this.dialogName() + "')" + ".hide();");
        PrimeFacesUtil.update(this.formName());
    }

    public String dialogId() {
        return "id" + BASENAME;
    }

    public String dialogName() {
        return "dialogo" + BASENAME;
    }

    public String formName() {
        return "form" + BASENAME;
    }

    public String getDefaultHeader() {
        return this.defaultHeader;
    }

    public String getMediaFile() {
        return this.mediaFile;
    }

    public void setMediaFile(String mediaFile) {
        this.mediaFile = mediaFile;
    }

    public CommandButton getConfirmButton() {
        return this.confirmButton;
    }

    public void setConfirmButton(CommandButton confirmButton) {
        this.confirmButton = confirmButton;
    }

    public CommandButton getCancelButton() {
        return this.cancelButton;
    }

    public void setCancelButton(CommandButton cancelButton) {
        this.cancelButton = cancelButton;
    }

    public void setDefaultHeader(String defaultHeader) {
        this.defaultHeader = defaultHeader;
    }
}
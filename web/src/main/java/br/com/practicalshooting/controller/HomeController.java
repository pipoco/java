package br.com.practicalshooting.controller;

import java.io.IOException;
import java.lang.reflect.Type;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ConversationScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;

import org.apache.log4j.Logger;

import br.com.practicalshooting.model.dto.LinkDTO;
import br.com.practicalshooting.model.dto.UtilitieDTO;
import br.com.practicalshooting.util.EmailTemplateRead;
import br.com.practicalshooting.util.LoggerUtil;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

@Named
@ConversationScoped
public class HomeController extends BaseBean {

    /**
     * 
     */
    private static final long serialVersionUID = 3318715342752428836L;

    private static final Logger LOGGER = Logger.getLogger(LoggerUtil.LOGGER_INFO);

    private List<LinkDTO> links;
    private List<UtilitieDTO> utilities;

    @PostConstruct
    public void init() {
        this.initLinks();
        this.initUtilities();
    }

    public String buildPath(String imagePath) throws URISyntaxException {
        String myPath = EmailTemplateRead.getAbsoluteApplicationUrl();
        return myPath + imagePath;
    }

    private void initLinks() {
        try {
            String myPath = FacesContext.getCurrentInstance().getExternalContext().getRealPath("/");
            String content = new String(Files.readAllBytes(Paths.get(myPath + "/resources/contents/links.txt")));
            Gson gson = new Gson();
            Type type = new TypeToken<List<LinkDTO>>() {
            }.getType();
            this.links = gson.fromJson(content, type);
        } catch (IOException e) {
            LOGGER.error("ERROR", e);
        }
    }

    private void initUtilities() {
        try {
            String myPath = FacesContext.getCurrentInstance().getExternalContext().getRealPath("/");
            String content = new String(Files.readAllBytes(Paths.get(myPath + "/resources/contents/utilities.txt")));
            Gson gson = new Gson();
            Type type = new TypeToken<List<UtilitieDTO>>() {
            }.getType();
            this.utilities = gson.fromJson(content, type);
        } catch (IOException e) {
            LOGGER.error("ERROR", e);
        }
    }

    public List<LinkDTO> getLinks() {
        return this.links;
    }

    public void setLinks(List<LinkDTO> links) {
        this.links = links;
    }

    public List<UtilitieDTO> getUtilities() {
        return this.utilities;
    }

    public void setUtilities(List<UtilitieDTO> utilities) {
        this.utilities = utilities;
    }
}

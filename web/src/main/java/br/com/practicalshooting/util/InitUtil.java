package br.com.practicalshooting.util;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

public class InitUtil {

    private InitUtil() {
        // para sonar
    }

    /**
     * Inicializa uma lista lazy da classe recebida.
     * 
     * @param clazz
     * @return LazyDataModel<T>
     */
    public static <T> LazyDataModel<T> initLazyList(final Class<T> clazz) {
        LazyDataModel<T> modelList = new LazyDataModel<T>() {

            private static final long serialVersionUID = 2063154160932441405L;

            @Override
            public T getRowData(String rowKey) {
                return null;
            }

            @Override
            public String getRowKey(T objeto) {
                return null;
            }

            @Override
            public List<T> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {
                return new ArrayList<T>();
            }
        };
        modelList.setPageSize(5);
        return modelList;
    }
}
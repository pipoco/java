package br.com.practicalshooting.util.exception;

import java.util.Arrays;
import java.util.List;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

@Provider
public class DefaultRestExceptionHandler implements ExceptionMapper<Exception> {

    @Override
    public Response toResponse(Exception exception) {
        exception.printStackTrace();
        return this.error(Status.BAD_REQUEST, exception.getLocalizedMessage());
    }

    protected Response error(Status httpErrorCode, String... errorList) {
        return this.error(httpErrorCode, Arrays.asList(errorList));
    }

    protected Response error(Status httpErrorCode, List<String> errorList) {
        Gson gson = new Gson();
        JsonArray jsonArray = new JsonArray();
        for (String str : errorList) {
            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("message", str);
            jsonArray.add(jsonObject);
        }
        JsonObject errorWrapper = new JsonObject();
        errorWrapper.add("errors", jsonArray);
        return Response.status(httpErrorCode).entity(gson.toJson(errorWrapper)).build();
    }

}

package br.com.practicalshooting.util;

import java.util.List;

import javax.faces.component.UIComponent;
import javax.faces.component.UIForm;
import javax.faces.component.UIInput;
import javax.faces.component.UISelectBoolean;
import javax.faces.component.UISelectMany;
import javax.faces.component.UISelectOne;
import javax.faces.context.FacesContext;

import org.apache.log4j.Logger;
import org.primefaces.component.datatable.DataTable;
import org.primefaces.context.RequestContext;

public class PrimeFacesUtil {

    private static final Logger LOGGER = Logger.getLogger(LoggerUtil.LOGGER_INFO);

    private PrimeFacesUtil() {
        // para sonar
    }

    public static void execute(String script) {
        if (isAjaxRequest()) {
            RequestContext.getCurrentInstance().execute(script);
        } else {
            LOGGER.error("Não é possível executar, pois a requisição não é Ajax!");
        }
    }

    public static void abrirDialogo(String dialogVar) {
        execute("PF('" + dialogVar + "')" + ".show()");
    }

    public static void fecharDialogo(String dialogVar) {
        execute("PF('" + dialogVar + "')" + ".hide()");
    }

    public static void revalidarCampo(String formInput) {
        try {
            UIInput uiInput = (UIInput) FacesContext.getCurrentInstance().getViewRoot().findComponent(formInput);
            uiInput.setValid(false);
        } catch (Exception e) {
            LOGGER.error("Componente não encontrado", e);
        }
    }

    public static boolean isAjaxRequest() {
        if (RequestContext.getCurrentInstance() != null) {
            return RequestContext.getCurrentInstance().isAjaxRequest();
        }
        return false;

    }

    public static void resetarPaginacao(String formInput) {
        try {
            DataTable dataTable = (DataTable) FacesContext.getCurrentInstance().getViewRoot().findComponent(formInput);
            if (dataTable != null) {
                dataTable.setFirst(0);
            }
        } catch (Exception e) {
            LOGGER.error("resetarPaginacao: Componente não encontrado", e);
        }
    }

    public static void update(List<String> update) {
        if (isAjaxRequest()) {
            RequestContext.getCurrentInstance().update(update);
        } else {
            LOGGER.error("Não é possível executar, pois a requisição não é Ajax!");
        }
    }

    public static void update(String update) {
        if (isAjaxRequest()) {
            RequestContext.getCurrentInstance().update(update);
        } else {
            LOGGER.error("Não é possível executar, pois a requisição não é Ajax!");
        }
    }

    public static void invalidarCampo(String form, String formInput) {
        try {
            UIForm uiForm = (UIForm) FacesContext.getCurrentInstance().getViewRoot().findComponent(form);
            List<UIComponent> filhos = uiForm.getChildren();
            for (UIComponent uiComponent : filhos) {
                if (uiComponent.getId().equalsIgnoreCase(formInput)) {
                    if (uiComponent instanceof UIInput) {
                        ((UIInput) uiComponent).setValid(Boolean.FALSE);
                        continue;
                    }
                    if (uiComponent instanceof UISelectBoolean) {
                        ((UISelectBoolean) uiComponent).setValid(Boolean.FALSE);
                        continue;
                    }
                    if (uiComponent instanceof UISelectMany) {
                        ((UISelectMany) uiComponent).setValid(Boolean.FALSE);
                        continue;
                    }
                    if (uiComponent instanceof UISelectOne) {
                        ((UISelectOne) uiComponent).setValid(Boolean.FALSE);
                        continue;
                    }
                }
            }
        } catch (Exception e) {
            LOGGER.error("Componente não encontrado", e);
        }
    }

    public static void validarCampo(String formInput) {
        try {
            UIInput uiInput = (UIInput) FacesContext.getCurrentInstance().getViewRoot().findComponent(formInput);
            uiInput.setValid(true);
        } catch (Exception e) {
            LOGGER.error("Componente não encontrado", e);
        }
    }

    public static void revalidarFormulario(String form) {
        try {
            UIForm uiForm = (UIForm) FacesContext.getCurrentInstance().getViewRoot().findComponent(form);
            List<UIComponent> filhos = uiForm.getChildren();
            for (UIComponent uiComponent : filhos) {
                if (uiComponent instanceof UIInput) {
                    ((UIInput) uiComponent).setValid(true);
                    continue;
                }
                if (uiComponent instanceof UISelectBoolean) {
                    ((UISelectBoolean) uiComponent).setValid(true);
                    continue;
                }
                if (uiComponent instanceof UISelectMany) {
                    ((UISelectMany) uiComponent).setValid(true);
                    continue;
                }
                if (uiComponent instanceof UISelectOne) {
                    ((UISelectOne) uiComponent).setValid(true);
                    continue;
                }
            }
        } catch (Exception e) {
            LOGGER.error("Componente não encontrado", e);
        }
    }
}
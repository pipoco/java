package br.com.practicalshooting.util.exception;

import java.util.Iterator;

import javax.enterprise.context.NonexistentConversationException;
import javax.faces.FacesException;
import javax.faces.application.ViewExpiredException;
import javax.faces.context.ExceptionHandler;
import javax.faces.context.ExceptionHandlerWrapper;
import javax.faces.context.FacesContext;
import javax.faces.event.ExceptionQueuedEvent;
import javax.faces.event.ExceptionQueuedEventContext;

import br.com.practicalshooting.controller.ErrorHandlerBean;
import br.com.practicalshooting.util.ResourceMessages;
import br.com.practicalshooting.util.Util;

public class DefaultExceptionHandler extends ExceptionHandlerWrapper {

    private ErrorHandlerBean errorHandler;

    private final ExceptionHandler wrapped;

    public DefaultExceptionHandler(ExceptionHandler exception) {
        this.wrapped = exception;
    }

    private void addErrorMessage(String messageKey, Object... params) {
        ResourceMessages.addErrorMessage(messageKey, params);
    }

    private void addErrorMessageError(EventsException e) {
        ResourceMessages.addErrorMessage(e.getMessage());
    }

    private void handleRedirect(FacesContext fc, Throwable exception, String view) {
        try {
            // this.getErrorHandler().iniciarContextoErrorHandler(exception);
            if (!fc.getExternalContext().isResponseCommitted()) {
                fc.getExternalContext().redirect(fc.getExternalContext().getRequestContextPath() + view);
            }
            fc.renderResponse();
        } catch (Exception ex) {
            throw new FacesException(ex);
        }
    }

    @Override
    public ExceptionHandler getWrapped() {
        return this.wrapped;
    }

    @Override
    public void handle() {
        final Iterator<ExceptionQueuedEvent> i = this.getUnhandledExceptionQueuedEvents().iterator();
        while (i.hasNext()) {
            ExceptionQueuedEvent event = i.next();
            ExceptionQueuedEventContext context = (ExceptionQueuedEventContext) event.getSource();

            Throwable exception = this.getRootCause(context.getException());
            if (exception.getCause() != null) {
                exception = exception.getCause();
            }

            final FacesContext fc = FacesContext.getCurrentInstance();
            EventsException eventsException = this.hasEventsException(exception);
            MultipleEventsException multipleEventsException = this.hasMultipleEventsException(exception);

            try {
                if (exception instanceof ViewExpiredException || exception instanceof NonexistentConversationException) {
                    this.handleRedirect(fc, exception, "/templates/viewExpired.jsf");
                    break;
                }
                if (eventsException != null) {
                    this.addErrorMessageError(eventsException);
                    break;
                }
                if (multipleEventsException != null) {
                    for (EventsException e : multipleEventsException.getExceptions()) {
                        this.addErrorMessageError(e);
                    }
                    break;
                }
                if (Util.constraintViolationException(exception)) {
                    this.addErrorMessageError(new EventsException("msg.constraint.violation.exception"));
                    break;
                }
                this.redirectToErrorPage(fc, exception);
                break;
            } finally {
                i.remove();
            }
        }
        this.getWrapped().handle();
    }

    private MultipleEventsException hasMultipleEventsException(Throwable exception) {
        Throwable result = null;
        if (exception instanceof MultipleEventsException) {
            return (MultipleEventsException) exception;
        }
        result = exception.getCause();
        if (result != null && result instanceof MultipleEventsException) {
            return (MultipleEventsException) result;
        }
        result = result == null ? null : result.getCause();
        if (result != null && result instanceof MultipleEventsException) {
            return (MultipleEventsException) result;
        }
        return null;
    }

    private EventsException hasEventsException(Throwable exception) {
        Throwable result = null;
        if (exception instanceof EventsException) {
            return (EventsException) exception;
        }
        result = exception.getCause();
        if (result != null && result instanceof EventsException) {
            return (EventsException) result;
        }
        return null;
    }

    private void redirectToErrorPage(final FacesContext fc, Throwable e) {
        if (e.getMessage() != null && e.getMessage().contains("javax.persistence.OptimisticLockException")) {
            this.addErrorMessage("msg.o.registro.foi.removido.ou.alterado.em.outra.transacao");
        } else if (e.getMessage() != null && e.getMessage().contains("javax.persistence.NonUniqueResultException")) {
            this.addErrorMessage("msg.ocorreu.um.erro.mais.de.um.registro");
        } else {
            this.addErrorMessage("default.erro.desconhecido");
            this.handleRedirect(fc, e, "/templates/error.jsf");
        }
    }

    public ErrorHandlerBean getErrorHandler() {
        if (this.errorHandler == null) {
            FacesContext fc = FacesContext.getCurrentInstance();
            this.errorHandler = fc.getApplication().evaluateExpressionGet(fc, "#{errorHandlerBean}", ErrorHandlerBean.class);
        }
        return this.errorHandler;
    }
}

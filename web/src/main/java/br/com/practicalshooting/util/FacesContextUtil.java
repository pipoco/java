package br.com.practicalshooting.util;

import javax.el.ExpressionFactory;
import javax.el.MethodExpression;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.servlet.http.HttpServletRequest;

public class FacesContextUtil {

	private FacesContextUtil() {
		// para sonar
	}

	public static FacesContext getCurrentInstance() {
		return FacesContext.getCurrentInstance();
	}

	public static String getIpAddress() {
		HttpServletRequest request = (HttpServletRequest) getCurrentInstance()
				.getExternalContext().getRequest();
		String ipAddress = request.getHeader("X-FORWARDED-FOR");
		if (ipAddress == null) {
			ipAddress = request.getRemoteAddr();
		}
		if (ipAddress == null) {
			ipAddress = request.getLocalAddr();
		}
		return ipAddress;
	}

	public static void redirecionar(String uri) {
		getCurrentInstance().getExternalContext().encodeActionURL(
				getCurrentInstance().getApplication().getViewHandler()
						.getActionURL(getCurrentInstance(), uri));
	}

	public static MethodExpression createMethodExpression(String action) {
		FacesContext facesContext = FacesContext.getCurrentInstance();
		ExpressionFactory expressionFactory = facesContext.getApplication()
				.getExpressionFactory();
		return expressionFactory.createMethodExpression(
				facesContext.getELContext(), action, null,
				new Class[] { ActionEvent.class });
	}

	public static boolean isValidFacesSession() {
		return getCurrentInstance() != null
				&& getCurrentInstance().getExternalContext() != null;
	}

	public static void invalidateSession() {
		getCurrentInstance().getExternalContext().invalidateSession();
	}

	public static String getViewId() {
		FacesContext facesContext = FacesContext.getCurrentInstance();
		if (facesContext.getViewRoot() != null) {
			return facesContext.getViewRoot().getViewId();
		}
		return "";
	}
}

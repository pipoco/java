jQuery.fn.updateMaskDate = function() {
	aplicarMascarasDefault();
};

jQuery.fn.aplicarHintRowEditor = function(){
	aplicarHintRowEditor();
};

jQuery.fn.updateMasks = function() {
	aplicarMascarasDefault();
};

jQuery(document).ready(function() {
	aplicarMascarasDefault();
	aplicarHintRowEditor();
});

jQuery(document).ajaxSuccess(function() {
	aplicarMascarasDefault();
	aplicarHintRowEditor();
});

jQuery(document).ajaxError(function() {
	aplicarMascarasDefault();
	aplicarHintRowEditor();
});

function desabilitarDomingos(date)
{
	var day = date.getDay();
	return [(day != 0), '']
}

function desabilitarFDS(date)
{
	var day = date.getDay();
	return [(day != 0 && day != 6), ''];
}

function aplicarMascarasDefault(){
	jQuery('.maskDate').children('input').mask('99/99/9999');
	jQuery('.maskDateTime').children('input').mask('99/99/9999 99:99:99');
	jQuery('.maskDateTimeMin').children('input').mask('99/99/9999 99:99');
	jQuery('.maskTimeMin').children('input').mask('99:99');
	jQuery('.maskTimeMin').mask('99:99');
	jQuery('.maskCep').children('input').mask('99.999-999');
	jQuery('.maskCep').mask('99.999-999');
	jQuery('.maskCpf').children('input').mask('999.999.999-99');
	jQuery('.maskCpf').mask('999.999.999-99');
	jQuery('.maskCnpj').mask('99.999.999/9999-99');
	jQuery('.maskCepOpt').mask('?99.999-999');
	jQuery('.maskCpfOpt').mask('?999.999.999-99');
	jQuery('.maskCnpjOpt').mask('?99.999.999/9999-99');
	jQuery('.maskCepOdpv').mask('99999-999');
	jQuery('.maskCepOdpvOpt').mask('?99999-999');
	jQuery('.maskMonetaria').maskMoney({thousands:'', decimal:',', allowZero:false, allowNegative: false});

	$(".inteiro").keydown(function (e) {
		// Allow: backspace, delete, tab, escape, enter and .
		if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110]) !== -1 ||
				(e.keyCode === 65 && e.ctrlKey === true) ||
				(e.keyCode >= 35 && e.keyCode <= 39)) {
			return;
		}
		// Ensure that it is a number and stop the keypress
		if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
			e.preventDefault();
		}
	});

	$(".numero").keydown(function (e) {
		// Allow: backspace, delete, tab, escape, enter and .
		if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
				(e.keyCode === 65 && e.ctrlKey === true) ||
				(e.keyCode >= 35 && e.keyCode <= 39)) {
			return;
		}
		// Ensure that it is a number and stop the keypress
		if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
			e.preventDefault();
		}
	});
	
	$(".commanumber").keydown(function (e) {
		// Allow: backspace, delete, tab, escape, enter and ,
		if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 188]) !== -1 ||
				(e.keyCode === 65 && e.ctrlKey === true) ||
				(e.keyCode >= 35 && e.keyCode <= 39)) {
			return;
		}
		// Ensure that it is a number and stop the keypress
		if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
			e.preventDefault();
		}
	});
	
	$(function() {
		$('.maskMonthYear').datepicker(
				{
					dateFormat: "mm/yy",
					changeMonth: true,
					changeYear: true,
					constrainInput: true,
		            showOn: 'button',
		            buttonText: ' ',
					onClose: function(dateText, inst) {
						function isDonePressed(){
							return ($('#ui-datepicker-div').html().indexOf('ui-datepicker-close ui-state-default ui-priority-primary ui-corner-all ui-state-hover') > -1);
						}

						if (isDonePressed()){
							var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
							var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
							$(this).datepicker('setDate', new Date(year, month, 1)).trigger('change');

							$('.maskMonthYear').focusout();//Added to remove focus from datepicker input box on selecting date
						}
					},
					beforeShow : function(input, inst) {
						inst.dpDiv.addClass('month_year_datepicker');
						if ((datestr = $(this).val()).length > 0) {
							year = datestr.substring(datestr.length-4, datestr.length);
							month = datestr.substring(0, 2);
							$(this).datepicker('option', 'defaultDate', new Date(year, month-1, 1));
							$(this).datepicker('setDate', new Date(year, month-1, 1));
							$(this).datepicker($.datepicker.regional['pt_BR']);
							$(".ui-datepicker-calendar").hide();
						}
					}
				});
				if(!$('.maskMonthYear').hasClass( "ui-state-disabled" )) {
				$(".ui-datepicker-trigger").mouseover(function() {
					$(this).css('cursor', 'pointer');
					$(this).addClass('ui-state-hover');
				});
				$(".ui-datepicker-trigger").mouseout(function() {
					$(this).removeClass('ui-state-hover');
				});
				$(".ui-datepicker-trigger").mousedown(function() {
					$(this).css('cursor', 'pointer');
					$(this).addClass('ui-state-active');
				});
				$(".ui-datepicker-trigger").mouseup(function() {
					$(this).removeClass('ui-state-active');
				});
				$(".ui-datepicker-trigger").addClass('ui-datepicker-trigger ui-button ui-widget ui-state-default ui-corner-all ui-button-icon-only');
				$(".ui-datepicker-trigger").append("<span class='ui-button-icon-left ui-icon ui-icon-calendar'></span><span class='ui-button-text'>ui-button</span>" );
				}
				
	});
	
	$('.celular').mask("99 99999999?9").focusout(function (event) {  
        var target, element;  
        target = (event.currentTarget) ? event.currentTarget : event.srcElement;  
        element = $(target);  
        element.unmask();  
        element.mask("99 99999999?9");  
    });
};

function mascara(str) {
	var phone, element;
	element = $(str);
	phone = element.val().replace(/\D/g, '');
	if (phone.length === 11) {
		$(str).mask("999.999.999-99?99999");
	} else if (phone.length === 14) {
		$(str).mask("99.999.999/9999-99");
	}
}

//Funcao de formatacao CPF
function cpf(valor) {
	// Remove qualquer caracter digitado que não seja numero
	valor = valor.replace(/\D/g, "");

	// Adiciona um ponto entre o terceiro e o quarto digito
	valor = valor.replace(/(\d{3})(\d)/, "$1.$2");

	// Adiciona um ponto entre o terceiro e o quarto dígitos desta vez para o segundo bloco
	valor = valor.replace(/(\d{3})(\d)/, "$1.$2");

	// Adiciona um hifen entre o terceiro e o quarto dígitos
	valor = valor.replace(/(\d{3})(\d)/, "$1-$2");
	return valor;
}

//Funcao de formatacao CNPJ
function cnpj(valor) {
	// Remove qualquer caracter digitado que não seja numero
	valor = valor.replace(/\D/g, "");

	// Adiciona um ponto entre o segundo e o terceiro dígitos
	valor = valor.replace(/^(\d{2})(\d)/, "$1.$2");

	// Adiciona um ponto entre o quinto e o sexto dígitos
	valor = valor.replace(/^(\d{2})\.(\d{3})(\d)/, "$1.$2.$3");

	// Adiciona uma barra entre o oitavaloro e o nono dígitos
	valor = valor.replace(/\.(\d{3})(\d)/, ".$1/$2");

	// Adiciona um hífen depois do bloco de quatro dígitos
	valor = valor.replace(/(\d{4})(\d)/, "$1-$2");
	return valor;
}

var RelogioJs = function() {
	var now = new Date();
	var hour = now.getHours();
	if (hour < 10) {
		hour = "0" + hour;
	}
	var minutes = now.getMinutes();
	if (minutes < 10) {
		minutes = "0" + minutes;
	}
	var seconds = now.getSeconds();
	if (seconds < 10) {
		seconds = "0" + seconds;
	}
	var watch = document.getElementById('watch-area');
	watch.innerHTML = hour + ":" + minutes + ":" + seconds;
	setTimeout('RelogioJs()', 1000);
};

var segundosRestantes;
var contando = true;

var FinishSession = function() {
	$('#formTimeout\\:btn-timeout-end').click();
};

var CountTime = function() {
	segundosRestantes = segundosRestantes - 1;
	if (segundosRestantes <= 0) {
		FinishSession();
	} else {
		var minutos = Math.floor(segundosRestantes / 60);
		var segundos = segundosRestantes - minutos * 60;
		var html = "0" + minutos + ":";
		if (segundos < 10) {
			html += "0";
		}
		html += segundos;
		$('#watch-area').html(html);
		if (contando) {
			setTimeout(CountTime, 1000);
		}
	}
};

var StartKeepSessionAlive = function() {
	PF('dialogoTimeout').show();
	$('#formTimeout\\:mensagem').html('Você será deslogado em <span id="watch-area"></span> minutos.');
	segundosRestantes = 300;
	contando = true;
	CountTime();
};

var KeepSessionAlive = function() {
	contando = false;
	$('#formTimeout\\:btn-timeout-alive').click();
};

var RedirectTimeout = function() {
	window.location.replace(window.location.protocol + "//" + window.location.host + "/" + window.location.pathname.split("/")[1]);
};

$(document).ready(function(){
	var $bodyElem = $('body');
    var $navbarElem = $('.navbar'); //Referencia do menu
    var $contentElem = $('.content'); //Referencia do container do conteúdo
    var $navbarHeight = $navbarElem.height();
    
    var setMarginFromContent = function() {
    	$contentElem.css({'top' : $navbarElem.height() + 2});
    };
        
    //Corrige o content quando a tela carregar pela primeira vez
    setMarginFromContent();

 	//Evento de redimensionamento da página
    $(window).resize(setMarginFromContent);
  });
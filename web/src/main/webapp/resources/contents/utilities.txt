[
	{
		"title" : "Clubes",
		"subtitle" : "Tenha acesso direto aos melhores clubes de tiro da sua cidade e região.",
		"path" : "/pages/club/list.jsf",
		"imagePath" : "/resources/img/Search.svg"
	},
	{
		"title" : "Professores certificados",
		"subtitle" : "Encontre os professores certificados e autorizados para o ensino da prática de tiro.",
		"path" : "/pages/instructor/list.jsf",
		"imagePath" : "/resources/img/precision-wheel.svg"
	}
]